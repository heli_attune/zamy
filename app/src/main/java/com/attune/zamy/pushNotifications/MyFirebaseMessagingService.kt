package com.attune.zamy.pushNotifications

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.util.Log
import androidx.core.app.NotificationCompat
import androidx.core.content.ContextCompat
import com.attune.zamy.R
import com.attune.zamy.activity.CustomerSupportActivity
import com.attune.zamy.activity.MainActivity
import com.attune.zamy.activity.OrderDetailActivity
import com.attune.zamy.utils.keyOrderId
import com.google.firebase.iid.FirebaseInstanceId
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import com.hopmeal.android.utils.*


class MyFirebaseMessagingService : FirebaseMessagingService() {
    private val TAG = MyFirebaseMessagingService::class.java.simpleName
    private var mChannel: NotificationChannel? = null
    private var notifManager: NotificationManager? = null

    override fun onMessageReceived(remoteMessage: RemoteMessage) {

//        Log.d(TAG, "From: " + remoteMessage.from)
        if (remoteMessage.data.isNotEmpty()) {
            Log.d(TAG, "Message data payload: " + remoteMessage.data)
//            sendNotification(remoteMessage.data["message"]!!)
            displayCustomNotificationForOrders(remoteMessage)
        } else {

        }

        // Check if message contains a notification payload.
        if (remoteMessage.notification != null) {
            Log.d(TAG, "Message Notification Body: " + remoteMessage.notification?.body)
//            sendNotification(remoteMessage.notification?.body!!)
//            displayCustomNotificationForOrders(remoteMessage.data["title"]!!,remoteMessage.data["message"]!!)

        }


        // Also if you intend on generating your own notifications as a result of a received FCM
        // message, here is where that should be initiated. See sendNotification method below.
    }
    // [END receive_message]

    /**
     * Create and show a simple notification containing the received FCM message.
     *
     * @param messageBody FCM message body received.
     */
    private fun sendNotification(messageBody: String) {
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(
            this, 0 /* Request code */, intent,
            PendingIntent.FLAG_ONE_SHOT
        )


        val icon = getNotificationIcon()
        val largeIcon =
            BitmapFactory.decodeResource(applicationContext.resources, R.mipmap.ic_launcher)
        val notificationBuilder = NotificationCompat.Builder(this, "default")
            .setColor(ContextCompat.getColor(applicationContext, R.color.colorPrimary))
            .setSmallIcon(icon)
            .setLargeIcon(largeIcon)
            .setContentTitle(getString(R.string.app_name))
            .setStyle(NotificationCompat.BigTextStyle().bigText(messageBody))
            .setContentText(messageBody)
            .setAutoCancel(true)
//                .setSound(defaultSoundUri)
//                .setVibrate(vibrate)
            .setPriority(NotificationCompat.PRIORITY_MAX)
            .setContentIntent(pendingIntent)

        val notificationManager =
            getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager

        notificationManager.notify(System.currentTimeMillis().toInt(), notificationBuilder.build())
    }


    override fun onNewToken(p0: String) {
        super.onNewToken(p0)
        val refreshedToken = FirebaseInstanceId.getInstance().token

        Log.d(TAG, "Refreshed token: " + refreshedToken!!)
    }


    private fun displayCustomNotificationForOrders(remoteMessage: RemoteMessage) {
        val title = remoteMessage.data["title"]!!
        val description = remoteMessage.data["message"]!!

        val notificationType = remoteMessage.data["notification_type"]!!.toInt()
        val intent: Intent

        Log.e("TAG", "type$notificationType")
        when (notificationType) {
            orderReceived, orderCancelled -> {
                intent = Intent(this, OrderDetailActivity::class.java)
                intent.putExtra(keyOrderId, remoteMessage.data[keyOrderId]!!.toInt())
            }
            orderOutForDelivered -> {
                intent = Intent(this, OrderDetailActivity::class.java)
                intent.putExtra(keyOrderId, remoteMessage.data[keyOrderId]!!)
                /*   intent.putExtra(keySubOrderId, remoteMessage.data[keySubOrderId]!!)*/
            }
            orderDelivered -> {
                intent = Intent(this, OrderDetailActivity::class.java)
                intent.putExtra(keyOrderId, remoteMessage.data[keyOrderId]!!)
                //intent.putExtra(keySubOrderId, remoteMessage.data[keySubOrderId]!!)
            }
            ticketReplyFor -> {
                intent = Intent(this, CustomerSupportActivity::class.java)
                intent.putExtra("ticket_id", remoteMessage.data["ticket_id"]!!)
            }
            ticketStatusChanged -> {
                intent = Intent(this, CustomerSupportActivity::class.java)
                intent.putExtra("ticket_id", remoteMessage.data["ticket_id"]!!)
            }
//            keySingleProductOnTheWay -> {
//                intent = Intent(this, OrderDetailActivity::class.java)
//                intent.putExtra(keyOrderId, remoteMessage.data[keyOrderId]!!.toInt())
//            }

//            keySingleProductDelivered -> {
//                intent = Intent(this, OrderDetailActivity::class.java)
//                intent.putExtra(keyOrderId, remoteMessage.data[keyOrderId]!!.toInt())
//            }

            else -> {
                intent = Intent(this, OrderDetailActivity::class.java)
                intent.putExtra(keyOrderId, remoteMessage.data[keyOrderId]!!)
            }
        }

        if (notifManager == null) {
            notifManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        }

        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent: PendingIntent =
            PendingIntent.getActivity(this, 0, intent, PendingIntent.FLAG_ONE_SHOT)

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val builder: NotificationCompat.Builder = NotificationCompat.Builder(this, "0")

            val importance = NotificationManager.IMPORTANCE_HIGH
            if (mChannel == null) {
                mChannel = NotificationChannel("0", description, importance)
                mChannel?.description = description
                mChannel?.enableVibration(true)
                notifManager?.createNotificationChannel(mChannel!!)
            }
            intent.flags = Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_SINGLE_TOP

            builder.setContentTitle(title)
                .setSmallIcon(getNotificationIcon()) // required
                .setContentText(description)  // required
                .setDefaults(Notification.DEFAULT_ALL)
                .setAutoCancel(true)
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher_round))
                .setStyle(NotificationCompat.BigTextStyle().bigText(description))
                .setPriority(NotificationCompat.PRIORITY_MAX)
                //.setBadgeIconType(R.drawable.ic_app_icon)
                .setContentIntent(pendingIntent)
                .setSound(RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION))
            val notification = builder.build()
            notifManager?.notify(System.currentTimeMillis().toInt(), notification)
        } else {


            val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val notificationBuilder = NotificationCompat.Builder(this, "0")
                .setContentTitle(title)
                .setContentText(description)
                .setAutoCancel(true)
                .setColor(ContextCompat.getColor(baseContext, R.color.colorPrimary))
                .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
                .setSound(defaultSoundUri)
                .setSmallIcon(getNotificationIcon())
                .setContentIntent(pendingIntent)
                .setPriority(NotificationCompat.PRIORITY_MAX)
                .setStyle(
                    NotificationCompat.BigTextStyle().setBigContentTitle(title).bigText(
                        description
                    )
                )

            val notificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.notify(
                System.currentTimeMillis().toInt(),
                notificationBuilder.build()
            )
        }
    }

    private fun getNotificationIcon(): Int {
        val useWhiteIcon = Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP
        return if (useWhiteIcon) R.drawable.ic_app_icon else R.mipmap.ic_launcher_round
    }
}
