package com.attune.zamy.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.interaface.onItemAddressSelected
import com.attune.zamy.model.CommonModel
import com.attune.zamy.model.GetAddressModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import retrofit2.Call
import retrofit2.Response


class AddressAdapter(
    var mContext: Context,
    val addressList: MutableList<GetAddressModel.DataBean.AddressBookBean>?,
    var onItemAddressSelected: onItemAddressSelected
) :
    RecyclerView.Adapter<AddressAdapter.ViewHolder>() {
    lateinit var userId: String
    private var row_index: Int = -1
    var selected: RadioButton? = null


    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): AddressAdapter.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.row_adddress_item, viewGroup, false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        return addressList!!.size
    }

    fun removeAt(position: Int) {
        addressList!!.removeAt(position)
        notifyItemRemoved(position)
        //  notifyItemRangeChanged(position, cartList.size)
    }

    @SuppressLint("ResourceAsColor", "SetTextI18n")
    override fun onBindViewHolder(holder: AddressAdapter.ViewHolder, position: Int) {
        val addressData = addressList?.get(position)
        userId = SharedPref.getUserId(mContext)
        if (addressData!!.name != null && addressData.name != "") {
            holder.tvFullName.text = addressData.name
        }
        if (addressData.address_1 != null && addressData.address_1 != "") {
            holder.tvFullAddress.text =
                addressData.address_1 + "," + addressData.address_2 + "," + addressData.shipping_area + "," + addressData.city + "," +
                        addressData.postcode
        }
        if (addressData.address_2 != null && addressData.address_2 != "") {
            holder.tvMobile.text = "Mobile: "+addressData.phone
        }
        if (addressData.city != null && addressData.city != "") {
            holder.tvEmail.text = "Email: "+addressData.email
        }
        holder.rlAddress.setOnClickListener {
            row_index = position
            notifyDataSetChanged()
            onItemAddressSelected.onAddressSelected(addressData, true, false)
        }
        //on selected color change
        if (row_index == position) {
            holder.imgDone.visibility = View.VISIBLE
        } else {
            holder.imgDone.visibility = View.GONE
        }
        holder.imgDone.visibility = View.GONE
        if (addressData.address_type.equals("work")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.imgAddressType.setImageDrawable(mContext.getDrawable(R.drawable.ic_office_address))
            }
        } else if (addressData.address_type.equals("home")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.imgAddressType.setImageDrawable(mContext.getDrawable(R.drawable.ic_home_address))
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.imgAddressType.setImageDrawable(mContext.getDrawable(R.drawable.ic_other_icon_address))
            }
        }
        holder.imgEdit.setOnClickListener {
            onItemAddressSelected.onAddressSelected(addressData, false, true)

        }

        holder.imgDelete.setOnClickListener {
            apiDeleteAddress(holder.adapterPosition, addressData)
        }


    }

    private fun apiDeleteAddress(
        position: Int,
        addressData: GetAddressModel.DataBean.AddressBookBean
    ) {

        CustomProgressbar.showProgressBar(mContext, false)

        val map = HashMap<String, String>()
        map["address_id"] = addressData.id.toString()

        CustomProgressbar.showProgressBar(mContext, false)
        RetrofitClientSingleton
            .getInstance()
            .deleteAddres(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                removeAt(position)
                                Toast(mContext, "" + homeScreenData!!.message)

                            }
                            FAILURE -> {
                                Toast(mContext, "" + homeScreenData!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("address", response.body().toString())
                }
            })

    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvFullName: TextView = itemView.findViewById(R.id.tvFullName)
        var tvFullAddress: TextView = itemView.findViewById(R.id.tvFullAddress)
        var tvMobile: TextView = itemView.findViewById(R.id.tvMobile)
        var tvEmail: TextView = itemView.findViewById(R.id.tvEmail)
        var imgAddressType: ImageView = itemView.findViewById(R.id.imgAddressType)
        var rlAddress: RelativeLayout = itemView.findViewById(R.id.rlAddress)
        var imgEdit: ImageView = itemView.findViewById(R.id.imgEdit)
        var imgDelete: ImageView = itemView.findViewById(R.id.imgDelete)
        var imgDone: ImageView = itemView.findViewById(R.id.imgDone)


    }

}
