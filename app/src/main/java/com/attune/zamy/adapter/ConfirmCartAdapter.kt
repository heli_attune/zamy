package com.attune.zamy.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.model.*
import com.attune.zamy.utils.*


class ConfirmCartAdapter(
    var mContext: Context,
    val cartList: MutableList<CartListModel.DataBean.ItemsBean>?
) :
    RecyclerView.Adapter<ConfirmCartAdapter.ViewHolder>() {
    lateinit var userId: String
    private var row_index: Int = -1
    var selected: RadioButton? = null


    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): ConfirmCartAdapter.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_row_confirm_cart, viewGroup, false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        return cartList!!.size
    }

    fun removeAt(position: Int) {
        cartList!!.removeAt(position)
        notifyItemRemoved(position)
        //  notifyItemRangeChanged(position, cartList.size)
    }
    @SuppressLint("ResourceAsColor", "SetTextI18n")
    override fun onBindViewHolder(holder: ConfirmCartAdapter.ViewHolder, position: Int) {
        val addressData = cartList?.get(position)
        userId = SharedPref.getUserId(mContext)
        val dataList = this!!.cartList!![position]
        if(dataList!=null ){
            holder.tvItemName.text = dataList.menu_name
        }
        if(dataList.variation_name!=null ){
            holder.tvItemDetail.text = dataList.variation_name
        }
        if (dataList.price != null) {
            dataList.price = dataList.price
            holder.tvPrice.text =
                mContext.getString(R.string.rupees) + " " + ((dataList.price!!.toDouble() * dataList.qty).toString())
            //tableData.pricetotal = tableData.price!!.toInt() * tableData.qty
            dataList.pricetotal = dataList.price!!.toDouble() * dataList.qty
            // holder.txtPrice.text = mcontext.getString(R.string.rupees)+tableData.pricetotal
        } else {
            dataList.price = 0.toString()
            holder.tvPrice.text = mContext.getString(R.string.rupees) + " " + dataList.price
        }
        if(dataList.qty!=null){
            holder.txtQty.text = dataList.qty.toString()
        }
        /*if(dataList!!.menu_logo!=null){
            Glide.with(mContext)
                .load(data.menu_logo)
                .error(R.drawable.ic_place_holder)
                .into(img)

        }else{
            Glide.with(mContext)
                .load(R.drawable.ic_place_holder)
                .error(R.drawable.ic_place_holder)
                .into(img)
        }*/

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvItemName: TextView = itemView.findViewById(R.id.tvItemName)
        var tvItemDetail: TextView = itemView.findViewById(R.id.tvItemDetail)
        var tvPrice: TextView = itemView.findViewById(R.id.tvPrice)
        var txtQty: TextView = itemView.findViewById(R.id.txtQty)
        var imgItem:ImageView = itemView.findViewById(R.id.imgItem)


    }

}
