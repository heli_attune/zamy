package com.attune.zamy.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.CommonModel
import com.attune.zamy.model.TopRestaurantModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Response

class TopRestaurantPaginationAdapter(
    private val context: Context,
    private val dataListModels: MutableList<TopRestaurantModel.DataBean.TopRestaurantListBean>,
    internal var itemSelectedListener: OnItemSelectedListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var isLoaded = false


    override fun getItemViewType(position: Int): Int {
        return if (position == dataListModels.size - 1 && isLoaded) LOADING else ITEM
    }


    // inflates the cell layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
         when (viewType) {
            ITEM -> {
                val view = mInflater.inflate(R.layout.top_restaurant_item_row, parent, false)
                return ViewHolder(view)
            }
            LOADING -> return LoadingHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_loading,
                    parent,
                    false
                )
            )
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    // binds the data to the textview, imageview in each cell
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        when (viewHolder.itemViewType) {
            ITEM -> {
                val holder = viewHolder as ViewHolder
                val mData = dataListModels[position]
                holder.tvRestName.text = mData.res_name

                if (mData.total_fav_count != null && mData.total_fav_count != "0") {
                    holder.tvCountFav.text = mData.total_fav_count
                }

                if (mData.res_name != null && mData.res_name != "") {
                    holder.tvRestName.text = mData.res_name
                }
                if (mData.area != null && mData.area != "" && mData.landmark != null && mData.landmark != "") {
                    holder.tvAddress.text = mData.area + ", " + mData.landmark
                }
                if (mData.approx_delivery_time != null && mData.approx_delivery_time != "" && mData.approx_delivery_time.equals(
                        "NA"
                    )
                ) {
                    holder.tvTime.visibility = View.VISIBLE
                    holder.tvTime.text = mData.approx_delivery_time
                } else {
                    holder.tvTime.visibility = View.GONE
                }
                if (mData.service_type != null) {
                    holder.tvTypesOfFood.text = mData.service_type
                }
                if (mData.logo != null && mData.logo != "") {
                    Glide.with(context)
                        .load(mData.logo)
                        .error(R.drawable.ic_place_holder)
                        .into(holder.imgTopRestaurants)
                }
             /*   if (mData.favourite_flag.equals("yes")) {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.imgFavorite.setImageResource(R.drawable.ic_favorite_selected_red)
                    }
                } else {
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.imgFavorite.setImageResource(R.drawable.ic_favorite_unselected_red)
                    }
                }*/
                /*holder.imgFavorite.setOnClickListener {
                    if (mData.favourite_flag.equals("yes")) {

                        addRemoveFavouriteAPI(mData.id!!, position, holder.imgFavorite, true)
                    } else {

                        addRemoveFavouriteAPI(mData.id!!, position, holder.imgFavorite, false)
                    }
                    *//*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                        holder.imgFavorite.setImageResource(R.drawable.ic_favorite_selected_red)
                    }*//*
                }*/

                holder.tvOnlineOrder.setOnClickListener {

                }
                holder.rlTopRest.setOnClickListener {
                    itemSelectedListener.onItemSelected(mData, position, holder.adapterPosition)
                }
            }
            LOADING -> {
            }
        }
    }


    override fun getItemCount(): Int {

        return dataListModels.size
    }

    internal inner class LoadingHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    inner class ViewHolder
        (itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvRestName: TextView = itemView.findViewById(R.id.tvRestName)
        internal var tvAddress: TextView = itemView.findViewById(R.id.tvAddress)
        internal var tvTypesOfFood: TextView = itemView.findViewById(R.id.tvTypesOfFood)
        internal var tvOnlineOrder: TextView = itemView.findViewById(R.id.tvOnlineOrder)
        internal var rlTopRest: RelativeLayout = itemView.findViewById(R.id.rlTopRest)
        internal var tvTime: TextView = itemView.findViewById(R.id.tvTime)
        internal var imgTopRestaurants: ImageView = itemView.findViewById(R.id.imgTopRestaurants)
        internal var imgFavorite: ImageView = itemView.findViewById(R.id.imgFavorite)
        internal var tvCountFav: TextView = itemView.findViewById(R.id.tvFavCount)

    }

    interface OnItemSelectedListener {
        fun onItemSelected(
            item: TopRestaurantModel.DataBean.TopRestaurantListBean,
            position: Int,
            adapterPostion: Int
        )
    }

    fun addItem(applicationHistory: TopRestaurantModel.DataBean.TopRestaurantListBean) {
        dataListModels.add(applicationHistory)
        notifyItemInserted(dataListModels.size - 1)
    }

    fun addAllItem(applicationHistories: List<TopRestaurantModel.DataBean.TopRestaurantListBean>) {
        for (applicationHistory in applicationHistories) {
            addItem(applicationHistory)
        }
    }

    fun removeAllItem() {
        dataListModels.clear()
        notifyDataSetChanged()
    }

    fun addLoading() {
        isLoaded = true
        addItem(TopRestaurantModel.DataBean.TopRestaurantListBean())
    }

    fun removeLoading() {
        isLoaded = false
        val position = dataListModels.size - 1
        val dataList = getItem(position)
        if (dataList != null) {
            dataListModels.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun getItem(position: Int): TopRestaurantModel.DataBean.TopRestaurantListBean? {
        return dataListModels[position]
    }

    companion object {
        private val ITEM = 0
        private val LOADING = 1
    }

    private fun addRemoveFavouriteAPI(
        id: String,
        position: Int,
        imgFavorite: ImageView,
        isFav: Boolean
    ) {

        val map = HashMap<String, String>()
        map["restaurant_id"] = id
        map["user_id"] = SharedPref.getUserId(context)

        CustomProgressbar.showProgressBar(context, false)
        RetrofitClientSingleton
            .getInstance()
            .addRemoveFavouriteRestaurant(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                if (isFav) {
                                    imgFavorite.setImageResource(R.drawable.ic_favorite_unselected_red)
                                } else {
                                    imgFavorite.setImageResource(R.drawable.ic_favorite_selected_red)
                                }

                            }
                            FAILURE -> {
                                Toast(context, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }
}
