package com.attune.zamy.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.interaface.onItemVarianceSelected
import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.RestaurantsMenuModel


class ProductVarianceAdapter(
    var mContext: Context,
    val tableList: List<RestaurantsMenuModel.DataBean.CategoryBean.MenuBean.ProductVariationBean>?,
    var onItemVarianceSelected: onItemVarianceSelected,
    val catId: String?,
    val foodmenuId: String?,
    val menuName: String?,
    val taxPrice: Double
) :
    RecyclerView.Adapter<ProductVarianceAdapter.ViewHolder>() {
    private var row_index: Int = -1
    var selected: RadioButton? = null


    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): ProductVarianceAdapter.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_row_variance, viewGroup, false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        Log.e("variancesize", tableList!!.size.toString())
        return tableList.size
    }

    @SuppressLint("ResourceAsColor")
    override fun onBindViewHolder(holder: ProductVarianceAdapter.ViewHolder, position: Int) {
        val tableData = tableList?.get(position)
        holder.txtName.text = tableData!!.variation_name
        if (tableData.variation_sale_price != null && tableData.variation_sale_price != "" && tableData.variation_sale_price != "0") {
            holder.txtPrice.text =
                mContext.resources.getString(R.string.rupees) + tableData.variation_sale_price
        } else if (tableData.variation_price != null && tableData.variation_price != "") {
            holder.txtPrice.text =
                mContext.resources.getString(R.string.rupees) + tableData.variation_price
        }

        holder.lnVariance.setOnClickListener {

            if (selected != null) {
                selected!!.isChecked = false
            }
            holder.radioVariance.isChecked = true
            selected = holder.radioVariance
            var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> =
                ArrayList<CartRestaurantsMenuModel>()
            val favoriteList = CartRestaurantsMenuModel()
            favoriteList.cat_id = catId
            favoriteList.menu_name = menuName
            favoriteList.variation_name = tableData.variation_name
            favoriteList.foodmenu_id = foodmenuId
            favoriteList.variation_id = tableData.variation_id
            favoriteList.variation_name = tableData.variation_name

            if (tableData.variation_sale_price != null && tableData.variation_sale_price != "" && tableData.variation_sale_price != "0") {
                favoriteList.price = tableData.variation_sale_price!!
                favoriteList.priceCart = tableData.variation_sale_price!!.toDouble()
            } else if (tableData.variation_price != null && tableData.variation_price != "") {
                favoriteList.price = tableData.variation_price!!
                favoriteList.priceCart = tableData.variation_price!!.toDouble()
            }
            CartItemArrayModel.add(favoriteList)
            onItemVarianceSelected.onVarianceItemSelected(favoriteList)
            //favoriteList.foodmenu_click_position=mposition
            //row_index=position
            //notifyDataSetChanged()
        }

        holder.radioVariance.setOnClickListener {

            if (selected != null) {
                selected!!.isChecked = false
            }
            holder.radioVariance.isChecked = true
            selected = holder.radioVariance
            var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> =
                ArrayList<CartRestaurantsMenuModel>()
            val favoriteList = CartRestaurantsMenuModel()
            favoriteList.cat_id = catId
            favoriteList.menu_name = menuName
            favoriteList.variation_name = tableData.variation_name
            favoriteList.foodmenu_id = foodmenuId
            favoriteList.variation_id = tableData.variation_id
            favoriteList.variation_name = tableData.variation_name

            if (tableData.variation_sale_price != null && tableData.variation_sale_price != "") {
                favoriteList.price = tableData.variation_sale_price!!
                favoriteList.priceCart = tableData.variation_sale_price!!.toDouble()
            } else if (tableData.variation_price != null && tableData.variation_price != "") {
                favoriteList.price = tableData.variation_price!!
                favoriteList.priceCart = tableData.variation_price!!.toDouble()
            }
            CartItemArrayModel.add(favoriteList)
            onItemVarianceSelected.onVarianceItemSelected(favoriteList)
            //favoriteList.foodmenu_click_position=mposition
            //row_index=position
            //notifyDataSetChanged()
        }
        /* holder.txtTableno.text = this!!.tableList!![position].table_no
         holder.parentView.setOnClickListener {
             tableData?.let { it1 -> itemClickListener.onItemSelected(it1) }
         }


         if (!tableData!!.time_taken!!.isEmpty()) {
             holder.txtTime.text=tableData.time_taken + " min"
         }
         if (!tableData!!.sub_total!!.isEmpty()) {
             holder.txtAmount.text=mcontext.getString(R.string.rupees)+ tableData.sub_total
         }

         holder.imgPrint.visibility=View.GONE
         holder.imgSave.visibility=View.GONE*/


    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var radioVariance: RadioButton = itemView.findViewById(R.id.radioVariance)
        var txtName: TextView = itemView.findViewById(R.id.txtName)
        var txtPrice: TextView = itemView.findViewById(R.id.txtPrice)
        var lnVariance: LinearLayout = itemView.findViewById(R.id.lnVariance)
        /*var txtTableno: TextView = itemView.findViewById<TextView>(R.id.txtTableno)
        var parentView: CardView= itemView.findViewById(R.id.parentView)
        var rlParent:RelativeLayout= itemView.findViewById(R.id.rlParent)
        var imgtable:ImageView = itemView.findViewById(R.id.imgtable)
        var imgPrint:ImageView= itemView.findViewById(R.id.imgPrint)

        var imgSave:ImageView= itemView.findViewById(R.id.imgSave)
        var  txtTime:TextView =itemView.findViewById(R.id.txtTime)
        var  txtAmount:TextView =itemView.findViewById(R.id.txtAmount)*/


    }

}
