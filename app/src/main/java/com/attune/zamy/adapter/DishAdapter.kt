package com.attune.zamy.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.model.SearchDataModel
import com.attune.zamy.utils.SharedPref
import com.bumptech.glide.Glide


class DishAdapter(
    internal var mContext: Context,
    internal var mArrayListString: List<SearchDataModel.DataBean.SearchDataBean>,
    var onDishItemSelectedListener: OnDishItemSelectedListener


) : RecyclerView.Adapter<DishAdapter.Holder>() {

    lateinit var userId: String

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.item_row_dishview, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val categoryBean = mArrayListString[position]
        holder.linearLayout.removeAllViews()

        holder.mTextView.text = categoryBean.restaurant!!.res_name
        userId = SharedPref.getUserId(mContext)

        for (i in 0 until categoryBean.restaurant!!.menu!!.size) {

            val view = LayoutInflater.from(mContext)
                .inflate(R.layout.item_row_dishview, null)
            //   TextView textView = new TextView(mContext);
            val tvSubTitle = view.findViewById<TextView>(R.id.tvSubTitle)
            val tvItemName = view.findViewById<TextView>(R.id.tvItemName)
            val tvPrice = view.findViewById<TextView>(R.id.tvPrice)
            holder.linearLayout.addView(view)
            val rlheader = view.findViewById<RelativeLayout>(R.id.rlheader)
            rlheader.visibility = View.GONE
            val tvCustom = view.findViewById<TextView>(R.id.tvCustom)

            val tvAddToCart = view.findViewById<TextView>(R.id.tvAddToCart)
            tvAddToCart.text = "Order Now"
            val tvRemove = view.findViewById<TextView>(R.id.tvRemove)
            tvAddToCart.visibility = View.VISIBLE
            var lnPrice = view.findViewById<LinearLayout>(R.id.lnPrice)
            var imgMenu = view.findViewById<ImageView>(R.id.imgMenu)
            if (categoryBean.restaurant!!.menu!![i].product_variation!!.size > 0) {
                tvPrice.text = categoryBean.restaurant!!.menu!![i].variation_price
                tvCustom.visibility = View.VISIBLE
            } else {
                tvCustom.visibility = View.GONE
            }
            if (categoryBean.restaurant!!.menu!![i].menu_logo != null && categoryBean.restaurant!!.menu!![i].menu_logo != "") {
                Glide.with(mContext)
                    .load(categoryBean.restaurant!!.menu!![i].menu_logo)
                    .into(imgMenu)
            }
            Log.e("postion", categoryBean.restaurant!!.menu!![i].menu_name + position)
            if (categoryBean.restaurant!!.menu!!.size == 1) {
                rlheader.visibility = View.VISIBLE
            }

            if (categoryBean.restaurant!!.menu!![i].long_description != null && categoryBean.restaurant!!.menu!![i].long_description != "") {
                tvSubTitle.text = categoryBean.restaurant!!.menu!![i].long_description
            }
            if (i == 0) {


            } else if (i == categoryBean.restaurant!!.menu!!.size - 1) {
                rlheader.visibility = View.VISIBLE

            }
            if (categoryBean.restaurant!!.menu!![i].menu_name != null && categoryBean.restaurant!!.menu!![i].menu_name !== "") {
                tvItemName.text = categoryBean.restaurant!!.menu!![i].menu_name
            }

            if (categoryBean.restaurant!!.menu!![i].tax_price != 0.0) {
                tvPrice.text =
                    mContext.resources.getString(R.string.rupees) + categoryBean.restaurant!!.menu!![i].tax_price
            } else if (categoryBean.restaurant!!.menu!![i].reduced_price != null) {
                tvPrice.text =
                    mContext.resources.getString(R.string.rupees) + categoryBean.restaurant!!.menu!![i].reduced_price
            }
            if (categoryBean.restaurant!!.menu!![i].product_variation!!.size > 0) {
                tvPrice.text =
                    mContext.resources.getString(R.string.rupees) + categoryBean.restaurant!!.menu!![i].variation_price
            } else {

            }

            var llMainChild = view.findViewById<LinearLayout>(R.id.llMainChild)
            llMainChild.setOnClickListener {
                onDishItemSelectedListener.onDishItemSelected(categoryBean, position)
            }


        }
    }

    override fun getItemCount(): Int {
        return mArrayListString.size
    }

    interface OnDishItemSelectedListener {
        fun onDishItemSelected(
            item: SearchDataModel.DataBean.SearchDataBean,
            position: Int
        )
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var mTextView: TextView
        internal var tvSubTitle: TextView
        internal var tvItemName: TextView
        internal var linearLayout: LinearLayout

        init {
            mTextView = itemView.findViewById(R.id.tvItemName)
            tvItemName = itemView.findViewById(R.id.tvItemName)
            tvSubTitle = itemView.findViewById(R.id.tvSubTitle)
            linearLayout = itemView.findViewById(R.id.llMainChild)


        }
    }


}