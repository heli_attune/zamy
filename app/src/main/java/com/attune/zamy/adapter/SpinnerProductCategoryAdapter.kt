package com.attune.zamy.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import android.widget.TextView
import com.attune.zamy.R
import com.attune.zamy.model.ShippingAreaModel
import java.util.*


class SpinnerProductCategoryAdapter(internal var context: Context, spinneritem: ArrayList<ShippingAreaModel.DataBean>) : BaseAdapter() {

    private var inflater: LayoutInflater? = null

    internal var data: List<ShippingAreaModel.DataBean> = ArrayList()

    internal var position: Int = 0


    init {
        data = spinneritem
    }


    override fun getCount(): Int {
        // TODO Auto-generated method stub
        return data.size
    }

    override fun getItem(position: Int): Any {
        return data[position]
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    fun getSlug(position: Int): String {
        return data[position].area!!
    }
//
//    inner class Holder {
//        internal var txt_sp: TextView? = null
//    }


    override fun getView(position: Int, view: View?, parent: ViewGroup): View {
        var convertView = view
        // TODO Auto-generated method stub

        this.position = position
//        val holder = Holder()

        if (convertView == null) {

            inflater = context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            convertView = inflater!!.inflate(R.layout.view_spinner_item, null)
        }


        val txt_sp: TextView? = convertView!!.findViewById<View>(R.id.tvSpinner) as TextView
        txt_sp!!.text = data[position].area
        return convertView
    }

}
