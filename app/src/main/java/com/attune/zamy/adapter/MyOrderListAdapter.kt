package com.attune.zamy.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.interaface.onItemOrderListClick
import com.attune.zamy.model.MyOrderListModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class MyOrderListAdapter(
    val mContext: Context,
    val apiResponse: MyOrderListModel.DataBean?,
    val onItemOrderListClick: onItemOrderListClick
) :
    RecyclerView.Adapter<MyOrderListAdapter.OrderListAdapter>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): OrderListAdapter {
        val view =
            LayoutInflater.from(parent.context).inflate(R.layout.row_my_order_items, parent, false)
        return OrderListAdapter(view)
    }

    override fun onBindViewHolder(holder: OrderListAdapter, position: Int) {
        apiResponse!!.list!![position].let { holder.bind(apiResponse.list) }
    }

    override fun getItemCount(): Int {
        return apiResponse!!.list!!.size
    }

    inner class OrderListAdapter(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgOrderImage = itemView.findViewById<ImageView>(R.id.imgOrderImage)
        val tvOrderTitle = itemView.findViewById<AppCompatTextView>(R.id.tvOrderTitle)
        val tvOrderId = itemView.findViewById<AppCompatTextView>(R.id.tvOrderId)
        val tvOrderDate = itemView.findViewById<AppCompatTextView>(R.id.tvOrderDate)
        val tvOrderStatus: AppCompatTextView = itemView.findViewById(R.id.tvOrderStatus)
        val tvOrderPrice = itemView.findViewById<AppCompatTextView>(R.id.tvOrderPrice)


        @SuppressLint("CheckResult", "SetTextI18n")
        fun bind(apiResponse: List<MyOrderListModel.DataBean.ListBean>?) {



            tvOrderTitle.text = apiResponse!![position].res_name
            tvOrderId.text = apiResponse[position].id
            tvOrderDate.text = apiResponse[position].created_date

            tvOrderStatus.text = apiResponse[position].order_status


            if (apiResponse[position].order_status.equals("Food Prepared", ignoreCase = true)) {
                tvOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green))
            }
            if (apiResponse[position].order_status.equals("Accepted", ignoreCase = true)) {
                tvOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green))
            }
            if (apiResponse[position].order_status.equals(
                    "Waiting for Acceptance",
                    ignoreCase = true
                )
            ) {
                tvOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green))
            }
            if (apiResponse[position].order_status.equals("failed", ignoreCase = true)) {
                tvOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green))
            }
            if (apiResponse[position].order_status.equals("refunded", ignoreCase = true)) {
                tvOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green))
            }
            if (apiResponse[position].order_status.equals("cancelled", ignoreCase = true)) {
                tvOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green))
            }
            if (apiResponse[position].order_status.equals("refund_cancelled", ignoreCase = true)) {
                tvOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green))
            }
            if (apiResponse[position].order_status.equals("refund_approved", ignoreCase = true)) {
                tvOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green))
            }
            if (apiResponse[position].order_status.equals("completed", ignoreCase = true)) {
                tvOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green))
            }
            if (apiResponse[position].order_status.equals("refund_requested", ignoreCase = true)) {
                tvOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green))
            }
            if (apiResponse[position].order_status.equals("cancel_request", ignoreCase = true)) {
                tvOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.green))
            }

            if (apiResponse[position].order_status.equals("processing", ignoreCase = true)) {
                tvOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.orange))
            }

            if (apiResponse[position].order_status.equals("pending", ignoreCase = true)) {
                tvOrderStatus.setTextColor(ContextCompat.getColor(mContext, R.color.yellow))
            }


            tvOrderPrice.text = "₹ " + apiResponse[position].order_total

            val requestOptions = RequestOptions()
            requestOptions.placeholder(R.drawable.ic_place_holder)
            requestOptions.error(R.drawable.ic_place_holder)
            Glide.with(mContext)
                .load(apiResponse[position].logo)
                .apply(requestOptions)
                .into(imgOrderImage)

            itemView.setOnClickListener {
                onItemOrderListClick.myOrderClick(apiResponse[position].id)
            }

        }
    }
}
