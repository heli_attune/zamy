package com.attune.zamy.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.model.CouponListModel
import kotlinx.android.synthetic.main.row_promo_code_view.view.*

class PromoCodeDemoAdapter(
    val mContext: Context,
    val mPromoArrayList: ArrayList<CouponListModel.DataBean>
) :
    RecyclerView.Adapter<PromoCodeDemoAdapter.DemoAdapter>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DemoAdapter {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.row_promo_code_view, parent, false)
        return DemoAdapter(view)
    }

    override fun onBindViewHolder(holder: DemoAdapter, position: Int) {
        holder.bind(mPromoArrayList[position])
    }

    override fun getItemCount() = mPromoArrayList.size

    inner class DemoAdapter(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(promoBean: CouponListModel.DataBean) {
            itemView.tvPromoTitle.text = promoBean.title
            itemView.tvPromoName.text = promoBean.coupon_code
            itemView.tvPromoInfo.text = "${promoBean.description} "
            if (promoBean.end_date != "")
                itemView.tvPromoValidInfo.text = "Valid upto ${promoBean.end_date}"
            else
                itemView.tvPromoValidInfo.visibility = View.INVISIBLE
            itemView.rdPromo.setOnCheckedChangeListener(null)
        }
    }
}
