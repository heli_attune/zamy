package com.attune.zamy.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.listener.PromoCodeListener
import com.attune.zamy.model.CouponListModel
import kotlinx.android.synthetic.main.row_promo_code_view.view.*

class PromoCodeAdapter(
    private val mContext: Context,
    private val promoCodeListener: PromoCodeListener,
    val pageRedirect: String
)
    : RecyclerView.Adapter<PromoCodeAdapter.PromoViewHolder>() {

    private val mArrayList: ArrayList<CouponListModel.DataBean> = ArrayList()
    private var selectedItem = -1

    fun setList(arrayList: ArrayList<CouponListModel.DataBean>) {
        this.mArrayList.clear()
        this.mArrayList.addAll(arrayList)
        notifyDataSetChanged()
    }

    fun deSelectRadio() {
        selectedItem = -1
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PromoViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_promo_code_view, parent, false)
        return PromoViewHolder(view)
    }

    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: PromoViewHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    inner class PromoViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(promoBean: CouponListModel.DataBean) {
            if(promoBean.dis_type.equals("fix")){
                itemView.tvPromoTitle.text = "ENJOY "+ mContext.resources.getString(R.string.rupees) +promoBean.discount +" OFF"
            }else{
                itemView.tvPromoTitle.text = "ENJOY "+ promoBean.discount+ mContext.resources.getString(R.string.percentage) +" OFF"
            }
            if(promoBean.min_spend!=null && promoBean.min_spend!="" && promoBean.min_spend!="0") {
                itemView.tvPromoInfo.text =
                    "Valid only if your cart amount is equal or more than " + promoBean.min_spend
            }else{

            }
            itemView.tvPromoName.text = promoBean.coupon_code
            itemView.tvPromoInfo.text = "${promoBean.description} "
            if (promoBean.end_date != "")
                itemView.tvPromoValidInfo.text = "Valid upto ${promoBean.end_date}"
            else
                itemView.tvPromoValidInfo.visibility = View.INVISIBLE
            itemView.rdPromo.setOnCheckedChangeListener(null)
            /*val requestOptions = RequestOptions()
            requestOptions.placeholder(R.drawable.ic_place_holder)
            requestOptions.centerCrop()
            requestOptions.dontAnimate()
            requestOptions.error(R.drawable.ic_place_holder)
            Glide.with(view.context)
                    .load(promoBean.coupon_image)
                    .apply(requestOptions)
                    .into(itemView.ivPromoImage)*/

            itemView.rdPromo.isChecked = selectedItem == adapterPosition
           /* val content = SpannableString(itemView.tvPromoTermsCond.text)
            content.setSpan(UnderlineSpan(), 0, itemView.tvPromoTermsCond.text.length, 0)
            itemView.tvPromoTermsCond.text = content*/
            /*itemView.tvPromoTermsCond.setOnClickListener {
                if (promoBean.terms_and_conditions == "") {

                } else
                    promoCodeListener.onTermsClick(adapterPosition, promoBean)
            }*/
            if(pageRedirect.equals("0")){
                itemView.rdPromo.visibility=View.VISIBLE
                itemView.rdPromo.setOnCheckedChangeListener { _, _ ->
                    selectedItem = adapterPosition
                    promoCodeListener.onPromoRadioOnClick(adapterPosition, promoBean)
                    notifyDataSetChanged()
                }
                itemView.rllPromoCode.setOnClickListener {
                    selectedItem = adapterPosition
                    promoCodeListener.onPromoRadioOnClick(adapterPosition, promoBean)
                    notifyDataSetChanged()
                }
            }else{
                itemView.rdPromo.visibility=View.GONE
            }


        }
    }
}