package com.attune.zamy.adapter

import android.annotation.SuppressLint
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.text.SpannableStringBuilder
import android.text.Spanned
import android.text.style.StrikethroughSpan
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.activity.LoginPage
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.interaface.onReadMore
import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.CommonModel
import com.attune.zamy.model.RestaurantsMenuModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.dialog_logout.*
import retrofit2.Call
import retrofit2.Response


class DemoRestaurantDetailMenuAdapter(
    internal var mContext: Context,
    internal var mArrayListString: RestaurantsMenuModel.DataBean?,
    internal var itemSelectedListener: AddtoCartItemClickListener,
    internal var itemVariationListener: VarianceDialogClickListener,
    val restaurantId: String,
    val readmore: onReadMore


) : RecyclerView.Adapter<DemoRestaurantDetailMenuAdapter.Holder>() {

    lateinit var userId: String

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.restaurant_detail_item_row_child, parent, false)
        return Holder(view)
    }


    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: Holder, position: Int) {
        val categoryBean = mArrayListString
        holder.linearLayout.removeAllViews()
        var pos: Int = 0
        holder.mTextView.text = categoryBean!!.category!![position].cat_display_name
        userId = SharedPref.getUserId(mContext)
        if (position == 0) {
            pos = 0
            Log.e("category", mArrayListString!!.category!![position].cat_name + position)
        }



        for (i in 0 until categoryBean.category!![position].menu!!.size) {


            val view = LayoutInflater.from(mContext)
                .inflate(R.layout.restaurant_detail_item_row_child, null)
            //   TextView textView = new TextView(mContext);
            val tvSubTitle = view.findViewById<TextView>(R.id.tvSubTitle)
            val tvReadMore = view.findViewById<TextView>(R.id.tvReadMore)
            val tvItemName = view.findViewById<TextView>(R.id.tvItemName)
            val tvDeliveryTime = view.findViewById<TextView>(R.id.tvDeliveryTime)
            val tvPrice = view.findViewById<TextView>(R.id.tvPrice)
            val tvReducePrice = view.findViewById<TextView>(R.id.tvReducePrice)
            holder.linearLayout.addView(view)
            val rlheader = view.findViewById<RelativeLayout>(R.id.rlheader)
            rlheader.visibility = View.GONE
            val tvCustom = view.findViewById<TextView>(R.id.tvCustom)
            val lnCart = view.findViewById<LinearLayout>(R.id.lnCart)
            lnCart.visibility = View.GONE
            val tvAddToCart = view.findViewById<TextView>(R.id.tvAddToCart)
            val tvRemove = view.findViewById<TextView>(R.id.tvRemove)
            tvAddToCart.visibility = View.VISIBLE
            //var lnPrice = view.findViewById<LinearLayout>(R.id.lnPrice)
            var imgMenu = view.findViewById<ImageView>(R.id.imgMenu)
            var rlSpace = view.findViewById<RelativeLayout>(R.id.rlSpace)
            rlSpace.visibility = View.GONE

            /*    for (i in 0..categoryBean.category!![position].menu!!.size-1) {
                    if (categoryBean.category!![position].menu!![i].long_description != ""
                        &&
                        categoryBean.category!![position].menu!![i].long_description != null) {
                        tvReadMore.visibility = View.VISIBLE
                    } else {
                        tvReadMore.visibility = View.GONE
                    }
                }*/




            if (categoryBean.category!![position].menu!![i].product_variation!!.size > 0) {
                tvPrice.text = categoryBean.category!![position].menu!![i].variation_price
                tvCustom.visibility = View.VISIBLE
            } else {
                tvCustom.visibility = View.GONE
            }
            if (categoryBean.category!![position].menu!!.size == 1) {
                rlheader.visibility = View.VISIBLE
            } else {
                rlheader.visibility = View.GONE
            }
            if (i == 0) {

            } else if (i == categoryBean.category!![position].menu!!.size - 1) {
                rlheader.visibility = View.VISIBLE
                rlSpace.visibility = View.GONE
            } else {

            }
            if (i == pos) {
                rlSpace.visibility = View.VISIBLE

            }



            Log.e("pos", categoryBean.category!![position].menu!![i].menu_name + i)

            /**helee**/
            if (categoryBean.category!![position].menu!![i].cart != null) {
                if (categoryBean.category!![position].menu!![i].cart.equals("no")) {
                    tvAddToCart.visibility = View.VISIBLE

                    for (i in 0..categoryBean.category!!.size) {
                        Log.e("TAG", categoryBean.category!!.size.toString())
                        if (categoryBean.restaurant_details.restaurant_online_status.equals("0")) {
                            tvAddToCart.visibility = View.GONE
                            tvCustom.visibility = View.GONE
                        }

                    }

                    tvRemove.visibility = View.GONE

                } else {
                    val CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> = ArrayList()
                    val favouriteModel = CartRestaurantsMenuModel()
                    favouriteModel.cat_id = categoryBean.category!![position].menu!![i].cat_id
                    favouriteModel.foodmenu_id =
                        categoryBean.category!![position].menu!![i].foodmenu_id
                    favouriteModel.menu_name = categoryBean.category!![position].menu!![i].menu_name
                    favouriteModel.qty = 1
                    favouriteModel.tax_price =
                        categoryBean.category!![position].menu!![i].tax_price.toString()
                    if (categoryBean.category!![position].menu!![i].reduced_price > 0) {
                        favouriteModel.priceCart =
                            categoryBean.category!![position].menu!![i].reduced_price.toDouble()
                    } else if (categoryBean.category!![position].menu!![i].price > 0) {
                        favouriteModel.priceCart =
                            categoryBean.category!![position].menu!![i].price.toDouble()
                    } else {
                        favouriteModel.priceCart = 0.0
                    }

                    favouriteModel.isSelected = false
                    favouriteModel.position = holder.adapterPosition
                    CartItemArrayModel.add(favouriteModel)
                    itemSelectedListener.onMenuItemSelected(favouriteModel, false, false)

                    if (categoryBean.category!![position].menu!![i].product_variation!!.size > 0) {
                        /*tvAddToCart.visibility = View.GONE

                    tvRemove.visibility = View.VISIBLE*/
                        tvCustom.text = "Added to Cart"
                    } else {
                        tvAddToCart.visibility = View.GONE
                        tvRemove.visibility = View.VISIBLE
                    }


                }
            }



            if (categoryBean.category!![position].menu!![i].menu_logo != null) {
                Glide.with(mContext)
                    .load(categoryBean.category!![position].menu!![i].menu_logo)
                    .error(R.drawable.ic_place_holder)
                    .into(imgMenu)

            } else {
                Glide.with(mContext)
                    .load(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(imgMenu)
            }



            if (categoryBean.category!![position].menu!![i].long_description != null && categoryBean.category!![position].menu!![i].long_description != "") {
                tvSubTitle.text = categoryBean.category!![position].menu!![i].long_description
                tvReadMore.visibility = View.VISIBLE
            }




            if (categoryBean.category!![position].menu!![i].menu_name != null && categoryBean.category!![position].menu!![i].menu_name !== "") {
                tvItemName.text = categoryBean.category!![position].menu!![i].menu_name
            }
            if (categoryBean.category!![position].menu!![i].minimum_preparation_time != null && categoryBean.category!![position].menu!![i].minimum_preparation_time !== "") {
                tvDeliveryTime.text =
                    categoryBean.category!![position].menu!![i].minimum_preparation_time
            }
            if (categoryBean.category!![position].menu!![i].reduced_price != null && categoryBean.category!![position].menu!![i].reduced_price != 0) {
                val ssBuilder =
                    SpannableStringBuilder(categoryBean.category!![position].menu!![i].reduced_price.toString())

                val text = categoryBean.category!![position].menu!![i].reduced_price.toString()
                val strikethroughSpan = StrikethroughSpan()
                ssBuilder.setSpan(
                    strikethroughSpan,
                    0,
                    text.length,
                    Spanned.SPAN_EXCLUSIVE_EXCLUSIVE
                )

                tvReducePrice.text = ssBuilder
            }

            tvPrice.text =
                mContext.resources.getString(R.string.rupees) + categoryBean.category!![position].menu!![i].price
            if (categoryBean.category!![position].menu!![i].product_variation!!.isNotEmpty()) {
                tvPrice.text =
                    mContext.resources.getString(R.string.rupees) + categoryBean.category!![position].menu!![i].variation_price
            }

            tvReadMore.setOnClickListener {
                readmore.readmore(
                    categoryBean.category!![position].menu!![i].long_description,
                    categoryBean.category!![position].menu!![i].menu_name
                )
            }
            tvAddToCart.setOnClickListener {

                if (userId != "") {
                    var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> =
                        ArrayList()
                    val favouriteModel = CartRestaurantsMenuModel()
                    favouriteModel.cat_id = categoryBean.category!![position].menu!![i].cat_id
                    favouriteModel.foodmenu_id =
                        categoryBean.category!![position].menu!![i].foodmenu_id
                    favouriteModel.menu_name = categoryBean.category!![position].menu!![i].menu_name
                    favouriteModel.qty = 1
                    favouriteModel.tax_price =
                        categoryBean.category!![position].menu!![i].tax_price.toString()
                    if (categoryBean.category!![position].menu!![i].reduced_price > 0) {
                        favouriteModel.priceCart =
                            categoryBean.category!![position].menu!![i].reduced_price.toDouble()
                    } else if (categoryBean.category!![position].menu!![i].price > 0) {
                        favouriteModel.priceCart =
                            categoryBean.category!![position].menu!![i].price.toDouble()
                    } else {
                        favouriteModel.priceCart = 0.0
                    }

                    favouriteModel.isSelected = false
                    favouriteModel.position = holder.adapterPosition
                    CartItemArrayModel.add(favouriteModel)
                    //itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)

                    if (categoryBean.category!![position].menu!![i].product_variation!!.isNotEmpty()) {
                        // itemSelectedListener.onMenuItemSelected(favouriteModel,true)
                        itemVariationListener.onVarianceDialogSelected(
                            categoryBean.category!![position].menu!![i].product_variation!!,
                            categoryBean.category!![position].cat_id,
                            categoryBean.category!![position].menu!![i].foodmenu_id,
                            categoryBean.category!![position].menu!![i].menu_name,
                            categoryBean.category!![position].menu!![i].tax_price,
                            tvCustom,
                            tvRemove
                        )
                    } else {
                        itemSelectedListener.onMenuItemSelected(favouriteModel, false, false)
                        addtocartAPI(
                            categoryBean.category!![position].menu!![i],
                            tvAddToCart,
                            tvRemove
                        )

                    }
                    /*tvAddToCart.visibility= View.GONE

                tvRemove.visibility=View.VISIBLE*/
                } else {
                    val ine = Intent(mContext, LoginPage::class.java)
                    ine.putExtra(SCREEN_REDIRECTION, "1")
                    mContext.startActivity(ine)


                }


            }

            tvRemove.setOnClickListener {
                var variationId = ""
                removeCartItem(
                    categoryBean.category!![position].menu!![i],
                    tvAddToCart,
                    tvRemove,
                    variationId
                )
            }


            /**helee**/
            tvCustom.setOnClickListener {
                if (userId != "") {
                    var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> =
                        ArrayList<CartRestaurantsMenuModel>()
                    val favouriteModel = CartRestaurantsMenuModel()
                    favouriteModel.cat_id = categoryBean.category!![position].menu!![i].cat_id
                    favouriteModel.foodmenu_id =
                        categoryBean.category!![position].menu!![i].foodmenu_id
                    favouriteModel.menu_name = categoryBean.category!![position].menu!![i].menu_name
                    favouriteModel.qty = 1
                    favouriteModel.tax_price =
                        categoryBean.category!![position].menu!![i].tax_price.toString()
                    if (categoryBean.category!![position].menu!![i].reduced_price > 0) {
                        favouriteModel.priceCart =
                            categoryBean.category!![position].menu!![i].reduced_price.toDouble()
                    } else if (categoryBean.category!![position].menu!![i].price > 0) {
                        favouriteModel.priceCart =
                            categoryBean.category!![position].menu!![i].price.toDouble()
                    } else {
                        favouriteModel.priceCart = 0.0
                    }

                    favouriteModel.isSelected = false
                    favouriteModel.position = holder.adapterPosition
                    CartItemArrayModel.add(favouriteModel)
                    //itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)

                    if (categoryBean.category!![position].menu!![i].product_variation!!.isNotEmpty()) {
                        // itemSelectedListener.onMenuItemSelected(favouriteModel,true)
                        itemVariationListener.onVarianceDialogSelected(
                            categoryBean.category!![position].menu!![i].product_variation!!,
                            categoryBean.category!![position].cat_id,
                            categoryBean.category!![position].menu!![i].foodmenu_id,
                            categoryBean.category!![position].menu!![i].menu_name,
                            categoryBean.category!![position].menu!![i].tax_price,
                            tvCustom,
                            tvRemove
                        )
                        //   tvCustom.setText("Added to Cart")
                    } else {
                        //  itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)
                        addtocartAPI(
                            categoryBean.category!![position].menu!![i],
                            tvAddToCart,
                            tvRemove
                        )

                    }

                } else {
                    val ine = Intent(mContext, LoginPage::class.java)
                    ine.putExtra(SCREEN_REDIRECTION, "1")
                    mContext.startActivity(ine)


                }
            }
        }


    }

    interface AddtoCartItemClickListener {

        fun onMenuItemSelected(
            listBean: CartRestaurantsMenuModel,
            isProductVariance: Boolean,
            isRemove: Boolean
        )

    }

    interface VarianceDialogClickListener {

        fun onVarianceDialogSelected(
            dataBean: List<RestaurantsMenuModel.DataBean.CategoryBean.MenuBean.ProductVariationBean>,
            catId: String?,
            foodmenuId: String?,
            menuName: String?,
            taxPrice: Double,
            tvCustom: TextView,
            tvRemove: TextView
        )

    }

    interface CartViewVisible {
        fun onCartItemSelected(isCartView: Boolean)

    }

    override fun getItemCount(): Int {
        return mArrayListString!!.category!!.size
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var mTextView: TextView
        internal var tvSubTitle: TextView
        internal var tvItemName: TextView
        internal var tvDeliveryTime: TextView
        internal var linearLayout: LinearLayout

        init {
            mTextView = itemView.findViewById(R.id.tvItemName)
            tvItemName = itemView.findViewById(R.id.tvItemName)
            tvSubTitle = itemView.findViewById(R.id.tvSubTitle)
            tvDeliveryTime = itemView.findViewById(R.id.tvDeliveryTime)
            linearLayout = itemView.findViewById(R.id.llMainChild)


        }
    }

    private fun addtocartAPI(
        listBean: RestaurantsMenuModel.DataBean.CategoryBean.MenuBean,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {

        CustomProgressbar.showProgressBar(mContext, false)

        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurantId
        map["food_menu_id"] = listBean.foodmenu_id.toString()
        map["food_menu_name"] = listBean.menu_name.toString()
        map["user_id"] = userId

        RetrofitClientSingleton
            .getInstance()
            .addtoCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("RestaurantDetail", t.message.toString())
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        // val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //top
                                tvAddToCart.visibility = View.GONE
                                tvRemove.visibility = View.VISIBLE
                                // dialogForAddCart(response.body()!!.message.toString(),listBean,tvAddToCart,tvRemove)
                                Toast(mContext, "" + response.body()!!.message)
                            }
                            FAILURE -> {
                                dialogForAddCart(
                                    response.body()!!.message.toString(),
                                    listBean,
                                    tvAddToCart,
                                    tvRemove
                                )
                                //  Toast(mContext!!, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })

    }

    private fun dialogForAddCart(
        message: String,
        listBean: RestaurantsMenuModel.DataBean.CategoryBean.MenuBean,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {

        val itemDialog = Dialog(mContext)
        itemDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        itemDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        itemDialog.setContentView(R.layout.dialog_logout)
        itemDialog.tvLogoutDialogTitle.text = message
        itemDialog.show()

        itemDialog.btn_cencle.setOnClickListener { itemDialog.dismiss() }

        itemDialog.btn_ok.setOnClickListener {
            clearCart(listBean, tvAddToCart, tvRemove)
            itemDialog.dismiss()

        }
        /*  val builder = AlertDialog.Builder(mContext,R.style.AlertDialogCustom)
          builder.setMessage(message)
          builder.setPositiveButton("Yes"){dialog, which ->
              clearCart(listBean,tvAddToCart,tvRemove)
              dialog.cancel()
          }
          builder.setNegativeButton("No") { dialog, which ->
              dialog.cancel()
          }
          val dialog: AlertDialog = builder.create()
          dialog.show()*/


    }

    private fun clearCart(
        listBean: RestaurantsMenuModel.DataBean.CategoryBean.MenuBean,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {
        CustomProgressbar.showProgressBar(mContext, false)

        val map = HashMap<String, String>()
        map["user_id"] = userId

        CustomProgressbar.showProgressBar(mContext, false)
        RetrofitClientSingleton
            .getInstance()
            .clearCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        /*   val homeScreenData = response.body()*/
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                // mContext?.let { Toast(it, "" + response.body()!!.message) }
                                addtocartAPI(listBean, tvAddToCart, tvRemove)
                            }
                            FAILURE -> {
                                mContext.let { Toast(it, "" + response.body()!!.message) }
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("Cart clear", response.body().toString())
                }
            })
    }


    private fun removeCartItem(
        dataList: RestaurantsMenuModel.DataBean.CategoryBean.MenuBean, tvAddToCart: TextView,
        tvRemove: TextView, variationId: String
    ) {

        CustomProgressbar.showProgressBar(mContext, false)

        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurantId
        map["food_menu_id"] = dataList.foodmenu_id.toString()
        map["food_menu_name"] = dataList.menu_name.toString()
        map["user_id"] = userId
        map["variation_id"] = variationId

        CustomProgressbar.showProgressBar(mContext, false)
        RetrofitClientSingleton
            .getInstance()
            .removeItemfromCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                tvAddToCart.visibility = View.VISIBLE
                                tvRemove.visibility = View.GONE
                                Toast(mContext, "" + response.body()!!.message)

                            }
                            FAILURE -> {
                                Toast(mContext, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })

    }

/* private fun updateTotal() {
     // var total: Int = 0
     var total: Double = 0.00

     //var totalprice: Int
     var totalprice: Double
     for (i in 0 until cartList!!.size) {

         totalprice = cartList[i].pricetotal
         total = total + totalprice
     }


     finalTotalValue = total
     mView.tvSubTotal.setText(getString(R.string.rupees) + " " + total)
     mView.tvCarttotal.setText(getString(R.string.rupees) + " " + total)


 }*/

}