package com.attune.zamy.adapter

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.activity.LoginPage
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.fragment.HomeFragment
import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.CommonModel
import com.attune.zamy.model.HomeScreenModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.dialog_logout.*
import kotlinx.android.synthetic.main.home_popular_item_row.view.*
import retrofit2.Call
import retrofit2.Response

class HomePopularMonthAdapter(
    var activity: FragmentActivity,
    var data: List<HomeScreenModel.DataBean.PopularThisMonthBean>?,
    var context: HomeFragment,
    internal var itemSelectedListener: AddtoCartItemClickListener,
    internal var itemVariationListener: VarianceDialogClickListener,
    var itemClickListener: PopularItemClickListener
) : RecyclerView.Adapter<HomePopularMonthAdapter.PopularAdapter>() {
    val userId = SharedPref.getUserId(activity)
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopularAdapter {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.home_popular_item_row, parent, false)
        return PopularAdapter(view)
    }

    override fun onBindViewHolder(holder: PopularAdapter, position: Int) {
        data!!.let { holder.bind() }
    }

    override fun getItemCount(): Int {
        return data!!.size
    }

    interface AddtoCartItemClickListener {

        fun onMenuItemSelected(
            favouriteModel: CartRestaurantsMenuModel,
            isProductVariance: Boolean,
            isRemove: Boolean
        )

    }


    inner class PopularAdapter(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind() {
            val tvAddToCart = itemView.findViewById<TextView>(R.id.tvAddToCart)
            val tvRemove = itemView.findViewById<TextView>(R.id.tvRemove)
            val tvCustom = itemView.findViewById<TextView>(R.id.tvCustom)
            var dataModel = data!![adapterPosition]

            if (dataModel.images != null) {
                Glide.with(activity)
                    .load(dataModel.images)
                    .error(R.drawable.ic_place_holder)
                    .into(itemView.imgItem)

            } else {
                Glide.with(activity)
                    .load(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(itemView.imgItem)
            }
            itemView.tvTitle.text = dataModel.menu_name
            itemView.tvItemDetail.text = dataModel.short_code
            if (dataModel.price != null) {
                itemView.txtPrice.text = context.getString(R.string.rupees) + dataModel.price
            }
            if (dataModel.area != null && dataModel.area != "" && dataModel.landmark != null && dataModel.landmark != "") {
                itemView.tvAddress.text = dataModel.area + ", " + dataModel.landmark
            }
            if (dataModel.res_name != null) {
                itemView.tvCompanyName.text = dataModel.res_name
            }

            /* if (dataModel.cart != null && dataModel.cart != "") {
                 if (dataModel.cart.equals("yes", ignoreCase = true)) {
                     tvRemove.visibility = View.VISIBLE
                     tvAddToCart.visibility = View.GONE
                 } else {
                     tvAddToCart.visibility = View.VISIBLE
                     tvRemove.visibility = View.GONE
                 }
             }*/

            /*for (i in data!!.indices) {
                if (dataModel.restaurant_online_status.equals("1")) {
                    itemView.tvAddToCart.visibility = View.GONE
                    itemView.tvCustom.visibility = View.GONE
                } else {
                    itemView.tvAddToCart.visibility = View.VISIBLE
                    itemView.tvCustom.visibility = View.VISIBLE
                }
            }*/
            itemView.setOnClickListener {
                itemClickListener.onPopularItemSelected(dataModel)
            }


            if (dataModel.product_variation!!.size != 0) {
                for (i in data!!.indices) {
                    if (dataModel.restaurant_online_status.equals("0")) {
                        tvCustom.visibility = View.GONE
                    } else {
                        tvCustom.visibility = View.VISIBLE
                    }
                }


                tvRemove.visibility = View.GONE
            } else {
                tvCustom.visibility = View.GONE
            }




            if (dataModel.cart != null) {
                if (dataModel.cart.equals("no", ignoreCase = true)) {
                    for (i in 0..data!!.size) {
                        if (dataModel.restaurant_online_status.equals("0")) {
                            tvAddToCart.visibility = View.GONE
                        } else {
                            tvAddToCart.visibility = View.VISIBLE
                        }
                    }

                    tvRemove.visibility = View.GONE

                    /*if(categoryBean.menu!![i].product_variation!!.size>0){
                        for (j in 0 until categoryBean.menu!![i].product_variation!!.size){
                            if(categoryBean.menu!![i].product_variation!![j].cart_variation_id.equals("yes")){
                                tvCustom.setText("Added to Cart")
                                break
                            }

                        }
*/

                } else {
                    var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> =
                        ArrayList<CartRestaurantsMenuModel>()
                    val favouriteModel = CartRestaurantsMenuModel()
                    favouriteModel.cat_id = dataModel.kitchen_id
                    favouriteModel.foodmenu_id = dataModel.food_menu_id
                    favouriteModel.menu_name = dataModel.menu_name
                    favouriteModel.qty = 1
                    favouriteModel.tax_price = dataModel.price.toString()
                    /*if (categoryBean.menu!![i].reduced_price > 0) {
                        favouriteModel.priceCart =
                            categoryBean.menu!![i].reduced_price.toDouble()
                    } else if (categoryBean.menu!![i].price > 0) {
                        favouriteModel.priceCart = categoryBean.menu!![i].price.toDouble()
                    } else {
                        favouriteModel.priceCart = 0.0
                    }*/

                    favouriteModel.isSelected = false
                    favouriteModel.position = adapterPosition
                    CartItemArrayModel.add(favouriteModel)
                    itemSelectedListener.onMenuItemSelected(favouriteModel, false, false)
                    if (dataModel.product_variation!!.size > 0) {
                        for (i in 0..data!!.size) {
                            if (dataModel.restaurant_online_status.equals("0")) {
                                tvAddToCart.visibility = View.GONE
                                tvCustom.visibility = View.GONE
                            } else {
                                tvAddToCart.visibility = View.VISIBLE
                            }
                        }

                        /*tvRemove.visibility = View.VISIBLE*/
                        //tvCustom.text = "Added to Cart"
                    } else {
                        for (i in 0..data!!.size) {
                            if (dataModel.restaurant_online_status.equals("0")) {
                                tvAddToCart.visibility = View.GONE
                                tvCustom.visibility = View.GONE
                            } else {
                                tvAddToCart.visibility = View.VISIBLE
                            }
                        }
                        tvRemove.visibility = View.VISIBLE
                    }


                }
            }


            tvAddToCart.setOnClickListener {

                if (userId != "") {
                    var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> =
                        ArrayList<CartRestaurantsMenuModel>()
                    val favouriteModel = CartRestaurantsMenuModel()
                    favouriteModel.cat_id = dataModel.kitchen_id
                    favouriteModel.foodmenu_id = dataModel.food_menu_id
                    favouriteModel.menu_name = dataModel.menu_name
                    favouriteModel.qty = 1
                    favouriteModel.tax_price = dataModel.price!!
                    /*if (categoryBean.menu!![i].reduced_price > 0) {
                        favouriteModel.priceCart =
                            categoryBean.menu!![i].reduced_price.toDouble()
                    } else if (categoryBean.menu!![i].price > 0) {
                        favouriteModel.priceCart = categoryBean.menu!![i].price.toDouble()
                    } else {
                        favouriteModel.priceCart = 0.0
                    }*/

                    favouriteModel.isSelected = false
                    favouriteModel.position = adapterPosition
                    CartItemArrayModel.add(favouriteModel)
                    //itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)

                    if (dataModel.product_variation!!.isNotEmpty()) {
                        // itemSelectedListener.onMenuItemSelected(favouriteModel,true)
                        itemVariationListener.onVarianceDialogSelected(
                            data!![adapterPosition].product_variation!!,
                            dataModel.kitchen_id,
                            dataModel.food_menu_id,
                            dataModel.menu_name,
                            dataModel.price!!,
                            tvCustom,
                            tvRemove
                        )

                    } else {
                        //  itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)

                        addtocartAPI(dataModel, tvAddToCart, tvRemove)

                    }

                } else {
                    val ine = Intent(activity, LoginPage::class.java)
                    ine.putExtra(SCREEN_REDIRECTION, "1")
                    activity.startActivity(ine)


                }
            }



            tvCustom.setOnClickListener {
                if (userId != "") {
                    var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> =
                        ArrayList<CartRestaurantsMenuModel>()
                    val favouriteModel = CartRestaurantsMenuModel()
                    favouriteModel.cat_id = dataModel.kitchen_id
                    favouriteModel.foodmenu_id = dataModel.food_menu_id
                    favouriteModel.menu_name = dataModel.menu_name
                    favouriteModel.qty = 1
                    favouriteModel.tax_price = dataModel.price.toString()
                    /* if (categoryBean.menu!![i].reduced_price > 0) {
                         favouriteModel.priceCart =
                             categoryBean.menu!![i].reduced_price.toDouble()
                     } else if (categoryBean.menu!![i].price > 0) {
                         favouriteModel.priceCart = categoryBean.menu!![i].price.toDouble()
                     } else {
                         favouriteModel.priceCart = 0.0
                     }*/
                    favouriteModel.isSelected = false
                    favouriteModel.position = adapterPosition
                    CartItemArrayModel.add(favouriteModel)
                    //itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)

                    if (dataModel.product_variation!!.isNotEmpty()) {
                        // itemSelectedListener.onMenuItemSelected(favouriteModel,true)
                        itemVariationListener.onVarianceDialogSelected(
                            data!![adapterPosition].product_variation!!, dataModel.kitchen_id,
                            dataModel.food_menu_id,
                            dataModel.menu_name,
                            dataModel.price.toString(), tvCustom, tvRemove
                        )
                        //   tvCustom.setText("Added to Cart")
                    } else {
                        //  itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)
                        addtocartAPI(dataModel, tvAddToCart, tvRemove)

                    }

                } else {
                    val ine = Intent(activity, LoginPage::class.java)
                    ine.putExtra(SCREEN_REDIRECTION, "1")
                    activity.startActivity(ine)


                }
            }


            tvRemove.setOnClickListener {
                val variationId = ""
                removeCartItem(dataModel, tvAddToCart, tvRemove, variationId)
            }

        }
    }

    interface PopularItemClickListener {
        fun onPopularItemSelected(listBean: HomeScreenModel.DataBean.PopularThisMonthBean)
    }

    interface VarianceDialogClickListener {

        fun onVarianceDialogSelected(
            dataBean: List<HomeScreenModel.DataBean.PopularThisMonthBean.ProductVariationBean>,
            catId: String?,
            foodmenuId: String?,
            menuName: String?,
            taxPrice: String,
            tvCustom: TextView,
            tvRemove: TextView
        )

    }

    private fun addtocartAPI(
        listBean: HomeScreenModel.DataBean.PopularThisMonthBean,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {

        CustomProgressbar.showProgressBar(activity, false)
        val map = HashMap<String, String>()
        map["restaurant_id"] = listBean.kitchen_id.toString()
        map["food_menu_id"] = listBean.food_menu_id.toString()
        map["food_menu_name"] = listBean.menu_name.toString()
        map["user_id"] = SharedPref.getUserId(activity)

        RetrofitClientSingleton
            .getInstance()
            .addtoCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("RestaurantDetail", t.message.toString())
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        // val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //top
                                //tvAddToCart.visibility = View.GONE
                                tvRemove.visibility = View.VISIBLE
                                // dialogForAddCart(response.body()!!.message.toString(),listBean,tvAddToCart,tvRemove)
                                Toast(activity, "" + response.body()!!.message)
                                /*notifyDataSetChanged()*/
                                context.getDashboardApi()


                            }
                            FAILURE -> {
                                dialogForAddCart(
                                    response.body()!!.message.toString(),
                                    listBean,
                                    tvAddToCart,
                                    tvRemove
                                )
                                context.getDashboardApi()
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })

    }

    private fun dialogForAddCart(
        message: String,
        listBean: HomeScreenModel.DataBean.PopularThisMonthBean,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {


        val itemDialog = Dialog(activity)
        itemDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        itemDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        itemDialog.setContentView(R.layout.dialog_logout)
        itemDialog.tvLogoutDialogTitle.text = message
        itemDialog.show()

        itemDialog.btn_cencle.setOnClickListener { itemDialog.dismiss() }

        itemDialog.btn_ok.setOnClickListener {
            clearCart(listBean, tvAddToCart, tvRemove)
            context.getDashboardApi()
            itemDialog.dismiss()

        }

        /*val builder = AlertDialog.Builder(activity, R.style.AlertDialogCustom)
        builder.setMessage(message)
        builder.setPositiveButton("Yes") { dialog, which ->
            clearCart(listBean, tvAddToCart, tvRemove)
            context.getDashboardApi()
            dialog.cancel()
        }
        builder.setNegativeButton("No") { dialog, which ->
            dialog.cancel()
        }
        val dialog: AlertDialog = builder.create()
        dialog.show()
*/

    }

    private fun clearCart(
        listBean: HomeScreenModel.DataBean.PopularThisMonthBean,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {
        CustomProgressbar.showProgressBar(activity, false)

        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(activity)

        CustomProgressbar.showProgressBar(activity, false)
        RetrofitClientSingleton
            .getInstance()
            .clearCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        /*   val homeScreenData = response.body()*/
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                // mContext?.let { Toast(it, "" + response.body()!!.message) }
                                notifyDataSetChanged()
                                addtocartAPI(listBean, tvAddToCart, tvRemove)

                            }
                            FAILURE -> {
                                activity.let { Toast(it, "" + response.body()!!.message) }
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("Cart clear", response.body().toString())
                }
            })
    }


    private fun removeCartItem(
        dataList: HomeScreenModel.DataBean.PopularThisMonthBean, tvAddToCart: TextView,
        tvRemove: TextView, variationId: String
    ) {

        CustomProgressbar.showProgressBar(activity, false)

        val map = HashMap<String, String>()
        map["restaurant_id"] = dataList.kitchen_id.toString()
        map["food_menu_id"] = dataList.food_menu_id.toString()
        map["food_menu_name"] = dataList.menu_name.toString()
        map["user_id"] = SharedPref.getUserId(activity)
        map["variation_id"] = variationId

        CustomProgressbar.showProgressBar(activity, false)
        RetrofitClientSingleton
            .getInstance()
            .removeItemfromCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                tvAddToCart.visibility = View.VISIBLE
                                tvRemove.visibility = View.GONE
                                Toast(activity, "" + response.body()!!.message)
                                // notifyDataSetChanged()
                                context.getDashboardApi()

                            }
                            FAILURE -> {
                                Toast(activity, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })

    }


}
