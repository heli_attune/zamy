package com.attune.zamy.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.model.SearchDataModel
import com.bumptech.glide.Glide

class SearchRestaurantDishAdapter(
    private val context: Context,
    private val dataListModels: MutableList<SearchDataModel.DataBean.SearchDataBean>,
    internal var itemSelectedListener: OnItemSelectedListener
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mInflater: LayoutInflater = LayoutInflater.from(context)
    private var isLoaded = false


    override fun getItemViewType(position: Int): Int {
        return if (position == dataListModels.size - 1 && isLoaded) LOADING else ITEM
    }


    // inflates the cell layout from xml when needed
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM -> {
                val view = mInflater.inflate(R.layout.search_item_row, parent, false)
                return ViewHolder(view)
            }
            LOADING -> return LoadingHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_loading,
                    parent,
                    false
                )
            )
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    // binds the data to the textview, imageview in each cell
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        when (viewHolder.itemViewType) {
            ITEM -> {
                val holder = viewHolder as ViewHolder
                val mData = dataListModels[position]
                holder.tvItemName.text = mData.restaurant!!.res_name
                if (mData.restaurant!!.email != null && mData.restaurant!!.email != "") {
                    holder.tvCompanyName.text = mData.restaurant!!.email
                }
                if (mData.restaurant!!.area != null && mData.restaurant!!.area != "" && mData.restaurant!!.landmark != null && mData.restaurant!!.landmark != "") {
                    holder.tvAddress.text =
                        mData.restaurant!!.area + ", " + mData.restaurant!!.landmark
                }

                if (mData.restaurant!!.logo != null && mData.restaurant!!.logo != "") {
                    Glide.with(context)
                        .load(mData.restaurant!!.logo)
                        .into(holder.imgItem)
                }
                if (mData.restaurant!!.service_type != null) {
                    holder.tvItemDetail.text = mData.restaurant!!.service_type
                }

                holder.rlSearch.setOnClickListener {
                    itemSelectedListener.onItemSelected(mData, position, holder.adapterPosition)
                }
            }
            LOADING -> {
            }
        }
    }


    fun clearList() {
        dataListModels.clear()
    }

    override fun getItemCount(): Int {

        return dataListModels.size
    }

    internal inner class LoadingHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    inner class ViewHolder
        (itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvItemName: TextView = itemView.findViewById(R.id.tvItemName)
        internal var tvCompanyName: TextView = itemView.findViewById(R.id.tvCompanyName)
        internal var tvItemDetail: TextView = itemView.findViewById(R.id.tvItemDetail)
        internal var tvItemOffer: TextView = itemView.findViewById(R.id.tvItemOffer)
        internal var imgItem: ImageView = itemView.findViewById(R.id.imgItem)
        internal var tvAddress: TextView = itemView.findViewById(R.id.tvAddress)
        internal var rlSearch: RelativeLayout = itemView.findViewById(R.id.rlSearch)


    }

    interface OnItemSelectedListener {
        fun onItemSelected(
            item: SearchDataModel.DataBean.SearchDataBean,
            position: Int,
            adapterPostion: Int
        )
    }

    fun addItem(dataList: SearchDataModel.DataBean.SearchDataBean) {
        dataListModels.add(dataList)
        notifyItemInserted(dataListModels.size - 1)
    }

    fun addAllItem(dataLists: List<SearchDataModel.DataBean.SearchDataBean>) {
        for (applicationHistory in dataLists) {
            addItem(applicationHistory)
        }
    }

    fun removeAllItem() {
        dataListModels.clear()
        notifyDataSetChanged()
    }

    fun addLoading() {
        isLoaded = true
        addItem(SearchDataModel.DataBean.SearchDataBean())
    }

    fun removeLoading() {
        isLoaded = false
        val position = dataListModels.size - 1
        val applicationHistory = getItem(position)
        if (applicationHistory != null) {
            dataListModels.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun getItem(position: Int): SearchDataModel.DataBean.SearchDataBean? {
        return dataListModels[position]
    }

    companion object {
        private val ITEM = 0
        private val LOADING = 1
    }

}
