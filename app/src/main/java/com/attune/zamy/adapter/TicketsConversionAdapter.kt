package com.attune.zamy.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.model.TicketListingConversationModel
import kotlinx.android.synthetic.main.row_layout_conversion.view.*

class TicketsConversionAdapter(private val mContext: Context)
    : RecyclerView.Adapter<TicketsConversionAdapter.TicketViewHolder>() {

    private val mArrayList: ArrayList<TicketListingConversationModel.DataBean> = ArrayList()

    fun setList(arrayList: ArrayList<TicketListingConversationModel.DataBean>) {
        this.mArrayList.clear()
        this.mArrayList.addAll(arrayList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicketViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.row_layout_conversion, parent, false)
        return TicketViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mArrayList.size
    }

    override fun onBindViewHolder(holder: TicketViewHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    inner class TicketViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(conversion: TicketListingConversationModel.DataBean) {
            if (conversion.ticketSenderID != "1") {
                //admin
                itemView.ll_you.visibility = View.GONE
                itemView.ll_me.visibility = View.VISIBLE
                itemView.txt_my.text = conversion.ticketBody!!.trim()
                itemView.txt_time_me.text = conversion.ticketDateTime

            } else {
                // user
                itemView.ll_you.visibility = View.VISIBLE
                itemView.ll_me.visibility = View.GONE
                itemView.txt_you.text = conversion.ticketBody!!.trim()
                itemView.txt_time_you.text = conversion.ticketDateTime
            }
        }

    }
}