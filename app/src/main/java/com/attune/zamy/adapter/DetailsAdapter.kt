package com.attune.zamy.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.activity.LoginPage
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.CommonModel
import com.attune.zamy.model.RestaurantsMenuModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Response

class DetailsAdapter(
    val mContext: Context,
    var mArrayListString: MutableList<RestaurantsMenuModel.DataBean.CategoryBean>,
    internal var itemSelectedListener: DemoRestaurantDetailMenuAdapter.AddtoCartItemClickListener,
    internal var itemVariationListener: DemoRestaurantDetailMenuAdapter.VarianceDialogClickListener,
    val restaurantId: String
) :
    RecyclerView.Adapter<DetailsAdapter.DetailHolder>() {

    var userId: String = SharedPref.getUserId(mContext)


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailHolder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.restaurant_detail_item_row_child, parent, false)
        return DetailHolder(view)
    }

    fun clearData() {
        mArrayListString.clear()
    }

    override fun onBindViewHolder(holder: DetailHolder, position: Int) {
        mArrayListString.let { holder.bind(mArrayListString) }

    }

    override fun getItemCount(): Int {
        return mArrayListString.size
    }

    inner class DetailHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        lateinit var mTextView: TextView
        lateinit var tvSubTitle: TextView
        lateinit var tvItemName: TextView
        lateinit var tvPrice: TextView
        lateinit var tvDeliveryTime: TextView
        lateinit var tvRemove: TextView
        lateinit var linearLayout: LinearLayout
        lateinit var tvCustom: TextView
        lateinit var tvAddToCart: TextView
        lateinit var tvRemoveCart: TextView
        lateinit var imgMenu: ImageView
        lateinit var rlheader: RelativeLayout
        @SuppressLint("SetTextI18n")
        fun bind(mArrayListString: MutableList<RestaurantsMenuModel.DataBean.CategoryBean>) {
            tvRemoveCart = itemView.findViewById(R.id.tvRemove)
            tvAddToCart = itemView.findViewById(R.id.tvAddToCart)
            tvCustom = itemView.findViewById(R.id.tvCustom)
            tvPrice = itemView.findViewById(R.id.tvPrice)
            tvRemove = itemView.findViewById(R.id.tvRemove)
            mTextView = itemView.findViewById(R.id.tvItemName)
            tvItemName = itemView.findViewById(R.id.tvItemName)
            tvSubTitle = itemView.findViewById(R.id.tvSubTitle)
            tvDeliveryTime = itemView.findViewById(R.id.tvDeliveryTime)
            linearLayout = itemView.findViewById(R.id.llMainChild)
            imgMenu = itemView.findViewById(R.id.imgMenu)
            rlheader = itemView.findViewById(R.id.rlheader)
            rlheader.visibility = View.VISIBLE

            val menuBean = mArrayListString[adapterPosition]
            if (menuBean.menu!!.isNotEmpty()) {
                mTextView.text = menuBean.cat_display_name
            }
            for (i in menuBean.menu!!.indices) {

                if (menuBean.menu!![i].product_variation!!.isNotEmpty()) {
                    tvPrice.text = menuBean.menu!![i].variation_price
                    tvCustom.visibility = View.VISIBLE
                } else {
                    tvCustom.visibility = View.GONE
                }
                if (menuBean.menu!!.size == 1) {
                    rlheader.visibility = View.VISIBLE
                }
                if (i == 0) {

                } else if (i == menuBean.menu!!.size - 1) {
                    rlheader.visibility = View.VISIBLE
                }

                if (menuBean.menu!![i].cart != null) {
                    if (menuBean.menu!![i].cart.equals("no")) {
                        tvAddToCart.visibility = View.VISIBLE
                        //tvRemove.visibility = View.GONE


                    } else {

                        var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> =
                            ArrayList<CartRestaurantsMenuModel>()
                        val favouriteModel = CartRestaurantsMenuModel()
                        favouriteModel.cat_id = menuBean.menu!![i].cat_id
                        favouriteModel.foodmenu_id = menuBean.menu!![i].foodmenu_id
                        favouriteModel.menu_name = menuBean.menu!![i].menu_name
                        favouriteModel.qty = 1
                        favouriteModel.tax_price = menuBean.menu!![i].tax_price.toString()
                        if (menuBean.menu!![i].reduced_price > 0) {
                            favouriteModel.priceCart = menuBean.menu!![i].reduced_price.toDouble()
                        } else if (menuBean.menu!![i].price > 0) {
                            favouriteModel.priceCart = menuBean.menu!![i].price.toDouble()
                        } else {
                            favouriteModel.priceCart = 0.0
                        }

                        favouriteModel.isSelected = false
                        favouriteModel.position = adapterPosition
                        CartItemArrayModel.add(favouriteModel)
                        itemSelectedListener.onMenuItemSelected(favouriteModel, false, false)
                        if (menuBean.menu!![i].product_variation!!.isNotEmpty()) {
                            /*tvAddToCart.visibility = View.GONE
                            tvRemove.visibility = View.VISIBLE*/
                        } else {
                            tvAddToCart.visibility = View.GONE
                            // tvRemove.visibility = View.VISIBLE
                        }


                    }
                }

                if (menuBean.menu!![i].menu_logo != null) {
                    Glide.with(mContext)
                        .load(menuBean.menu!![i].menu_logo)
                        .error(R.drawable.ic_place_holder)
                        .into(imgMenu)

                } else {
                    Glide.with(mContext)
                        .load(R.drawable.ic_place_holder)
                        .error(R.drawable.ic_place_holder)
                        .into(imgMenu)
                }

                if (menuBean.menu!![i].long_description != null && menuBean.menu!![i].long_description !== "") {
                    tvSubTitle.text = menuBean.menu!![i].long_description

                }
                if (menuBean.menu!![i].menu_name != null && menuBean.menu!![i].menu_name !== "") {
                    tvItemName.text = menuBean.menu!![i].menu_name
                }
                if (menuBean.menu!![i].minimum_preparation_time != null && menuBean.menu!![i].minimum_preparation_time !== "") {
                    tvDeliveryTime.text = menuBean.menu!![i].minimum_preparation_time
                }
                if (menuBean.menu!![i].reduced_price != 0) {
                    tvPrice.text =
                        mContext.resources.getString(R.string.rupees) + menuBean.menu!![i].reduced_price
                } else {
                    tvPrice.text =
                        mContext.resources.getString(R.string.rupees) + menuBean.menu!![i].price
                }
                if (menuBean.menu!![i].product_variation!!.size > 0) {
                    tvPrice.text =
                        mContext.resources.getString(R.string.rupees) + menuBean.menu!![i].variation_price
                } else {

                }

                tvAddToCart.setOnClickListener {

                    if (userId != "") {
                        val CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> =
                            ArrayList<CartRestaurantsMenuModel>()
                        val favouriteModel = CartRestaurantsMenuModel()
                        favouriteModel.cat_id = menuBean.menu!![i].cat_id
                        favouriteModel.foodmenu_id = menuBean.menu!![i].foodmenu_id
                        favouriteModel.menu_name = menuBean.menu!![i].menu_name
                        favouriteModel.qty = 1
                        favouriteModel.tax_price = menuBean.menu!![i].tax_price.toString()
                        if (menuBean.menu!![i].reduced_price > 0) {
                            favouriteModel.priceCart =
                                menuBean.menu!![i].reduced_price.toDouble()
                        } else if (menuBean.menu!![i].price > 0) {
                            favouriteModel.priceCart = menuBean.menu!![i].price.toDouble()
                        } else {
                            favouriteModel.priceCart = 0.0
                        }

                        favouriteModel.isSelected = false
                        favouriteModel.position = adapterPosition
                        CartItemArrayModel.add(favouriteModel)
                        //itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)

                        if (menuBean.menu!![i].product_variation!!.isNotEmpty()) {
                            // itemSelectedListener.onMenuItemSelected(favouriteModel,true)
                            itemVariationListener.onVarianceDialogSelected(
                                menuBean.menu!![i].product_variation!!, menuBean.cat_id,
                                menuBean.menu!![i].foodmenu_id,
                                menuBean.menu!![i].menu_name,
                                menuBean.menu!![i].tax_price, tvAddToCart, tvRemove
                            )
                        } else {
                            itemSelectedListener.onMenuItemSelected(favouriteModel, false, false)
                            addtocartAPI(menuBean.menu!![i], tvAddToCart, tvRemove)

                        }
                        /*tvAddToCart.visibility= View.GONE

                    tvRemove.visibility=View.VISIBLE*/
                    } else {
                        val ine = Intent(mContext, LoginPage::class.java)
                        ine.putExtra(SCREEN_REDIRECTION, "1")
                        mContext.startActivity(ine)
                    }
                }

                tvRemove.setOnClickListener {
                    var variationId = ""
                    removeCartItem(menuBean.menu!![i], tvAddToCart, tvRemove, variationId)
                }

                tvCustom.setOnClickListener {
                    if (userId != "") {
                        val CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> =
                            ArrayList<CartRestaurantsMenuModel>()
                        val favouriteModel = CartRestaurantsMenuModel()
                        favouriteModel.cat_id = menuBean.menu!![i].cat_id
                        favouriteModel.foodmenu_id = menuBean.menu!![i].foodmenu_id
                        favouriteModel.menu_name = menuBean.menu!![i].menu_name
                        favouriteModel.qty = 1
                        favouriteModel.tax_price = menuBean.menu!![i].tax_price.toString()
                        if (menuBean.menu!![i].reduced_price > 0) {
                            favouriteModel.priceCart = menuBean.menu!![i].reduced_price.toDouble()
                        } else if (menuBean.menu!![i].price > 0) {
                            favouriteModel.priceCart = menuBean.menu!![i].price.toDouble()
                        } else {
                            favouriteModel.priceCart = 0.0
                        }

                        favouriteModel.isSelected = false
                        favouriteModel.position = adapterPosition
                        CartItemArrayModel.add(favouriteModel)
                        //itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)

                        if (menuBean.menu!![i].product_variation!!.isNotEmpty()) {
                            // itemSelectedListener.onMenuItemSelected(favouriteModel,true)
                            itemVariationListener.onVarianceDialogSelected(
                                menuBean.menu!![i].product_variation!!, menuBean.cat_id,
                                menuBean.menu!![i].foodmenu_id,
                                menuBean.menu!![i].menu_name,
                                menuBean.menu!![i].tax_price, tvAddToCart, tvRemove
                            )

                        } else {
                            //  itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)
                            // addtocartAPI(categoryBean.menu!![i], tvAddToCart, tvRemove)

                        }

                    } else {
                        val ine = Intent(mContext, LoginPage::class.java)
                        ine.putExtra(SCREEN_REDIRECTION, "1")
                        mContext.startActivity(ine)
                    }
                }
            }

        }
    }

    private fun addtocartAPI(
        menuBean: RestaurantsMenuModel.DataBean.CategoryBean.MenuBean,
        tvAddToCart: TextView?,
        tvRemove: TextView?
    ) {
        CustomProgressbar.showProgressBar(mContext, false)

        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurantId
        map["food_menu_id"] = menuBean.foodmenu_id.toString()
        map["food_menu_name"] = menuBean.menu_name.toString()
        map["user_id"] = userId

        RetrofitClientSingleton
            .getInstance()
            .addtoCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("RestaurantDetail", t.message.toString())
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        // val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //top
                                tvAddToCart!!.visibility = View.GONE
                                tvRemove!!.visibility = View.VISIBLE
                                // dialogForAddCart(response.body()!!.message.toString(),listBean,tvAddToCart,tvRemove)
                                Toast(mContext, "" + response.body()!!.message)
                            }
                            FAILURE -> {
                                /*dialogForAddCart(
                                    response.body()!!.message.toString(),
                                    menuBean,
                                    tvAddToCart!!,
                                    tvRemove!!
                                )*/
                                //  Toast(mContext!!, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }


    private fun clearCart(
        listBean: RestaurantsMenuModel.DataBean.CategoryBean.MenuBean,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {
        CustomProgressbar.showProgressBar(mContext, false)

        val map = HashMap<String, String>()
        map["user_id"] = userId

        CustomProgressbar.showProgressBar(mContext, false)
        RetrofitClientSingleton
            .getInstance()
            .clearCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        /*   val homeScreenData = response.body()*/
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                // mContext?.let { Toast(it, "" + response.body()!!.message) }
                                addtocartAPI(listBean, tvAddToCart, tvRemove)
                            }
                            FAILURE -> {
                                mContext.let { Toast(it, "" + response.body()!!.message) }
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("Cart clear", response.body().toString())
                }
            })
    }

    private fun removeCartItem(
        dataList: RestaurantsMenuModel.DataBean.CategoryBean.MenuBean, tvAddToCart: TextView,
        tvRemove: TextView, variationId: String
    ) {

        CustomProgressbar.showProgressBar(mContext, false)

        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurantId
        map["food_menu_id"] = dataList.foodmenu_id.toString()
        map["food_menu_name"] = dataList.menu_name.toString()
        map["user_id"] = userId
        map["variation_id"] = variationId

        CustomProgressbar.showProgressBar(mContext, false)
        RetrofitClientSingleton
            .getInstance()
            .removeItemfromCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                tvAddToCart.visibility = View.VISIBLE
                                tvRemove.visibility = View.GONE
                                Toast(mContext, "" + response.body()!!.message)

                            }
                            FAILURE -> {
                                Toast(mContext, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })

    }


}
