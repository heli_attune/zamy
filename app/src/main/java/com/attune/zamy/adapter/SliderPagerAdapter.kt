    import android.annotation.SuppressLint
    import android.content.Context
    import android.view.LayoutInflater
    import android.view.View
    import android.view.ViewGroup
    import android.widget.ImageView
    import androidx.fragment.app.FragmentActivity
    import androidx.viewpager.widget.PagerAdapter
    import com.attune.zamy.R
    import com.attune.zamy.model.HomeScreenModel
    import com.bumptech.glide.Glide
    import com.bumptech.glide.request.RequestOptions

    class SliderPagerAdapter(
        val activity: FragmentActivity,
        val couponList: List<HomeScreenModel.DataBean.CouponListBean>?
    ) : PagerAdapter() {
        private var layoutInflater: LayoutInflater? = null

        @SuppressLint("CheckResult")
        override fun instantiateItem(container: ViewGroup, position: Int): Any {
            layoutInflater =
                activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater

            val view = layoutInflater!!.inflate(R.layout.row_offer_items, container, false)
            val im_slider = view.findViewById<View>(R.id.imgOffers) as ImageView
            val requestOptions = RequestOptions()
            requestOptions.placeholder(R.drawable.ic_user_placeholder)
            requestOptions.error(R.drawable.ic_user_placeholder)
            Glide.with(activity.applicationContext)
                .load(couponList!![position].coupon_image)
                .dontAnimate()
                .dontAnimate()
                .into(im_slider)
            container.addView(view)
            return view
        }

        override fun getCount(): Int {
            return couponList!!.size
        }


        override fun isViewFromObject(view: View, obj: Any): Boolean {
            return view === obj
        }


        override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
            val view = `object` as View
            container.removeView(view)
        }
    }