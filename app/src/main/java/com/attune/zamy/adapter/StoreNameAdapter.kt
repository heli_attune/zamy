package com.attune.zamy.adapter

import android.app.Activity
import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import com.attune.zamy.R
import com.attune.zamy.model.StoreModel

class StoreNameAdapter(a: Activity, items: ArrayList<StoreModel.DataBean>) :
    ArrayAdapter<StoreModel?>(a, items!!.size)
{
    private var mInflater: LayoutInflater? = null
    private val activity: Activity
    lateinit var  data:List<StoreModel.DataBean>


    init {
        activity = a
        mInflater = activity
            .getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
        data=items
    }

       class ViewHolder {
        var title: AppCompatTextView? = null


    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var convertView: View? = convertView
        val holder: ViewHolder

        if (convertView == null) {
            holder = ViewHolder()
            convertView = mInflater?.inflate(
                R.layout.row_store,
                parent, false
            )
            holder.title = convertView
                ?.findViewById(R.id.tvStorename) as AppCompatTextView

            holder.title!!.setText(data[position].res_name)
            Log.d("Store name",data[position].res_name)


            convertView.setTag(holder)
        } else {
            holder = convertView.getTag() as ViewHolder
        }
        return convertView
    }


}