package com.attune.zamy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.attune.zamy.R;
import com.attune.zamy.model.RestaurantsMenuModel;

import java.util.List;


public class RestaurantDetailMenuAdapter extends RecyclerView.Adapter<RestaurantDetailMenuAdapter.Holder> {

    Context mContext;
    List<RestaurantsMenuModel.DataBean.CategoryBean> mArrayListString;

    public RestaurantDetailMenuAdapter(Context mContext, List<RestaurantsMenuModel.DataBean.CategoryBean> mArrayListString) {
        this.mContext = mContext;
        this.mArrayListString = mArrayListString;
    }

    @Override
    public Holder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(mContext).inflate(R.layout.restaurant_detail_item_row_child, parent, false);
        return new Holder(view);
    }

    @Override
    public void onBindViewHolder(Holder holder, int position) {
        RestaurantsMenuModel.DataBean.CategoryBean categoryBean = mArrayListString.get(position);
        holder.linearLayout.removeAllViews();

        holder.mTextView.setText(categoryBean.getCat_display_name());


        for (int i = 0; i < categoryBean.getMenu().size(); i++) {

            View view = LayoutInflater.from(mContext).inflate(R.layout.restaurant_detail_item_row_child, null);
            //   TextView textView = new TextView(mContext);
            TextView tvSubTitle = view.findViewById(R.id.tvSubTitle);
            TextView tvItemName = view.findViewById(R.id.tvItemName);
            TextView tvDeliveryTime = view.findViewById(R.id.tvDeliveryTime);
            TextView tvPrice = view.findViewById(R.id.tvPrice);
            holder.linearLayout.addView(view);
            RelativeLayout rlheader = view.findViewById(R.id.rlheader);
            rlheader.setVisibility(View.GONE);
            TextView tvCustom = view.findViewById(R.id.tvCustom);
            final LinearLayout lnCart= view.findViewById(R.id.lnCart);
            lnCart.setVisibility(View.GONE);
            final TextView tvAddToCart = view.findViewById(R.id.tvAddToCart);
            tvAddToCart.setVisibility(View.VISIBLE);
            tvAddToCart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    tvAddToCart.setVisibility(View.GONE);

                }
            });
            ImageView imgPlus = view.findViewById(R.id.imgPlus);
            ImageView imgMinus = view.findViewById(R.id.imgMinus);
            imgPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    //currentQTY = mArrayListString.getqty
                }
            });
            imgMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });

            //if product variance is available show custom button
            if (categoryBean.getMenu().get(i).getProduct_variation().size() > 0) {
                tvCustom.setVisibility(View.VISIBLE);
            } else {
                tvCustom.setVisibility(View.GONE);
            }
            if (i == 0) {

            } else if (i == categoryBean.getMenu().size() - 1) {
                rlheader.setVisibility(View.VISIBLE);

            }

            if (categoryBean.getMenu().get(i).getLong_description() != null && categoryBean.getMenu().get(i).getLong_description() != "") {
                tvSubTitle.setText(categoryBean.getMenu().get(i).getLong_description());

            }
            if (categoryBean.getMenu().get(i).getMenu_name() != null && categoryBean.getMenu().get(i).getMenu_name() != "") {
                tvItemName.setText(categoryBean.getMenu().get(i).getMenu_name());
            }
            if (categoryBean.getMenu().get(i).getMinimum_preparation_time() != null && categoryBean.getMenu().get(i).getMinimum_preparation_time() != "") {
                tvDeliveryTime.setText(categoryBean.getMenu().get(i).getMinimum_preparation_time());
            }
            if (categoryBean.getMenu().get(i).getTax_price() != 0) {
                tvPrice.setText(mContext.getResources().getString(R.string.rupees) + categoryBean.getMenu().get(i).getMinimum_preparation_time());
            }


        }
    }

    @Override
    public int getItemCount() {
        return mArrayListString.size();
    }

    public class Holder extends RecyclerView.ViewHolder {

        TextView mTextView, tvSubTitle, tvItemName, tvDeliveryTime;
        LinearLayout linearLayout;

        public Holder(View itemView) {
            super(itemView);
            mTextView = itemView.findViewById(R.id.tvItemName);
            tvItemName = itemView.findViewById(R.id.tvItemName);
            tvSubTitle = itemView.findViewById(R.id.tvSubTitle);
            tvDeliveryTime = itemView.findViewById(R.id.tvDeliveryTime);
            linearLayout = itemView.findViewById(R.id.llMainChild);

        }
    }

}