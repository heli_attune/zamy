package com.attune.zamy.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.activity.LoginPage
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.CommonModel
import com.attune.zamy.model.RestaurantsMenuModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Response


class DemoPaginationAdappter(
    internal var mContext: Context,
    internal var mArrayListString: MutableList<RestaurantsMenuModel.DataBean.CategoryBean>,
    internal var itemSelectedListener: AddtoCartItemClickListener,
    internal var itemVariationListener: VarianceDialogClickListener,
    val restaurantId: String


) : RecyclerView.Adapter<DemoPaginationAdappter.Holder>() {

    lateinit var userId: String

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.restaurant_detail_item_row_child, parent, false)
        return Holder(view)
    }
       fun addAll(arrayList: MutableList<RestaurantsMenuModel.DataBean.CategoryBean>) {
           mArrayListString.addAll(arrayList)
           notifyDataSetChanged()
       }
    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: Holder, position: Int) {
        val categoryBean = mArrayListString[position]
        holder.linearLayout.removeAllViews()

        holder.mTextView.text = categoryBean.cat_display_name
        userId= SharedPref.getUserId(mContext)

        for (i in 0 until categoryBean.menu!!.size) {

            val view = LayoutInflater.from(mContext)
                .inflate(R.layout.restaurant_detail_item_row_child, null)
            //   TextView textView = new TextView(mContext);
            val tvSubTitle = view.findViewById<TextView>(R.id.tvSubTitle)
            val tvItemName = view.findViewById<TextView>(R.id.tvItemName)
            val tvDeliveryTime = view.findViewById<TextView>(R.id.tvDeliveryTime)
            val tvPrice = view.findViewById<TextView>(R.id.tvPrice)
            holder.linearLayout.addView(view)
            val rlheader = view.findViewById<RelativeLayout>(R.id.rlheader)
            rlheader.visibility = View.GONE
            val tvCustom = view.findViewById<TextView>(R.id.tvCustom)
            val lnCart = view.findViewById<LinearLayout>(R.id.lnCart)
            lnCart.visibility = View.GONE
            val tvAddToCart = view.findViewById<TextView>(R.id.tvAddToCart)
            val tvRemove = view.findViewById<TextView>(R.id.tvRemove)
            tvAddToCart.visibility = View.VISIBLE
            var imgMenu = view.findViewById<ImageView>(R.id.imgMenu)
            if (categoryBean.menu!![i].product_variation!!.size > 0) {
                tvCustom.visibility = View.VISIBLE
            } else {
                tvCustom.visibility = View.GONE
            }
            if (i == 0) {

            } else if (i == categoryBean.menu!!.size - 1) {
                rlheader.visibility = View.VISIBLE

            }

            if(categoryBean.menu!![i].cart!=null){
                if(categoryBean.menu!![i].cart.equals("no")){
                    tvAddToCart.visibility=View.VISIBLE
                    tvRemove.visibility=View.GONE


                }else{

                    var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> = ArrayList<CartRestaurantsMenuModel>()
                    val favouriteModel = CartRestaurantsMenuModel()
                    favouriteModel.cat_id =categoryBean.menu!![i].cat_id
                    favouriteModel.foodmenu_id =categoryBean.menu!![i].foodmenu_id
                    favouriteModel.menu_name =categoryBean.menu!![i].menu_name
                    favouriteModel.qty =1
                    favouriteModel.tax_price =categoryBean.menu!![i].tax_price.toString()
                    if(categoryBean.menu!![i].reduced_price>0){
                        favouriteModel.priceCart =categoryBean.menu!![i].reduced_price.toDouble()
                    }else if(categoryBean.menu!![i].price>0){
                        favouriteModel.priceCart =categoryBean.menu!![i].price.toDouble()
                    }else{
                        favouriteModel.priceCart =0.0
                    }

                    favouriteModel.isSelected=false
                    favouriteModel.position= holder.adapterPosition
                    CartItemArrayModel.add(favouriteModel)
                    itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)
                    tvAddToCart.visibility=View.GONE
                    tvRemove.visibility=View.VISIBLE

                }
            }

            if(categoryBean.menu!![i].menu_logo!=null){
                Glide.with(mContext)
                    .load(categoryBean.menu!![i].menu_logo)
                    .error(R.drawable.ic_place_holder)
                    .into(imgMenu)

            }else{
                Glide.with(mContext)
                    .load(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(imgMenu)
            }

            if (categoryBean.menu!![i].long_description != null && categoryBean.menu!![i].long_description !== "") {
                tvSubTitle.text = categoryBean.menu!![i].long_description

            }
            if (categoryBean.menu!![i].menu_name != null && categoryBean.menu!![i].menu_name !== "") {
                tvItemName.text = categoryBean.menu!![i].menu_name
            }
            if (categoryBean.menu!![i].minimum_preparation_time != null && categoryBean.menu!![i].minimum_preparation_time !== "") {
                tvDeliveryTime.text = categoryBean.menu!![i].minimum_preparation_time
            }
            if (categoryBean.menu!![i].tax_price != 0.0) {
                tvPrice.text =
                    mContext.resources.getString(R.string.rupees) + categoryBean.menu!![i].tax_price
            }else {
                tvPrice.text =
                    mContext.resources.getString(R.string.rupees) + categoryBean.menu!![i].reduced_price
            }
            tvAddToCart.setOnClickListener {

                if (userId != "") {
                    var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> = ArrayList<CartRestaurantsMenuModel>()
                    val favouriteModel = CartRestaurantsMenuModel()
                    favouriteModel.cat_id =categoryBean.menu!![i].cat_id
                    favouriteModel.foodmenu_id =categoryBean.menu!![i].foodmenu_id
                    favouriteModel.menu_name =categoryBean.menu!![i].menu_name
                    favouriteModel.qty =1
                    favouriteModel.tax_price =categoryBean.menu!![i].tax_price.toString()
                    if(categoryBean.menu!![i].reduced_price>0){
                        favouriteModel.priceCart =categoryBean.menu!![i].reduced_price.toDouble()
                    }else if(categoryBean.menu!![i].price>0){
                        favouriteModel.priceCart =categoryBean.menu!![i].price.toDouble()
                    }else{
                        favouriteModel.priceCart =0.0
                    }

                    favouriteModel.isSelected=false
                    favouriteModel.position= holder.adapterPosition
                    CartItemArrayModel.add(favouriteModel)
                    //itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)

                    if(categoryBean.menu!![i].product_variation!!.isNotEmpty()){
                        // itemSelectedListener.onMenuItemSelected(favouriteModel,true)
                        itemVariationListener.onVarianceDialogSelected(categoryBean.menu!![i].product_variation!!,categoryBean.cat_id,
                            categoryBean.menu!![i].foodmenu_id,
                            categoryBean.menu!![i].menu_name,
                            categoryBean.menu!![i].tax_price,tvAddToCart,tvRemove)
                    }else{
                        itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)
                        addtocartAPI(categoryBean.menu!![i],tvAddToCart,tvRemove)

                    }
                    /*tvAddToCart.visibility= View.GONE

                    tvRemove.visibility=View.VISIBLE*/
                }else{
                    val ine = Intent(mContext, LoginPage::class.java)
                    ine.putExtra(SCREEN_REDIRECTION,"1")
                    mContext.startActivity(ine)


                }



            }
            tvRemove.setOnClickListener {
                removeCartItem(categoryBean.menu!![i],tvAddToCart,tvRemove)
            }
            tvCustom.setOnClickListener {
                if (userId != "") {
                    var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> = ArrayList<CartRestaurantsMenuModel>()
                    val favouriteModel = CartRestaurantsMenuModel()
                    favouriteModel.cat_id =categoryBean.menu!![i].cat_id
                    favouriteModel.foodmenu_id =categoryBean.menu!![i].foodmenu_id
                    favouriteModel.menu_name =categoryBean.menu!![i].menu_name
                    favouriteModel.qty =1
                    favouriteModel.tax_price =categoryBean.menu!![i].tax_price.toString()
                    if(categoryBean.menu!![i].reduced_price>0){
                        favouriteModel.priceCart =categoryBean.menu!![i].reduced_price.toDouble()
                    }else if(categoryBean.menu!![i].price>0){
                        favouriteModel.priceCart =categoryBean.menu!![i].price.toDouble()
                    }else{
                        favouriteModel.priceCart =0.0
                    }

                    favouriteModel.isSelected=false
                    favouriteModel.position= holder.adapterPosition
                    CartItemArrayModel.add(favouriteModel)
                    //itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)

                    if(categoryBean.menu!![i].product_variation!!.isNotEmpty()){
                        // itemSelectedListener.onMenuItemSelected(favouriteModel,true)
                        itemVariationListener.onVarianceDialogSelected(categoryBean.menu!![i].product_variation!!,categoryBean.cat_id,
                            categoryBean.menu!![i].foodmenu_id,
                            categoryBean.menu!![i].menu_name,
                            categoryBean.menu!![i].tax_price,tvAddToCart,tvRemove)

                    }else{
                        //  itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)
                        addtocartAPI(categoryBean.menu!![i],tvAddToCart,tvRemove)

                    }

                }else{
                    val ine = Intent(mContext, LoginPage::class.java)
                    ine.putExtra(SCREEN_REDIRECTION,"1")
                    mContext.startActivity(ine)


                }
            }

            /*
                 if (categoryBean.isSelected){
                     tvRemove.visibility = View.VISIBLE
                     tvAddToCart.visibility=View.GONE
                 }else{
                     tvRemove.visibility = View.GONE
                     tvAddToCart.visibility=View.VISIBLE
                 }
     */


        }
    }
    interface AddtoCartItemClickListener {

        fun onMenuItemSelected(listBean: CartRestaurantsMenuModel,isProductVariance :Boolean,isRemove :Boolean)

    }

    interface VarianceDialogClickListener {

        fun onVarianceDialogSelected(
            dataBean: List<RestaurantsMenuModel.DataBean.CategoryBean.MenuBean.ProductVariationBean>,
            catId: String?,
            foodmenuId: String?,
            menuName: String?,
            taxPrice: Double,
            tvAddToCart: TextView,
            tvRemove: TextView
        )

    }

    interface CartViewVisible {
        fun onCartItemSelected(isCartView: Boolean)

    }
    override fun getItemCount(): Int {
        return mArrayListString.size
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var mTextView: TextView
        internal var tvSubTitle: TextView
        internal var tvItemName: TextView
        internal var tvDeliveryTime: TextView
        internal var linearLayout: LinearLayout

        init {
            mTextView = itemView.findViewById(R.id.tvItemName)
            tvItemName = itemView.findViewById(R.id.tvItemName)
            tvSubTitle = itemView.findViewById(R.id.tvSubTitle)
            tvDeliveryTime = itemView.findViewById(R.id.tvDeliveryTime)
            linearLayout = itemView.findViewById(R.id.llMainChild)


        }
    }

    private fun addtocartAPI(
        listBean: RestaurantsMenuModel.DataBean.CategoryBean.MenuBean,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {

        CustomProgressbar.showProgressBar(mContext, false)

        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurantId
        map["food_menu_id"] = listBean.foodmenu_id.toString()
        map["food_menu_name"] = listBean.menu_name.toString()
        map["user_id"] = userId

        RetrofitClientSingleton
            .getInstance()
            .addtoCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("RestaurantDetail", t.message.toString())
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        // val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //top
                                tvAddToCart.visibility= View.GONE
                                tvRemove.visibility=View.VISIBLE
                                Toast(mContext, "" + response.body()!!.message)
                            }
                            FAILURE -> {
                                Toast(mContext, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })

    }
    private fun removeCartItem(
        dataList: RestaurantsMenuModel.DataBean.CategoryBean.MenuBean,  tvAddToCart: TextView,
        tvRemove: TextView
    ) {

        CustomProgressbar.showProgressBar(mContext, false)

        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurantId
        map["food_menu_id"] = dataList.foodmenu_id.toString()
        map["food_menu_name"] = dataList.menu_name.toString()
        map["user_id"] = userId

        CustomProgressbar.showProgressBar(mContext, false)
        RetrofitClientSingleton
            .getInstance()
            .removeItemfromCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                tvAddToCart.visibility=View.VISIBLE
                                tvRemove.visibility=View.GONE
                                Toast(mContext, "" + response.body()!!.message)

                            }
                            FAILURE -> {
                                Toast(mContext, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })

    }

    /* private fun updateTotal() {
         // var total: Int = 0
         var total: Double = 0.00

         //var totalprice: Int
         var totalprice: Double
         for (i in 0 until cartList!!.size) {

             totalprice = cartList[i].pricetotal
             total = total + totalprice
         }


         finalTotalValue = total
         mView.tvSubTotal.setText(getString(R.string.rupees) + " " + total)
         mView.tvCarttotal.setText(getString(R.string.rupees) + " " + total)


     }*/

}
    /*private val mContext: Context,
    private val mArrayList: ArrayList<RestaurantsMenuModel.DataBean.CategoryBean>)
    : RecyclerView.Adapter<DemoPaginationAdappter.ProductViewHolder>() {

    private val LIST_ITEM = 0
    private var isSwitchView = true


    override fun getItemCount() = mArrayList.size

    override fun onBindViewHolder(holder: ProductViewHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProductViewHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.restaurant_detail_item_row_child, null)

        return ProductViewHolder(view)
    }

    fun addAll(arrayList: List<RestaurantsMenuModel.DataBean.CategoryBean>) {
        mArrayList.addAll(arrayList)
        notifyDataSetChanged()
    }

    inner class ProductViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(productData: RestaurantsMenuModel.DataBean.CategoryBean) {

            itemView.tvItemName.text = productData.cat_name


        }
    }


}*/