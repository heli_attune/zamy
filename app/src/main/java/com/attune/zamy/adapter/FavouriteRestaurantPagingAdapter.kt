import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.interaface.onItemOrderListClick
import com.attune.zamy.model.CommonModel
import com.attune.zamy.model.FavouriteReastaurantModel
import com.attune.zamy.model.MyOrderListModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.favourite_restaurant_row_item.view.*
import retrofit2.Call
import retrofit2.Response

class FavouriteRestaurantPagingAdapter(
    private val mContext: Context,
    private val myFavouriteListModels: MutableList<FavouriteReastaurantModel.DataBean.ListBean>,
    internal var itemSelectedListener: onItemOrderListClick
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mInflater: LayoutInflater
    private var isLoaded = false


    init {
        this.mInflater = LayoutInflater.from(mContext)

    }

    override fun getItemViewType(position: Int): Int {
        return if (position == myFavouriteListModels.size - 1 && isLoaded) LOADING else ITEM
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM -> {
                val view =
                    LayoutInflater.from(parent.context)
                        .inflate(R.layout.favourite_restaurant_row_item, parent, false)
                return ViewHolder(view)
            }
            LOADING -> return LoadingHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_loading,
                    parent,
                    false
                )
            )
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    // binds the data to the textview, imageview in each cell
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        when (viewHolder.itemViewType) {
            ITEM -> {
                val holder = viewHolder as ViewHolder
                val mData = myFavouriteListModels[position]
                holder.tvAddress.text = mData.address
                holder.tvRestName.text = mData.res_name
                holder.tvTypesOfFood.text = mData.service_type

                val requestOptions = RequestOptions()
                requestOptions.placeholder(R.drawable.ic_place_holder)
                requestOptions.error(R.drawable.ic_place_holder)
                Glide.with(mContext)
                    .load(mData.logo)
                    .apply(requestOptions)
                    .into(holder.imgTopRestaurants!!)

                holder.itemView.imgDelete.setOnClickListener {
                    addRemoveFavouriteAPI(
                        myFavouriteListModels[position].id!!,
                        holder.adapterPosition
                    )
                }


            }
            LOADING -> {
            }
        }
    }

    fun addRemoveFavouriteAPI(id: String, position: Int) {

        val map = HashMap<String, String>()
        map["restaurant_id"] = id
        map["user_id"] = SharedPref.getUserId(mContext)

        CustomProgressbar.showProgressBar(mContext, false)
        RetrofitClientSingleton
            .getInstance()
            .addRemoveFavouriteRestaurant(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                removeAt(position)
                                Toast(mContext, "Remove Successfully")

                            }
                            FAILURE -> {
                                Toast(mContext, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }

    private fun removeAt(position: Int) {
        myFavouriteListModels.removeAt(position)
        notifyItemRemoved(position)
    }


    override fun getItemCount(): Int {

        return myFavouriteListModels.size
    }

    internal inner class LoadingHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    inner class ViewHolder
        (itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgTopRestaurants: ImageView? = null
        internal var tvAddress: TextView
        internal var tvRestName: TextView
        internal var tvTypesOfFood: TextView
        internal var tvTime: TextView
        internal var imgDelete: ImageView
        internal var conFav: ConstraintLayout

        init {
            imgTopRestaurants = itemView.findViewById<ImageView>(R.id.imgTopRestaurants)
            imgDelete = itemView.findViewById<ImageView>(R.id.imgDelete)
            tvAddress = itemView.findViewById<AppCompatTextView>(R.id.tvAddress)
            tvRestName = itemView.findViewById<AppCompatTextView>(R.id.tvRestName)
            tvTypesOfFood = itemView.findViewById<AppCompatTextView>(R.id.tvTypesOfFood)
            tvTime = itemView.findViewById<AppCompatTextView>(R.id.tvTime)
            conFav = itemView.findViewById<ConstraintLayout>(R.id.conFav)


        }
    }

    interface OnItemSelectedListener {
        fun onItemSelected(
            item: MyOrderListModel.DataBean.ListBean,
            position: Int,
            action: String,
            adapterPostion: Int
        )
    }

    fun addItem(dataList: FavouriteReastaurantModel.DataBean.ListBean) {
        myFavouriteListModels.add(dataList)
        notifyItemInserted(myFavouriteListModels.size - 1)
    }

    fun addAllItem(dataBeans: List<FavouriteReastaurantModel.DataBean.ListBean>) {
        for (dataBeanList in dataBeans) {
            addItem(dataBeanList)
        }
    }

    fun removeAllItem() {
        myFavouriteListModels.clear()
        notifyDataSetChanged()
    }

    fun addLoading() {
        isLoaded = true
        addItem(FavouriteReastaurantModel.DataBean.ListBean())
    }

    fun removeLoading() {
        isLoaded = false
        val position = myFavouriteListModels.size - 1
        val applicationHistory = getItem(position)
        if (applicationHistory != null) {
            myFavouriteListModels.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun getItem(position: Int): FavouriteReastaurantModel.DataBean.ListBean {
        return myFavouriteListModels[position]
    }

    companion object {
        private val ITEM = 0
        val LOADING = 1
    }

}
