package com.attune.zamy.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.interaface.onItemOrderListClick
import com.attune.zamy.model.MyOrderListModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions

class MyOrderPaginationAdapter(
    private val mContext: Context,
    private val myOrderListModels: MutableList<MyOrderListModel.DataBean.ListBean>,
    internal var itemSelectedListener: onItemOrderListClick
) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {
    private val mInflater: LayoutInflater
    private var isLoaded = false


    init {
        this.mInflater = LayoutInflater.from(mContext)

    }

    override fun getItemViewType(position: Int): Int {
        return if (position == myOrderListModels.size - 1 && isLoaded) LOADING else ITEM
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            ITEM -> {
                val view = mInflater.inflate(R.layout.row_my_order_items, parent, false)
                return ViewHolder(view)
            }
            LOADING -> return LoadingHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.item_loading,
                    parent,
                    false
                )
            )
            else -> throw IllegalArgumentException("Invalid view type")
        }
    }

    // binds the data to the textview, imageview in each cell
    override fun onBindViewHolder(viewHolder: RecyclerView.ViewHolder, position: Int) {

        when (viewHolder.itemViewType) {
            ITEM -> {
                val holder = viewHolder as ViewHolder
                val mData = myOrderListModels[position]
                holder.tvOrderTitle.text = mData.res_name
                holder.tvOrderTitle.text = mData.res_name
                holder.tvOrderId.text = "Order Id:" + mData.id
                holder.tvOrderDate.text = "Date :" + mData.created_date

                holder.tvOrderStatus.text = mData.order_status


                if (mData.order_status.equals("Food Prepared", ignoreCase = true)) {
                    holder.tvOrderStatus.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.green
                        )
                    )
                }
                if (mData.order_status.equals("Accepted", ignoreCase = true)) {
                    holder.tvOrderStatus.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.green
                        )
                    )
                }
                if (mData.order_status.equals(
                        "Waiting for Acceptance",
                        ignoreCase = true
                    )
                ) {
                    holder.tvOrderStatus.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.green
                        )
                    )
                }
                if (mData.order_status.equals("failed", ignoreCase = true)) {
                    holder.tvOrderStatus.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.green
                        )
                    )
                }
                if (mData.order_status.equals("refunded", ignoreCase = true)) {
                    holder.tvOrderStatus.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.green
                        )
                    )
                }
                if (mData.order_status.equals("cancelled", ignoreCase = true)) {
                    holder.tvOrderStatus.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.green
                        )
                    )
                }
                if (mData.order_status.equals("refund_cancelled", ignoreCase = true)) {
                    holder.tvOrderStatus.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.green
                        )
                    )
                }
                if (mData.order_status.equals("refund_approved", ignoreCase = true)) {
                    holder.tvOrderStatus.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.green
                        )
                    )
                }
                if (mData.order_status.equals("completed", ignoreCase = true)) {
                    holder.tvOrderStatus.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.green
                        )
                    )
                }
                if (mData.order_status.equals("refund_requested", ignoreCase = true)) {
                    holder.tvOrderStatus.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.green
                        )
                    )
                }
                if (mData.order_status.equals("cancel_request", ignoreCase = true)) {
                    holder.tvOrderStatus.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.green
                        )
                    )
                }

                if (mData.order_status.equals("processing", ignoreCase = true)) {
                    holder.tvOrderStatus.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.orange
                        )
                    )
                }

                if (mData.order_status.equals("pending", ignoreCase = true)) {
                    holder.tvOrderStatus.setTextColor(
                        ContextCompat.getColor(
                            mContext,
                            R.color.yellow
                        )
                    )
                }


                holder.tvOrderPrice.text = "₹ " + mData.order_total

                val requestOptions = RequestOptions()
                requestOptions.placeholder(R.drawable.ic_place_holder)
                requestOptions.error(R.drawable.ic_place_holder)
                Glide.with(mContext)
                    .load(mData.logo)
                    .apply(requestOptions)
                    .into(holder.imgOrderImage)

                holder.rlOrder.setOnClickListener {
                    itemSelectedListener.myOrderClick(mData.id)
                }
            }
            LOADING -> {
            }
        }
    }


    override fun getItemCount(): Int {

        return myOrderListModels.size
    }

    internal inner class LoadingHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
    inner class ViewHolder
        (itemView: View) : RecyclerView.ViewHolder(itemView) {
        internal var tvOrderTitle: TextView
        internal var imgOrderImage: ImageView
        internal var tvOrderId: TextView
        internal var tvOrderDate: TextView
        internal var tvOrderStatus: TextView
        internal var tvOrderPrice: TextView
        internal var rlOrder: RelativeLayout

        init {
            imgOrderImage = itemView.findViewById<ImageView>(R.id.imgOrderImage)
            tvOrderTitle = itemView.findViewById<AppCompatTextView>(R.id.tvOrderTitle)
            tvOrderId = itemView.findViewById<AppCompatTextView>(R.id.tvOrderId)
            tvOrderDate = itemView.findViewById<AppCompatTextView>(R.id.tvOrderDate)
            tvOrderStatus = itemView.findViewById<AppCompatTextView>(R.id.tvOrderStatus)
            tvOrderPrice = itemView.findViewById<AppCompatTextView>(R.id.tvOrderPrice)
            rlOrder = itemView.findViewById(R.id.rlOrder)

        }
    }

    interface OnItemSelectedListener {
        fun onItemSelected(
            item: MyOrderListModel.DataBean.ListBean,
            position: Int,
            action: String,
            adapterPostion: Int
        )
    }

    fun addItem(dataList: MyOrderListModel.DataBean.ListBean) {
        myOrderListModels.add(dataList)
        notifyItemInserted(myOrderListModels.size - 1)
    }

    fun addAllItem(dataBeans: List<MyOrderListModel.DataBean.ListBean>) {
        for (dataBeanList in dataBeans) {
            addItem(dataBeanList)
        }
    }

    fun removeAllItem() {
        myOrderListModels.clear()
        notifyDataSetChanged()
    }

    fun addLoading() {
        isLoaded = true
        addItem(MyOrderListModel.DataBean.ListBean())
    }

    fun removeLoading() {
        isLoaded = false
        val position = myOrderListModels.size - 1
        val applicationHistory = getItem(position)
        if (applicationHistory != null) {
            myOrderListModels.removeAt(position)
            notifyItemRemoved(position)
        }
    }

    fun getItem(position: Int): MyOrderListModel.DataBean.ListBean? {
        return myOrderListModels[position]
    }

    companion object {
        private val ITEM = 0
        val LOADING = 1
    }

}
