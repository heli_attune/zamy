package com.attune.zamy.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.attune.zamy.R;
import com.attune.zamy.model.TopRestaurantModel;

import java.util.List;

public class PaginationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
        private List<TopRestaurantModel.DataBean.TopRestaurantListBean> applicationHistoryModels;
        private Context context;
        private LayoutInflater mInflater;
        OnItemSelectedListener itemSelectedListener;
        private static final int ITEM = 0;
        private static final int LOADING = 1;
        private boolean isLoaded = false;


        public PaginationAdapter(Context context, List<TopRestaurantModel.DataBean.TopRestaurantListBean> applicationHistoryModels,
                                         OnItemSelectedListener itemSelectedListener) {
            this.context = context;
            this.mInflater = LayoutInflater.from(context);
            this.applicationHistoryModels = applicationHistoryModels;
            this.itemSelectedListener = itemSelectedListener;

        }
        @Override
        public int getItemViewType(int position) {
            return (position == applicationHistoryModels.size() - 1 && isLoaded) ? LOADING : ITEM;
        }


        // inflates the cell layout from xml when needed
        @NonNull
        @Override
        public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
            switch (viewType) {
                case ITEM:
                    View view = mInflater.inflate(R.layout.top_restaurant_item_row, parent, false);
                    return new ViewHolder(view);
                case LOADING:
                    return new LoadingHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.item_loading, parent, false));
            }
            return null;
        }
        // binds the data to the textview, imageview in each cell
        @Override
        public void onBindViewHolder(@NonNull final RecyclerView.ViewHolder viewHolder, final int position) {

            switch (viewHolder.getItemViewType()) {
                case ITEM: {
                    final ViewHolder holder = (ViewHolder) viewHolder;
                    final TopRestaurantModel.DataBean.TopRestaurantListBean mData = applicationHistoryModels.get(position);
                    holder.tvRestName.setText(mData.getRes_name());
                    break;
                }
                case LOADING: {
                    break;
                }
            }
        }



        @Override
        public int getItemCount() {

            return applicationHistoryModels.size();
        }
        class LoadingHolder extends RecyclerView.ViewHolder {
            LoadingHolder(View itemView) {
                super(itemView);
            }
        }
        public class ViewHolder extends RecyclerView.ViewHolder {
            TextView tvRestName;
         /*   TextView txtName;
            public TextView txtServiceName;
            TextView txtDesc;
            LinearLayout parent;
            ImageView imgapp;
            TextView txtTime;*/

            public ViewHolder(View itemView) {
                super(itemView);
                tvRestName = itemView.findViewById(R.id.tvRestName);
                /*txtName = itemView.findViewById(R.id.txtName);
                txtDesc = itemView.findViewById(R.id.txtDesc);
                parent = itemView.findViewById(R.id.parent);
                txtStatus=itemView.findViewById(R.id.txtStatus);
                txtTime=itemView.findViewById(R.id.txtTime);
*/
            }
        }

        public interface OnItemSelectedListener {
            void onItemSelected(TopRestaurantModel.DataBean.TopRestaurantListBean item, int position, String action,int adapterPostion);
        }

        public void addItem(TopRestaurantModel.DataBean.TopRestaurantListBean applicationHistory) {
            applicationHistoryModels.add(applicationHistory);
            notifyItemInserted(applicationHistoryModels.size() - 1);
        }

        public void addAllItem(List<TopRestaurantModel.DataBean.TopRestaurantListBean> applicationHistories) {
            for (TopRestaurantModel.DataBean.TopRestaurantListBean applicationHistory : applicationHistories) {
                addItem(applicationHistory);
            }
        }

        public void removeAllItem() {
            applicationHistoryModels.clear();
            notifyDataSetChanged();
        }

        public void addLoading() {
            isLoaded = true;
            addItem(new TopRestaurantModel.DataBean.TopRestaurantListBean());
        }

        public void removeLoading() {
            isLoaded = false;
            int position = applicationHistoryModels.size() - 1;
            TopRestaurantModel.DataBean.TopRestaurantListBean applicationHistory = getItem(position);
            if (applicationHistory != null) {
                applicationHistoryModels.remove(position);
                notifyItemRemoved(position);
            }
        }

        public TopRestaurantModel.DataBean.TopRestaurantListBean getItem(int position) {
            return applicationHistoryModels.get(position);
        }

    }
