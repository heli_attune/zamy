package com.attune.zamy.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.model.GalerryResponse
import com.bumptech.glide.Glide

class RestaurantsGalleryAdapter(
    val mContext: Context,
    val galleryResponse: GalerryResponse.DataBean?
) : RecyclerView.Adapter<RestaurantsGalleryAdapter.GalleryHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): GalleryHolder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.row_gallery_item, parent, false)
        return GalleryHolder(view)
    }

    override fun onBindViewHolder(holder: GalleryHolder, position: Int) {
                galleryResponse.let { holder.bind(galleryResponse!!.gallery) }
    }

    override fun getItemCount(): Int {
        return galleryResponse!!.gallery!!.size
    }

    inner class GalleryHolder(itemView: View) : RecyclerView.ViewHolder(itemView){
        val img=itemView.findViewById<ImageView>(R.id.restoGallery)
        fun bind(gallery: List<String>?) {
            if(gallery!=null){
                Glide.with(mContext)
                    .load(gallery[position])
                    .error(R.drawable.ic_place_holder)
                    .into(img)

            }else{
                Glide.with(mContext)
                    .load(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(img)
            }
        }
    }
}
