package com.attune.zamy.adapter

import android.Manifest
import android.app.Activity
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.ContextCompat.startActivity
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.activity.MainActivity
import com.attune.zamy.activity.TicketsDetailActivity
import com.attune.zamy.interaface.callClick
import com.attune.zamy.interaface.onItemAddressSelected
import com.attune.zamy.model.CustomerSupportList
import com.attune.zamy.utils.convertDateTime
import com.attune.zamy.utils.keyOrderId
import kotlinx.android.synthetic.main.row_item_ticket_list.view.*


class TicketsListingAdapter(private val mContext: Context,var callClick: callClick) :
    RecyclerView.Adapter<TicketsListingAdapter.TicketViewHolder>() {

    private val mArrayList: ArrayList<CustomerSupportList.DataBean> = ArrayList()

    fun setList(arrayList: List<CustomerSupportList.DataBean>) {
        this.mArrayList.clear()
        this.mArrayList.addAll(arrayList)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): TicketViewHolder {
        val view =
            LayoutInflater.from(mContext).inflate(R.layout.row_item_ticket_list, parent, false)

        return TicketViewHolder(view)
    }

    override fun getItemCount(): Int {
        return mArrayList.size
    }

    override fun onBindViewHolder(holder: TicketViewHolder, position: Int) {
        holder.bind(mArrayList[position])
    }

    inner class TicketViewHolder(val view: View) : RecyclerView.ViewHolder(view) {
        fun bind(ticketList: CustomerSupportList.DataBean) {


            itemView.txt_ticket_number.text = ticketList.ticketID
            itemView.txt_status.text = ticketList.ticketstatus
            itemView.txtOrderId.text = "# " + ticketList.order_id
            itemView.txtProductName.text = ticketList.subject

//            itemView.txtSubOrderID.text = ticketList.sub_order_id

            if (ticketList.ticketDateTime != null) {
                val convertedDateTime = convertDateTime(
                    "yyyy-MM-dd HH:mm:ss",
                    "MMM dd HH:mm",
                    ticketList.ticketDateTime!!
                )
                itemView.txtDate.text = convertedDateTime

            }

            if (ticketList.ticketstatus != null) {
                when (ticketList.ticketstatus!!) {
                    "Open" -> {
                        itemView.txt_status.setTextColor(
                            ContextCompat.getColor(
                                mContext,
                                R.color.colorPrimary
                            )
                        )
                    }
                    "Close" -> {
                        itemView.txt_status.setTextColor(
                            ContextCompat.getColor(
                                mContext,
                                R.color.yellow
                            )
                        )
                    }

                }
            }
            itemView.imgChat.setOnClickListener {
                val intent = Intent(mContext, TicketsDetailActivity::class.java)
                intent.putExtra(keyOrderId, "" + ticketList.order_id!!)
                intent.putExtra("chat_id", "" + ticketList.ticketChatID!!)
                intent.putExtra("ticket_status", "" + ticketList.ticketstatus!!)
                mContext.startActivity(intent)
            }
            itemView.imgCall.setOnClickListener {
                callClick.callClick()

            }


        }


    }
}
