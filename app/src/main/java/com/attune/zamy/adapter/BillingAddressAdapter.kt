package com.attune.zamy.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.RadioButton
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.activity.AddAddressActivity
import com.attune.zamy.interaface.onItemAddressSelected
import com.attune.zamy.model.GetAddressModel
import com.attune.zamy.utils.SharedPref


class BillingAddressAdapter(
    var mContext: Context,
    val addressList: MutableList<GetAddressModel.DataBean.AddressBookBean>?,
    var onItemAddressSelected: onItemAddressSelected
) :
    RecyclerView.Adapter<BillingAddressAdapter.ViewHolder>() {
    lateinit var userId: String
    private var row_index: Int = -1
    var selected: RadioButton? = null


    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): BillingAddressAdapter.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.row_billing_adddress_item, viewGroup, false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        return addressList!!.size
    }

    fun removeAt(position: Int) {
        addressList!!.removeAt(position)
        notifyItemRemoved(position)
        //  notifyItemRangeChanged(position, cartList.size)
    }

    @SuppressLint("ResourceAsColor", "SetTextI18n")
    override fun onBindViewHolder(holder: BillingAddressAdapter.ViewHolder, position: Int) {
        val addressData = addressList?.get(position)
        userId = SharedPref.getUserId(mContext)
        if (addressData!!.name != null && addressData.name != "") {
            holder.tvFullName.text = addressData.name
        }
        if (addressData.address_1 != null && addressData.address_1 != "") {
            holder.tvAddress1.text =
                addressData.address_1 + "," + addressData.address_2 + "," + addressData.shipping_area + " " + addressData.city + " " +
                        addressData.postcode + "," + addressData.state + " " +
                        addressData.country
        }
        /* if (addressData.address_2 != null && addressData.address_2 != "") {
             holder.tvAddress2.text = addressData.address_2
         }
         if (addressData.city != null && addressData.city != "") {
             holder.tvAddress3.text = addressData.shipping_area + " " + addressData.city + " " +
                     addressData.postcode
         }
         if (addressData.state != null && addressData.state != "") {
             holder.tvAddress4.text = addressData.state + " " +
                     addressData.country
         }*/
        holder.rlAddress.setOnClickListener {
            if (selected != null) {
                selected!!.isChecked = false
            }
            holder.radioAddress.isChecked = true
            selected = holder.radioAddress
            // notifyDataSetChanged()
            onItemAddressSelected.onAddressSelected(addressData, true, false)
        }
        //on selected color change
        holder.radioAddress.setOnClickListener {
            if (selected != null) {
                selected!!.isChecked = false
            }
            holder.radioAddress.isChecked = true
            selected = holder.radioAddress
            // notifyDataSetChanged()
            onItemAddressSelected.onAddressSelected(addressData, true, false)
        }

        holder.tvEditAddress.setOnClickListener {
            /*  val intent=Intent(MainApp.context,AddAddressActivity::class.java)
              intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK
              intent.putExtra("Edit",1)
              ContextCompat.startActivity(MainApp.context!!,intent,null)*/
            val intent = Intent(mContext, AddAddressActivity::class.java)
            intent.putExtra("edit", 1)
            intent.putExtra("AddressModel", addressData)
            mContext.startActivity(intent)
        }

    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var tvFullName: TextView = itemView.findViewById(R.id.tvFullName)
        var tvAddress1: TextView = itemView.findViewById(R.id.tvAddress1)
        var radioAddress: RadioButton = itemView.findViewById(R.id.radioAddress)
        var rlAddress: RelativeLayout = itemView.findViewById(R.id.rlAddress)
        var tvEditAddress: AppCompatTextView = itemView.findViewById(R.id.tvEdit)


    }

}
