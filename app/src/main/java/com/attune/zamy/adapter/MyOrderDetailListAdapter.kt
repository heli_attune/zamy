package com.attune.zamy.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.interaface.onReturnItemClick
import com.attune.zamy.model.MyOrderDetailsModel
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.my_order_details_row_items.view.*
import kotlinx.android.synthetic.main.row_my_order_items.view.tvOrderTitle

class MyOrderDetailListAdapter(
    val context: Context,
    val apiResponse: MyOrderDetailsModel.DataBean.OrderBean,
    val onReturnItemClick: onReturnItemClick
) :
    RecyclerView.Adapter<MyOrderDetailListAdapter.DetailsListHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): DetailsListHolder {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.my_order_details_row_items, parent, false)
        return DetailsListHolder(view)
    }

    override fun onBindViewHolder(holder: DetailsListHolder, position: Int) {
        apiResponse.order_item!![position].let { holder.bind(apiResponse) }
    }

    override fun getItemCount(): Int {
        return apiResponse.order_item!!.size
    }

    inner class DetailsListHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        @SuppressLint("CheckResult", "SetTextI18n")
        fun bind(apiResponse: MyOrderDetailsModel.DataBean.OrderBean) {

            itemView.tvOrderTitle.text = apiResponse.order_item!![position].menu_name
            if (apiResponse.order_item!![position].qty != null) {
                itemView.tvOrderQty.text = "QTY:-" + apiResponse.order_item!![position].qty
            }
            if (apiResponse.order_item!![position].variation_name != null) {
                itemView.tvOrderVariationName.text =
                    apiResponse.order_item!![position].variation_name
            }


            if (apiResponse.order_item!![position].menu_logo != null && apiResponse.order_item!![position].menu_logo != "") {
                val requestOptions = RequestOptions()
                requestOptions.placeholder(R.drawable.ic_place_holder)
                requestOptions.error(R.drawable.ic_place_holder)
                Glide.with(context)
                    .load(apiResponse.order_item!![position].menu_logo)
                    .into(itemView.imgOrderDetailsImage)
            }
            if(apiResponse.order_item!![position].subtotal!=null && apiResponse.order_item!![position].subtotal!="") {
                itemView.tvPrice.text = "₹ " + apiResponse.order_item!![position].subtotal
            }

            itemView.imgReturn.setOnClickListener {
                onReturnItemClick.onItemReturn(apiResponse.id,
                    apiResponse.order_item!![position].foodmenu_id,
                    apiResponse.order_item!![position].qty,
                    apiResponse.order_item!![position]
                )
            }


        }
    }
}
