package com.attune.zamy.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.fragment.GroceryFragment
import com.attune.zamy.model.HomeScreenModel
import com.bumptech.glide.Glide

class HomeGroceryAdapter(
    var activity: FragmentActivity,
    var data: HomeScreenModel.DataBean?,
    var context: GroceryFragment
) : RecyclerView.Adapter<HomeGroceryAdapter.ViewHolder>() {


    private var row_index: Int = -1

    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): HomeGroceryAdapter.ViewHolder {
        val view =
            LayoutInflater.from(viewGroup.context).inflate(R.layout.home_category_item_row, viewGroup, false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        return data!!.category_list!!.size
    }

    override fun onBindViewHolder(holder: HomeGroceryAdapter.ViewHolder, position: Int) {
        val categoryData = data!!.category_list!![position]
        if(categoryData.cat_name!=null && categoryData.cat_name!="") {
            holder.tvCatName.text = categoryData.cat_name
        }
        if(categoryData.cat_logo!=null){
            holder.imgCategory.alpha=0.5f
            Glide.with(activity)
                .load(categoryData.cat_logo)
                .error(R.drawable.ic_place_holder)
                .into(holder.imgCategory)

        }else{
            Glide.with(activity)
                .load(R.drawable.ic_place_holder)
                .error(R.drawable.ic_place_holder)
                .into(holder.imgCategory)
        }




    }

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgCategory: ImageView = itemView.findViewById<ImageView>(R.id.imgCategory)
        var tvCatName: TextView = itemView.findViewById<TextView>(R.id.tvCatName)

    }
}
