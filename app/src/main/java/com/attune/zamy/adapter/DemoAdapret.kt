package com.attune.zamy.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.adapter.DemoAdapret.DemoHolder

class DemoAdapret : RecyclerView.Adapter<DemoHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): DemoHolder {
        return DemoHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.top_restaurant_item_row, parent, false)
        )
    }

    override fun onBindViewHolder(holder: DemoHolder, position: Int) {}
    override fun getItemCount(): Int {
        return 10
    }

    inner class DemoHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView){

    }
}