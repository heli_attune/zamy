package com.attune.zamy.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RadioButton
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.interaface.onItemVarianceSelected
import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.HomeScreenModel

class ProductHomeVarianceAdapter(
    var mContext: Context,
    val tableList: List<HomeScreenModel.DataBean.PopularThisMonthBean.ProductVariationBean>?,
    var onItemVarianceSelected: onItemVarianceSelected,
    val catId: String?,
    val foodmenuId: String?,
    val menuName: String?,
    val taxPrice: String
) :
    RecyclerView.Adapter<ProductHomeVarianceAdapter.ViewHolder>() {
    private var row_index: Int = -1
    var selected: RadioButton? = null


    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): ProductHomeVarianceAdapter.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.item_row_variance, viewGroup, false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        Log.e("variancesize", tableList!!.size.toString())
        return tableList.size
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var radioVariance: RadioButton = itemView.findViewById(R.id.radioVariance)
        var txtName: TextView = itemView.findViewById(R.id.txtName)
        var txtPrice: TextView = itemView.findViewById(R.id.txtPrice)
        var lnVariance: LinearLayout = itemView.findViewById(R.id.lnVariance)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val tableData = tableList?.get(position)
        holder.txtName.text = tableData!!.variation_name
        if (tableData.variation_sale_price != null && tableData.variation_sale_price != "" && tableData.variation_sale_price != "0") {
            holder.txtPrice.text =
                mContext.resources.getString(R.string.rupees) + tableData.variation_sale_price
        } else if (tableData.variation_price != null && tableData.variation_price != "") {
            holder.txtPrice.text =
                mContext.resources.getString(R.string.rupees) + tableData.variation_price
        }

        holder.lnVariance.setOnClickListener {

            if (selected != null) {
                selected!!.isChecked = false
            }
            holder.radioVariance.isChecked = true
            selected = holder.radioVariance
            var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> =
                ArrayList<CartRestaurantsMenuModel>()
            val favoriteList = CartRestaurantsMenuModel()
            favoriteList.cat_id = catId
            favoriteList.menu_name = menuName
            favoriteList.variation_name = tableData.variation_name
            favoriteList.foodmenu_id = foodmenuId
            favoriteList.variation_id = tableData.variation_id
            favoriteList.variation_name = tableData.variation_name

            if (tableData.variation_sale_price != null && tableData.variation_sale_price != "" && tableData.variation_sale_price != "0") {
                favoriteList.price = tableData.variation_sale_price!!
                favoriteList.priceCart =
                    tableData.variation_sale_price!!.toDouble()
            } else if (tableData.variation_price != null && tableData.variation_price != "") {
                favoriteList.price = tableData.variation_price!!
                favoriteList.priceCart =
                    tableData.variation_price!!.toDouble()
            }
            CartItemArrayModel.add(favoriteList)
            onItemVarianceSelected.onVarianceItemSelected(favoriteList)
        }

        holder.radioVariance.setOnClickListener {

            if (selected != null) {
                selected!!.isChecked = false
            }
            holder.radioVariance.isChecked = true
            selected = holder.radioVariance
            var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> =
                ArrayList<CartRestaurantsMenuModel>()
            val favoriteList = CartRestaurantsMenuModel()
            favoriteList.cat_id = catId
            favoriteList.menu_name = menuName
            favoriteList.variation_name = tableData.variation_name
            favoriteList.foodmenu_id = foodmenuId
            favoriteList.variation_id = tableData.variation_id
            favoriteList.variation_name = tableData.variation_name

            CartItemArrayModel.add(favoriteList)
            onItemVarianceSelected.onVarianceItemSelected(favoriteList)
        }
    }
}