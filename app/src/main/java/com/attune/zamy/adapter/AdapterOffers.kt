package com.attune.zamy.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.adapter.AdapterOffers.OfferHolder
import com.attune.zamy.fragment.HomeFragment
import com.attune.zamy.model.HomeScreenModel
import com.bumptech.glide.Glide

class AdapterOffers(
    val activity: FragmentActivity,
    val offerlist: List<HomeScreenModel.DataBean.CouponListBean>?,
    val homeFragment: HomeFragment
) :
    RecyclerView.Adapter<OfferHolder>() {
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): OfferHolder {
        val mView =
            LayoutInflater.from(parent.context).inflate(R.layout.row_offer_items, parent, false)
        return OfferHolder(mView)
    }

    override fun onBindViewHolder(holder: OfferHolder, position: Int) {
        holder.bind(offerlist?.get(position)!!)
    }

    override fun getItemCount(): Int {
        return offerlist!!.size
    }

    inner class OfferHolder(itemView: View) :
        RecyclerView.ViewHolder(itemView) {
        lateinit var offerImageView: ImageView
        fun bind(couponListBean: HomeScreenModel.DataBean.CouponListBean) {
            offerImageView = itemView.findViewById(R.id.imgOffers)
            Glide.with(activity)
                .load(couponListBean.coupon_image)
                .into(offerImageView)
        }
    }
}