package com.attune.zamy.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup

import android.widget.ImageView
import com.attune.zamy.R
import com.attune.zamy.interaface.onItemClickListnerTopResturants
import com.attune.zamy.model.HomeScreenModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.home_top_restaurants_item_row.view.*


class HomeTopRestaurantsAdapter(
    private val mContext: Context,
    var respon: HomeScreenModel.DataBean?,
    var mAdapterPosition: onItemClickListnerTopResturants
) : androidx.recyclerview.widget.RecyclerView.Adapter<HomeTopRestaurantsAdapter.CategoriesViewHolder>() {




    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CategoriesViewHolder {
        val view = LayoutInflater.from(mContext).inflate(R.layout.home_top_restaurants_item_row, parent, false)
        return CategoriesViewHolder(view)
    }

    override fun getItemCount(): Int {
        return respon!!.top_restaurant_list!!.size
    }

    override fun onBindViewHolder(holder: CategoriesViewHolder, position: Int) {
        respon!!.top_restaurant_list!![position].let { holder.bind(respon!!.top_restaurant_list!![position].images) }
    }

    inner class CategoriesViewHolder(val view: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(view) {
        fun bind(homeScreenModel: String?) {
            var img: ImageView = view.findViewById(R.id.imgSingle)

            if(respon!!.top_restaurant_list!![position].logo!=null){
                Glide.with(mContext)
                    .load(respon!!.top_restaurant_list!![position].logo)
                    .error(R.drawable.ic_place_holder)
                    .into(img)

            }else{
                Glide.with(mContext)
                    .load(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(img)
            }
           /* Glide.with(mContext)
                    .load(respon!!.top_restaurant_list!![position].images)
                    .into(img)*/

            itemView.tvTitle.text= respon!!.top_restaurant_list!![position].res_name

            itemView.setOnClickListener {
                mAdapterPosition.adapterItemPosition(respon!!.top_restaurant_list!![position].id,
                    respon!!.top_restaurant_list!![position]
                )
            }
        }
    }


}
