package com.attune.zamy.adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.CommonModel
import com.attune.zamy.model.FavouriteReastaurantModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Response

class FavouritePagingAdapter(
    val context: Context,
    val apiResponse: MutableList<FavouriteReastaurantModel.DataBean.ListBean>?
) : RecyclerView.Adapter<FavouritePagingAdapter.HolderAdapter>() {


    private val VIEW_TYPE_LOADING = 0
    private val VIEW_TYPE_NORMAL = 1
    private val isLoaderVisible = false


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HolderAdapter {
        val view =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.favourite_restaurant_row_item, parent, false)
        return HolderAdapter(view)
    }

    override fun onBindViewHolder(holder: HolderAdapter, position: Int) {
        apiResponse!![position].let { holder.bind(apiResponse) }

        holder.imgDelete.setOnClickListener {
            addRemoveFavouriteAPI(
                apiResponse[position].id!!,
                holder.adapterPosition
            )
        }
    }


    fun removeAll() {
        apiResponse!!.clear()
    }


    fun addAll(arrayList: List<FavouriteReastaurantModel.DataBean.ListBean>) {
        apiResponse!!.addAll(arrayList)
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int {
        return apiResponse!!.size
    }

    fun removeAt(position: Int) {
        apiResponse!!.removeAt(position)
        notifyItemRemoved(position)
        //  notifyItemRangeChanged(position, cartList.size)
    }


    inner class HolderAdapter(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val imgDelete = itemView.findViewById<ImageView>(R.id.imgDelete)
        val tvAddress = itemView.findViewById<AppCompatTextView>(R.id.tvAddress)
        val tvRestName = itemView.findViewById<AppCompatTextView>(R.id.tvRestName)
        val tvTypesOfFood = itemView.findViewById<AppCompatTextView>(R.id.tvTypesOfFood)
        val tvTime = itemView.findViewById<AppCompatTextView>(R.id.tvTime)
        val imgTopRestaurants = itemView.findViewById<ImageView>(R.id.imgTopRestaurants)
        fun bind(apiResponse: MutableList<FavouriteReastaurantModel.DataBean.ListBean>) {
            tvRestName.text = apiResponse[position].res_name
            tvAddress.text = apiResponse[position].address
            tvTypesOfFood.text = apiResponse[position].service_type
            tvTime.text = ""

            if (apiResponse[position].logo != null) {
                Glide.with(context)
                    .load(apiResponse[position].logo)
                    .error(R.drawable.ic_place_holder)
                    .into(imgTopRestaurants)

            } else {
                Glide.with(context)
                    .load(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(imgTopRestaurants)
            }
        }
    }

    fun addRemoveFavouriteAPI(id: String, position: Int) {

        val map = HashMap<String, String>()
        map["restaurant_id"] = id
        map["user_id"] = SharedPref.getUserId(context)

        CustomProgressbar.showProgressBar(context, false)
        RetrofitClientSingleton
            .getInstance()
            .addRemoveFavouriteRestaurant(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                removeAt(position)
                                Toast(context, "Remove Successfully")

                            }
                            FAILURE -> {
                                Toast(context, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }
}
