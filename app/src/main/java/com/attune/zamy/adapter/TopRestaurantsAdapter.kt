package com.attune.zamy.adapter

import android.content.Context
import android.os.Build
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.listener.onTopRestaurantItemClickListener
import com.attune.zamy.model.CommonModel
import com.attune.zamy.model.TopRestaurantModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.AUTH_FAILURE
import com.attune.zamy.utils.FAILURE
import com.attune.zamy.utils.SUCCESS
import com.attune.zamy.utils.Toast
import com.bumptech.glide.Glide
import retrofit2.Call
import retrofit2.Response


class TopRestaurantsAdapter(
    var data: List<TopRestaurantModel.DataBean.TopRestaurantListBean>?,
    var context: Context,
    var itemClick: onTopRestaurantItemClickListener
) : RecyclerView.Adapter<TopRestaurantsAdapter.ViewHolder>() {


    override fun onCreateViewHolder(
        viewGroup: ViewGroup,
        viewType: Int
    ): TopRestaurantsAdapter.ViewHolder {
        val view = LayoutInflater.from(viewGroup.context)
            .inflate(R.layout.top_restaurant_item_row, viewGroup, false)
        return ViewHolder(view)
    }


    override fun getItemCount(): Int {
        return data!!.size
    }

    override fun onBindViewHolder(holder: TopRestaurantsAdapter.ViewHolder, position: Int) {
        val modelData = this.data!![position]
        if (modelData.res_name != null && modelData.res_name != "") {
            holder.tvRestName.text = modelData.res_name
        }
        if (modelData.area != null && modelData.area != "" && modelData.landmark != null && modelData.landmark != "") {
            holder.tvAddress.text = modelData.area + ", " + modelData.landmark
        }
        if (modelData.approx_delivery_time != null && modelData.approx_delivery_time != "") {
            holder.tvTime.visibility = View.VISIBLE
            holder.tvTime.text = modelData.approx_delivery_time
        } else {
            holder.tvTime.visibility = View.GONE
        }
        if (modelData.images != null && modelData.images != "") {
            Glide.with(context)
                .load(modelData.images)
                .into(holder.imgTopRestaurants)
        }
        if (modelData.favourite_flag.equals("yes")) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.imgFavorite.setImageResource(R.drawable.ic_favorite_selected_red)
            }
        } else {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.imgFavorite.setImageResource(R.drawable.ic_favorite_unselected_red)
            }
        }
        holder.imgFavorite.setOnClickListener {
            if (modelData.favourite_flag.equals("yes")) {
                Toast(context, modelData.favourite_flag.toString())
                //addRemoveFavouriteAPI(modelData.id!!, position, holder.imgFavorite, true)
            } else {
                Toast(context, modelData.favourite_flag.toString())
                //addRemoveFavouriteAPI(modelData.id!!, position, holder.imgFavorite, false)
            }
            /*if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                holder.imgFavorite.setImageResource(R.drawable.ic_favorite_selected_red)
            }*/
        }

        holder.tvOnlineOrder.setOnClickListener {

        }
        holder.rlTopRest.setOnClickListener {
            itemClick.onItemSelected(modelData)
        }


    }

    private fun addRemoveFavouriteAPI(id: String, position: Int, imgFavorite: ImageView, isFav: Boolean) {

        val map = HashMap<String, String>()
        map["restaurant_id"] = id
        map["user_id"] = "1"

        CustomProgressbar.showProgressBar(context, false)
        RetrofitClientSingleton
            .getInstance()
            .addRemoveFavouriteRestaurant(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                if (isFav) {
                                    imgFavorite.setImageResource(R.drawable.ic_favorite_unselected_red)
                                } else {
                                    imgFavorite.setImageResource(R.drawable.ic_favorite_selected_red)
                                }

                            }
                            FAILURE -> {
                                Toast(context, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }


    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        var imgTopRestaurants: ImageView = itemView.findViewById(R.id.imgTopRestaurants)
        var imgFavorite: ImageView = itemView.findViewById(R.id.imgFavorite)
        var tvRestName: TextView = itemView.findViewById(R.id.tvRestName)
        var tvAddress: TextView = itemView.findViewById(R.id.tvAddress)
        var tvTypesOfFood: TextView = itemView.findViewById(R.id.tvTypesOfFood)
        var tvOnlineOrder: TextView = itemView.findViewById(R.id.tvOnlineOrder)
        var rlTopRest: RelativeLayout = itemView.findViewById(R.id.rlTopRest)
        var tvTime: TextView = itemView.findViewById(R.id.tvTime)

    }

}
