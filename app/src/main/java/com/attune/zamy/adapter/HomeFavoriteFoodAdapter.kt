package com.attune.zamy.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.fragment.HomeFragment
import com.attune.zamy.model.HomeScreenModel
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.home_favourite_item_row.view.*

class HomeFavoriteFoodAdapter(
    private val mContext: Context,
    var response: HomeScreenModel.DataBean?,
    var mAdapterPosition: HomeFragment

) : RecyclerView.Adapter<HomeFavoriteFoodAdapter.FoodHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FoodHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.home_favourite_item_row, parent, false)
        return FoodHolder(view)
    }

    override fun onBindViewHolder(holder: FoodHolder, position: Int) {
        response!!.fav_food_list?.get(position).let { holder.bind(it!!.price ) }
    }

    override fun getItemCount(): Int {
        return response!!.fav_food_list!!.size
    }

    inner class FoodHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        fun bind(url: String?) {
            var data = response!!.fav_food_list?.get(position)
            val img: ImageView = itemView.findViewById(R.id.imgFavorite)
            val tvRestName: TextView = itemView.findViewById(R.id.tvRestName)


            val txtPrice: TextView = itemView.findViewById(R.id.txtPrice)
            if (data!!.price != null && data.price != "") {
                //txtPrice.text = mContext.resources.getString(R.string.rupees) + data.price

            }

//            val myPrice = String.format("%.0f", data.price)
  //          txtPrice.text = mContext.resources.getString(R.string.rupees) + data!!.price

            if (data.menu_logo != null) {
                Glide.with(mContext)
                    .load(data.menu_logo)
                    .error(R.drawable.ic_place_holder)
                    .into(img)

            } else {
                Glide.with(mContext)
                    .load(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(img)
            }
/*
             Glide.with(mContext)
                 .load(data!!.images)
                 .into(img)*/
            if (data.menu_name != null && data.menu_name != "") {
                itemView.tvTitle.text = data.menu_name
            }
            if (data.res_name != null && data.res_name != "") {
                tvRestName.text = data.res_name
            }
            var rlFavourite: RelativeLayout = itemView.findViewById(R.id.rlFavourite)
            rlFavourite.setOnClickListener {
                mAdapterPosition.adapterFavouriteItemPosition(data)
            }

        }
    }

    interface   onItemClickListnerFavResturants {
        fun adapterFavouriteItemPosition(dataList: HomeScreenModel.DataBean.FavFoodListBean)
    }

}
