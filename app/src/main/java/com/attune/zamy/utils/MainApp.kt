package com.attune.zamy.utils

import android.app.Activity
import android.app.Application
import android.content.Context
import android.os.Bundle
import android.util.Log


class MainApp : Application(), Application.ActivityLifecycleCallbacks {
    lateinit var context: Context
    override fun onCreate() {
        super.onCreate()
        context = this
    }

    override fun onActivityCreated(activity: Activity, bundle: Bundle) {

        Log.e(TAG,activity.toString())
        context = activity
        TAG = activity::class.java.simpleName
    }

    override fun onActivityStarted(activity: Activity) {
        context = activity
        TAG = activity::class.java.simpleName

    }

    override fun onActivityResumed(activity: Activity) {
        context = activity
        TAG = activity::class.java.simpleName

    }

    override fun onActivityPaused(activity: Activity) {}

    override fun onActivityStopped(activity: Activity) {

    }

    override fun onActivitySaveInstanceState(activity: Activity, bundle: Bundle) {

    }

    override fun onActivityDestroyed(activity: Activity) {

    }

    companion object {


        private var instance: MainApp? = null

        fun applicationContext() : Context {
            return instance!!.applicationContext
        }

        fun actContext():Context{
            return instance!!.context
        }
        var TAG = MainApp.javaClass.simpleName

    }


    init {
        instance = this
    }


}
