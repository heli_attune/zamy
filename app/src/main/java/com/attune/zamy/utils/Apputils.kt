package com.attune.zamy.utils

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Build
import android.provider.Settings
import android.text.TextUtils
import android.util.Log
import android.view.View
import android.view.Window
import android.view.animation.Animation
import android.view.animation.Transformation
import android.widget.RelativeLayout
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.attune.zamy.R
import com.attune.zamy.activity.MainActivity
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import kotlinx.android.synthetic.main.alart_layout.*
import kotlinx.android.synthetic.main.layout_no_network.view.*
import kotlinx.android.synthetic.main.no_data_layout.view.*
import kotlinx.android.synthetic.main.update_app_dialoge.*
import kotlinx.android.synthetic.main.update_app_dialoge.btn_ok
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File
import java.text.SimpleDateFormat
import java.util.*


class Apputils


private var mGoogleSignInClient: GoogleSignInClient? = null


fun expand(v: View) {
    val matchParentMeasureSpec =
        View.MeasureSpec.makeMeasureSpec((v.parent as View).width, View.MeasureSpec.EXACTLY)
    val wrapContentMeasureSpec =
        View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED)
    v.measure(matchParentMeasureSpec, wrapContentMeasureSpec)
    val targetHeight = v.measuredHeight

    // Older versions of android (pre API 21) cancel animations for views with a height of 0.
    v.layoutParams.height = 1
    v.visibility = View.VISIBLE
    val a = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            v.layoutParams.height = if (interpolatedTime == 1f)
                RelativeLayout.LayoutParams.WRAP_CONTENT
            else
                (targetHeight * interpolatedTime).toInt()
            v.requestLayout()
        }

        override fun willChangeBounds(): Boolean {
            return true
        }
    }

    // Expansion speed of 1dp/ms
    a.duration = (targetHeight / v.context.resources.displayMetrics.density).toInt().toLong()
    v.startAnimation(a)
}

fun collapse(v: View) {
    val initialHeight = v.measuredHeight

    val a = object : Animation() {
        override fun applyTransformation(interpolatedTime: Float, t: Transformation) {
            if (interpolatedTime == 1f) {
                v.visibility = View.GONE
            } else {
                v.layoutParams.height =
                    initialHeight - (initialHeight * interpolatedTime).toInt()
                v.requestLayout()
            }
        }

        override fun willChangeBounds(): Boolean {
            return true
        }
    }

    // Collapse speed of 1dp/ms
    a.duration = (initialHeight / v.context.resources.displayMetrics.density).toInt().toLong()
    v.startAnimation(a)
}

fun isValidText(str: String): Boolean {
    val expression = "^[a-zA-Z\\s]+"
    return str.matches(expression.toRegex())
}

fun showDialogPermissionRational(context: Context, strMessage: String) {
    val builder = AlertDialog.Builder(context)

    // Set the alert dialog title
    builder.setTitle("Permission Required")

    // Display a message on alert dialog
    builder.setMessage(strMessage)

    // Set a positive button and its click listener on alert dialog
    builder.setPositiveButton("YES") { _, _ ->
        val intent = Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS)
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        val uri = Uri.fromParts("package", context.packageName, null)
        intent.data = uri
        context.startActivity(intent)
    }
}

fun showNoDataLayout(view: View, str: String = "No data!") {
    view.llParentNoData.visibility = View.VISIBLE
    view.txtNodata.text = str
}

fun hideNoDataLayout(view: View) {
    view.llParentNoData.visibility = View.GONE
}


fun showNoInterNetLayout(view: View, str: String = "Please Check Network") {
    view.llNoNetwork.visibility = View.VISIBLE
    view.txtNoInternet.text = str
    view.btnRetry.visibility = View.VISIBLE

}

fun hideNoInterNetLayout(view: View) {
    view.llNoNetwork.visibility = View.GONE
}


fun hasPermissions(context: Context?, permissions: Array<String>): Boolean {
    if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null) {
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(
                    context,
                    permission
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                return false
            }
        }
    }
    return true
}

fun convertDateTime(fromFormat: String, toFormat: String, dateOriginalGot: String): String {

    try {
        //SimpleDateFormat fmt = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        //// Getting Source format here
        val fmt = SimpleDateFormat(fromFormat)

        fmt.timeZone = TimeZone.getDefault()

        val date = fmt.parse(dateOriginalGot)

        //SimpleDateFormat fmtOut = new SimpleDateFormat("EEE, MMM d, ''yyyy");

        //// Setting Destination format here
        val fmtOut = SimpleDateFormat(toFormat)

        return fmtOut.format(date)

    } catch (e: Exception) {

        e.printStackTrace()

        e.message

    }

    return ""

}

fun getStringRequestBody(strParam: String): RequestBody {
    return RequestBody.create(MediaType.parse("text/plain"), strParam)
}

fun getIntRequestBody(intParam: Int): RequestBody {
    return RequestBody.create(MediaType.parse("text/plain"), intParam.toString())
}

fun getImageRequestBody(file: File): RequestBody {
    return RequestBody.create(MediaType.parse("image/*"), file)
}


fun shareApp(context: Context, strMessage: String) {
    val packageName = context.packageName
    val intent = Intent("android.intent.action.SEND")
    intent.type = "text/plain"
    intent.putExtra("android.intent.extra.SUBJECT", "---")
    intent.putExtra(
        "android.intent.extra.TEXT",
        "$strMessage\nhttps://play.google.com/store/apps/details?id=$packageName"
    )
    context.startActivity(Intent.createChooser(intent, "Choose Sharing Option"))
}

fun updateAppDialog(context: Context) {
    val dialog = Dialog(context)
    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    dialog.setContentView(R.layout.update_app_dialoge)
    dialog.tvLogoutDialogTitle.text = context.getString(R.string.update_app)
    dialog.setCancelable(false)
    dialog.show()

    dialog.btn_ok.setOnClickListener {
        val uri = Uri.parse("market://details?id=" + context.packageName)
        val goToMarket = Intent(Intent.ACTION_VIEW, uri)
        try {
            context.startActivity(goToMarket)
        } catch (e: Exception) {
            context.startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://play.google.com/store/apps/details?id=" + context.packageName)
                )
            )
        }
    }
}


private var gso: GoogleSignInOptions? = null

fun signOut() {
    if (mGoogleSignInClient != null) {
        mGoogleSignInClient!!.signOut()
            .addOnCompleteListener {
                Log.e("TAG", "Signout Success")
            }
    }
}

fun initializeGoogleSignIn() {
    gso = GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
        // .requestIdToken("8108734821-he1v91une5t9rcc31k8nub4dck6q7fpa.apps.googleusercontent.com")
        .requestEmail()
        .build()

}

fun getGoogleSignInClient(context: Context): GoogleSignInClient {
    mGoogleSignInClient = GoogleSignIn.getClient(context, gso!!)
    return mGoogleSignInClient!!
}


fun isValidUserName(str: String): Boolean {
    val expression = "^[a-zA-Z0-9]*$"
    return str.matches(expression.toRegex())
}

fun isValidMail(mailString: String): Boolean {
    return !TextUtils.isEmpty(mailString) && android.util.Patterns.EMAIL_ADDRESS.matcher(mailString).matches()
}

fun getVisibleFragment(context: MainActivity): Fragment? {
    var fragmentManager = context.supportFragmentManager
    var fragments = fragmentManager.fragments as List<Fragment>
    fragments.iterator().forEach {
        if (it != null && it.isVisible)
            return it

    }

    return null

}

fun getDeviceID(context: Context): String {
    return Settings.Secure.getString(context.contentResolver, Settings.Secure.ANDROID_ID)
}

fun alertDialog(context: Context, message: String) {
    val myDialog = Dialog(context)
    myDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
    myDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    myDialog.setContentView(R.layout.alart_layout)
    myDialog.tvAlertTitle.text = message
    myDialog.show()

    myDialog.btn_ok.setOnClickListener {
        myDialog.dismiss()
    }
}

