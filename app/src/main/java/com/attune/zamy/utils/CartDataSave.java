package com.attune.zamy.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.attune.zamy.model.CartRestaurantsMenuModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Modifier;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class CartDataSave {
        public static final String PREFS_NAME = "MY_APP";
        public static final String FAVORITES = "Favorite";

        public void storeFavorites(Context context, List favorites) {
            // used for store arrayList in json format
            SharedPreferences settings;
            SharedPreferences.Editor editor;
            settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            editor = settings.edit();

            GsonBuilder builder = new GsonBuilder();
            builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
            builder.excludeFieldsWithoutExposeAnnotation();
            Gson sExposeGson = builder.create();
            String jsonFavorites = sExposeGson.toJson(favorites);
            editor.putString(FAVORITES, jsonFavorites);
            editor.commit();
        }

        public ArrayList loadFavorites(Context context) {
            // used for retrieving arraylist from json formatted string
            SharedPreferences settings;
            List favorites;
            settings = context.getSharedPreferences(PREFS_NAME, Context.MODE_PRIVATE);
            if (settings.contains(FAVORITES)) {
                String jsonFavorites = settings.getString(FAVORITES, null);

                GsonBuilder builder = new GsonBuilder();
                builder.excludeFieldsWithModifiers(Modifier.FINAL, Modifier.TRANSIENT, Modifier.STATIC);
                builder.excludeFieldsWithoutExposeAnnotation();
                Gson sExposeGson = builder.create();
                CartRestaurantsMenuModel[] favoriteItems = sExposeGson.fromJson(jsonFavorites, CartRestaurantsMenuModel[].class);
                favorites = Arrays.asList(favoriteItems);
                favorites = new ArrayList(favorites);
            } else
                return null;
            return (ArrayList) favorites;
        }

        public void addFavorite(Context context, CartRestaurantsMenuModel myModel) {
            List favorites = loadFavorites(context);
            if (favorites == null)
                favorites = new ArrayList();
            favorites.add(myModel);
            storeFavorites(context, favorites);
        }

        public void removeFavorite(Context context, CartRestaurantsMenuModel myModel) {
            ArrayList favorites = loadFavorites(context);
            if (favorites != null) {
                favorites.remove(myModel);
                storeFavorites(context, favorites);
            }
        }
}
