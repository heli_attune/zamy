package com.hopmeal.android.utils

// key set

const val keyUserId = "user_id"
const val keyColor = "COLOR"


const val phone = "phone"
const val keyEmail = "email"
const val keyuser_url = "user_url"
const val keyuser_registered = "keyuser_registered"
const val keyuser_activation_key = "keyuser_activation_key"
const val keyuser_status = "keyuser_status"
const val keydisplay_name = "keydisplay_name"
const val keyaccess_token = "keyaccess_token"
const val keyfire_token = "keyfire_token"
const val keydevice_type = "keydevice_type"
const val keyPassword = "password"
const val location = "location"


const val keyIsActive = "is_active"
const val keyFirstName = "firstname"
const val keyMiddleName = "middle_name"
const val keyLastName = "lastname"
const val keyDisplayName = "displayname"
const val keyNickName = "nickname"
const val keyUserName = "username"
const val keyMobile = "mobile"
const val keyProductDetail = "productDetail"
const val keyLat = "lat"
const val keyLng = "lng"
const val ADD_LOCATION = 102
const val keyReferralCode = "referralCode"
const val keyAuthToken = "Authorization"
const val keyFirebaseToken = "firebase_token"
const val keyGoogleSignIn = 0
const val default_billing = "default_billing"
const val default_shipping = "default_shipping"

const val status = "1"

// permission constants
const val REQUEST_WRITE_STORAGE_PERMISSION = 1
const val WRITE_PERMISSION_CODE = 2
const val REQUEST_IMAGE = 3
const val SELECT_IMAGE_GALLERY = 4
const val REQUEST_READ_STORAGE_PERMISSION = 5
const val REQUEST_READ_STATE_PERMISSION = 6
const val CAMERA_PERMISSION_CODE = 7
const val REQUEST_CAMERA_IMAGE = 8
const val REQUEST_GALLERY_IMAGE = 9
const val REQUEST_LOCATION = 10


//socila login
const val isFblogin = "isFblogin"
const val isGmaillogin = "isGmaillogin"


const val InternalServerError = 500
const val PageNotFound = 400

//cart
const val cartCount = "cartCount"
const val wishCount = "wishCount"


const val RC_SIGN_IN = 50

//notification types
/*const val keyOrderPlacedSuccess = 3
const val keyOrderOutForDelivery = 4
const val keyOrderDelivered = 3
const val keyChatNotification = 6
const val keyChatNotificationStatus =7
const val keySingleProductOnTheWay =9
const val keySingleProductDelivered =10*/


const val orderDelivered = 3
const val orderReceived = 1
const val orderCancelled = 2
const val ticketReplyFor = 4
const val ticketStatusChanged = 5
const val orderOutForDelivered = 6