package com.attune.zamy.utils

import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.ConnectivityManager
import android.util.Patterns
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.activity.LoginPage
import java.text.SimpleDateFormat
import java.util.*

fun Toast(context: Context, text: String?) {
    if (context!=null && text != null) {
        val toast = android.widget.Toast.makeText(context, text, android.widget.Toast.LENGTH_SHORT)
        toast.show()
    }
}

fun isNetworkAvailable(context: Context): Boolean {
    val connectivityManager =
        context.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager
    val activeNetworkInfo = connectivityManager.activeNetworkInfo
    return activeNetworkInfo != null && activeNetworkInfo.isConnected
}

fun isValidEmail(email: String): Boolean {
    val pattern = Patterns.EMAIL_ADDRESS
    return pattern.matcher(email).matches()
}



/*fun showNoInternetLayout(view: View) {
    view.llParentNoData.visibility = View.VISIBLE
    view.txtNodata.text = view.context.getString(R.string.network_error)
//    view.ivNoData.setImageResource(R.drawable.ic_no_connection)
}*/

fun showLogOutDialog(context: Context) {
    val builder = AlertDialog.Builder(context)

    // Set the alert dialog title
    builder.setTitle("Log Out")

    // Display a message on alert dialog
    builder.setMessage("Are you sure you want to logout?")

    // Set a positive button and its click listener on alert dialog
    builder.setPositiveButton("YES") { dialog, which ->
        // Do something when user press the positive button


        // Change the app background color
        SharedPref.removeSharedData(context)
        Toast(context, "Logout Successfully")
        val LoginIntent = Intent(context, LoginPage::class.java)
        context.startActivity(LoginIntent)

    }


    // Display a negative button on alert dialog
    builder.setNegativeButton("No") { dialog, which ->
        dialog.dismiss()
    }


    // Finally, make the alert dialog using builder
    val dialog: AlertDialog = builder.create()

    // Display the alert dialog on app interface
    dialog.show()

}


fun hideSoftKeyBoard(context: Context, view: View) {
    try {
        val imm = context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, InputMethodManager.HIDE_NOT_ALWAYS)
    } catch (e: Exception) {
        // TODO: handle exception
        e.printStackTrace()
    }

}

fun AppCompatActivity.hideKeyboard() {
    val view = this.currentFocus
    if (view != null) {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(view.windowToken, 0)
    }
    // else {
    window.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN)
    // }
}

fun getCurrentDate(): String {
    //val sdf = SimpleDateFormat("dd/M/yyyy")
    val sdf = SimpleDateFormat("yyyy/MM/dd")
    val currentDate = sdf.format(Date())
    System.out.println(" C DATE is  " + currentDate)
    return currentDate
}


fun getCurrentTime(): String {
    val sdftime = SimpleDateFormat("HH:mm")
    val currentTime = sdftime.format(Date())
    System.out.println(" C time is  " + currentTime)
    return currentTime
}


//if string is null or "" return 0 to int

fun stringToIntNotNullorEmpty(value: String?): Int {
    var passvalue: Int = 0
    if (value != null && value != "") {
        passvalue = value.toInt()
    } else {
        passvalue = 0
    }
    return passvalue
}

//if string is null or "" return 0 to int

fun stringNotNullorEmpty(value: String?): String {
    var passstr: String
    if (value != null && value != "") {
        passstr = value
    } else {
        passstr = ""
    }
    return passstr
}

fun isCheckEmptyorNull(editText: EditText): String {
    var str: String?

    if (!editText.toString().isEmpty()) {
        str = editText.text.toString().trim()
    } else {
        str = ""
    }
    return str
}

fun internetConnectionDialog(context: Context) {

    val alertDialogBuilder = AlertDialog.Builder(context)
    alertDialogBuilder.setTitle("No Internet Connection")
    alertDialogBuilder.setMessage("You are offline please check your internet connection")
    alertDialogBuilder.setPositiveButton(
        "Retry"
    ) { arg0, arg1 ->

        //Toast.makeText(MainActivity.this,"Internet Connection Available", Toast.LENGTH_LONG).show();
    }
    alertDialogBuilder.setNegativeButton(
        "Exit"
    ) { arg0, arg1 ->
        //Toast.makeText(MainActivity.this,"Internet Connection Available", Toast.LENGTH_LONG).show();
    }

    val alertDialog = alertDialogBuilder.create()
    alertDialog.show()
}