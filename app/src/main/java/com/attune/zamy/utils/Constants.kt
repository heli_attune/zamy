package com.attune.zamy.utils


const val SUCCESS = 1
const val FAILURE = 0
const val AUTH_FAILURE = 2
const val RESTO_AVAILABLE = 3
const val USER_ID = "user_id"
const val USER_NAME = "name"
const val USER_PHONE = "phone"
const val USER_EMAIL = "email"
const val USER_LOCATION = "location"
const val USER_FIRE_TOKEN="token"
const val LATITUDE = "LATITUDE"
const val LONGITUDE = "LONGITUDE"
const val LOCATION_ADDRESS = "ADDRESS"
const val USER_CODE = "user_code"
const val USER_STATUS = "status"
const val USER_MOBILE_NO = "mobile_no"


const val TABLE_NO_PASS = "table_value_pass"
const val TABLE_NAME = "table_name"
const val TABLE_NO = "table_no"

const val RESTAURANT_ID_PASS = "restaurant_no_pass"
const val TABLE_DATE = "TABLE_IN_DATE"
const val TABLE_TIME = "TABLE_IN_TIME"
const val ORDER_ID = "ORDER_ID"
const val KOT_ID = "KOT_ID"
const val SALES_ID = "SALES_ID"
const val ORDER_TYPE = "ORDER_TYPE"
const val KOT_ENABLE = "KOT_ENABLE"

const val HOME_Delivery = "HOME_Delivery"
const val TAKE_AWAY = "TAKE_AWAY"
const val SETTINGS = "SETTINGS"
const val CARTITEMS = "CARTITEMS"
const val SCREEN_REDIRECTION = "SCREEN_REDIRECTION"
const val keyOrderId = "order_id"
const val PROMOCODE = "PROMOCODE"
const val REMOVE_CODE = "REMOVE_CODE"
const val APPLY_COUPAN = "APPLY_COUPAN"

