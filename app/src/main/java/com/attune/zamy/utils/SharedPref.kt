package com.attune.zamy.utils

import android.content.Context
import android.content.SharedPreferences
import android.net.Uri
import android.preference.PreferenceManager
import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.OrderCart
import com.attune.zamy.model.UserModel
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


object SharedPref {

    private fun getSharedPreferences(context: Context): SharedPreferences {
        return context.getSharedPreferences(context.packageName, Context.MODE_PRIVATE)
    }

    fun getStringValue(context: Context, key: String, default: String = ""): String {
        return getSharedPreferences(context).getString(key, default)!!
    }

    fun getUserId(context: Context): String =
        context.getSharedPreferences("USERDATA", Context.MODE_PRIVATE).getString(USER_ID, "")!!


    fun getUserName(context: Context): String =
        context.getSharedPreferences("USERDATA", Context.MODE_PRIVATE).getString(USER_NAME, "")!!

    fun getUserEmail(context: Context): String =
        context.getSharedPreferences("USERDATA", Context.MODE_PRIVATE).getString(USER_EMAIL, "")!!


    fun setStringValue(context: Context, key: String, newValue: String?) {
        val editor = getSharedPreferences(context).edit()
        editor.putString(key, newValue)
        editor.apply()
    }


    fun getIntValue(context: Context, key: String, default: Int = 0): Int {
        return getSharedPreferences(context).getInt(key, default)
    }

    fun setIntValue(context: Context, key: String, newValue: Int) {
        val editor = getSharedPreferences(context).edit()
        editor.putInt(key, newValue)
        editor.apply()
    }

    fun getBooleanValue(context: Context, key: String, default: Boolean = false): Boolean {
        return getSharedPreferences(context).getBoolean(key, default)
    }

    fun setBooleanValue(context: Context, key: String, newValue: Boolean) {
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(key, newValue)
        editor.apply()
    }

    fun clear(context: Context) {
        val editor = getSharedPreferences(context).edit()
        editor.clear()
        editor.apply()
    }


    fun removeSharedData(context: Context) {
        val sharedPreferences = context.getSharedPreferences("USERDATA", Context.MODE_PRIVATE)
        sharedPreferences.edit().clear().commit()
    }

    fun getUserData(context: Context): UserModel.DataBean {
        return context.getSharedPreferences("USERDATA", Context.MODE_PRIVATE) as UserModel.DataBean
    }


    fun saveBooleanHomeDelivery(mContext: Context, key: String, value: Boolean) {
        val preferences =
            mContext.getSharedPreferences(HOME_Delivery, android.content.Context.MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putBoolean(key, value)
        editor.commit()
    }

    fun getBooleanHomeDelivery(mContext: Context, key: String): Boolean {
        val preferences =
            mContext.getSharedPreferences(HOME_Delivery, android.content.Context.MODE_PRIVATE)
        return preferences.getBoolean(key, false)
    }

    fun saveBooleanTakeAway(mContext: Context, key: String, value: Boolean) {
        val preferences =
            mContext.getSharedPreferences(TAKE_AWAY, android.content.Context.MODE_PRIVATE)
        val editor = preferences.edit()
        editor.putBoolean(key, value)
        editor.commit()
    }

    fun getBooleanTakeAway(mContext: Context, key: String): Boolean {
        val preferences =
            mContext.getSharedPreferences(TAKE_AWAY, android.content.Context.MODE_PRIVATE)
        return preferences.getBoolean(key, false)
    }


    fun saveLogin(context: Context, userData: UserModel.DataBean) {
        val preferences = context.getSharedPreferences("USERDATA", Context.MODE_PRIVATE)
        val editor = preferences.edit()

        editor.putString(USER_ID, userData.id)
        editor.putString(USER_NAME, userData.name)
        editor.putString(USER_PHONE, userData.phone)
        editor.putString(USER_EMAIL, userData.email)
        editor.putString(USER_LOCATION, userData.location)
        editor.putString(USER_FIRE_TOKEN, userData.fire_token)

        editor.commit()
        editor.apply()
    }


    /*fun saveLocation(context: Context){
        val preferences = context.getSharedPreferences("Location", Context.MODE_PRIVATE)
        val editor = preferences.edit()

        editor.putString(LATITUDE, userData!!.id)
        editor.putString(LONGITUDE, userData.name)
        editor.putString(LOCATION_ADDRESS, userData.phone)
        editor.commit()
        editor.apply()
    }*/
    fun saveArrayListCart(list: java.util.ArrayList<OrderCart>, key: String, context: Context) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()

        val gson = Gson()
        val json = gson.toJson(list)
        editor.putString(key, json)
        editor.apply()     // This line is IMPORTANT !!!
    }

    fun getCartArrayList(key: String, context: Context): java.util.ArrayList<OrderCart> {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = prefs.getString(key, null)
        if (json != null) {
            val type = object : TypeToken<ArrayList<OrderCart>>() {

            }.type
            return gson.fromJson(json, type)

        } else {
            val type = object : TypeToken<ArrayList<OrderCart>>() {

            }.type
            return gson.fromJson("[]", type)
        }
    }

    fun getArrayList(key: String, context: Context): ArrayList<OrderCart>? {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = prefs.getString(key, null)
        /*if (json != null) {*/
        val type = object : TypeToken<ArrayList<OrderCart>>() {

        }.type
        return gson.fromJson<ArrayList<OrderCart>>(json, type)

        /*}else {
            Type type = new TypeToken<ArrayList<OrderCart>>() {
            }.getType();
            return gson.fromJson("[]", type);
        }*/
    }

    fun savedata(
        context: Context,
        mStudentObject: MutableList<CartRestaurantsMenuModel>
    ) {

        val appSharedPrefs = PreferenceManager
            .getDefaultSharedPreferences(context)
        val prefsEditor = appSharedPrefs.edit()
        val gson = Gson()
        val json = gson.toJson(mStudentObject)
        prefsEditor.putString("MyObject", json)
        prefsEditor.commit()
    }


    fun getCartObject(context: Context) {
        val appSharedPrefs = PreferenceManager
            .getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = appSharedPrefs.getString("MyObject", "")
        val mStudentObject = gson.fromJson<CartRestaurantsMenuModel>(json, OrderCart::class.java)
    }


    fun saveArrayList(
        list: MutableList<CartRestaurantsMenuModel>, key: String,
        context: Context
    ) {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val editor = prefs.edit()
        val gson = Gson()
        val json = gson.toJson(list)
        editor.putString(key, json)
        editor.apply()     // This line is IMPORTANT !!!
    }

    fun getArrayListData(key: String, context: Context): ArrayList<CartRestaurantsMenuModel> {
        val prefs = PreferenceManager.getDefaultSharedPreferences(context)
        val gson = Gson()
        val json = prefs.getString(key, null)
        val type = object : TypeToken<ArrayList<CartRestaurantsMenuModel>>() {

        }.type
        return gson.fromJson(json, type)
    }

    fun setGmaillLogin(context: Context, key: String, newValue: Boolean) {
        val editor = getSharedPreferences(context).edit()
        editor.putBoolean(key, newValue)
        editor.apply()
    }


    fun saveProfileIntoShardPref(context: Context, imageUrl: Uri) {
        val appSharedPrefs = getSharedPreferences(context)
        val prefsEditor = appSharedPrefs.edit()
        prefsEditor.putString("ShardProfileImage", imageUrl.toString())
        prefsEditor.commit()
    }

    fun getProfileIntoShardPref(context: Context): String =
        getSharedPreferences(context).getString("ShardProfileImage", "")!!

}