package com.attune.zamy.fragment

import SliderPagerAdapter
import android.Manifest
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.attune.zamy.R
import com.attune.zamy.activity.MainActivity
import com.attune.zamy.activity.TopRestaurantsPage
import com.attune.zamy.adapter.*
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.custom.LocationTrack
import com.attune.zamy.interaface.onItemClickListnerTopResturants
import com.attune.zamy.interaface.onItemVarianceSelected
import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.CommonModel
import com.attune.zamy.model.HomeScreenModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.gms.location.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.hopmeal.android.utils.cartCount
import kotlinx.android.synthetic.main.bottom_sheet_variance.*
import kotlinx.android.synthetic.main.dialog_android_view.*
import kotlinx.android.synthetic.main.fragment_grocery.view.*
import kotlinx.android.synthetic.main.home_layout.view.*
import kotlinx.android.synthetic.main.home_popular_item_row.*
import kotlinx.android.synthetic.main.layout_no_network.view.*
import kotlinx.android.synthetic.main.no_data_layout.*
import kotlinx.android.synthetic.main.no_data_layout.view.*
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class GroceryFragment : Fragment(),
    onItemClickListnerTopResturants,
    HomePopularMonthAdapter.PopularItemClickListener,
    HomePopularMonthAdapter.AddtoCartItemClickListener,
    HomePopularMonthAdapter.VarianceDialogClickListener,
    HomeFavoriteFoodAdapter.onItemClickListnerFavResturants,
    onItemVarianceSelected,
    LocationListener {

    lateinit var groceryView: View
    var TOTAL_PAGE: Int = 0
    var isLoading: Boolean = false
    var isLastPage: Boolean = false
    val PAGE_START = 1
    var currentPage: Int = PAGE_START
    lateinit var swipeLayout: SwipeRefreshLayout
    var mainActivity = MainActivity()
    var dialog: Dialog? = null
    var user_id = ""
    var cartArrayList: MutableList<CartRestaurantsMenuModel> = ArrayList()
    var longitude: Double = 0.0
    var latitude: Double = 0.0
    private var permissionsToRequest = ArrayList<String>()
    private val permissions = ArrayList<String>()
    var locationTrack: LocationTrack? = null
    lateinit var rvTopRestaurants: RecyclerView
    lateinit var rvFavouriteRestaurants: RecyclerView
    lateinit var rvHomeCategory: RecyclerView
    lateinit var rvOfferRestaurants: RecyclerView
    lateinit var rvPopular: RecyclerView
    private lateinit var varianceitem: CartRestaurantsMenuModel
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mSettingsClient: SettingsClient? = null

    lateinit var shimmerHomeLayout: ShimmerFrameLayout
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        groceryView = inflater.inflate(R.layout.fragment_grocery, container, false)
        mainActivity = activity as MainActivity
        mainActivity.showToolbar()
        shimmerHomeLayout=groceryView.findViewById(R.id.homeShimmer)
        initView()

        val permission =
            ContextCompat.checkSelfPermission(activity!!, Manifest.permission.ACCESS_FINE_LOCATION)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.e("TAG", " Permission to record denied ")
        } else {
            findUnAskedPermissions(permissions)
        }

        groceryView.homeSwiperefreshlayout.setOnRefreshListener {
            if (isNetworkAvailable(activity!!)) {
                getGrocerryData()
            }
            groceryView.homeSwiperefreshlayout.isRefreshing = false
        }

        groceryView.btnRetry.setOnClickListener {
            initView()
        }
        return groceryView
    }

    private fun initView() {
        user_id=activity?.let { SharedPref.getUserId(it) }!!
        shimmerHomeLayout.startLayoutAnimation()

        mSettingsClient = LocationServices.getSettingsClient(activity!!)
        rvTopRestaurants = groceryView.findViewById(R.id.rvTopRestaurants)
        rvFavouriteRestaurants = groceryView.findViewById(R.id.rvFavouriteRestaurants)
        rvHomeCategory = groceryView.findViewById(R.id.rvHomeCategory)
        rvOfferRestaurants = groceryView.findViewById(R.id.rvOfferRestaurants)
        rvPopular = groceryView.findViewById(R.id.rvPopular)
        locationTrack = LocationTrack(activity!!)
        permissionsToRequest = findUnAskedPermissions(permissions)
        groceryView.tvViewAllRestaurants.visibility = View.GONE

        groceryView.tvViewAllRestaurants.setOnClickListener {
            val intent = Intent(activity, TopRestaurantsPage::class.java)
            intent.putExtra("PageName", "Grocery")
            activity!!.startActivity(intent)
        }


        if (isNetworkAvailable(activity!!)) {
            requestNewLocationData()
            findUnAskedPermissions(permissions)
            getGrocerryData()
            bindRecycleview()
            hideNoInterNetLayout(groceryView.llNoNetwork)

        } else {
            showNoInterNetLayout(groceryView.llNoNetwork)
            groceryView.homeLayout.visibility = View.GONE
            Toast(activity!!, getString(R.string.network_error))
        }


        // srRefresh()
    }

    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            mFusedLocationClient!!.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
            )
            return
        }

    }


    override fun onResume() {
        super.onResume()
        shimmerHomeLayout.startLayoutAnimation()
    }

    override fun onPause() {
        shimmerHomeLayout.stopShimmer()
        super.onPause()
    }
    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            latitude = mLastLocation.latitude
            longitude = mLastLocation.longitude
            /*if (latitude != 0.0 && longitude != 0.0) {
                Log.e("TAG", "Location $latitude $longitude")
                mGeoCodeList = geocoder!!.getFromLocation(latitude, longitude, 1)
                mAddress = mGeoCodeList[0].getAddressLine(0)
                mCity = mGeoCodeList[0].locality
                mState = mGeoCodeList[0].adminArea
                mPostCode = mGeoCodeList[0].postalCode
                mCountry = mGeoCodeList[0].countryName
                mCountryCode = mGeoCodeList[0].countryCode
                Log.e(
                    "TAG",
                    "Full Address:-${mGeoCodeList.size} " +
                            "\n address:-$mAddress" +
                            "\n city:-$mCity " +
                            "\n tate:-$mState  " +
                            "\n postCode:-$mPostCode " +
                            "\n country:-$mCountry " +
                            "\n countryCode:-$mCountryCode"
                )
                getAreaList()
            } else {
                Toast(activity!!, getString(R.string.network_error))
            }*/
        }
    }

    private fun findUnAskedPermissions(permissions: ArrayList<String>): ArrayList<String> {
        val result = ArrayList<String>()
        for (perm in permissions) {
            if (!hasPermission(perm)) {
                result.add(perm)
            }
        }
        return result
    }

    private fun hasPermission(perm: String): Boolean {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (ContextCompat.checkSelfPermission(
                    activity!!,
                    perm
                ) == PackageManager.PERMISSION_GRANTED)
            }
        }
        return true
    }

    private fun canMakeSmores(): Boolean {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1)
    }

    private fun bindRecycleview() {
        //top restaurant layout manager
        rvTopRestaurants.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        //favourite restaurant layout manager
        rvFavouriteRestaurants.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        //popular of month layout manager
        rvPopular.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.VERTICAL,
            false
        )
        rvOfferRestaurants.layoutManager = LinearLayoutManager(
            activity!!,
            LinearLayoutManager.HORIZONTAL,
            false
        )

    }

    private fun srRefresh() {
        swipeLayout.setOnRefreshListener {
            clearPaging()
            //getTopRestaurantDataAPI(true)
            swipeLayout.isRefreshing = false
        }
    }

    private fun clearPaging() {
        currentPage = PAGE_START
        isLoading = false
        isLastPage = false
    }

    fun setViewPager(
        activity: FragmentActivity,
        couponList: List<HomeScreenModel.DataBean.CouponListBean>?
    ) {
        groceryView.pager.adapter = SliderPagerAdapter(activity, couponList)
    }

    fun getGrocerryData() {
       // CustomProgressbar.showProgressBar(activity!!, false)
        shimmerHomeLayout.startLayoutAnimation()
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(activity!!)
        map["franchise_type"] = "2"
        RetrofitClientSingleton
            .getInstance()
            .getDashboardData(map)
            .enqueue(object : retrofit2.Callback<HomeScreenModel> {
                override fun onFailure(call: Call<HomeScreenModel>, t: Throwable) {
                    //CustomProgressbar.hideProgressBar()
                    Toast(activity!!, getString(R.string.something_went_wrong))

                }

                override fun onResponse(
                    call: Call<HomeScreenModel>,
                    response: Response<HomeScreenModel>
                ) {
                    //CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val homeScreenData = response.body()
                        if (homeScreenData != null) {
                            groceryView.tvViewAllRestaurants.text = "View All"
                            groceryView.homeLayout.visibility = View.VISIBLE
                            hideNoDataLayout(groceryView.llParentNoData)
                            when (response.body()!!.status) {
                                SUCCESS -> {
                                    if (activity != null) {
                                        //mainActivity.cartSet(response.body()!!.data!!.cart_count)
                                        SharedPref.setIntValue(
                                            requireContext(),
                                            cartCount,
                                            response.body()!!.data!!.cart_count
                                        )
                                        mainActivity.updateCartBadge(response.body()!!.data!!.cart_count)

                                        if (homeScreenData.data!!.top_restaurant_list != null) {
                                            if (homeScreenData.data!!.top_restaurant_list!!.isNotEmpty()) {
                                                groceryView.tvTopRestaurants.text = "Top Grocery"
                                                rvTopRestaurants.adapter =
                                                    HomeTopGroceryAdapter(
                                                        activity!!,
                                                        homeScreenData.data,
                                                        this@GroceryFragment
                                                    )
                                            } else {
                                                groceryView.tvTopRestaurants.text = ""
                                                rvTopRestaurants.visibility = View.GONE
                                            }

                                        } else {
                                            groceryView.tvTopRestaurants.text = ""
                                            rvTopRestaurants.visibility = View.GONE
                                        }

                                        if (homeScreenData.data!!.fav_food_list != null) {
                                            if (homeScreenData.data!!.fav_food_list!!.isNotEmpty()) {
                                                rvFavouriteRestaurants.adapter =
                                                    HomeFavoriteGroceryAdapter(
                                                        activity!!,
                                                        homeScreenData.data,
                                                        this@GroceryFragment
                                                    )
                                            } else {
                                                rvFavouriteRestaurants.visibility = View.GONE
                                            }
                                            //favourite Restaurants adapter

                                        } else {
                                            rvFavouriteRestaurants.visibility = View.GONE
                                        }
                                        //category Restaurants adapter
                                        if (homeScreenData.data!!.category_list != null) {
                                            if (homeScreenData.data!!.category_list!!.isNotEmpty()) {
                                                rvHomeCategory.layoutManager = LinearLayoutManager(
                                                    activity,
                                                    LinearLayoutManager.HORIZONTAL,
                                                    false
                                                )
                                                rvHomeCategory.adapter = HomeGroceryAdapter(
                                                    activity!!,
                                                    homeScreenData.data,
                                                    this@GroceryFragment
                                                )
                                            } else {
                                                rvHomeCategory.visibility = View.GONE
                                            }

                                        } else {
                                            rvHomeCategory.visibility = View.GONE
                                        }
                                        if (homeScreenData.data!!.coupon_list != null) {
                                            if (homeScreenData.data!!.coupon_list!!.isNotEmpty()) {
                                                groceryView.tvOfferRestaurants.text = "Offers"
                                                setViewPager(
                                                    activity!!,
                                                    homeScreenData.data!!.coupon_list
                                                )
                                            } else {
                                                groceryView.tvOfferRestaurants.text = ""
                                                groceryView.pager.visibility = View.GONE
                                            }
                                        } else {
                                            groceryView.tvOfferRestaurants.text = ""
                                            groceryView.pager.visibility = View.GONE
                                        }


                                        if (homeScreenData.data!!.popular_this_month != null) {
                                            if (homeScreenData.data!!.popular_this_month!!.isNotEmpty()) {
                                                groceryView.tvPopular.text = "Popular Of Month"
                                                rvPopular.adapter = HomeGroceryMonthAdapter(
                                                    activity!!,
                                                    homeScreenData.data!!.popular_this_month,
                                                    this@GroceryFragment, this@GroceryFragment,
                                                    this@GroceryFragment,
                                                    this@GroceryFragment
                                                )
                                            } else {
                                                groceryView.tvPopular.text = ""
                                                rvPopular.visibility = View.GONE
                                            }

                                        } else {
                                            groceryView.tvPopular.text = ""
                                            rvPopular.visibility = View.GONE
                                        }
                                    } else {
                                        //CustomProgressbar.hideProgressBar()
                                    }
                                }
                                FAILURE -> {
                                    //CustomProgressbar.hideProgressBar()
                                    Toast(activity!!, "" + response.body()!!.message)
                                    showNoDataLayout(
                                        llParentNoData,
                                        getString(R.string.no_data_fnd)
                                    )
                                    groceryView.homeLayout.visibility = View.GONE


                                }
                                AUTH_FAILURE -> {
                                    //CustomProgressbar.hideProgressBar()
                                    showNoDataLayout(
                                        llParentNoData,
                                        getString(R.string.no_data_fnd)
                                    )
                                    groceryView.homeLayout.visibility = View.GONE

                                }
                            }
                        } else {
                            showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                            groceryView.homeLayout.visibility = View.GONE


                        }
                    } else {
                        //CustomProgressbar.hideProgressBar()
                        showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                        groceryView.homeLayout.visibility = View.GONE
                    }
                }
            })
    }

    override fun onMenuItemSelected(
        listBean: CartRestaurantsMenuModel,
        isProductVariance: Boolean,
        isRemove: Boolean
    ) {
        if (isRemove) {
            isProductAvailable(cartArrayList, listBean.foodmenu_id.toString(), listBean)
            cartArrayList.remove(listBean)
        } else {
            cartArrayList.add(listBean)
        }
        if (isProductVariance) {
        } else {
            activity?.let { SharedPref.savedata(it, cartArrayList) }
            activity?.let { SharedPref.saveArrayList(cartArrayList, "data", it) }
            /*if (user_id != "") {
                //addtocartAPI(listBean)
            } else {
                activity?.let { Toast(it, "Please login to add products into cart ") }
                val ine = Intent(activity, LoginPage::class.java)
                ine.putExtra(SCREEN_REDIRECTION, "1")
                startActivity(ine)
            }*/
        }
    }

    private fun isProductAvailable(
        cartArrayList: MutableList<CartRestaurantsMenuModel>,
        foodmenu_id: String,
        listBean: CartRestaurantsMenuModel
    ): Boolean {

        var isAvail = false

        for (cartModel in cartArrayList) {

            if (cartModel.foodmenu_id.equals(foodmenu_id)) {
                cartArrayList.remove(listBean)
                Log.e("removesize", cartArrayList.size.toString())
                isAvail = true
            }
        }

        return isAvail
    }

    override fun onVarianceDialogSelected(
        dataBean: List<HomeScreenModel.DataBean.PopularThisMonthBean.ProductVariationBean>,
        catId: String?,
        foodmenuId: String?,
        menuName: String?,
        taxPrice: String,
        tvCustom: TextView,
        tvRemove: TextView
    ) {
        openBottomSheetDialog(
            dataBean,
            catId,
            foodmenuId,
            menuName,
            taxPrice,
            tvAddToCart,
            tvRemove
        )
    }

    private fun openBottomSheetDialog(
        listBean: List<HomeScreenModel.DataBean.PopularThisMonthBean.ProductVariationBean>,
        catId: String?,
        foodmenuId: String?,
        menuName: String?,
        taxPrice: String,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {
        val view = layoutInflater.inflate(R.layout.bottom_sheet_variance, null)
        dialog = activity?.let { BottomSheetDialog(it) }
        dialog!!.setContentView(view)
        val varainceList = listBean
        (dialog as BottomSheetDialog).tvAddVariance.visibility = View.GONE
        dialog!!.imgClose.setOnClickListener {

            (dialog as BottomSheetDialog).cancel()
        }
        dialog!!.txtMenuName.text = menuName
        dialog!!.tvAddVariance.setOnClickListener {
            Log.e("dialog", "click")
            cartArrayList.add(varianceitem)
            addtocartAPI(varianceitem, tvAddToCart, tvRemove)
            dialog!!.cancel()
        }


        dialog!!.show()

        if (listBean.size > 0) {
            val mLayoutManager = LinearLayoutManager(activity)
            dialog!!.rvVariance!!.layoutManager = mLayoutManager
            dialog!!.rvVariance!!.itemAnimator = DefaultItemAnimator()
            val varianceAdapter = activity?.let {
                ProductHomeVarianceAdapter(
                    it,
                    varainceList,
                    this,
                    catId,
                    foodmenuId,
                    menuName,
                    taxPrice
                )
            }
            dialog!!.rvVariance.adapter = varianceAdapter


        }

    }

    private fun addtocartAPI(
        listBean: CartRestaurantsMenuModel,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {

        CustomProgressbar.showProgressBar(activity!!, false)

        val map = HashMap<String, String>()
        map["restaurant_id"] = listBean.cat_id.toString()
        map["food_menu_id"] = listBean.foodmenu_id.toString()
        map["food_menu_name"] = listBean.menu_name.toString()
        map["variation_id"] = listBean.variation_id.toString()
        map["variation_name"] = listBean.variation_name.toString()
        map["user_id"] = user_id

        RetrofitClientSingleton
            .getInstance()
            .addtoCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("RestaurantDetail", t.message)
                }

                override fun onResponse(
                    call: Call<CommonModel>,
                    response: Response<CommonModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        // val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //top
                                tvAddToCart.visibility = View.VISIBLE
                                //tvRemove.visibility=View.VISIBLE
                                Toast(activity!!, "" + response.body()!!.message)
                                getGrocerryData()
                            }
                            FAILURE -> {
                                dialogForAddCart(
                                    response.body()!!.message.toString(),
                                    listBean,
                                    tvAddToCart,
                                    tvRemove
                                )

                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    } else {
                        Log.e("HOME FRAGMENT", response.body().toString())
                    }
                }
            })
    }

    private fun dialogForAddCart(
        message: String,
        listBean: CartRestaurantsMenuModel,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {
        val itemDialog = Dialog(activity!!)
        itemDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        itemDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        itemDialog.setContentView(R.layout.dialog_logout)
        itemDialog.tvLogoutDialogTitle.text = message
        itemDialog.show()

        itemDialog.btn_cencle.setOnClickListener { itemDialog.dismiss() }

        itemDialog.btn_ok.setOnClickListener {
            clearCart(listBean, tvAddToCart, tvRemove)
            itemDialog.dismiss()
            getGrocerryData()

        }

        /* val builder = AlertDialog.Builder(activity, R.style.AlertDialogCustom)

         // Display a message on alert dialog
         builder.setMessage(message)

         // Set a positive button and its click listener on alert dialog
         builder.setPositiveButton("Yes") { dialog, which ->
             // Do something when user press the positive button

             getDashboardApi()
             dialog.cancel()
         }

         builder.setNegativeButton("No") { dialog, which ->

             // Do something when user press the positive button

             dialog.cancel()
         }
         val dialog: AlertDialog = builder.create()

         // Display the alert dialog on app interface
         dialog.show()*/
    }

    private fun clearCart(
        listBean: CartRestaurantsMenuModel,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {
        CustomProgressbar.showProgressBar(activity, false)

        val map = HashMap<String, String>()
        map["user_id"] = user_id

        CustomProgressbar.showProgressBar(activity, false)
        RetrofitClientSingleton
            .getInstance()
            .clearCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(
                    call: Call<CommonModel>,
                    response: Response<CommonModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        /*   val homeScreenData = response.body()*/
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //activity?.let { Toast(it, "" + response.body()!!.message) }
                                addtocartAPI(listBean, tvAddToCart, tvRemove)
                            }
                            FAILURE -> {
                                activity?.let { Toast(it, "" + response.body()!!.message) }
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("Cart clear", response.body().toString())
                }
            })
    }

    override fun onVarianceItemSelected(item: CartRestaurantsMenuModel) {
        varianceitem = item
        dialog!!.tvAddVariance.visibility = View.VISIBLE
        Log.e("variance", item.variation_name)

    }

    override fun onLocationChanged(location: Location?) {
        /*  latitude = location!!.latitude
          longitude = location.longitude*/

    }

    override fun adapterFavouriteItemPosition(dataList: HomeScreenModel.DataBean.FavFoodListBean) {
        if (dataList.restaurant_id != null && dataList.restaurant_id != "") {
            val intent = Intent(activity, TopRestaurantsPage::class.java)
            intent.putExtra("PageName", "Detail")
            intent.putExtra(RESTAURANT_ID_PASS, dataList.restaurant_id)
            activity!!.startActivity(intent)
        }
    }

    override fun adapterItemPosition(
        id: String?,
        dataList: HomeScreenModel.DataBean.TopRestaurantListBean
    ) {
        val intent = Intent(activity, TopRestaurantsPage::class.java)
        intent.putExtra("PageName", "Detail")
        intent.putExtra(RESTAURANT_ID_PASS, dataList.id)
        intent.putExtra(USER_ID, SharedPref.getUserId(activity!!))
        activity!!.startActivity(intent)
    }

    override fun onPopularItemSelected(listBean: HomeScreenModel.DataBean.PopularThisMonthBean) {
        val intent = Intent(activity, TopRestaurantsPage::class.java)
        intent.putExtra("PageName", "Detail")
        intent.putExtra(RESTAURANT_ID_PASS, listBean.kitchen_id)
        intent.putExtra(USER_ID, SharedPref.getUserId(activity!!))
        activity!!.startActivity(intent)
    }

    companion object {
        private const val PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 100
    }

}