package com.attune.zamy.fragment

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.activity.CheckOutActivity
import com.attune.zamy.activity.MainActivity
import com.attune.zamy.activity.PromoCodeActivity
import com.attune.zamy.activity.VerificationMobile
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.ApplyPromoCode
import com.attune.zamy.model.CartListModel
import com.attune.zamy.model.CommonModel
import com.attune.zamy.model.RemoveCoupanModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import com.facebook.shimmer.ShimmerFrameLayout
import kotlinx.android.synthetic.main.cart_contain.view.*
import kotlinx.android.synthetic.main.cart_contain.view.rvCartList
import kotlinx.android.synthetic.main.cartbottom_sheet.view.ApplyPromoCode
import kotlinx.android.synthetic.main.cartbottom_sheet.view.RemovePromoCode
import kotlinx.android.synthetic.main.cartbottom_sheet.view.edPromoCode
import kotlinx.android.synthetic.main.cartbottom_sheet.view.llPromo
import kotlinx.android.synthetic.main.cartbottom_sheet.view.tvCarttotal
import kotlinx.android.synthetic.main.cartbottom_sheet.view.tvDeliveryCharge
import kotlinx.android.synthetic.main.cartbottom_sheet.view.tvGstNew
import kotlinx.android.synthetic.main.cartbottom_sheet.view.tvPrmoCodeDiscount
import kotlinx.android.synthetic.main.cartbottom_sheet.view.tvProcess
import kotlinx.android.synthetic.main.cartbottom_sheet.view.tvProfile
import kotlinx.android.synthetic.main.cartbottom_sheet.view.tvSubTotal
import kotlinx.android.synthetic.main.cartbottom_sheet.view.txtComplementry
import kotlinx.android.synthetic.main.dialog_logout.*
import kotlinx.android.synthetic.main.empty_cart_layout.view.*
import kotlinx.android.synthetic.main.fragment_cart.view.*
import kotlinx.android.synthetic.main.fragmrnt_toolbar.view.*
import retrofit2.Call
import retrofit2.Response
import kotlin.math.ceil


class CartToolbarFragment : androidx.fragment.app.Fragment() {

    private lateinit var restaurant_id: String
    lateinit var datumList: MutableList<CartListModel.DataBean.ItemsBean>
    var finalTotalValue: Double = 0.0
    private lateinit var rvMenu: RecyclerView
    lateinit var cartAdapter: CartAdapter
    lateinit var userId: String
    lateinit var mView: View
    var coupanCode: String = ""
    var totalValue: String = ""
    var delivery_charges: String = "0"
    var first = 1
    var userPhone = ""
    var minimum_order = ""
    lateinit var screenName: String
    lateinit var shimmerCart: ShimmerFrameLayout
    lateinit var mainCart: ConstraintLayout

    lateinit var btncnt: AppCompatTextView


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_cart_toolbar, null)
        userId = SharedPref.getUserId(activity!!)
        shimmerCart = mView.findViewById(R.id.shimmerCart)
        mainCart = mView.findViewById(R.id.mainCart)
        initView()


        if (mView.edPromoCode.text!!.isNotEmpty()) {
            mView.ApplyPromoCode.visibility = View.GONE
            mView.RemovePromoCode.visibility = View.VISIBLE
        } else {
            mView.ApplyPromoCode.visibility = View.VISIBLE
            mView.RemovePromoCode.visibility = View.GONE
        }


        return mView
    }


    private fun initView() {
        datumList = ArrayList()
        mView.tvProfile.setOnClickListener {
            if (userPhone != "") {
                val intent = Intent(activity, CheckOutActivity::class.java)

                activity!!.startActivity(intent)
            } else {
                val intent = Intent(activity, VerificationMobile::class.java)
                intent.putExtra("carttoolbarfragment", 2)
                activity!!.startActivity(intent)
            }
        }
        mView.ToolBartitle.text = "Cart"
        mView.imgback.visibility = View.VISIBLE


        val args = arguments

        if (args!!.equals("0")) {
            screenName = "Main"
        } else {
            screenName = "Details"
        }

        Log.e("TAG", "initView: $args")



        mView.imgback.setOnClickListener {
            //activity!!.onBackPressed()
            /* val args = Bundle()
             args.putString(RESTAURANT_ID_PASS, restaurant_id)
             val toFragment = RestaurantsDetailsFragment()
             toFragment.arguments = args
             fragmentManager!!
                 .beginTransaction()
                 .replace(R.id.fragment_restaurant, toFragment, "RestaurantsDetailsFragment")
                 .commit()*/
        }
        mView.imgback.visibility = View.GONE

        if (args != null) {
            restaurant_id = args.getString(RESTAURANT_ID_PASS)!!
        }
        userId = activity?.let { SharedPref.getUserId(it) }!!
        mView.tvProcess.setOnClickListener {
            if (totalValue.toDouble() >= minimum_order.toDouble()) {
                val intent = Intent(activity, CheckOutActivity::class.java)
                intent.putExtra(RESTAURANT_ID_PASS, restaurant_id)
                activity!!.startActivity(intent)
            } else {
                alertDialog(
                    activity!!,
                    "Minimum order amount ${getString(R.string.rs)}$minimum_order is required"
                )
            }
        }
        /* var cartList =activity?.let { SharedPref.getArrayListData("data", it) }
         Log.e("cartsize", cartList!!.size.toString())*/
        mView.imgDelete.setOnClickListener {
            if (datumList.size > 0)
                showCartRemoveDialog()
        }
        bindRecyclerview()
        val statusBarHeight =
            ceil((25 * context!!.resources.displayMetrics.density).toDouble()).toInt()
        val cartToolbar = mView.findViewById<View>(R.id.cartToolbar)
        cartToolbar.imgback.setOnClickListener {

        }
        /* val param = cartToolbar.layoutParams as LinearLayout.LayoutParams
         param.setMargins(0, statusBarHeight, 0, 0)
         cartToolbar.layoutParams = param*/
        Log.e("TAG", "Height of$statusBarHeight")
        /*  mView.imgInfo.setOnClickListener {
              val LoginIntent = Intent(activity, PromoCodeActivity::class.java)
              LoginIntent.putExtra(RESTAURANT_ID_PASS, restaurant_id)
              activity!!.startActivityForResult(LoginIntent, 5)
          }*/
        mView.RemovePromoCode.setOnClickListener {
            removePromoCode()
        }

        mView.ApplyPromoCode.setOnClickListener {
            /*  if (mView.btnApplayPromoCode.text.equals("Apply")) {

                  if (!edPromoCode.text!!.isEmpty()) {
                      coupanCode = edPromoCode.text.toString()
                      applyPromoCode()
                  } else {
                      Toast(activity!!, "Please enter Promo Code")
                  }
              } else {
                  if (!edPromoCode.text!!.isEmpty()) {
                      coupanCode = edPromoCode.text.toString()
                      removePromoCode()
                  } else {
                      Toast(activity!!, "Please enter Promo Code")
                  }
              }*/
            val LoginIntent = Intent(activity, PromoCodeActivity::class.java)
            LoginIntent.putExtra("CartToolbarFragment", false)
            LoginIntent.putExtra(RESTAURANT_ID_PASS, restaurant_id)
            activity!!.startActivityForResult(LoginIntent, 5)
        }
    }


    private fun removePromoCode() {
        // CustomProgressbar.showProgressBar(activity!!, false)
        val map = HashMap<String, String>()
        map["user_id"] = userId
        map["cart_total"] = totalValue
        map["delivery_fees"] = delivery_charges
        RetrofitClientSingleton
            .getInstance()
            .removePromoCode(map)
            .enqueue(object : retrofit2.Callback<RemoveCoupanModel> {
                override fun onFailure(call: Call<RemoveCoupanModel>, t: Throwable) {
                    //   CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(
                    call: Call<RemoveCoupanModel>,
                    response: Response<RemoveCoupanModel>
                ) {
                    // CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        /*   val homeScreenData = response.body()*/
                        when (response.body()!!.status) {
                            SUCCESS -> {

                                activity?.let { Toast(it, "" + response.body()!!.message) }
//                                mView.btnApplayPromoCode.text = "Apply"
                                mView.edPromoCode.setText("")
                                //    mView.tvPrmoCodeDiscount.setText("")
                                //   mView.tvSubTotal.setText(activity!!.resources.getString(R.string.rupees) +" "+response.body()!!.data)
                                //mView.imgInfo.visibility = View.VISIBLE
                                mView.RemovePromoCode.visibility = View.GONE
                                mView.ApplyPromoCode.visibility = View.VISIBLE
                                getCartList()
                            }
                            FAILURE -> {
                                activity?.let { Toast(it, "" + response.body()!!.message) }
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("Cart clear", response.body().toString())
                }
            })

    }

    override fun onStop() {
        shimmerCart.stopShimmer()
        super.onStop()
    }

    override fun onResume() {
        super.onResume()

        btncnt=mView.findViewById(R.id.btncnt)
//        btncnt.setText("aaa")
        btncnt.setOnClickListener {
            val mIntent = Intent(activity!!, MainActivity::class.java)
            startActivity(mIntent)
        }
        shimmerCart.startLayoutAnimation()
        getCartList()
        userId = SharedPref.getUserId(activity!!)
        try {
            LocalBroadcastManager.getInstance(activity!!).unregisterReceiver(
                mMessageReceiver
            )
        } catch (e: Exception) {

            Log.e("TAG", "$e")
        }

        try {
            LocalBroadcastManager.getInstance(activity!!).registerReceiver(
                mMessageReceiver,
                IntentFilter(APPLY_COUPAN)
            )
        } catch (e: Exception) {

            Log.e("TAG", "$e")
        }


    }

    // Our handler for received Intents. This will be called whenever an Intent
    // with an action named "custom-event-name" is broadcasted.
    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            // Get extra data included in the Intent
            /* coupanCode = intent.getStringExtra(PROMOCODE)
             restaurant_id = intent.getStringExtra(RESTAURANT_ID_PASS)
             if (coupanCode != null) {
                 mView.edPromoCode.setText(coupanCode)
                 Log.d(
                     "receiver",
                     "Got message: $coupanCode"
                 )
             }*/
            coupanCode = intent.getStringExtra(PROMOCODE)
            restaurant_id = intent.getStringExtra(RESTAURANT_ID_PASS)
            mView.edPromoCode.setText(coupanCode)
            Log.d("receiver", "Got message: $coupanCode")
            myApplyPromoCode()
        }
    }

    private fun myApplyPromoCode() {
        // CustomProgressbar.showProgressBar(activity, false)
        val map = HashMap<String, String>()
        map["user_id"] = userId
        map["coupon_code"] = coupanCode
        map["cart_total"] = totalValue
        map["restaurant_id"] = restaurant_id
        RetrofitClientSingleton.getInstance()
            .applyPromoCode(map)
            .enqueue(object : retrofit2.Callback<ApplyPromoCode> {
                override fun onFailure(call: Call<ApplyPromoCode>?, t: Throwable?) {
                    //CustomProgressbar.hideProgressBar()
                    Toast(activity!!, resources.getString(R.string.something_went_wrong))
                    mView.edPromoCode.text!!.clear()
                }

                override fun onResponse(
                    call: Call<ApplyPromoCode>?,
                    response: Response<ApplyPromoCode>?
                ) {
                    //CustomProgressbar.hideProgressBar()
                    if (response?.isSuccessful!!) {
                        try {
                            when (response.body()?.success) {
                                SUCCESS -> {
                                    val subtotal = response.body()!!.data!!.sub_total
                                    val discount = response.body()!!.data!!.discount_amount
                                    val total = response.body()!!.data!!.total

                                    mView.RemovePromoCode.visibility = View.VISIBLE
                                    mView.ApplyPromoCode.visibility = View.GONE
                                    mView.edPromoCode.isEnabled = false
                                    getCartList()
                                }
                                FAILURE -> {
                                    Toast(activity!!, response.body()?.msg)
                                    mView.edPromoCode.text!!.clear()
                                }
                                AUTH_FAILURE -> {
                                    //forceLogout(this@PromoCodeActivity)
                                }
                            }
                        } catch (e: Exception) {
                            Log.e("TAG", e.message.toString())
                        }
                    } else {
                        Toast(activity!!, response.body()?.msg)
                        mView.edPromoCode.text!!.clear()
                    }
                }

            })
    }

    private fun bindRecyclerview() {

        val mLayoutManager = LinearLayoutManager(activity)
        mView.rvCartList!!.layoutManager = mLayoutManager
        mView.rvCartList!!.itemAnimator = DefaultItemAnimator()
        if (!activity?.let { isNetworkAvailable(it) }!!) {
            activity?.let { Toast(it, getString(R.string.network_error)) }
        } else {
            getCartList()
        }


    }

    private fun getCartList() {
        //CustomProgressbar.showProgressBar(activity, false)


        val map = HashMap<String, String>()
        map["user_id"] = userId

        RetrofitClientSingleton
            .getInstance()
            .getCartList(map)
            .enqueue(object : retrofit2.Callback<CartListModel> {
                override fun onFailure(call: Call<CartListModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    activity?.let { Toast(it, t.message) }

                }

                override fun onResponse(
                    call: Call<CartListModel>,
                    response: Response<CartListModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        shimmerCart.stopShimmer()
                        shimmerCart.hideShimmer()
                        mainCart.visibility=View.VISIBLE

                        response.body()!!.data!!
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                restaurant_id = response.body()!!.data!!.id.toString()
                                if (response.body()!!.data!!.user_phone.equals("")) {
                                    mView.tvProcess.visibility = View.INVISIBLE
                                    mView.tvProfile.visibility = View.VISIBLE

                                } else {
                                    mView.tvProcess.visibility = View.VISIBLE
                                    mView.tvProfile.visibility = View.GONE

                                }
                                first = first + 1
                                val cartDataResponse = response.body()
                                datumList = cartDataResponse!!.data!!.items!!
                                try {
                                    cartAdapter = CartAdapter(datumList, activity!!)
                                } catch (e: Exception) {

                                }


                                totalValue = cartDataResponse.data!!.total.toString()
                                minimum_order = cartDataResponse.data!!.min_order_amount.toString()
                                delivery_charges =
                                    cartDataResponse.data!!.delivery_charge.toString()
                                mView.tvDeliveryCharge.text =
                                    getString(R.string.rupees) + delivery_charges
                                mView.tvCarttotal.text =
                                    getString(R.string.rupees) + cartDataResponse.data!!.total
                                mView.tvGstNew.text =
                                    getString(R.string.rupees) + cartDataResponse.data!!.gst
                                mView.tvSubTotal.text =
                                    getString(R.string.rupees) + cartDataResponse.data!!.total_amount
                                mView.tvPrmoCodeDiscount.text =
                                    getString(R.string.rupees) + cartDataResponse.data!!.coupon_amount


                                mView.edPromoCode.setText(cartDataResponse.data!!.coupon_code)
                                if (cartDataResponse.data!!.coupon_code != null
                                    && cartDataResponse.data!!.coupon_code != ""
                                ) {
//                                    mView.btnApplayPromoCode.text = "Remove"

                                    // mView.imgInfo.visibility = View.GONE
                                }
                                mView.rvCartList!!.adapter = cartAdapter

                                cartAdapter.notifyDataSetChanged()
                                if (cartDataResponse.data!!.complementery!!.length > 0) {
                                    mView.txtComplementry.text =
                                        "*" + cartDataResponse.data!!.complementery
                                } else {

                                }
                                if (datumList.isNotEmpty()) {
                                    mView.lnEmpptyCart.visibility = View.GONE
                                    // mView.llPromo.visibility=View.VISIBLE
                                    mView.rvCartList.visibility = View.VISIBLE


                                } else {
                                    mView.rvCartList.visibility = View.GONE
                                    // mView.llPromo.visibility=View.GONE
                                    mView.lnEmpptyCart.visibility = View.VISIBLE

                                }


                            }
                            FAILURE -> {
                                mView.rvCartList.visibility = View.GONE
                                mView.llPromo.visibility = View.GONE
                                mView.lnEmpptyCart.visibility = View.VISIBLE

                            }
                            AUTH_FAILURE -> {
                                mView.rvCartList.visibility = View.GONE
                                mView.llPromo.visibility = View.GONE
                                mView.lnEmpptyCart.visibility = View.VISIBLE
                            }
                        }

                    } else {
                        mView.rvCartList.visibility = View.GONE
                        mView.llPromo.visibility = View.GONE
                        mView.lnEmpptyCart.visibility = View.VISIBLE
                    }

                }

            })
    }

    /*  private fun removePromoCode() {
          CustomProgressbar.showProgressBar(activity!!, false)

          val map = HashMap<String, String>()
          map["user_id"] = userId
          map["cart_total"] = totalValue
          map["delivery_fees"] = delivery_charges

          CustomProgressbar.showProgressBar(context, false)
          RetrofitClientSingleton
              .getInstance()
              .removePromoCode(map)
              .enqueue(object : retrofit2.Callback<RemoveCoupanModel> {
                  override fun onFailure(call: Call<RemoveCoupanModel>, t: Throwable) {
                      CustomProgressbar.hideProgressBar()
                  }

                  override fun onResponse(call: Call<RemoveCoupanModel>, response: Response<RemoveCoupanModel>) {
                      CustomProgressbar.hideProgressBar()
                      if (response.isSuccessful) {
                          *//*   val homeScreenData = response.body()*//*
                        when (response.body()!!.status) {
                            SUCCESS -> {

                                activity?.let { Toast(it, "" + response.body()!!.message) }
                                mView.btnApplayPromoCode.setText("Apply")
                                mView.edPromoCode.setText("")
                            }
                            FAILURE -> {
                                activity?.let { Toast(it, "" + response.body()!!.message) }
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("Cart clear", response.body().toString())
                }
            })

    }*/

    fun showCartRemoveDialog() {

        val mDialog = Dialog(activity!!)

        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialog.setContentView(R.layout.dialog_logout)
        mDialog.tvLogoutDialogTitle.text = getString(R.string.cart_delete_message)
        mDialog.show()

        mDialog.btn_cencle.setOnClickListener { mDialog.dismiss() }

        mDialog.btn_ok.setOnClickListener {
            mDialog.dismiss()
            cartClearAPI()
        }

    }

    fun sendBroadCastToDetail() {

    }

    private fun cartClearAPI() {
        //  CustomProgressbar.showProgressBar(activity!!, false)

        val map = HashMap<String, String>()
        map["user_id"] = userId
        RetrofitClientSingleton
            .getInstance()
            .clearCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    //CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    //CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                datumList.clear()
                                cartAdapter.notifyDataSetChanged()
                                sendBroadCastToDetail()
                                activity?.let {
                                    Toast(it, "" + response.body()!!.message)
                                    mView.rvCartList.visibility = View.GONE
                                    mView.llPromo.visibility = View.GONE
                                    mView.lnEmpptyCart.visibility = View.VISIBLE
                                }
                                getCartList()

                            }
                            FAILURE -> {
                                activity?.let { Toast(it, "" + response.body()!!.message) }
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }


    inner class CartAdapter(
        val cartList: MutableList<CartListModel.DataBean.ItemsBean>,
        var mcontext: Context
    ) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {


        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.row_cart_list, viewGroup, false)
            return ViewHolder(view)
        }


        override fun getItemCount(): Int {
            return cartList.size
            //return 50s
        }

        fun removeItem(position: Int) {
            cartList
        }

        fun removeAt(position: Int) {
            cartList.removeAt(position)
            notifyItemRemoved(position)
            //  notifyItemRangeChanged(position, cartList.size)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val dataList = this.cartList[position]
            if (dataList != null) {
                holder.txtItemName.text = dataList.menu_name
            }
            if (dataList.variation_name != null) {
                holder.tvSubTitle.text = dataList.variation_name
            }
            if (dataList.price != null) {
                dataList.price = dataList.price
                holder.tvCartPrice.text =
                    mcontext.getString(R.string.rupees) + " " + ((dataList.price!!.toDouble() * dataList.qty).toString())
                //tableData.pricetotal = tableData.price!!.toInt() * tableData.qty
                dataList.pricetotal = dataList.price!!.toDouble() * dataList.qty
                // holder.txtPrice.text = mcontext.getString(R.string.rupees)+tableData.pricetotal
            } else {
                dataList.price = 0.toString()
                holder.tvCartPrice.text = mcontext.getString(R.string.rupees) + " " + dataList.price
            }
            if (dataList.qty != null) {
                holder.txtCount.text = dataList.qty.toString()
            }
            updateTotal()
            holder.imgPlus.setOnClickListener {
                var mqty = dataList.qty + 1
                updateCartItems(
                    dataList,
                    position,
                    holder.adapterPosition,
                    holder.tvCartPrice,
                    true,
                    mqty.toString()
                )

            }
            holder.imgMinus.setOnClickListener {
                if (dataList.qty == 1) {
                    removeCartItem(dataList, holder.position, position)
                } else {
                    val mqty = dataList.qty - 1
                    updateCartItems(
                        dataList,
                        position,
                        holder.adapterPosition,
                        holder.tvCartPrice,
                        false,
                        mqty.toString()
                    )
                }


            }
            holder.imgRemove.setOnClickListener {
                //remove item api
                val mDialog = Dialog(activity!!)

                mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                mDialog.setContentView(R.layout.dialog_logout)
                mDialog.tvLogoutDialogTitle.text = getString(R.string.item_remove_message)
                mDialog.show()

                mDialog.btn_cencle.setOnClickListener { mDialog.dismiss() }

                mDialog.btn_ok.setOnClickListener {
                    mDialog.dismiss()
                    removeCartItem(dataList, position, holder.adapterPosition)
                }

                /*val builder = AlertDialog.Builder(mcontext, R.style.AlertDialogCustom)

                // Display a message on alert dialog
                builder.setMessage(getString(R.string.item_remove_message))

                // Set a positive button and its click listener on alert dialog
                builder.setPositiveButton("Yes") { dialog, which ->
                    // Do something when user press the positive button

                    removeCartItem(dataList, position, holder.adapterPosition)
                }
                builder.setNegativeButton("No") { dialog, which ->
                    // Do something when user press the positive button
                    dialog.cancel()

                }
                val dialog: AlertDialog = builder.create()

                // Display the alert dialog on app interface
                dialog.show()*/

            }

            if (dataList.menu_logo != null) {
                Glide.with(mcontext)
                    .load(dataList.menu_logo)
                    .error(R.drawable.ic_place_holder)
                    .into(holder.imgMenu)

            } else {
                Glide.with(mcontext)
                    .load(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(holder.imgMenu)
            }


        }

        private fun updateTotal() {
            // var total: Int = 0
            var total: Double = 0.00

            //var totalprice: Int
            var totalprice: Double
            for (i in 0 until cartList.size) {

                totalprice = cartList[i].pricetotal
                total = total + totalprice
            }


            finalTotalValue = total
            /* mView.tvSubTotal.text = getString(R.string.rupees) + " " + total
             mView.tvCarttotal.text = getString(R.string.rupees) + " " + total*/


        }

        private fun updateCartItems(
            listBean: CartListModel.DataBean.ItemsBean,
            position: Int,
            adapterPosition: Int,
            tvCartPrice: TextView,
            isAddorRemove: Boolean,
            qty: String
        ): Boolean {

            //CustomProgressbar.showProgressBar(activity!!, false)
            var status: Boolean = false
            val map = HashMap<String, String>()
            map["restaurant_id"] = listBean.restaurant_id.toString()
            map["food_menu_id"] = listBean.food_menu_id.toString()
            map["variation_id"] = listBean.variation_id.toString()
            map["qty"] = qty
            map["user_id"] = userId

            RetrofitClientSingleton
                .getInstance()
                .updateCart(map)
                .enqueue(object : retrofit2.Callback<CommonModel> {
                    override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                        //    CustomProgressbar.hideProgressBar()
                        Log.e("RestaurantDetail", t.message)
                        status = false
                    }

                    override fun onResponse(
                        call: Call<CommonModel>,
                        response: Response<CommonModel>
                    ) {
                        //    CustomProgressbar.hideProgressBar()
                        if (response.isSuccessful) {
                            // val homeScreenData = response.body()
                            when (response.body()!!.status) {
                                SUCCESS -> {
                                    //top
                                    Toast(activity!!, "" + response.body()!!.message)
                                    if (isAddorRemove) {
                                        var currentQTY: Int = listBean.qty.toInt()
                                        currentQTY = currentQTY + 1
                                        listBean.qty = currentQTY

                                        val revisedPrice =
                                            (listBean.price!!.toDouble() * (listBean.qty))
                                        //tableData.pricetotal = revisedPrice
                                        listBean.pricetotal = revisedPrice.toDouble()
                                        tvCartPrice.text =
                                            mcontext.getString(R.string.rupees) + " " + listBean.pricetotal


                                        cartAdapter.notifyDataSetChanged()

                                        //update total
                                        updateTotal()
                                        getCartList()
                                    } else {
                                        var currentQTY: Int = listBean.qty

                                        if (currentQTY == 1) {
                                            currentQTY = 1
                                        } else {
                                            currentQTY = currentQTY - 1
                                        }
                                        listBean.qty = currentQTY

                                        val revisedPrice =
                                            (listBean.price!!.toDouble() * (listBean.qty))
                                        //tableData.pricetotal = revisedPrice
                                        listBean.pricetotal = revisedPrice.toDouble()
                                        tvCartPrice.text =
                                            mcontext.getString(R.string.rupees) + " " + listBean.pricetotal


                                        cartAdapter.notifyDataSetChanged()

                                        //update total
                                        updateTotal()
                                        getCartList()
                                    }
                                }
                                FAILURE -> {
                                    Toast(activity!!, "" + response.body()!!.message)
                                    status = false
                                }
                                AUTH_FAILURE -> {
                                    status = false
                                }

                            }
                        }
                        status = false
                        Log.e("HOME FRAGMENT", response.body().toString())
                    }
                })
            return status
        }

        private fun removeCartItem(
            dataList: CartListModel.DataBean.ItemsBean,
            position: Int,
            adapterPosition: Int
        ) {

            //CustomProgressbar.showProgressBar(activity!!, false)

            val map = HashMap<String, String>()
            map["restaurant_id"] = dataList.restaurant_id.toString()
            map["food_menu_id"] = dataList.food_menu_id.toString()
            map["variation_id"] = dataList.variation_id.toString()
            map["user_id"] = userId

            RetrofitClientSingleton
                .getInstance()
                .removeItemfromCart(map)
                .enqueue(object : retrofit2.Callback<CommonModel> {
                    override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                        // CustomProgressbar.hideProgressBar()
                    }

                    override fun onResponse(
                        call: Call<CommonModel>,
                        response: Response<CommonModel>
                    ) {
                        //CustomProgressbar.hideProgressBar()
                        if (response.isSuccessful) {
                            val homeScreenData = response.body()
                            when (response.body()!!.status) {
                                SUCCESS -> {
                                    removeAt(adapterPosition)
                                    getCartList()
                                    /* cartAdapter.notifyItemRemoved(adapterPosition)

                                     notifyItemRemoved(adapterPosition)
                                     notifyItemRangeChanged(adapterPosition, cartList!!.size)*/
                                    Toast(mcontext, "" + response.body()!!.message)

                                }
                                FAILURE -> {
                                    Toast(mcontext, "" + response.body()!!.message)
                                }
                                AUTH_FAILURE -> {
                                }

                            }
                        }
                        Log.e("HOME FRAGMENT", response.body().toString())
                    }
                })

        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var txtItemName = itemView.findViewById<TextView>(R.id.tvItemName)
            var tvSubTitle = itemView.findViewById<TextView>(R.id.tvSubTitle)
            var tvCartPrice = itemView.findViewById<TextView>(R.id.tvCartPrice)
            var txtCount = itemView.findViewById<TextView>(R.id.txtCount)
            var imgPlus = itemView.findViewById<TextView>(R.id.imgPlus)
            var imgMinus = itemView.findViewById<TextView>(R.id.imgMinus)
            var imgRemove = itemView.findViewById<ImageView>(R.id.imgRemove)
            var imgMenu = itemView.findViewById<ImageView>(R.id.imgMenu)
        }
    }
}