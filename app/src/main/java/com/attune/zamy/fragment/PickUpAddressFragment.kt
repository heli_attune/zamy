package com.attune.zamy.fragment

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.attune.zamy.R
import com.attune.zamy.activity.Delivery2Activity
import com.attune.zamy.adapter.StoreAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.CalculateDistanceModel
import com.attune.zamy.model.StoreModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.activity_delivery1.*
import kotlinx.android.synthetic.main.fragment_pick_up_address.*
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class PickUpAddressFragment : Fragment() {

    lateinit var pickUpView: View
    lateinit var btndelivery1NextStep: MaterialButton
    lateinit var covidFragment: CovidFragment

    //getstore api
    lateinit var storeList: List<StoreModel.DataBean>

    //distance api
    private lateinit var strPickupAddress: String
    private lateinit var strDeliveryAddress: String
    private lateinit var distance: String
    private lateinit var amount: String
    private lateinit var latitudeFrom: String
    private lateinit var longitudeFrom: String
    private lateinit var latitudeTo: String
    private lateinit var longitudeTo: String

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        pickUpView = inflater.inflate(R.layout.fragment_pick_up_address, container, false)
        covidFragment= parentFragment as CovidFragment

        btndelivery1NextStep=pickUpView.findViewById(R.id.btnnext)
        btndelivery1NextStep.setOnClickListener {
            validation()
        }


        if (!activity?.let { isNetworkAvailable(it) }!!) {
            activity?.let { Toast(it, getString(R.string.network_error)) }
        } else {
            callgetStoreApi()
        }

        return pickUpView
    }

//    override fun onResume() {
//        super.onResume()
//        //pick up input layout
//         tilItemDetailsFragment.editText?.doOnTextChanged { text, start, count, after ->
//             when (text!!.length) {
//                 0 -> {
//                     tilItemDetails.error = "Please Enter Item Details"
//                 }
//                 else -> {
//                     tilItemDetails.error = ""
//                 }
//
//             }
//         }
//
//
//         tilPickupAddressFragment.editText?.doOnTextChanged { text, start, count, after ->
//             when (text!!.length) {
//                 0 -> {
//                     tilPickupAddressFragment.error = "Please Enter Pick up address"
//                 }
//                 in 8..50 -> {
//                     tilPickupAddressFragment.error = ""
//                     tilPickupAddressFragment.setEndIconTintMode(PorterDuff.Mode.CLEAR)
//                 }
//                 else -> {
//                     tilPickupAddressFragment.error = ""
//                     tilPickupAddressFragment.setEndIconTintMode(PorterDuff.Mode.CLEAR)
//                 }
//
//             }
//         }
//
//         tilPickUpPinCodeFragment.editText?.doOnTextChanged { text, start, count, after ->
//             when (text!!.length) {
//                 0 -> {
//                     tilPickUpPinCodeFragment.error = "Please Enter Pin Code"
//                 }
//                 in 1..5 -> {
//                     tilPickUpPinCodeFragment.error = "Please Enter Valid Pin Code"
//                 }
//                 else -> {
//                     tilPickUpPinCodeFragment.error = ""
//                     tilPickUpPinCodeFragment.setEndIconTintMode(PorterDuff.Mode.CLEAR)
//                 }
//
//             }
//         }
//
//         tnlPickupContactNoFragment.editText?.doOnTextChanged { text, start, count, after ->
//
//             when (text!!.length) {
//                 0 -> {
//                     tnlPickupContactNoFragment.error = "Please Enter Mobile"
//                 }
//                 in 1..9 -> {
//                     tnlPickupContactNoFragment.error = "Please Enter Valid Mobile"
//                 }
//                 else -> {
//                     tnlPickupContactNoFragment.error = ""
//                     tnlPickupContactNoFragment.setEndIconTintMode(PorterDuff.Mode.CLEAR)
//                 }
//
//             }
//         }
//
//         //delivery input layout
//         tilDeliveryAddressFragment.editText?.doOnTextChanged { text, start, count, after ->
//
//             when (text!!.length) {
//                 0 -> {
//                     tilDeliveryAddressFragment.error = "Please Enter Delivery Address"
//                 }
//                 else -> {
//                     tilDeliveryAddressFragment.error = ""
//                     tilDeliveryAddressFragment.setEndIconTintMode(PorterDuff.Mode.CLEAR)
//                 }
//
//             }
//         }
//
//         tilDeliveryPinCodeFragment.editText?.doOnTextChanged { text, start, count, after ->
//             when (text!!.length) {
//                 0 -> {
//                     tilDeliveryPinCodeFragment.error = "Please Enter Pin Code"
//                 }
//                 in 1..5 -> {
//                     tilDeliveryPinCodeFragment.error = "Please Enter Valid Pin Code"
//                 }
//                 else -> {
//                     tilDeliveryPinCodeFragment.error = ""
//                     tilDeliveryPinCodeFragment.setEndIconTintMode(PorterDuff.Mode.CLEAR)
//                 }
//
//             }
//         }
//
//         tilDeliveryContactNoFragment.editText?.doOnTextChanged { text, start, count, after ->
//
//             when (text!!.length) {
//                 0 -> {
//                     tilDeliveryContactNoFragment.error = "Please Enter Mobile"
//                 }
//                 in 1..9 -> {
//                     tilDeliveryContactNoFragment.error = "Please Enter Valid Mobile"
//                 }
//                 else -> {
//                     tilDeliveryContactNoFragment.error = ""
//                     tilDeliveryContactNoFragment.setEndIconTintMode(PorterDuff.Mode.CLEAR)
//                 }
//
//             }
//         }
//    }

    private fun sendBroadcast(Pickupaddress: String) {
        val intent = Intent("CalculateDistance")
        intent.putExtra(
            "user_id",
            SharedPref.getUserId(activity!!)
        )
        intent.putExtra("store", actvstorenameFragment.text.toString())
        intent.putExtra(
            "item_details",
            edItemdetailsforpickupFragment?.text.toString()
        )
        intent.putExtra("pickup_address", edPickupaddressFragment?.text.toString())
        intent.putExtra("pickup_pincode", edPincodeFragment?.text.toString())
        intent.putExtra(
            "pickup_contact_no",
            edPickupcontactnoFragment?.text.toString()
        )
        intent.putExtra(
            "delivery_address",
            edDeliveryaddressFragment?.text.toString()
        )
        intent.putExtra(
            "delivery_pincode",
            edDeliveryPincodeFragment?.text.toString()
        )
        intent.putExtra(
            "delivery_contact_no",
            edDeliverycontactnoFragment?.text.toString()
        )
        intent.putExtra(
            "from_to_distance",
            distance
        )
        intent.putExtra(
            "km_amount",
            amount
        )
        intent.putExtra(
            "pickup_lattitude",latitudeFrom
        )
        intent.putExtra(
            "pickup_longitude",longitudeFrom
        )
        intent.putExtra(
            "delivery_lattitude",latitudeTo
        )
        intent.putExtra(
            "delivery_longitude",longitudeTo
        )
        LocalBroadcastManager.getInstance(activity!!).sendBroadcast(intent)
    }

    private fun validation() {
        if (edItemdetailsforpickup?.text.toString().trim().isEmpty()) {
            tilItemDetailsFragment.error = "Please Enter Pick Up Item"
        } else if (edPickupaddressFragment?.text.toString().trim().isEmpty()) {
            tilPickupAddressFragment.error = getString(R.string.paddress)
        } else if (edPincodeFragment.text.toString().trim().isEmpty()) {
            tilPickUpPinCodeFragment.error = "Please Enter Pin Code"
        } else if (edPickupcontactnoFragment.text.toString().trim().isEmpty()) {
            tnlPickupContactNoFragment.error = "Please Enter Pick Up Contact"
        } else if (edDeliveryaddressFragment?.text.toString().trim().isEmpty()) {
            tilDeliveryAddressFragment.error = getString(R.string.daddress)
        } else if (edDeliveryPincodeFragment.text.toString().trim().isEmpty()) {
            tilDeliveryPinCodeFragment.error = "Please Enter Delivery Pin Code"
        } else if (edDeliverycontactnoFragment.text.toString().trim().isEmpty()) {
            tilDeliveryContactNoFragment.error = "Please Enter Delivery Contact Number"
        } else {
            strPickupAddress = edPickupaddressFragment?.text.toString()
            strDeliveryAddress = edDeliveryaddressFragment?.text.toString()
            if (!isNetworkAvailable(activity!!)) {
                Toast(activity!!, getString(R.string.network_error))
            } else {
                CalculateDistance()
            }
        }

    }

    private fun callgetStoreApi() {
        CustomProgressbar.showProgressBar(activity
            , false
        )
        RetrofitClientSingleton.getInstance().getStore()
            .enqueue(object : retrofit2.Callback<StoreModel> {
                override fun onFailure(call: Call<StoreModel>?, t: Throwable?) {
                    CustomProgressbar.hideProgressBar()
                    Toast(activity!!, getString(R.string.something_went_wrong))
                }

                override fun onResponse(call: Call<StoreModel>?, response: Response<StoreModel>?) {
                    CustomProgressbar.hideProgressBar()
                    if (response!!.isSuccessful) {
                        val categoryList = response.body()
                        storeList = response.body()!!.data!!
                        when (response.body()?.status) {
                            SUCCESS -> {
                                if (response.isSuccessful)
                                {
                                    //spinner
                                    val adapter = StoreAdapter(activity!!,
                                        storeList as ArrayList<StoreModel.DataBean>
                                    )
//                                    spstore.adapter = adapter
                                    //custom list adapter
                                    /*val AutoCompleteAdapter =
                                        StoreNameAdapter(this@DeliveryActivity,
                                            storeList as ArrayList<StoreModel.DataBean>
                                        )
                                    actvstorename.setAdapter(AutoCompleteAdapter)*/

                                    //static list adapter
                                    // Get the string array
//                                    val countries: Array<out String> = resources.getStringArray(R.array.countries_array)
                                    val countries: List<String?> =storeList.map { it.res_name }
                                    // Create the adapter and set it to the AutoCompleteTextView
                                    ArrayAdapter<String>(activity!!, android.R.layout.simple_list_item_1, countries).also { adapter ->
                                        actvstorenameFragment.setAdapter(adapter)
                                    }

                                } else {
                                    Toast(
                                        activity!!,
                                        getString(R.string.something_went_wrong)
                                    )
                                }

                                /**Set item at position using predefine**/
                                /* if (isEdit) {
                                     spShippingArea.setSelection(
                                         getIndex(
                                             spShippingArea,
                                             mAddressObject.shipping_area.toString(),
                                             shippingAreaList
                                         )
                                     )
                                 }*/

                            }
                            FAILURE -> {
                                Toast(activity!!, categoryList!!.message)
                            }
                            AUTH_FAILURE -> {
                                //forceLogout(this@ComposeTicketActivity)
                            }
                        }
                    }
                }

            })
    }

    private fun CalculateDistance() {
        CustomProgressbar.showProgressBar(activity!!, false)
        val map = HashMap<String, String>()
        map["pickup_address"] = strPickupAddress
        map["delivery_address"] = strDeliveryAddress
        RetrofitClientSingleton.getInstance().CalculateDistanceApi(map)
            .enqueue(object : retrofit2.Callback<CalculateDistanceModel> {
                override fun onFailure(call: Call<CalculateDistanceModel>, t: Throwable) {
                    Toast(activity!!, t.message)
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(
                    call: Call<CalculateDistanceModel>,
                    response: Response<CalculateDistanceModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    val CalculateDistanceResponse = response.body()
                    if (CalculateDistanceResponse != null) {
                        when (CalculateDistanceResponse.status) {
                            SUCCESS -> {
                                covidFragment.getPagePosition().currentItem=1
                                val userBean = CalculateDistanceResponse.data
                                userBean?.let {

                                }
                                distance=CalculateDistanceResponse.data?.distance!!
                                amount=CalculateDistanceResponse.data?.amount!!
                                latitudeFrom=CalculateDistanceResponse.data?.latitudeFrom!!
                                latitudeTo=CalculateDistanceResponse.data?.longitudeFrom!!
                                longitudeFrom=CalculateDistanceResponse.data?.latitudeTo!!
                                longitudeTo=CalculateDistanceResponse.data?.longitudeTo!!
                                sendBroadcast(strPickupAddress)
                            }
                            FAILURE -> {
                                Toast(activity!!, CalculateDistanceResponse.message!!)
                            }
                            else -> {
                                //toast(this@LoginPage, R.string.something_went_wrong)
                            }
                        }
                    }
                }
            })
    }

}