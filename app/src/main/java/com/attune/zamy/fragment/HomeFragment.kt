package com.attune.zamy.fragment


import SliderPagerAdapter
import android.Manifest.permission.ACCESS_COARSE_LOCATION
import android.Manifest.permission.ACCESS_FINE_LOCATION
import android.app.Dialog
import android.content.Intent
import android.content.pm.PackageManager
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.LayerDrawable
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat.checkSelfPermission
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.BuildConfig
import com.attune.zamy.R
import com.attune.zamy.activity.LoginPage
import com.attune.zamy.activity.MainActivity
import com.attune.zamy.activity.TopRestaurantsPage
import com.attune.zamy.adapter.*
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.custom.LocationTrack
import com.attune.zamy.interaface.onItemClickListnerTopResturants
import com.attune.zamy.interaface.onItemVarianceSelected
import com.attune.zamy.model.*
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.gms.location.*
import com.google.android.material.bottomsheet.BottomSheetDialog
import com.hopmeal.android.utils.REQUEST_LOCATION
import com.hopmeal.android.utils.cartCount
import kotlinx.android.synthetic.main.bottom_sheet_variance.*
import kotlinx.android.synthetic.main.dialog_logout.*
import kotlinx.android.synthetic.main.fragment_home.view.*
import kotlinx.android.synthetic.main.home_layout.view.*
import kotlinx.android.synthetic.main.home_popular_item_row.*
import kotlinx.android.synthetic.main.layout_no_network.view.*
import kotlinx.android.synthetic.main.no_data_layout.*
import kotlinx.android.synthetic.main.no_data_layout.view.*
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap


class HomeFragment : androidx.fragment.app.Fragment(),
    onItemClickListnerTopResturants,
    HomePopularMonthAdapter.PopularItemClickListener,
    HomePopularMonthAdapter.AddtoCartItemClickListener,
    HomePopularMonthAdapter.VarianceDialogClickListener,
    HomeFavoriteFoodAdapter.onItemClickListnerFavResturants,
    onItemVarianceSelected,
    LocationListener {
    var mainActivity = MainActivity()
    var dialog: Dialog? = null
    var user_id = ""
    var cartArrayList: MutableList<CartRestaurantsMenuModel> = ArrayList()
    var longitude: Double = 0.0
    var latitude: Double = 0.0
    private var permissionsToRequest = ArrayList<String>()
    private val permissions = ArrayList<String>()
    var locationTrack: LocationTrack? = null
    lateinit var mview: View
    lateinit var rvTopRestaurants: RecyclerView
    lateinit var rvFavouriteRestaurants: RecyclerView
    lateinit var rvHomeCategory: RecyclerView
    lateinit var rvOfferRestaurants: RecyclerView
    lateinit var rvPopular: RecyclerView
    private lateinit var varianceitem: CartRestaurantsMenuModel
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var mSettingsClient: SettingsClient? = null
    lateinit var cartBadge: LayerDrawable

    lateinit var shimmerHomeLayout: ShimmerFrameLayout
    lateinit var restaurantId: String
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mview = inflater.inflate(R.layout.fragment_home, null)
        mainActivity = activity as MainActivity
        permissions.add(ACCESS_FINE_LOCATION)
        permissions.add(ACCESS_COARSE_LOCATION)
        user_id = SharedPref.getUserId(activity!!)
        mainActivity.showToolbar()

        shimmerHomeLayout=mview.findViewById(R.id.homeShimmer)
        initView()

        /*  val homeViewModel = ViewModelProviders.of(requireActivity()).get(HomeViewModel::class.java)

          homeViewModel.dashBoardsDataLive!!.observe(viewLifecycleOwner, Observer {

          })*/

        //mainActivity.cartSet(mainActivity.getCount())*/

        /**Api Call**/
        //mainActivity.hideNav()
        //geocoder = Geocoder(activity!!, Locale.getDefault())

        val permission = checkSelfPermission(activity!!, ACCESS_FINE_LOCATION)

        if (permission != PackageManager.PERMISSION_GRANTED) {
            Log.e("TAG", " Permission to record denied ")
        } else {
            findUnAskedPermissions(permissions)
        }

        mview.homeSwiperefreshlayout.setOnRefreshListener {
            if (isNetworkAvailable(activity!!)) {
                shimmerHomeLayout.startLayoutAnimation()
                getDashboardApi()
            }
            mview.homeSwiperefreshlayout.isRefreshing = false
        }

        mview.btnRetry.setOnClickListener {
            initView()
        }


        return mview
    }


    override fun onResume() {
        super.onResume()
        shimmerHomeLayout.startLayoutAnimation()
    }

    override fun onPause() {
        shimmerHomeLayout.stopShimmer()
        super.onPause()
    }
    private fun initView() {
        mSettingsClient = LocationServices.getSettingsClient(activity!!)
        rvTopRestaurants = mview.findViewById(R.id.rvTopRestaurants)
        rvFavouriteRestaurants = mview.findViewById(R.id.rvFavouriteRestaurants)
        rvHomeCategory = mview.findViewById(R.id.rvHomeCategory)
        rvOfferRestaurants = mview.findViewById(R.id.rvOfferRestaurants)
        rvPopular = mview.findViewById(R.id.rvPopular)
        locationTrack = LocationTrack(activity!!)
        permissionsToRequest = findUnAskedPermissions(permissions)

        mview.tvViewAllRestaurants.setOnClickListener {
            val intent = Intent(activity, TopRestaurantsPage::class.java)
            intent.putExtra("PageName", "Top")
            activity!!.startActivity(intent)
        }


        if (isNetworkAvailable(activity!!)) {
            requestNewLocationData()
            findUnAskedPermissions(permissions)
            getDashboardApi()
            bindRecycleview()
            checkVersion()
            hideNoInterNetLayout(mview.llNoNetwork)

        } else {
            showNoInterNetLayout(mview.llNoNetwork)
            mview.homeLayout.visibility = View.GONE
            Toast(activity!!, getString(R.string.network_error))
        }


    }


    private fun requestNewLocationData() {
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        mLocationRequest.interval = 0
        mLocationRequest.fastestInterval = 0
        mLocationRequest.numUpdates = 1

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(activity!!)
        if (ActivityCompat.checkSelfPermission(
                requireContext(),
                ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                requireContext(),
                ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            mFusedLocationClient!!.requestLocationUpdates(
                mLocationRequest, mLocationCallback,
                Looper.myLooper()
            )
            return
        }

    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult) {
            val mLastLocation: Location = locationResult.lastLocation
            latitude = mLastLocation.latitude
            longitude = mLastLocation.longitude
            /*if (latitude != 0.0 && longitude != 0.0) {
                Log.e("TAG", "Location $latitude $longitude")
                mGeoCodeList = geocoder!!.getFromLocation(latitude, longitude, 1)
                mAddress = mGeoCodeList[0].getAddressLine(0)
                mCity = mGeoCodeList[0].locality
                mState = mGeoCodeList[0].adminArea
                mPostCode = mGeoCodeList[0].postalCode
                mCountry = mGeoCodeList[0].countryName
                mCountryCode = mGeoCodeList[0].countryCode
                Log.e(
                    "TAG",
                    "Full Address:-${mGeoCodeList.size} " +
                            "\n address:-$mAddress" +
                            "\n city:-$mCity " +
                            "\n tate:-$mState  " +
                            "\n postCode:-$mPostCode " +
                            "\n country:-$mCountry " +
                            "\n countryCode:-$mCountryCode"
                )
                getAreaList()
            } else {
                Toast(activity!!, getString(R.string.network_error))
            }*/
        }
    }


    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
            } else {
                showDialogPermissionRational(
                    activity!!,
                    "Go to settings and allow location permission"
                )
            }
        }
    }

    private fun findUnAskedPermissions(permissions: ArrayList<String>): ArrayList<String> {
        val result = ArrayList<String>()
        for (perm in permissions) {
            if (!hasPermission(perm)) {
                result.add(perm)
            }
        }
        return result
    }

    private fun hasPermission(perm: String): Boolean {
        if (canMakeSmores()) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return (checkSelfPermission(
                    activity!!,
                    perm
                ) == PackageManager.PERMISSION_GRANTED)
            }
        }
        return true
    }

    private fun canMakeSmores(): Boolean {
        return (Build.VERSION.SDK_INT > Build.VERSION_CODES.LOLLIPOP_MR1)
    }

    private fun checkVersion() {

        RetrofitClientSingleton.getInstance()
            .checkVersion().enqueue(object : retrofit2.Callback<CheckVersionModel> {
                override fun onFailure(call: Call<CheckVersionModel>, t: Throwable) {
                    Log.e("TAG", "checkVersion t: >> $t")
                }

                override fun onResponse(
                    call: Call<CheckVersionModel>,
                    response: Response<CheckVersionModel>
                ) {
                    if (response.isSuccessful) when (response.body()?.status) {
                        SUCCESS -> {
                            Log.d(
                                "TAG",
                                "checkVersion : version >> ${response.body()!!.data!!.android}"
                            )
                            if (BuildConfig.VERSION_NAME != response.body()!!.data!!.android) {
//                                updateAppDialog(activity!!)
                            }
                        }

                        FAILURE -> {
                            Log.d(
                                "TAG",
                                "checkVersion : version >> ${response.body()!!.message}"
                            )
                        }
                        else -> {
                            Log.d(
                                "TAG",
                                "checkVersion : version >> ${response.body()!!.message}"
                            )
                        }
                    }
                }
            })
    }


    private fun getAreaList() {
        RetrofitClientSingleton
            .getInstance()
            .getShippingArea()
            .enqueue(object : retrofit2.Callback<ShippingAreaModel> {
                override fun onFailure(call: Call<ShippingAreaModel>, t: Throwable) {
                    Log.e("TAG", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ShippingAreaModel>,
                    response: Response<ShippingAreaModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.body() != null) {
                        val shippingAreaList = response.body()!!.data
                        if (shippingAreaList!!.isNotEmpty()) {
                            if (response.isSuccessful) {
                                when (response.body()!!.status) {
                                    SUCCESS -> {
                                        if (response.body() != null) {
                                            if (activity != null) {
                                                val adapter = SpinnerProductCategoryAdapter(
                                                    activity!!,
                                                    shippingAreaList as ArrayList<ShippingAreaModel.DataBean>
                                                )
                                                try {
                                                    if (shippingAreaList.isNotEmpty()) {
                                                        /*  mview.spHomeShippingArea.adapter =
                                                              adapter


                                                          mview.spHomeShippingArea.onItemSelectedListener =
                                                              object :
                                                                  AdapterView.OnItemSelectedListener {
                                                                  override fun onNothingSelected(
                                                                      parent: AdapterView<*>?
                                                                  ) {

                                                                  }

                                                                  override fun onItemSelected(
                                                                      parent: AdapterView<*>?,
                                                                      view: View?,
                                                                      position: Int,
                                                                      id: Long
                                                                  ) {
                                                                      if (shippingArea == mPostCode) {
                                                                          shippingArea =
                                                                              shippingAreaList[position].area.toString()
                                                                      }

                                                                      *//*for (i in 0..shippingAreaList.size-1) {
                                                                        if (mPostCode == shippingAreaList[i].zipcode) {
                                                                            Log.e("TAG","MyPost")
                                                                        }
                                                                        Log.e(
                                                                            "TAG",
                                                                            "MyCode" + shippingAreaList[i].zipcode
                                                                        )
                                                                    }*//*

                                                                }

                                                            }*/
                                                    }
                                                } catch (e: Exception) {

                                                }


                                            }
                                        }
                                    }
                                    FAILURE -> {

                                    }
                                    AUTH_FAILURE -> {

                                    }
                                }
                            } else {
                                Toast(activity!!, getString(R.string.something_went_wrong))

                            }
                        } else {
                            Toast(activity!!, getString(R.string.something_went_wrong))

                        }
                    } else {
                        Toast(activity!!, getString(R.string.something_went_wrong))
                    }
                }
            })
    }

    fun setViewPager(
        activity: FragmentActivity,
        couponList: List<HomeScreenModel.DataBean.CouponListBean>?
    ) {
        mview.pager.adapter = SliderPagerAdapter(activity, couponList)
    }

    private fun bindRecycleview() {
        //top restaurant layout manager
        rvTopRestaurants.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )

        //favourite restaurant layout manager
        rvFavouriteRestaurants.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.HORIZONTAL,
            false
        )
        //popular of month layout manager
        rvPopular.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.VERTICAL,
            false
        )
        rvOfferRestaurants.layoutManager = LinearLayoutManager(
            activity!!,
            LinearLayoutManager.HORIZONTAL,
            false
        )

    }


    override fun onAttachFragment(childFragment: Fragment) {
        super.onAttachFragment(childFragment)
        // getAreaList()
    }


    fun getDashboardApi() {
        //CustomProgressbar.showProgressBar(activity!!, false)
        shimmerHomeLayout.startLayoutAnimation()
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(activity!!)
        map["franchise_type"] = ""
        RetrofitClientSingleton
            .getInstance()
            .getDashboardData(map)
            .enqueue(object : retrofit2.Callback<HomeScreenModel> {
                override fun onFailure(call: Call<HomeScreenModel>, t: Throwable) {
                    //CustomProgressbar.hideProgressBar()
                    Toast(activity!!, getString(R.string.something_went_wrong))

                }

                override fun onResponse(
                    call: Call<HomeScreenModel>,
                    response: Response<HomeScreenModel>
                ) {

                    if (response.isSuccessful) {
                        shimmerHomeLayout.hideShimmer()
                        val homeScreenData = response.body()
                        if (homeScreenData != null) {
                            mview.tvViewAllRestaurants.text = "View All"
                            mview.homeLayout.visibility = View.VISIBLE
                            hideNoDataLayout(mview.llParentNoData)
                            when (response.body()!!.status) {
                                SUCCESS -> {
                                    if (activity != null) {
                                        mainActivity.updateCartBadge(response.body()!!.data!!.cart_count)
                                        SharedPref.setIntValue(
                                            requireContext(),
                                            cartCount,
                                            response.body()!!.data!!.cart_count
                                        )
                                        mainActivity.updateCartBadge(response.body()!!.data!!.cart_count)

                                        if (homeScreenData.data!!.top_restaurant_list != null) {
                                            mview.tvTopRestaurants.text =
                                                getString(R.string.top_restaurants)
                                            rvTopRestaurants.adapter =
                                                HomeTopRestaurantsAdapter(
                                                    activity!!,
                                                    homeScreenData.data,
                                                    this@HomeFragment
                                                )
                                        } else {
                                            rvTopRestaurants.visibility = View.GONE
                                        }

                                        if (homeScreenData.data!!.fav_food_list != null) {
                                            //favourite Restaurants adapter
                                            mview.tvFavouriteRestaurants.text = "Exclusive"
                                            getString(R.string.zamy_exclusive_food)
                                            rvFavouriteRestaurants.adapter =
                                                HomeFavoriteFoodAdapter(
                                                    activity!!,
                                                    homeScreenData.data,
                                                    this@HomeFragment
                                                )
                                        } else {
                                            rvFavouriteRestaurants.visibility = View.GONE
                                            mview.tvFavouriteRestaurants.text = ""
                                        }
                                        //category Restaurants adapter
                                        if (homeScreenData.data!!.category_list != null) {
                                            rvHomeCategory.layoutManager = LinearLayoutManager(
                                                activity,
                                                LinearLayoutManager.HORIZONTAL,
                                                false
                                            )
                                            rvHomeCategory.adapter = HomeCategoryAdapter(
                                                activity!!,
                                                homeScreenData.data,
                                                this@HomeFragment
                                            )
                                        } else {
                                            rvHomeCategory.visibility = View.GONE
                                        }
                                        if (homeScreenData.data!!.coupon_list != null) {
                                            /* rvOfferRestaurants.adapter = AdapterOffers(
                                                 activity!!,
                                                 homeScreenData.data!!.coupon_list,
                                                 this@HomeFragment
                                             )*/
                                            mview.tvOfferRestaurants.text = "Offers"
                                            setViewPager(
                                                activity!!,
                                                homeScreenData.data!!.coupon_list
                                            )
                                        } else {
                                            mview.tvOfferRestaurants.text = ""
                                            mview.pager.visibility = View.GONE
                                        }


                                        if (homeScreenData.data!!.popular_this_month != null) {
                                            //popular month Restaurants adapter
                                            mview.tvPopular.text =
                                                getString(R.string.popular_of_this_month)
                                            rvPopular.adapter = HomePopularMonthAdapter(
                                                activity!!,
                                                homeScreenData.data!!.popular_this_month,
                                                this@HomeFragment, this@HomeFragment,
                                                this@HomeFragment,
                                                this@HomeFragment
                                            )
                                        } else {
                                            rvPopular.visibility = View.GONE
                                            mview.tvPopular.text = ""
                                        }
                                    } else {
                                       // CustomProgressbar.hideProgressBar()
                                        shimmerHomeLayout.hideShimmer()
                                    }
                                }
                                FAILURE -> {
                                    //CustomProgressbar.hideProgressBar()
                                    shimmerHomeLayout.hideShimmer()
                                    Toast(activity!!, "" + response.body()!!.message)
                                    showNoDataLayout(
                                        llParentNoData,
                                        getString(R.string.no_data_fnd)
                                    )
                                    mview.homeLayout.visibility = View.GONE


                                }
                                AUTH_FAILURE -> {
                                    //CustomProgressbar.hideProgressBar()
                                    shimmerHomeLayout.hideShimmer()
                                    showNoDataLayout(
                                        llParentNoData,
                                        getString(R.string.no_data_fnd)
                                    )
                                    mview.homeLayout.visibility = View.GONE

                                }
                            }
                        } else {
                            showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                            mview.homeLayout.visibility = View.GONE


                        }
                    } else {
                        //CustomProgressbar.hideProgressBar()
                        shimmerHomeLayout.hideShimmer()
                        showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                        mview.homeLayout.visibility = View.GONE
                    }
                }
            })
    }

    override fun onMenuItemSelected(
        listBean: CartRestaurantsMenuModel,
        isProductVariance: Boolean,
        isRemove: Boolean
    ) {
        if (isRemove) {
            isProductAvailable(cartArrayList, listBean.foodmenu_id.toString(), listBean)
            cartArrayList.remove(listBean)
        } else {
            cartArrayList.add(listBean)
        }
        if (isProductVariance) {
        } else {
            activity?.let { SharedPref.savedata(it, cartArrayList) }
            activity?.let { SharedPref.saveArrayList(cartArrayList, "data", it) }
            if (user_id != "") {
                //addtocartAPI(listBean)
            } else {
                activity?.let { Toast(it, "Please login to add products into cart ") }
                val ine = Intent(activity, LoginPage::class.java)
                ine.putExtra(SCREEN_REDIRECTION, "1")
                startActivity(ine)
            }
        }
    }

    private fun isProductAvailable(
        cartArrayList: MutableList<CartRestaurantsMenuModel>,
        foodmenu_id: String,
        listBean: CartRestaurantsMenuModel
    ): Boolean {

        var isAvail = false

        for (cartModel in cartArrayList) {

            if (cartModel.foodmenu_id.equals(foodmenu_id)) {
                cartArrayList.remove(listBean)
                Log.e("removesize", cartArrayList.size.toString())
                isAvail = true
            }
        }

        return isAvail
    }

    override fun onVarianceDialogSelected(
        dataBean: List<HomeScreenModel.DataBean.PopularThisMonthBean.ProductVariationBean>,
        catId: String?,
        foodmenuId: String?,
        menuName: String?,
        taxPrice: String,
        tvCustom: TextView,
        tvRemove: TextView
    ) {
        openBottomSheetDialog(
            dataBean,
            catId,
            foodmenuId,
            menuName,
            taxPrice,
            tvAddToCart,
            tvRemove
        )
    }

    private fun openBottomSheetDialog(
        listBean: List<HomeScreenModel.DataBean.PopularThisMonthBean.ProductVariationBean>,
        catId: String?,
        foodmenuId: String?,
        menuName: String?,
        taxPrice: String,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {
        val view = layoutInflater.inflate(R.layout.bottom_sheet_variance, null)
        dialog = activity?.let { BottomSheetDialog(it) }
        dialog!!.setContentView(view)
        val varainceList = listBean
        (dialog as BottomSheetDialog).tvAddVariance.visibility = View.GONE
        dialog!!.imgClose.setOnClickListener {

            (dialog as BottomSheetDialog).cancel()
        }
        dialog!!.txtMenuName.text = menuName
        dialog!!.tvAddVariance.setOnClickListener {
            Log.e("dialog", "click")
            cartArrayList.add(varianceitem)
            addtocartAPI(varianceitem, tvAddToCart, tvRemove)
            dialog!!.cancel()
        }


        dialog!!.show()

        if (listBean.size > 0) {
            val mLayoutManager = LinearLayoutManager(activity)
            dialog!!.rvVariance!!.layoutManager = mLayoutManager
            dialog!!.rvVariance!!.itemAnimator = DefaultItemAnimator()
            val varianceAdapter = activity?.let {
                ProductHomeVarianceAdapter(
                    it,
                    varainceList,
                    this,
                    catId,
                    foodmenuId,
                    menuName,
                    taxPrice
                )
            }
            dialog!!.rvVariance.adapter = varianceAdapter


        }

    }

    private fun addtocartAPI(
        listBean: CartRestaurantsMenuModel,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {

        CustomProgressbar.showProgressBar(activity!!, false)

        val map = HashMap<String, String>()
        map["restaurant_id"] = listBean.cat_id.toString()
        map["food_menu_id"] = listBean.foodmenu_id.toString()
        map["food_menu_name"] = listBean.menu_name.toString()
        map["variation_id"] = listBean.variation_id.toString()
        map["variation_name"] = listBean.variation_name.toString()
        map["user_id"] = user_id

        RetrofitClientSingleton
            .getInstance()
            .addtoCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("RestaurantDetail", t.message)
                }

                override fun onResponse(
                    call: Call<CommonModel>,
                    response: Response<CommonModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        // val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //top
                                tvAddToCart.visibility = View.VISIBLE
                                //tvRemove.visibility=View.VISIBLE
                                Toast(activity!!, "" + response.body()!!.message)
                                mainActivity.setrestaurantId(restaurantId = listBean.cat_id!!)
                                getDashboardApi()
                            }
                            FAILURE -> {
                                dialogForAddCart(
                                    response.body()!!.message.toString(),
                                    listBean,
                                    tvAddToCart,
                                    tvRemove
                                )

                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }

    private fun dialogForAddCart(
        message: String,
        listBean: CartRestaurantsMenuModel,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {
        val itemDialog = Dialog(activity!!)
        itemDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        itemDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        itemDialog.setContentView(R.layout.dialog_logout)
        itemDialog.tvLogoutDialogTitle.text = message
        itemDialog.show()

        itemDialog.btn_cencle.setOnClickListener { itemDialog.dismiss() }

        itemDialog.btn_ok.setOnClickListener {
            clearCart(listBean, tvAddToCart, tvRemove)
            itemDialog.dismiss()
            getDashboardApi()

        }

        /* val builder = AlertDialog.Builder(activity, R.style.AlertDialogCustom)

         // Display a message on alert dialog
         builder.setMessage(message)

         // Set a positive button and its click listener on alert dialog
         builder.setPositiveButton("Yes") { dialog, which ->
             // Do something when user press the positive button

             getDashboardApi()
             dialog.cancel()
         }

         builder.setNegativeButton("No") { dialog, which ->

             // Do something when user press the positive button

             dialog.cancel()
         }
         val dialog: AlertDialog = builder.create()

         // Display the alert dialog on app interface
         dialog.show()*/
    }

    private fun clearCart(
        listBean: CartRestaurantsMenuModel,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {
        CustomProgressbar.showProgressBar(activity, false)

        val map = HashMap<String, String>()
        map["user_id"] = user_id

        CustomProgressbar.showProgressBar(activity, false)
        RetrofitClientSingleton
            .getInstance()
            .clearCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(
                    call: Call<CommonModel>,
                    response: Response<CommonModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        /*   val homeScreenData = response.body()*/
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //activity?.let { Toast(it, "" + response.body()!!.message) }
                                addtocartAPI(listBean, tvAddToCart, tvRemove)
                            }
                            FAILURE -> {
                                activity?.let { Toast(it, "" + response.body()!!.message) }
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("Cart clear", response.body().toString())
                }
            })
    }

    override fun onVarianceItemSelected(item: CartRestaurantsMenuModel) {
        varianceitem = item
        dialog!!.tvAddVariance.visibility = View.VISIBLE
        Log.e("variance", item.variation_name)

    }

    override fun onLocationChanged(location: Location?) {
        /*  latitude = location!!.latitude
          longitude = location.longitude*/

    }

    override fun adapterFavouriteItemPosition(dataList: HomeScreenModel.DataBean.FavFoodListBean) {
        if (dataList.restaurant_id != null && dataList.restaurant_id != "") {
            val intent = Intent(activity, TopRestaurantsPage::class.java)
            intent.putExtra("PageName", "Detail")
            intent.putExtra(RESTAURANT_ID_PASS, dataList.restaurant_id)
            activity!!.startActivity(intent)
        }
    }

    override fun adapterItemPosition(
        id: String?,
        dataList: HomeScreenModel.DataBean.TopRestaurantListBean
    ) {
        val intent = Intent(activity, TopRestaurantsPage::class.java)
        intent.putExtra("PageName", "Detail")
        intent.putExtra(RESTAURANT_ID_PASS, dataList.id)
        intent.putExtra(USER_ID, SharedPref.getUserId(activity!!))
        activity!!.startActivity(intent)
    }

    override fun onPopularItemSelected(listBean: HomeScreenModel.DataBean.PopularThisMonthBean) {
        val intent = Intent(activity, TopRestaurantsPage::class.java)
        intent.putExtra("PageName", "Detail")
        intent.putExtra(RESTAURANT_ID_PASS, listBean.kitchen_id)
        intent.putExtra(USER_ID, SharedPref.getUserId(activity!!))
        activity!!.startActivity(intent)
    }

    companion object {
        private const val PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 100
    }


}
