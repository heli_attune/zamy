package com.attune.zamy.fragment


import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.attune.zamy.R
import com.attune.zamy.adapter.SearchRestaurantDishAdapter
import com.attune.zamy.model.SearchModel
import com.attune.zamy.utils.showNoDataLayout
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.no_data_layout.*
import java.util.*


class SearchFragment : androidx.fragment.app.Fragment() {

    var strsearch: String = ""
    var searchDataModel: SearchModel.DataBean? = null
    lateinit var mView: View
    lateinit var edtSeaach: EditText
    lateinit var rvSearch: RecyclerView
    lateinit var dataModels: MutableList<SearchModel.DataBean.SearchDataBean>
    var TOTAL_PAGE: Int = 0
    var isLoading: Boolean = false
    var isLastPage: Boolean = false
    val PAGE_START = 1
    var currentPage: Int = PAGE_START
    lateinit var madapter: SearchRestaurantDishAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_search, container, false)
        initView()
        return mView
    }

    private fun viewpagerSet() {
        val viewPager = mView.findViewById(R.id.viewpager) as ViewPager
        setupViewPager(viewPager)
        // Set Tabs inside Toolbar
        val tabs = mView.findViewById(R.id.result_tabs) as TabLayout
        tabs.setupWithViewPager(viewPager)
    }

    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = Adapter(childFragmentManager)
        adapter.addFragment(SearchRestaurantsFragment(), "Restaurant")
        adapter.addFragment(SearchDishFragment(), "Dish")
        viewPager.adapter = adapter
    }


    class Adapter(manager: FragmentManager) : FragmentPagerAdapter(manager) {

        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            val fragment: Fragment? = null
            when (position) {
                0 -> {
                    val restaurantsFragment = SearchRestaurantsFragment()
                    val args = Bundle()
                    args.putInt("code", 1)

                    restaurantsFragment.arguments = args
                    return restaurantsFragment
                }
                1 -> {
                    val dishFragment = SearchDishFragment()
                    val args = Bundle()
                    args.putInt("code", 2)
                    dishFragment.arguments = args
                    return dishFragment
                }
            }
            return fragment!!
            // return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }

    private fun initView() {

        showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
        clearPaging()
        dataModels = ArrayList()
        edtSeaach = mView.findViewById(R.id.edtSeaach)
        //   madapter = activity?.let { SearchRestaurantDishAdapter(it, dataModels, this) }!!
        edtSeaach.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                //getSearchRestaurantDish(false, strsearch)
                return true
            }
        })
        //  rvTopRestaurantBind()
        edtSeaach.addTextChangedListener(object : TextWatcher {

            override fun afterTextChanged(s: Editable) {
                strsearch = s.toString()
            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
            }
        })
        showNoDataLayout(llParentNoData, "Data Not Found")

    }

    private fun clearPaging() {
        currentPage = PAGE_START
        isLoading = false
        isLastPage = false
    }

    /*  private fun rvTopRestaurantBind() {
          val mLayoutManager = LinearLayoutManager(activity)
            rvSearch!!.layoutManager = mLayoutManager
            madapter = activity?.let { SearchRestaurantDishAdapter(it, dataModels, this) }!!
            rvSearch.adapter = madapter
            rvSearch!!.addOnScrollListener(object : PaginationScrollListener(mLayoutManager) {
                override val totalPageCount: Int
                    get() = TOTAL_PAGE

                override fun isLastPage(): Boolean {
                    return isLastPage
                }

                override fun isLoading(): Boolean {
                    return isLoading
                }

                override fun loadMoreItems() {
                    isLoading = true
                    //you have to call loadmore items to get more data
                    currentPage += 1
                    lodeMore(strsearch)
                }
            })

      }*/

    /* private fun getSearchRestaurantDish(isrefresh: Boolean, strsearch: String) {
         if (!isrefresh) {
             CustomProgressbar.showProgressBar(activity!!, false)
         }
         val map = HashMap<String, String>()
         map["search_keyword"] = strsearch
         map["pageno"] = currentPage.toString()


         RetrofitClientSingleton
             .getInstance()
             .getsearchDishRestaurant(map)
             .enqueue(object : retrofit2.Callback<SearchModel> {
                 override fun onFailure(call: Call<SearchModel>, t: Throwable) {
                     CustomProgressbar.hideProgressBar()
                     Log.e("Search FRAGMENT", t.message)
                 }

                 override fun onResponse(call: Call<SearchModel>, response: Response<SearchModel>) {
                     CustomProgressbar.hideProgressBar()
                     if (response.isSuccessful) {
                         val homeScreenData = response.body()
                         when (response.body()!!.status) {
                             SUCCESS -> {
                                 viewpagerSet()
                                 var dataList = homeScreenData!!.data!!.search_data
                                 searchDataModel = homeScreenData.data
                                 if (dataList != null && dataList.size > 0) {

                                     madapter.removeAllItem()
                                     madapter.addAllItem(dataList)
                                     TOTAL_PAGE = homeScreenData.data!!.total_pages.toInt()
                                     isLoading = false
                                     if (currentPage < TOTAL_PAGE) {
                                         madapter.addLoading()
                                     } else {
                                         isLastPage = true
                                     }
                                 } else {
                                     madapter.removeAllItem()
                                 }
                             }
                             FAILURE -> {
                                 Toast(activity!!, "" + response.body()!!.message)
                             }
                             AUTH_FAILURE -> {
                             }

                         }
                     }
                     Log.e("HOME FRAGMENT", response.body().toString())
                 }
             })
     }

     fun lodeMore(strsearch: String) {
         val map = HashMap<String, String>()
         map["search_keyword"] = this.strsearch
         map["pageno"] = currentPage.toString()

         CustomProgressbar.showProgressBar(activity!!, false)
         RetrofitClientSingleton
             .getInstance()
             .getsearchDishRestaurant(map)
             .enqueue(object : retrofit2.Callback<SearchModel> {
                 override fun onFailure(call: Call<SearchModel>, t: Throwable) {
                     CustomProgressbar.hideProgressBar()
                     Log.e("HOME FRAGMENT", t.message)
                 }

                 override fun onResponse(call: Call<SearchModel>, response: Response<SearchModel>) {
                     CustomProgressbar.hideProgressBar()
                     if (response.isSuccessful) {
                         val homeScreenData = response.body()
                         when (response.body()!!.status) {
                             SUCCESS -> {

                                 var dataList = homeScreenData!!.data!!.search_data
                                 if (dataList != null && dataList.size > 0) {

                                     madapter.removeLoading()
                                     isLoading = false
                                     madapter.addAllItem(dataList)


                                     if (currentPage != TOTAL_PAGE) {
                                         madapter.addLoading()
                                     } else {
                                         isLastPage = true
                                     }
                                 } else {
                                     //madapter.removeAllItem()
                                 }
                                 //top
                                 *//* rvTopRest.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                                 rvTopRest.adapter = activity?.let {
                                     TopRestaurantsAdapter(homeScreenData!!.data!!.top_restaurant_list,
                                         it,this@TopRestaurantFragment)*//*


                            }
                            FAILURE -> {
                                Toast(activity!!, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }*/
}

