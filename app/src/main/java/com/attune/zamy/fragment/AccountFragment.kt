package com.attune.zamy.fragment

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ExpandableListAdapter
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import com.attune.zamy.R
import com.attune.zamy.activity.*
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.ProfileModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import com.facebook.shimmer.ShimmerFrameLayout
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.dialog_logout.*
import kotlinx.android.synthetic.main.fragment_account.view.*
import kotlinx.android.synthetic.main.layout_no_network.view.*
import kotlinx.android.synthetic.main.no_data_layout.view.*
import kotlinx.android.synthetic.main.profile_layout.view.*
import retrofit2.Call
import retrofit2.Response


class AccountFragment : androidx.fragment.app.Fragment() {

    internal var adapter: ExpandableListAdapter? = null
    internal var titleList: List<String>? = null
    lateinit var mview: View
    private var mRelativeZaplon: RelativeLayout? = null
    private var mRelativeToSlide: LinearLayout? = null
    lateinit var tvUserName: TextView
    lateinit var tvEmail: TextView
    lateinit var tvMobile: TextView
    var dialog: Dialog? = null
    lateinit var shimmerFrameLayout:ShimmerFrameLayout

    var mainActivity=MainActivity()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup  ?,
        savedInstanceState: Bundle?
    ): View? {
        mview = inflater.inflate(R.layout.fragment_account, null)
        initView()
        mainActivity= activity as MainActivity
        shimmerFrameLayout=mview.findViewById(R.id.accountShimmer)

        mainActivity.hideToolbar()

        mview.btnRetry.setOnClickListener {
            initView()
        }

        mview.SwipeRefreshLayoutAccount.setOnRefreshListener {
            if (isNetworkAvailable(activity!!)) {
                getUserProfile(true)
            }
            mview.SwipeRefreshLayoutAccount.isRefreshing = false
        }

        mview.btnRetry.setOnClickListener {
            initView()
        }

        return mview
    }

    override fun onResume() {
        super.onResume()
        shimmerFrameLayout.startLayoutAnimation()
    }

    override fun onStop() {
        shimmerFrameLayout.stopShimmer()
        super.onStop()
    }


    private fun initView() {

        mRelativeZaplon = mview.findViewById(R.id.relativeZaplon) as RelativeLayout
        mRelativeToSlide = mview.findViewById(R.id.relativevToSlide) as LinearLayout
        tvUserName = mview.findViewById(R.id.tvUserName) as TextView
        tvEmail = mview.findViewById(R.id.tvEmail) as TextView
        tvMobile = mview.findViewById(R.id.tvMobile) as TextView



        mview.imgAccount.setImageResource(R.drawable.ic_right_arrow)
        var heigtOf = 0
        //dynamic height set in list
        mRelativeToSlide!!.viewTreeObserver.addOnGlobalLayoutListener {
            mRelativeZaplon!!.layoutParams =
                LinearLayout.LayoutParams(
                    ViewGroup.LayoutParams.MATCH_PARENT,
                    ViewGroup.LayoutParams.WRAP_CONTENT
                )
            heigtOf = mRelativeToSlide!!.height
            Log.e("TAGI", "$heigtOf")

        }


        if (isNetworkAvailable(activity!!)) {
            getUserProfile(true)
        } else {
            mview.profileLayout.visibility = View.GONE
            showNoInterNetLayout(mview.llNoNetwork)
            Toast(activity!!, getString(R.string.network_error))
        }



        mview.tvEdit.setOnClickListener {
            val intent = Intent(activity!!, EditProfileActivity::class.java)
            startActivity(intent)
            //Toast(activity!!, "Working")
        }


        Log.e("TAG", "$heigtOf")
        var visible = false
        mRelativeZaplon!!.setOnClickListener {
            if (!visible) {
                expand(mRelativeToSlide!!)
                mview.imgAccount.animate().rotation(90.0F).setDuration(500).start()
                visible = true
            } else {
                collapse(mRelativeToSlide!!)
                mview.imgAccount.animate().rotation(0.0F).setDuration(500).start()
                //   mview.imgAccount.setImageResource(R.drawable.ic_right_arrow)
                visible = false
            }
        }



        mview.tvAddress.setOnClickListener {
            val intent = Intent(activity, ManageAddressActivity::class.java)
            startActivity(intent)
        }
        mview.tvLogout.setOnClickListener {
            logoutConfirmation()
        }
        mview.tvOrder.setOnClickListener {
            val intent = Intent(activity, MyOrderActivity::class.java)
            startActivity(intent)
        }
        mview.tvFavourites.setOnClickListener {
            val intent = Intent(activity, FavouriteActivity::class.java)
            startActivity(intent)
        }
        mview.tvCustomSupport.setOnClickListener {
            val intent = Intent(activity, CustomerSupportActivity::class.java)
            startActivity(intent)
        }
        mview.rlDelivery.setOnClickListener {
            val intent= Intent(activity,DeliveryActivity::class.java)
            startActivity(intent)
        }
        mview.rlContactUs.setOnClickListener {
            val intent = Intent(activity, ContactUsActivity::class.java)
            startActivity(intent)
        }

        mview.rlAboutUs.setOnClickListener {
            val intent = Intent(activity, AboutUsActivity::class.java)
            startActivity(intent)
        }

        mview.tvReferral.setOnClickListener {
            val intent = Intent(activity, ReferalActivity::class.java)
            intent.putExtra("ACCOUNT", "1")
            startActivity(intent)
        }

    }

    private fun opernBottomSheetDialog() {
        val view = layoutInflater.inflate(R.layout.dialog_bottomsheet_edit_profile, null)
        dialog = BottomSheetDialog(activity!!)
        dialog!!.setContentView(view)
        dialog!!.show()

    }

    fun getUserProfile(isLoderVisible: Boolean) {
        if (isLoderVisible)
            CustomProgressbar.showProgressBar(activity!!, false)
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(activity!!)
        RetrofitClientSingleton
            .getInstance()
            .getUserProfile(map)
            .enqueue(object : retrofit2.Callback<ProfileModel> {
                override fun onFailure(call: Call<ProfileModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("TAG", t.message)
                }

                override fun onResponse(
                    call: Call<ProfileModel>,
                    response: Response<ProfileModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        hideNoInterNetLayout(mview.llNoNetwork)
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                mview.profileLayout.visibility = View.VISIBLE
                                mview.llParentNoData.visibility = View.GONE
                                mview.txtNodata.visibility = View.GONE
                                mview.btnRetry.visibility = View.GONE
                                val profileRespo = response.body()!!.data
                                tvUserName.text = profileRespo!!.name
                                tvEmail.text = profileRespo.email
                                tvMobile.text = profileRespo.phone

                                if (profileRespo.profile_pic != null && profileRespo.profile_pic != "") {
                                    Glide.with(context!!)
                                        .load(profileRespo.profile_pic)
                                        .into(mview.userImage)
                                } else {
                                    Glide.with(context!!)
                                        .load(resources.getDrawable(R.drawable.ic_user_placeholder))
                                        .into(mview.userImage)
                                }

                            }
                            FAILURE -> {
                                Toast(
                                    activity!!,
                                    resources.getString(R.string.something_went_wrong)
                                )
                                showNoDataLayout(
                                    mview.llParentNoData,
                                    getString(R.string.no_data_fnd)
                                )
                            }
                            AUTH_FAILURE -> {
                                showNoDataLayout(
                                    mview.llParentNoData,
                                    getString(R.string.no_data_fnd)
                                )
                            }
                        }
                    } else {
                        showNoDataLayout(mview.llParentNoData, getString(R.string.no_data_fnd))
                    }

                }

            })
    }

    val data: HashMap<String, List<String>>
        get() {
            val listData = HashMap<String, List<String>>()

            val manageAddress = ArrayList<String>()
            val offers = ArrayList<String>()
            val customersupport = ArrayList<String>()
            val favourites = ArrayList<String>()

            listData["Manage Address"] = manageAddress
            listData["Customer Support"] = customersupport
            listData["Favourites"] = favourites
            listData["Referral"] = offers

/*
            payment.add("COD")
            payment.add("Paytm")
            manageAddress.add("Address-1")
            manageAddress.add("Address-2")
            offers.add("offers 1")
            offers.add("offers 2")
*/


            return listData
        }

    private fun logoutConfirmation() {
            val dialog = Dialog(activity!!)
            dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
            dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
            dialog.setContentView(R.layout.dialog_logout)

            dialog.show()
            dialog.tvLogoutDialogTitle.text = getString(R.string.logout_conformation)
            dialog.btn_cencle.setOnClickListener { dialog.dismiss() }
            dialog.btn_ok.setOnClickListener {
                SharedPref.clear(activity!!)
                SharedPref.removeSharedData(activity!!)
                startActivity(Intent(activity!!, LoginPage::class.java))
            }
    }

}

