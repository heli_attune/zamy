package com.attune.zamy.fragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.attune.zamy.R
import com.attune.zamy.activity.PaymentActivity
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.DeliveryResponseModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.FAILURE
import com.attune.zamy.utils.SUCCESS
import com.attune.zamy.utils.Toast
import com.attune.zamy.utils.keyOrderId
import com.google.android.material.button.MaterialButton
import kotlinx.android.synthetic.main.activity_delivery_payment.*
import kotlinx.android.synthetic.main.fragment_calculate_distance.*
import kotlinx.android.synthetic.main.fragment_pick_up_drop_payment.*
import retrofit2.Call
import retrofit2.Response

class PickUpDropPaymentFragment : Fragment() {


    lateinit var paymentView:View
    lateinit var covidFragment: CovidFragment
    lateinit var btndeliverypaymentBack: MaterialButton
    lateinit var btndeliverySubmit: MaterialButton

    //local
    private lateinit var user_id:String
    private lateinit var store:String
    private lateinit var item_details:String
    private lateinit var pickup_address:String
    private lateinit var pickup_pincode:String
    private lateinit var pickup_contact_no:String
    private lateinit var delivery_address:String
    private lateinit var delivery_pincode:String
    private lateinit var delivery_contact_no:String
    private lateinit var from_to_distance:String
    private lateinit var km_amount:String
    private lateinit var additional_amount:String
    private lateinit var total_amount:String
    private lateinit var pickup_lattitude:String
    private lateinit var pickup_longitude:String
    private lateinit var delivery_lattitude:String
    private lateinit var delivery_longitude:String

    var payment_method: String="Paytm"

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_pick_up_drop_payment, container, false)
        paymentView = inflater.inflate(R.layout.fragment_pick_up_drop_payment, container, false)

        LocalBroadcastManager.getInstance(activity!!)
            .registerReceiver(paymentbroadCastReceiver, IntentFilter("Pass_payment"))



        covidFragment = parentFragment as CovidFragment
        btndeliverypaymentBack = paymentView.findViewById(R.id.btndeliverypaymentBack)
        btndeliverypaymentBack.setOnClickListener {
            covidFragment.getPagePosition()!!.currentItem = 1
        }


        btndeliverySubmit = paymentView.findViewById(R.id.btndeliverySubmit)
        btndeliverySubmit.setOnClickListener {
            callPickupDropAddApi()
        }

        return paymentView
    }

    override fun onResume() {
        super.onResume()
        Deliveryradio_groupFragment.setOnCheckedChangeListener { radioGroup, optionId ->
            run {
                when (optionId) {
                    R.id.DeliveryradioPaytmFragment -> {
                        payment_method = "paytm"

                    }
                    R.id.DeliveryradioInstaMojoFragment -> {
                        payment_method = "instamojo"

                    }
                    R.id.DeliveryradioRezorpayFragment -> {
                        payment_method = "razorpay"
                    }

                }
            }
        }
    }

    val paymentbroadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, i: Intent?) {
//            strPickupAddress= i?.getStringExtra("Pickup") ?: Log.e(MainApp.TAG, "My Value "+strPickupAddress)
//                .toString()
            user_id= i?.getStringExtra("user_id").toString()
            store= i?.getStringExtra("store").toString()
            item_details= i?.getStringExtra("item_details").toString()
            pickup_address= i?.getStringExtra("pickup_address").toString()
            pickup_pincode= i?.getStringExtra("pickup_pincode").toString()
            pickup_contact_no= i?.getStringExtra("pickup_contact_no").toString()
            delivery_address= i?.getStringExtra("delivery_address").toString()
            delivery_pincode= i?.getStringExtra("delivery_pincode").toString()
            delivery_contact_no= i?.getStringExtra("delivery_contact_no").toString()
            from_to_distance= i?.getStringExtra("from_to_distance").toString()
            km_amount= i?.getStringExtra("km_amount").toString()
            additional_amount= i?.getStringExtra("additional_amount").toString()
            total_amount= i?.getStringExtra("total_amount").toString()
            pickup_lattitude= i?.getStringExtra("pickup_lattitude").toString()
            pickup_longitude= i?.getStringExtra("pickup_longitude").toString()
            delivery_lattitude= i?.getStringExtra("delivery_lattitude").toString()
            delivery_longitude= i?.getStringExtra("delivery_longitude").toString()
//            Toast(activity!!,delivery_contact_no)

        }
    }

    private fun callPickupDropAddApi() {
        CustomProgressbar.showProgressBar(activity, false)
        val map = HashMap<String, String>()
        map["user_id"] = user_id
        map["store"] = store
        map["item_details"] = item_details
        map["pickup_address"] = pickup_address
        map["pickup_pincode"] = pickup_pincode
        map["pickup_lattitude"] = pickup_lattitude
        map["pickup_longitude"] = pickup_longitude
        map["pickup_contact_no"] = pickup_contact_no
        map["delivery_address"] = delivery_address
        map["delivery_pincode"] = delivery_pincode
        map["delivery_lattitude"] = delivery_lattitude
        map["delivery_longitude"] = delivery_longitude
        map["delivery_contact_no"] = delivery_contact_no
        map["from_to_distance"] = from_to_distance
        map["payment_method"] = payment_method
        map["km_amount"] = km_amount
        map["additional_amount"] = additional_amount
        map["total_amount"] = total_amount
        RetrofitClientSingleton.getInstance().PickupDropAddApi(map)
            .enqueue(object : retrofit2.Callback<DeliveryResponseModel> {
                override fun onFailure(call: Call<DeliveryResponseModel>, t: Throwable) {
                    Toast(activity!!, t.message)
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(
                    call: Call<DeliveryResponseModel>,
                    response: Response<DeliveryResponseModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    val DeliveryResponse = response.body()
                    if (DeliveryResponse != null) {
                        when (DeliveryResponse.getStatus()) {
                            SUCCESS -> {
                                val userBean = DeliveryResponse.getData()
                                userBean?.let {

                                }
//                                Toast(
//                                    activity!!,
//                                    DeliveryResponse.getMessage()!!
//                                )
                                val intent = Intent(activity!!, PaymentActivity::class.java)
                                intent.putExtra(
                                    keyOrderId,
                                    DeliveryResponse.getData()?.getOrder_id()
                                )
                                intent.putExtra(
                                    "payment_url",
                                    DeliveryResponse.getData()?.getPaytm_link()
                                )
//                                Toast(this@DeliveryPaymentActivity, DeliveryResponse.getData()?.getOrder_id()!!+","+DeliveryResponse.getData()?.getOrder_id()!!)
                                startActivity(intent)
                            }
                            FAILURE -> {
                                Toast(activity!!, DeliveryResponse.getMessage()!!)
                            }
                            else -> {
                                //toast(this@LoginPage, R.string.something_went_wrong)
                            }
                        }
                    }
                }
            })
    }




}
