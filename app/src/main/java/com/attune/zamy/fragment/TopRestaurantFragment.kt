package com.attune.zamy.fragment

import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.attune.zamy.R
import com.attune.zamy.adapter.TopRestaurantPaginationAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.listener.PaginationScrollListener
import com.attune.zamy.model.TopRestaurantModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.fragment_top_restaurant.view.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import kotlinx.android.synthetic.main.no_data_layout.*
import retrofit2.Call
import retrofit2.Response


class TopRestaurantFragment() : Fragment(),
    TopRestaurantPaginationAdapter.OnItemSelectedListener {
    override fun onItemSelected(
        item: TopRestaurantModel.DataBean.TopRestaurantListBean,
        position: Int,
        adapterPostion: Int
    ) {

        val args = Bundle()
        args.putString(RESTAURANT_ID_PASS, item.id)
        val toFragment = RestaurantsDetailsFragment()
        toFragment.arguments = args
        fragmentManager!!
            .beginTransaction()
            .add(R.id.fragment_restaurant, toFragment, "RestaurantsDetailsFragment")
            .addToBackStack("RestaurantsDetailsFragment").commit()
    }


    lateinit var madapter: TopRestaurantPaginationAdapter
    lateinit var mview: View
    var TOTAL_PAGE: Int = 0
    var isLoading: Boolean = false
    var isLastPage: Boolean = false
    val PAGE_START = 1
    var currentPage: Int = PAGE_START
    lateinit var dataModels: MutableList<TopRestaurantModel.DataBean.TopRestaurantListBean>
    lateinit var swipeLayout: SwipeRefreshLayout
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mview = inflater.inflate(R.layout.fragment_top_restaurant, null)
        initView()
        return mview
    }

    private fun initView() {
        clearPaging()
        dataModels = ArrayList()
        mview.txtTitle.text = getString(R.string.top_restaurants)
        mview.imgback.setOnClickListener {
            activity!!.onBackPressed()
        }


        swipeLayout = mview.findViewById(R.id.sr)
        rvTopRestaurantBind()

        if (!activity?.let { isNetworkAvailable(it) }!!) {
            activity?.let { Toast(it, getString(R.string.no_internet_connection)) }
        } else {
            getTopRestaurantDataAPI(false)
        }
        srRefresh()


    }

    //swipe to refresh layout
    private fun srRefresh() {
        swipeLayout.setOnRefreshListener {
            clearPaging()
            getTopRestaurantDataAPI(true)
            swipeLayout.isRefreshing = false
        }
        /*swipeLayout.setOnRefreshListener {
            clearPaging()
            if (!activity?.let { isNetworkAvailable(it) }!!) {
                activity?.let { Toast(it, getString(R.string.no_internet_connection)) }
            } else {
                getTopRestaurantDataAPI()
            }
            swipeLayout.setRefreshing(false)
        }*/
    }

    private fun clearPaging() {
        currentPage = PAGE_START
        isLoading = false
        isLastPage = false
    }

    private fun rvTopRestaurantBind() {
        val mLayoutManager = LinearLayoutManager(activity)
        mview.rvTopRest!!.layoutManager = mLayoutManager
        madapter = activity?.let { TopRestaurantPaginationAdapter(it, dataModels, this) }!!
        mview.rvTopRest.adapter = madapter
        mview.rvTopRest!!.addOnScrollListener(object : PaginationScrollListener(mLayoutManager) {
            override val totalPageCount: Int
                get() = TOTAL_PAGE

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                //you have to call loadmore items to get more data
                currentPage += 1
                lodeMore()
            }
        })

    }

    private fun getTopRestaurantDataAPI(isrefresh: Boolean) {
        if (!isrefresh) {
            CustomProgressbar.showProgressBar(activity!!, false)
        }
        val map = HashMap<String, String>()
        map["pageno"] = currentPage.toString()
        map["franchise_type"] = ""

        RetrofitClientSingleton
            .getInstance()
            .getTopRestaurantList(map)
            .enqueue(object : retrofit2.Callback<TopRestaurantModel> {
                override fun onFailure(call: Call<TopRestaurantModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                    Log.e("HOME FRAGMENT", t.message)
                }

                override fun onResponse(
                    call: Call<TopRestaurantModel>,
                    response: Response<TopRestaurantModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {

                                val dataList = homeScreenData!!.data!!.top_restaurant_list
                                if (dataList != null && dataList.size > 0) {

                                    madapter.removeAllItem()
                                    madapter.addAllItem(dataList)
                                    TOTAL_PAGE = homeScreenData.data!!.total_pages.toInt()
                                    isLoading = false
                                    if (currentPage < TOTAL_PAGE) {
                                        madapter.addLoading()
                                    } else {
                                        isLastPage = true
                                    }
                                } else {
                                    madapter.removeAllItem()
                                }

                            }
                            FAILURE -> {
                                Toast(activity!!, "" + response.body()!!.message)
                                showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    } else {

                        Log.e("HOME FRAGMENT", response.body().toString())
                        showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                    }
                }
            })
    }

    fun lodeMore() {
        val map = HashMap<String, String>()
        map["pageno"] = currentPage.toString()

        CustomProgressbar.showProgressBar(activity!!, false)
        RetrofitClientSingleton
            .getInstance()
            .getTopRestaurantList(map)
            .enqueue(object : retrofit2.Callback<TopRestaurantModel> {
                override fun onFailure(call: Call<TopRestaurantModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("HOME FRAGMENT", t.message)
                }

                override fun onResponse(
                    call: Call<TopRestaurantModel>,
                    response: Response<TopRestaurantModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {

                                val dataList = homeScreenData!!.data!!.top_restaurant_list
                                if (dataList != null && dataList.isNotEmpty()) {

                                    madapter.removeLoading()
                                    isLoading = false
                                    madapter.addAllItem(dataList)


                                    if (currentPage != TOTAL_PAGE) {
                                        madapter.addLoading()
                                    } else {
                                        isLastPage = true
                                    }
                                }
                            }
                            FAILURE -> {
                                Toast(activity!!, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }
}