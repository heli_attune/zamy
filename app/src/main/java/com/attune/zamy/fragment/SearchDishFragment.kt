package com.attune.zamy.fragment


import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.activity.MainActivity
import com.attune.zamy.activity.TopRestaurantsPage
import com.attune.zamy.adapter.DishAdapter
import com.attune.zamy.adapter.SearchRestaurantDishAdapter
import com.attune.zamy.custom.SearchProgressBar
import com.attune.zamy.interaface.RecyclerSectionItemDecoration
import com.attune.zamy.listener.PaginationScrollListener
import com.attune.zamy.model.SearchDataModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.fragment_search_dish.view.*
import kotlinx.android.synthetic.main.no_data_layout.view.*
import kotlinx.android.synthetic.main.search_box_layout.view.*
import kotlinx.android.synthetic.main.search_progressbar.view.*
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set


class SearchDishFragment : Fragment(), SearchRestaurantDishAdapter.OnItemSelectedListener,
    DishAdapter.OnDishItemSelectedListener {

    var strsearch: String = ""
    var searchDataModel: SearchDataModel.DataBean? = null
    lateinit var mView: View
    lateinit var edtSeaach: EditText
    lateinit var rvSearchRestaurants: RecyclerView
    lateinit var rvSearchDish: RecyclerView
    lateinit var dataModels: MutableList<SearchDataModel.DataBean.SearchDataBean>
    var TOTAL_PAGE: Int = 0
    var isLoading: Boolean = false
    var isLastPage: Boolean = false
    val PAGE_START = 1
    var currentPage: Int = PAGE_START
    lateinit var madapter: SearchRestaurantDishAdapter
    lateinit var menudataList: List<SearchDataModel.DataBean.SearchDataBean>
    lateinit var tvRestaurant: TextView
    lateinit var tvDish: TextView
    var isFirst = 0
    lateinit var rlRestdishView: LinearLayout
    var mainActivity=MainActivity()


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment


        mView = inflater.inflate(R.layout.fragment_search_dish, container, false)

        mainActivity = activity as MainActivity
        mainActivity.hideToolbar()
        initView()
        //mView.icSearchData.visibility=View.VISIBLE

        /*mView.searchprogressbar.visibility=View.GONE
        mView.icClose.visibility=View.VISIBLE*/
        mView.icClose.visibility = View.GONE
        return mView

    }

    private fun SelectOptions(bMenu: Boolean, bInfo: Boolean, bGallary: Boolean) {

        if (bMenu) {
            tvRestaurant.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
            tvRestaurant.setTextColor(resources.getColor(R.color.white))
            tvDish.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
            tvDish.setTextColor(resources.getColor(R.color.red_ea1b25))
            rvSearchDish.visibility = View.GONE
            rvSearchRestaurants.visibility = View.VISIBLE

        } else if (bInfo) {
            tvRestaurant.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
            tvRestaurant.setTextColor(resources.getColor(R.color.red_ea1b25))
            tvDish.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
            tvDish.setTextColor(resources.getColor(R.color.white))
            rvSearchDish.visibility = View.VISIBLE
            rvSearchRestaurants.visibility = View.GONE
        }


    }

    private fun initView() {


        showNoDataLayout(mView.llParentNoData, "Search Data  ")
        mView.icSearchData.visibility = View.VISIBLE

        //mView.searchprogressbar.visibility = View.GONE
        clearPaging()
        dataModels = ArrayList()
        edtSeaach = mView.findViewById(R.id.edSearchBox)
        rvSearchRestaurants = mView.findViewById(R.id.rvSearchRestaurants)
        rvSearchRestaurants.visibility = View.GONE
        rvSearchDish = mView.findViewById(R.id.rvSearchDish)
        rvSearchDish.visibility = View.VISIBLE
        rlRestdishView = mView.findViewById(R.id.rlRestdishView)
        rlRestdishView.visibility = View.GONE
        madapter = activity?.let { SearchRestaurantDishAdapter(it, dataModels, this) }!!
        edtSeaach.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                rlRestdishView.visibility = View.VISIBLE
                // getSearchRestaurantDish(false, strsearch)
                //getRestaurantDish()
                return true
            }
        })


        if (edtSeaach.text.isNotEmpty()) {
            mView.searchprogressbar.visibility = View.VISIBLE
            mView.icClose.visibility = View.GONE

        } else {
            mView.searchprogressbar.visibility = View.GONE
            mView.icClose.visibility = View.VISIBLE
        }

        mView.icClose.setOnClickListener {
            edtSeaach.text.clear()
        }

        rvTopRestaurantBind()
        rvDishBind()

        edtSeaach.addTextChangedListener(object : TextWatcher {
            override fun afterTextChanged(s: Editable) {
                Log.e("TAG", "afterTextChanged-->>>>$s")
                strsearch = s.toString()
                rlRestdishView.visibility = View.VISIBLE
                mView.searchprogressbar.visibility = View.VISIBLE
                when {
                    s.length > 3 -> {
                        getRestaurantDish(s.toString())
                        getSearchRestaurantDish(false, s.toString())
                        mView.icClose.visibility = View.VISIBLE
                        mView.lnSearch.visibility = View.VISIBLE
                        hideNoDataLayout(mView.llParentNoData)
                        mView.icSearchData.visibility = View.GONE
                    }
                    s.length<3 -> {
                        mView.icClose.visibility = View.GONE
                        madapter.clearList()
                        madapter.notifyDataSetChanged()
                        mView.lnSearch.visibility = View.GONE
                        mView.icClose.visibility = View.GONE
                        showNoDataLayout(mView.llParentNoData, "Search Data  ")
                        mView.icSearchData.visibility = View.VISIBLE
                        mView.searchBar.visibility=View.GONE
                    }
                    s.isEmpty() -> {
                        mView.icClose.visibility = View.GONE
                        madapter.clearList()
                        madapter.notifyDataSetChanged()
                        mView.lnSearch.visibility = View.GONE
                        mView.icClose.visibility = View.GONE
                        showNoDataLayout(mView.llParentNoData, "Search Data  ")
                        mView.icSearchData.visibility = View.VISIBLE
                        mView.searchBar.visibility=View.GONE
                    }
                }


            }

            override fun beforeTextChanged(
                s: CharSequence, start: Int,
                count: Int, after: Int
            ) {
                Log.e("TAG", "beforeTextChanged-->>>>$s")
            }

            override fun onTextChanged(
                s: CharSequence, start: Int,
                before: Int, count: Int
            ) {
                Log.e("TAG", "onTextChanged-->>>>$s")
            }
        })
        tvRestaurant = mView.findViewById(R.id.tvRestaurant)
        tvRestaurant.setOnClickListener {
            SelectOptions(true, false, false)
        }
        tvDish = mView.findViewById(R.id.tvDish)
        tvDish.setOnClickListener {
            SelectOptions(false, true, false)
        }
        SelectOptions(true, false, false)

    }

    private fun clearPaging() {
        currentPage = PAGE_START
        isLoading = false
        isLastPage = false
    }

    private fun rvTopRestaurantBind() {
        val mLayoutManager = LinearLayoutManager(activity)
        rvSearchRestaurants.layoutManager = mLayoutManager
        madapter = activity?.let { SearchRestaurantDishAdapter(it, dataModels, this) }!!
        rvSearchRestaurants.adapter = madapter
        rvSearchRestaurants.addOnScrollListener(object : PaginationScrollListener(mLayoutManager) {
            override val totalPageCount: Int
                get() = TOTAL_PAGE

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                //you have to call loadmore items to get more data
                currentPage += 1
                lodeMore(strsearch)
            }
        })

    }

    fun rvDishBind() {
        // getRestaurantDish()
    }

    private fun getSearchRestaurantDish(isrefresh: Boolean, strsearch: String) {

        mView.searchBar.visibility = View.VISIBLE
        if (!isrefresh) {
            // SearchProgressBar.showSearchProgressBar(activity!!, false)
        }
        val map = HashMap<String, String>()
        map["search_keyword"] = strsearch
        map["pageno"] = currentPage.toString()


        RetrofitClientSingleton
            .getInstance()
            .getsearchDishRestaurant(map)
            .enqueue(object : retrofit2.Callback<SearchDataModel> {
                override fun onFailure(call: Call<SearchDataModel>, t: Throwable) {
                    SearchProgressBar.hideSearchProgressBar()
                    Log.e("Search FRAGMENT", t.message)
                }

                override fun onResponse(
                    call: Call<SearchDataModel>,
                    response: Response<SearchDataModel>
                ) {

                    SearchProgressBar.hideSearchProgressBar()
                    if (response.isSuccessful) {
                        mView.searchBar.visibility = View.GONE
                        mView.icSearchData.visibility = View.GONE
                        mView.searchprogressbar.visibility = View.GONE

                        val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {

                                val dataList = homeScreenData!!.data!!.search_data
                                searchDataModel = homeScreenData.data
                                if (dataList != null && dataList.size > 0) {
                                    madapter.removeAllItem()
                                    madapter.addAllItem(dataList)
                                    TOTAL_PAGE = homeScreenData.data!!.total_pages
                                    isLoading = false
                                    if (currentPage < TOTAL_PAGE) {
                                        madapter.addLoading()
                                    } else {
                                        isLastPage = true
                                    }
                                    mView.llParentNoData.visibility = View.GONE
                                    mView.lnSearch.visibility = View.VISIBLE
                                } else {
                                    mView.llParentNoData.visibility = View.VISIBLE
                                    mView.lnSearch.visibility = View.GONE
                                    madapter.removeAllItem()
                                }
                            }
                            FAILURE -> {
                                Toast(activity!!, "" + response.body()!!.message)
                                mView.llParentNoData.visibility = View.VISIBLE
                                mView.icSearchData.visibility = View.VISIBLE

                                mView.lnSearch.visibility = View.GONE

                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    } else {
                        mView.llParentNoData.visibility = View.VISIBLE
                        mView.icSearchData.visibility = View.VISIBLE
                        mView.lnSearch.visibility = View.GONE
                        mView.searchBar.visibility = View.GONE
                        madapter.removeAllItem()
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }

    fun lodeMore(strsearch: String) {
        val map = HashMap<String, String>()
        map["search_keyword"] = this.strsearch
        map["pageno"] = currentPage.toString()

        //  CustomProgressbar.showProgressBar(activity!!, false)
        RetrofitClientSingleton
            .getInstance()
            .getsearchDishRestaurant(map)
            .enqueue(object : retrofit2.Callback<SearchDataModel> {
                override fun onFailure(call: Call<SearchDataModel>, t: Throwable) {
                    // CustomProgressbar.hideProgressBar()
                    Log.e("HOME FRAGMENT", t.message)
                }

                override fun onResponse(
                    call: Call<SearchDataModel>,
                    response: Response<SearchDataModel>
                ) {
                    // CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {

                                var dataList = homeScreenData!!.data!!.search_data
                                if (dataList != null && dataList.size > 0) {

                                    madapter.removeLoading()
                                    isLoading = false
                                    madapter.addAllItem(dataList)


                                    if (currentPage != TOTAL_PAGE) {
                                        madapter.addLoading()
                                    } else {
                                        isLastPage = true
                                    }
                                    mView.lnSearch.visibility = View.VISIBLE
                                    mView.llParentNoData.visibility = View.GONE
                                } else {
                                    mView.llParentNoData.visibility = View.VISIBLE
                                    mView.lnSearch.visibility = View.GONE
                                    //madapter.removeAllItem()
                                }
                                //top
                                /* rvTopRest.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                                 rvTopRest.adapter = activity?.let {
                                     TopRestaurantsAdapter(homeScreenData!!.data!!.top_restaurant_list,
                                         it,this@TopRestaurantFragment)*/


                            }
                            FAILURE -> {
                                Toast(activity!!, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    } else {
                        mView.llParentNoData.visibility = View.VISIBLE
                        mView.lnSearch.visibility = View.GONE
                        madapter.removeAllItem()
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }

    private fun getRestaurantDish(key: String) {

        //SearchProgressBar.showSearchProgressBar(activity, false)
        val map = HashMap<String, String>()
        map["search_keyword"] = key
        map["pageno"] = currentPage.toString()

        RetrofitClientSingleton
            .getInstance()
            .getsearchDishRestaurant(map)
            .enqueue(object : retrofit2.Callback<SearchDataModel> {
                override fun onFailure(call: Call<SearchDataModel>, t: Throwable) {
                    //SearchProgressBar.hideSearchProgressBar()
                    activity?.let { Toast(it, t.message) }

                }

                override fun onResponse(
                    call: Call<SearchDataModel>,
                    response: Response<SearchDataModel>
                ) {
                    //SearchProgressBar.hideSearchProgressBar()
                    if (response.isSuccessful) {
                        val menuList = response.body()!!.data
                        response.body()!!.data!!.search_data
                        //llRestaurantDetails.visibility = View.VISIBLE
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                isFirst += 1
                                if (menuList!!.search_data!!.size > 0) {

                                    val menuHeader = menuList

                                    rvSearchDish.layoutManager =
                                        LinearLayoutManager(activity)

                                    menudataList = response.body()!!.data!!.search_data!!

                                    var adapter = activity?.let {
                                        DishAdapter(
                                            it,
                                            response.body()!!.data!!.search_data!!,
                                            this@SearchDishFragment
                                        )
                                    }!!

                                    rvSearchDish.adapter = adapter

                                    if (isFirst == 1) {
                                        val sectionItemDecoration = RecyclerSectionItemDecoration(
                                            LinearLayout.LayoutParams.WRAP_CONTENT,
                                            true, getSectionCallback(menuHeader.search_data!!)

                                        )
                                        rvSearchDish.addItemDecoration(sectionItemDecoration)
                                    }
                                    mView.llParentNoData.visibility = View.GONE
                                    mView.lnSearch.visibility = View.VISIBLE
                                }
                            }
                            FAILURE -> {
                                mView.llParentNoData.visibility = View.VISIBLE
                                mView.lnSearch.visibility = View.GONE
                                madapter.removeAllItem()
                            }
                            AUTH_FAILURE -> {
                            }
                        }

                    } else {
                        mView.llParentNoData.visibility = View.VISIBLE
                        mView.lnSearch.visibility = View.GONE
                        mView.searchprogressbar.visibility = View.GONE
                        mView.icClose.visibility = View.VISIBLE
                        madapter.removeAllItem()
                    }

                }

            })
    }

    private fun getSectionCallback(people: List<SearchDataModel.DataBean.SearchDataBean>): RecyclerSectionItemDecoration.SectionCallback {
        return object : RecyclerSectionItemDecoration.SectionCallback {
            override fun isSection(position: Int): Boolean {
                return position == 0 || people[position]
                    .restaurant!!.res_name !== people[position - 1]
                    .restaurant!!.res_name
            }

            override fun getSectionHeader(position: Int): CharSequence {
                return people[position]
                    .restaurant!!.res_name.toString()
            }
        }
    }

    override fun onDishItemSelected(item: SearchDataModel.DataBean.SearchDataBean, position: Int) {
        var intent = Intent(activity, TopRestaurantsPage::class.java)
        intent.putExtra("PageName", "Detail")
        intent.putExtra(RESTAURANT_ID_PASS, item.restaurant!!.id)

        activity!!.startActivity(intent)
    }

    override fun onItemSelected(
        item: SearchDataModel.DataBean.SearchDataBean,
        position: Int,
        adapterPostion: Int
    ) {
        var intent = Intent(activity, TopRestaurantsPage::class.java)
        intent.putExtra("PageName", "Detail")
        intent.putExtra(RESTAURANT_ID_PASS, item.restaurant!!.id)

        activity!!.startActivity(intent)
    }

}
