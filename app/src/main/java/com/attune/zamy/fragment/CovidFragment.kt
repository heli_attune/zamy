package com.attune.zamy.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewpager.widget.ViewPager
import com.attune.zamy.R
import com.attune.zamy.activity.MainActivity
import com.attune.zamy.adapter.DemoAdapret
import com.attune.zamy.custom.AdapterDropAndDelivery
import com.google.android.material.tabs.TabLayout

class CovidFragment : Fragment() {

//    var isLoading: Boolean = false
    var isLastPage: Boolean = false
    lateinit var adapter: DemoAdapret
    lateinit var covidView: View

    lateinit var mViewPager: ViewPager
    lateinit var tabs:TabLayout


    var mainActivity = MainActivity()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        covidView = inflater.inflate(R.layout.fragment_covid, container, false)
        mainActivity = activity as MainActivity
        mainActivity.showToolbar()
        mViewPager = covidView.findViewById(R.id.viewPager)
        tabs=covidView.findViewById(R.id.tabs)

        initView()
        return covidView
    }

     fun getPagePosition(): ViewPager
    {
        if (null == mViewPager) {
            mViewPager = covidView.findViewById(R.id.viewPager)
        }
        return mViewPager
    }

    private fun initView() {
        val adapter = AdapterDropAndDelivery(childFragmentManager!!)
        adapter.addFragment(PickUpAddressFragment(), "PickUpDrop")
        adapter.addFragment(CalculateDistanceFragment(), "Distance")
        adapter.addFragment(PickUpDropPaymentFragment(), "Payment")
        mViewPager.adapter=adapter
        tabs.tabGravity = TabLayout.GRAVITY_FILL
        tabs.setupWithViewPager(mViewPager)
        mViewPager.addOnPageChangeListener(TabLayout.TabLayoutOnPageChangeListener(tabs))
        tabs.addOnTabSelectedListener(object :
            TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                mViewPager.currentItem = tab.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab) {

            }

            override fun onTabReselected(tab: TabLayout.Tab) {

            }
        })
    }





}