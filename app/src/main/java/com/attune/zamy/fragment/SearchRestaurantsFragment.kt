package com.attune.zamy.fragment


import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.EditText
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.adapter.SearchRestaurantDishAdapter
import com.attune.zamy.model.SearchModel
import com.attune.zamy.utils.Toast

class SearchRestaurantsFragment : Fragment(){/*, SearchRestaurantDishAdapter.OnItemSelectedListener {
    override fun onItemSelected(
        item: SearchModel.DataBean.SearchDataBean,
        position: Int,
        adapterPostion: Int
    ) {

    }*/
    lateinit var searchstr: String
    lateinit var mview: View
    lateinit var edtSeaach: EditText
    lateinit var rvSearch: RecyclerView
    lateinit var dataModels: MutableList<SearchModel.DataBean.SearchDataBean>
    var TOTAL_PAGE: Int = 0
    var isLoading: Boolean = false
    var isLastPage: Boolean = false
    val PAGE_START = 1
    var currentPage: Int = PAGE_START
    lateinit var madapter: SearchRestaurantDishAdapter
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val mView = inflater.inflate(R.layout.fragment_search_restaurants, container, false)
            val codeForthisFragment = arguments!!.getInt("code")

            initView()

        return mView
    }

    private fun initView() {
        //getSearchRestaurantDish(false, strsearch)
        dataBind()
    }


    private fun clearPaging() {
        currentPage = PAGE_START
        isLoading = false
        isLastPage = false
    }

    fun dataBind(){
        val mfragment = SearchFragment()
        this.activity?.let { Toast(it,mfragment.strsearch) }
        Log.e("data+",mfragment.strsearch)
        if(mfragment.searchDataModel!=null) {
            Log.v("bind", mfragment.searchDataModel!!.search_data[0].menu_name)

        }


    }

    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if(isVisibleToUser){
            val mfragment = SearchFragment()
            this.activity?.let { Toast(it,mfragment.strsearch) }

        }
    }
  /*  private fun getSearchRestaurantDish(isrefresh: Boolean, searchstr: String) {
        if (!isrefresh) {
            CustomProgressbar.showProgressBar(activity!!, false)
        }
        val map = HashMap<String, String>()
        map["search_keyword"] = searchstr
        map["pageno"] = currentPage.toString()


        RetrofitClientSingleton
            .getInstance()
            .getsearchDishRestaurant(map)
            .enqueue(object : retrofit2.Callback<SearchModel> {
                override fun onFailure(call: Call<SearchModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("Search FRAGMENT", t.message)
                }

                override fun onResponse(call: Call<SearchModel>, response: Response<SearchModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {

                                var dataList = homeScreenData!!.data!!.search_data
                                if (dataList != null && dataList.size > 0) {

                                    madapter.removeAllItem()
                                    madapter.addAllItem(dataList)
                                    TOTAL_PAGE = homeScreenData.data!!.total_pages.toInt()
                                    isLoading = false
                                    if (currentPage < TOTAL_PAGE) {
                                        madapter.addLoading()
                                    } else {
                                        isLastPage = true
                                    }
                                } else {
                                    madapter.removeAllItem()
                                }
                            }
                            FAILURE -> {
                                Toast(activity!!, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }

    fun lodeMore(searchstr: String) {
        val map = HashMap<String, String>()
        map["search_keyword"] = this.searchstr
        map["pageno"] = currentPage.toString()

        CustomProgressbar.showProgressBar(activity!!, false)
        RetrofitClientSingleton
            .getInstance()
            .getsearchDishRestaurant(map)
            .enqueue(object : retrofit2.Callback<SearchModel> {
                override fun onFailure(call: Call<SearchModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("HOME FRAGMENT", t.message)
                }

                override fun onResponse(call: Call<SearchModel>, response: Response<SearchModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {

                                var dataList = homeScreenData!!.data!!.search_data
                                if (dataList != null && dataList.size > 0) {

                                    madapter.removeLoading()
                                    isLoading = false
                                    madapter.addAllItem(dataList)


                                    if (currentPage != TOTAL_PAGE) {
                                        madapter.addLoading()
                                    } else {
                                        isLastPage = true
                                    }
                                } else {
                                    //madapter.removeAllItem()
                                }
                                //top
                                *//* rvTopRest.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                                 rvTopRest.adapter = activity?.let {
                                     TopRestaurantsAdapter(homeScreenData!!.data!!.top_restaurant_list,
                                         it,this@TopRestaurantFragment)*//*


                            }
                            FAILURE -> {
                                Toast(activity!!, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }*/

}
