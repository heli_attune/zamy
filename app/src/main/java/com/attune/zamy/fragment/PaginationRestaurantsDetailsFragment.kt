package com.attune.zamy.fragment


import android.app.Dialog
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.OnScrollListener
import com.attune.zamy.R
import com.attune.zamy.adapter.DemoPaginationAdappter
import com.attune.zamy.interaface.RecyclerSectionItemDecoration
import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.OrderCart
import com.attune.zamy.model.RestaurantsMenuModel
import com.attune.zamy.model.RestaurantsMenuModel.DataBean.CategoryBean
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.google.android.material.bottomsheet.BottomSheetBehavior
import retrofit2.Call
import retrofit2.Response
import kotlin.collections.set


@Suppress("DEPRECATION")
class PaginationRestaurantsDetailsFragment : Fragment(),
    DemoPaginationAdappter.VarianceDialogClickListener,
    DemoPaginationAdappter.AddtoCartItemClickListener {
    override fun onMenuItemSelected(
        listBean: CartRestaurantsMenuModel,
        isProductVariance: Boolean,
        isRemove: Boolean
    ) {

    }

    override fun onVarianceDialogSelected(
        dataBean: List<CategoryBean.MenuBean.ProductVariationBean>,
        catId: String?,
        foodmenuId: String?,
        menuName: String?,
        taxPrice: Double,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {

    }


    var totalRecords: Int = 0
    val isSwitched: Boolean = true
    lateinit var menudataList: List<RestaurantsMenuModel.DataBean.CategoryBean>
    private var productListArray: ArrayList<RestaurantsMenuModel.DataBean.CategoryBean> =
        ArrayList()
    private lateinit var mAdapter: DemoPaginationAdappter
    var userId: String = ""
    var finalTotalValue: Double = 0.00
    private lateinit var varianceitem: CartRestaurantsMenuModel
    var dialog: Dialog? = null
    lateinit var mView: View
    var restaurant_id: String = "3"
    lateinit var tvMenu: TextView
    lateinit var tvInfo: TextView
    lateinit var tvGallary: TextView
    private lateinit var sheetBehavior: BottomSheetBehavior<LinearLayout>
    private lateinit var sheetBehaviorvariance: BottomSheetBehavior<LinearLayout>
    lateinit var txtItem: TextView
    lateinit var txtPrice: TextView
    lateinit var txtCharges: TextView
    lateinit var txtViewCart: TextView
    var cartArrayList: MutableList<CartRestaurantsMenuModel> = ArrayList<CartRestaurantsMenuModel>()
    var orderCartModel: ArrayList<OrderCart> = ArrayList<OrderCart>()
    var linearLayoutManager: LinearLayoutManager? = null
    lateinit var rvMenu: RecyclerView

    var TOTAL_PAGE: Int = 0
    var isLoading: Boolean = false
    var isLastPage: Boolean = false
    val PAGE_START = 1
    var currentPage: Int = PAGE_START
    lateinit var dataModels: MutableList<RestaurantsMenuModel.DataBean.CategoryBean>
    var isLoadingProducts = false
    var isLastPageProduct = false
    var page: Int = 1
    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.layout_demo_pagination_recyclerview, container, false)
        initView()


        return mView
    }

    private fun initView() {
        dataModels = ArrayList()
        /* val args = arguments

         if(args!=null)
         {
             restaurant_id = args!!.getString(RESTAURANT_ID_PASS)!!
         }
 */
        userId = activity?.let { SharedPref.getUserId(it) }!!
        //  restaurant_id = intent.getStringExtra(RESTAURANT_ID_PASS) ?: "0"
        /* val toolbar_layout = mView.findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)
         var toolbar = mView.findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar1)
       //  text_toolbar_title= mView.findViewById<TextView>(R.id.text_toolbar_title)
         toolbar_layout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
         toolbar_layout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
         if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
             toolbar.setTitle("xyz")
         }
         toolbar.setNavigationOnClickListener {
             activity!!.onBackPressed()

         }
         tvMenu = mView.findViewById(R.id.tvMenu)
         tvMenu.setOnClickListener {
             SelectOptions(true, false, false)
         }
         tvInfo = mView.findViewById(R.id.tvInfo)
         tvInfo.setOnClickListener {
             SelectOptions(false, true, false)
         }
         tvGallary = mView.findViewById(R.id.tvGallary)
         tvGallary.setOnClickListener {
             SelectOptions(false, false, true)
         }
         bottom_sheet = mView.findViewById(R.id.bottom_sheet)
         sheetBehavior = BottomSheetBehavior.from<LinearLayout>(bottom_sheet)
 //        bottom_sheet_variance = mView.findViewById(R.id.bottom_sheet_variance)
         //      sheetBehaviorvariance = BottomSheetBehavior.from<LinearLayout>(bottom_sheet_variance)

         sheetBehavior.setState(BottomSheetBehavior.STATE_HIDDEN)
         if (cartArrayList.size > 0) {
             sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
             bottomSheetForCart()
             sheetBehavior.isHideable = false
         }
         bottomSheetForCart()

         SelectOptions(true, false, false)*/


        rvMenu = mView.findViewById(R.id.rvMenu)
        linearLayoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.VERTICAL, false)
        rvMenu.layoutManager = linearLayoutManager
        /*rvMenu.layoutManager = LinearLayoutManager(activity)*/
        mAdapter = DemoPaginationAdappter(activity!!, productListArray,this,this,"3")
        rvMenu.adapter = mAdapter
        rvMenu.addOnScrollListener(recyclerViewOnScrollListener)
        if (!activity?.let { isNetworkAvailable(it) }!!) {
            activity?.let { Toast(it, getString(R.string.network_error)) }
        } else {
            //menu items call api
            getProductData()
        }


        /* adapter = activity?.let {
             DemoRestaurantDetailMenuAdapter(
                 it,
                 productListArray,
                 this@RestaurantsDetailsFragment,
                 this@RestaurantsDetailsFragment,restaurant_id
             )
         }!!

         rvMenu.adapter = adapter
         rvMenu.addOnScrollListener(recyclerViewOnScrollListener)
         if (!activity?.let { isNetworkAvailable(it) }!!) {
             activity?.let { Toast(it, getString(R.string.network_error)) }
         } else {
             //menu items call api
             getProductData()
         }*/

    }

    private fun getProductData() {

        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurant_id
        map["user_id"] = userId
        map["pageno"] = page.toString()

        // CustomProgressbar.showProgressBar(activity!!, false)
        RetrofitClientSingleton
            .getInstance()
            .getReastaurentMenu(map)
            //  showProgressDialog(this@ProductListActivity, rvProduct)

            //  RetrofitClientSingleton.getInstance().getProduct(SharedPref.getAuthToken(this@ProductListActivity), map)
            .enqueue(object : retrofit2.Callback<RestaurantsMenuModel> {

                override fun onResponse(
                    call: Call<RestaurantsMenuModel>,
                    response: Response<RestaurantsMenuModel>
                ) {
                    isLoadingProducts = false

                    if (page == 1) productListArray.clear()

                    //  stopProgress()

                    if (response.isSuccessful) {
                        val responseProductCategory = response.body()
                        when (responseProductCategory?.status) {
                            SUCCESS -> {

                                val productDataResponse = response.body()
                                productDataResponse?.let {

                                    if (productDataResponse.data!!.category != null) {
                                        val list = productDataResponse.data!!.category!! as MutableList
                                        if (list.size > 0) {
                                            totalRecords = productDataResponse.data!!.total_rows
                                            /*productArray = productDataResponse.data!!.category*/
                                            if (list.isNotEmpty()) {
                                                //  hideNoDataLayout(llParentNoData)
                                                mAdapter.addAll(list)
                                            } else {
                                                /* if (page == 1){

                                                 }*/
                                                //    showNoDataLayout(viewFragment!!.llParentNoData, getString(R.string.no_data_fnd))
                                            }

                                            /*for ()*/
                                            if (totalRecords <= mAdapter.itemCount) {
                                                isLastPageProduct = true
                                            }
                                            val sectionItemDecoration = RecyclerSectionItemDecoration(
                                                LinearLayout.LayoutParams.WRAP_CONTENT,
                                                true, getSectionCallback(productDataResponse.data!!.category!!)

                                            )

                                            rvMenu.addItemDecoration(sectionItemDecoration)
                                            /*if (list.size < 10) {
                                            isLastPageProduct = true
                                        }*/
                                        } else {
                                            //showNoDataLayout(llParentNoData, "No data found")
                                        }
                                    } else {
                                        //showNoDataLayout(llParentNoData, "No data found")
                                    }
                                }
                            }

                            FAILURE -> {
                                Toast(activity!!, responseProductCategory.message)
                            }

                            AUTH_FAILURE -> {
                                //forceLogout(this@ProductListActivity)
                            }
                        }
                    }
                }

                override fun onFailure(call: Call<RestaurantsMenuModel>, t: Throwable) {
                    isLoadingProducts = false

                    //   stopProgress()
                    // Log.e(TAG, t.message)
                    // toast(this@ProductListActivity, R.string.something_went_wrong)
                }
            })
    }
    private fun getSectionCallback(people: List<CategoryBean>): RecyclerSectionItemDecoration.SectionCallback {
        return object : RecyclerSectionItemDecoration.SectionCallback {
            override fun isSection(position: Int): Boolean {
                return position == 0 || people[position]
                    .cat_display_name !== people[position - 1]
                    .cat_display_name
            }

            override fun getSectionHeader(position: Int): CharSequence {
                return people[position]
                    .cat_display_name.toString()
            }
        }
    }
    private fun clearPaging() {
        currentPage = PAGE_START
        isLoading = false
        isLastPage = false
    }
    /* private fun rvTopRestaurantBind() {
         val mLayoutManager = LinearLayoutManager(activity)
         mView.rvMenu!!.layoutManager = mLayoutManager
         adapter = activity?.let { DemoPaginationAdappter(it,dataModels) }!!
         mView.rvMenu.adapter=adapter
         mView.rvMenu!!.addOnScrollListener(object : PaginationScrollListener(mLayoutManager) {
             override val totalPageCount: Int
                 get() = TOTAL_PAGE

             override fun isLastPage(): Boolean {
                 return isLastPage
             }

             override fun isLoading(): Boolean {
                 return isLoading
             }

             override fun loadMoreItems() {
                 isLoading = true
                 //you have to call loadmore items to get more data
                 currentPage += 1
                 lodeMore()
             }
         })

     }*/


    /* private val recyclerViewOnScrollListener = object : OnScrollListener() {
         override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
             super.onScrolled(recyclerView, dx, dy)
             val visibleItemCount = linearLayoutManager!!.childCount
             val firstVisibleItemPosition = linearLayoutManager!!.findFirstVisibleItemPosition()
             val totalItemCount = adapter!!.itemCount
             if (!isLoadingProducts && !isLastPageProduct) {
                 if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                     loadMoreProducts()
                 }
             }
         }
     }


      private fun loadMoreProducts() {
          isLoadingProducts = true

          page += 1

          if (!isNetworkAvailable(activity!!)) {

          } else {
              android.os.Handler().postDelayed({
                  //api call
                  getProductData()
              }, 1000)
          }

      }*/
    /*  private fun SelectOptions(bMenu: Boolean, bInfo: Boolean, bGallary: Boolean) {

          if (bMenu) {
              tvMenu.background =
                  ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
              tvMenu.setTextColor(resources.getColor(R.color.red_ea1b25))
              tvInfo.background =
                  ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
              tvInfo.setTextColor(resources.getColor(R.color.white))
              tvGallary.background =
                  ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
              tvGallary.setTextColor(resources.getColor(R.color.white))

          } else if (bInfo) {
              tvMenu.background =
                  ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
              tvMenu.setTextColor(resources.getColor(R.color.white))
              tvInfo.background =
                  ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
              tvInfo.setTextColor(resources.getColor(R.color.red_ea1b25))
              tvGallary.background =
                  ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
              tvGallary.setTextColor(resources.getColor(R.color.white))
          } else if (bGallary) {
              tvMenu.background =
                  ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
              tvMenu.setTextColor(resources.getColor(R.color.white))
              tvInfo.background =
                  ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
              tvInfo.setTextColor(resources.getColor(R.color.white))
              tvGallary.background =
                  ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
              tvGallary.setTextColor(resources.getColor(R.color.red_ea1b25))
          }


      }*/
    private val recyclerViewOnScrollListener = object : OnScrollListener() {
        override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
            super.onScrolled(recyclerView, dx, dy)
            val visibleItemCount = linearLayoutManager!!.childCount
            val firstVisibleItemPosition = linearLayoutManager!!.findFirstVisibleItemPosition()
            val totalItemCount = mAdapter.itemCount
            if (!isLoadingProducts && !isLastPageProduct) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                    loadMoreProducts()
                }
            }
        }
    }

    private fun loadMoreProducts() {
        isLoadingProducts = true

        page += 1

        if (!isNetworkAvailable(activity!!)) {

        } else {
            android.os.Handler().postDelayed({
                getProductData()
            }, 1000)
        }

    }
    /*fun lodeMore() {
        CustomProgressbar.showProgressBar(activity, false)
        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurant_id
        map["user_id"] = userId
        map["pageno"] = currentPage.toString()

        CustomProgressbar.showProgressBar(activity!!, false)
        RetrofitClientSingleton
            .getInstance()
            .getReastaurentMenu(map)

            .enqueue(object : retrofit2.Callback<RestaurantsMenuModel> {
                override fun onFailure(call: Call<RestaurantsMenuModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("HOME FRAGMENT", t.message)
                }

                override fun onResponse(call: Call<RestaurantsMenuModel>, response: Response<RestaurantsMenuModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {

                                var dataList= homeScreenData!!.data!!.category
                                if(dataList!=null && dataList.size>0) {
                                    val menuHeader = response.body()!!.data
                                    adapter.removeLoading()
                                    isLoading=false
                                    adapter.addAllItem(dataList)


                                    if (currentPage != TOTAL_PAGE) {
                                        adapter.addLoading()
                                    } else {
                                        isLastPage = true
                                    }
                                  *//*  val sectionItemDecoration = RecyclerSectionItemDecoration(
                                        LinearLayout.LayoutParams.WRAP_CONTENT,
                                        true, getSectionCallback(menuHeader!!.category!!)

                                    )

                                    rvMenu.addItemDecoration(sectionItemDecoration)*//*
                                }else {
                                    //madapter.removeAllItem()
                                }
                                //top
                                *//* rvTopRest.layoutManager = LinearLayoutManager(activity, LinearLayoutManager.VERTICAL, false)
                                 rvTopRest.adapter = activity?.let {
                                     TopRestaurantsAdapter(homeScreenData!!.data!!.top_restaurant_list,
                                         it,this@TopRestaurantFragment)*//*






                            }
                            FAILURE -> {
                                Toast(activity!!, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }*/
    /* private fun getTopRestaurantDataAPI() {
         //if (!isrefresh) {
             CustomProgressbar.showProgressBar(activity!!, false)
         //}
         CustomProgressbar.showProgressBar(activity, false)
         val map = HashMap<String, String>()
         map["restaurant_id"] = restaurant_id
         map["user_id"] = userId
         map["pageno"] = page.toString()

         RetrofitClientSingleton
             .getInstance()
             .getReastaurentMenu(map)
             .enqueue(object : retrofit2.Callback<RestaurantsMenuModel> {
                 override fun onFailure(call: Call<RestaurantsMenuModel>, t: Throwable) {
                     CustomProgressbar.hideProgressBar()
                     Log.e("HOME FRAGMENT", t.message)
                 }

                 override fun onResponse(call: Call<RestaurantsMenuModel>, response: Response<RestaurantsMenuModel>) {
                     CustomProgressbar.hideProgressBar()
                     isLoadingProducts = false

                     if (page == 1) productListArray.clear()

                     if (response.isSuccessful) {
                         val homeScreenData = response.body()
                         when (response.body()!!.status) {
                             SUCCESS -> {

                                 val productDataResponse = response.body()
                                 productDataResponse?.let {

                                     if (productDataResponse.data!!.category != null) {
                                         val list = productDataResponse.data!!.category!!
                                         if (list.size > 0) {
                                             totalRecords = productDataResponse.data!!.total_pages

                                             if (list.isNotEmpty()) {
                                                 //hideNoDataLayout(llParentNoData)
                                                 mAdapter!!.addAll(list)
                                             } else {
                                                 if (page == 1){}
                                                     //showNoDataLayout(viewFragment!!.llParentNoData, getString(R.string.no_data_fnd))
                                             }

                                             *//*for ()*//*
                                            if (totalRecords <= mAdapter!!.itemCount) {
                                                isLastPageProduct = true
                                            }

                                            *//*if (list.size < 10) {
                                            isLastPageProduct = true
                                        }*//*
                                        } else {
                                           // showNoDataLayout(llParentNoData, "No data found")
                                        }
                                    } else {
                                        //showNoDataLayout(llParentNoData, "No data found")
                                    }
                                }



                            }
                            FAILURE -> {
                                Toast(activity!!, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }*/
    /* private fun getRestaurentMenuList() {

         CustomProgressbar.showProgressBar(activity, false)
         val map = HashMap<String, String>()
         map["restaurant_id"] = restaurant_id
         map["user_id"] = userId
         map["pageno"] = currentPage.toString()

         RetrofitClientSingleton
             .getInstance()
             .getReastaurentMenu(map)
             .enqueue(object : retrofit2.Callback<RestaurantsMenuModel> {
                 override fun onFailure(call: Call<RestaurantsMenuModel>, t: Throwable) {
                     CustomProgressbar.hideProgressBar()
                     activity?.let { Toast(it, t.message) }

                 }

                 override fun onResponse(
                     call: Call<RestaurantsMenuModel>,
                     response: Response<RestaurantsMenuModel>
                 ) {
                     CustomProgressbar.hideProgressBar()
                     if (response.isSuccessful) {
                         val menuList = response.body()!!.data
                         response.body()!!.data!!.category
                         //llRestaurantDetails.visibility = View.VISIBLE
                         when (response.body()!!.status) {
                             SUCCESS -> {
                                 //Log.e("TAG", "${menuList!!.restaurant_details!!}")
                                 var restaurantdata = menuList!!.restaurant_details
                                 if (restaurantdata.images != null && restaurantdata.images != "") {
                                     activity?.let {
                                         Glide.with(it)
                                             .load(restaurantdata.images)
                                             .into(mView.imgRestaurant)
                                     }
                                 }
                                 if(restaurantdata.restaurant_favorite.equals("yes")){
                                     mView.imgFav.setImageDrawable(activity?.let {
                                         ContextCompat.getDrawable(
                                             it,R.drawable.ic_favorite_selected_red)
                                     })
                                 }else{
                                     mView.imgFav.setImageDrawable(activity?.let {
                                         ContextCompat.getDrawable(
                                             it,R.drawable.ic_favorite_unselected_red)
                                     })
                                 }

                                 if (restaurantdata.res_name != null && restaurantdata.res_name != "") {
                                     mView.tvReastoTitle.text = menuList.restaurant_details.res_name
                                     mView.toolbar_layout.title =
                                         menuList.restaurant_details.res_name

                                 }
                                 if (restaurantdata.additional_info != null && restaurantdata.additional_info != "") {
                                     mView.tvReastoDesc.text = menuList.restaurant_details.additional_info

                                 }

                                *//* tvReastoDesc.text =
                                      menuList.restaurant_details!!.area + "," + menuList.restaurant_details!!.landmark*//*


                                if (menuList.category!!.size > 0) {

                                    val menuHeader = menuList


                                    rvMenu.layoutManager =
                                        LinearLayoutManager(activity)

                                    menudataList = response.body()!!.data!!.category!!

                                    mAdapter = activity?.let {
                                        DemoPaginationAdappter(
                                            it,
                                            response.body()!!.data!!.category!! as MutableList<CategoryBean>,
                                        this,this,restaurant_id)
                                    }!!

                                    rvMenu.adapter = mAdapter

                                    val sectionItemDecoration = RecyclerSectionItemDecoration(
                                        LinearLayout.LayoutParams.WRAP_CONTENT,
                                        true, getSectionCallback(menuHeader.category!!)

                                    )

                                    rvMenu.addItemDecoration(sectionItemDecoration)
                                }
                            }
                            FAILURE -> { }
                            AUTH_FAILURE -> { }
                        }

                    }

                }

            })
    }*/

    /* private fun getSectionCallback(people: List<CategoryBean>): RecyclerSectionItemDecoration.SectionCallback {
         return object : RecyclerSectionItemDecoration.SectionCallback {
             override fun isSection(position: Int): Boolean {
                 return position == 0 || people[position]
                     .cat_display_name !== people[position - 1]
                     .cat_display_name
             }

             override fun getSectionHeader(position: Int): CharSequence {
                 return people[position]
                     .cat_display_name.toString()
             }
         }
     }

     private fun bottomSheetForCart() {

         txtItem = mView.findViewById(R.id.txtItem) as TextView
         txtPrice = mView.findViewById(R.id.txtPrice) as TextView
         txtCharges = mView.findViewById(R.id.txtItem) as TextView
         txtViewCart = mView.findViewById(R.id.txtViewCart) as TextView

         txtViewCart.setOnClickListener {
             Log.e("cart size", cartArrayList.size.toString())


             if (cartArrayList.size > 0) {
                 val args = Bundle()
                 args.putString(RESTAURANT_ID_PASS,restaurant_id)
                 val toFragment = CartToolbarFragment()
                 toFragment.setArguments(args)
                 fragmentManager!!
                     .beginTransaction()
                     .add(R.id.fragment_restaurant, toFragment,"CartToolbarFragment")
                     .addToBackStack("CartToolbarFragment").commit()
                 *//*val args = Bundle()
                args.putString(RESTAURANT_ID_PASS,restaurant_id)
                val manager = fragmentManager
                val transaction = manager!!.beginTransaction()
                transaction.replace(R.id.fragment_restaurant, CartToolbarFragment())
                transaction.commit()
                txtItem.text = cartArrayList.size.toString() + " Items"*//*

                adddatatoStore()
                //addDatatoJson()
            }


        }

        //bottom sheet

        */
    /**
     * bottom sheet state change listener
     * we are changing button text when sheet changed state
     * *//*
        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })
    }

    fun adddatatoStore(){
        var CartItemArrayModel: ArrayList<OrderCart> =
            ArrayList<OrderCart>()
        if (cartArrayList.size > 0) {
            for (i in 0 until cartArrayList.size) {

                val favoriteList = OrderCart()
                favoriteList.food_menu_id = cartArrayList[i].foodmenu_id
                favoriteList.menu_name = cartArrayList[i].menu_name
                favoriteList.variation_id = cartArrayList[i].variation_id
                favoriteList.variation_name = cartArrayList[i].variation_name
                favoriteList.qty = cartArrayList[i].qty
                favoriteList.price = cartArrayList[i].priceCart

                CartItemArrayModel.add(favoriteList)
            }
            Log.e("json", CartItemArrayModel.size.toString())


            Log.e("json", CartItemArrayModel.size.toString())
            orderCartModel=CartItemArrayModel
        }
        var data = activity?.let { SharedPref.getCartArrayList(CARTITEMS, it) }
        if (data != null) {
            if (data.size > 0) {
                data.addAll(0,orderCartModel)
                activity?.let { SharedPref.saveArrayListCart(data, CARTITEMS, it) }
            }else{
                activity?.let { SharedPref.saveArrayListCart(orderCartModel, CARTITEMS, it) }
            }
        } else {
            activity?.let { SharedPref.saveArrayListCart(orderCartModel, CARTITEMS, it) }
        }
    }


    fun updateTotal() {
        // var total: Int = 0
        var total: Double = 0.00

        //var totalprice: Int
        var totalprice: Double
        for (i in 0 until cartArrayList.size) {

            totalprice = cartArrayList[i].priceCart
            total = total + totalprice
        }


        finalTotalValue = total
        txtPrice.setText(getString(R.string.rupees) + " " + total)

        txtItem.setText("" + cartArrayList.size)

    }

    private fun isProductAvailable(
        cartArrayList: MutableList<CartRestaurantsMenuModel>,
        foodmenu_id: String,
        listBean: CartRestaurantsMenuModel
    ): Boolean {

        var isAvail = false

        for (cartModel in cartArrayList) {
            //if cart arraylist have menu items that we have click check condition
            if (cartModel.foodmenu_id.equals(foodmenu_id)) {
                //if cart items have variance than check with variance id also
                cartArrayList.remove(listBean)

                updateTotal()
                Log.e("removesize",cartArrayList.size.toString())

                //adapter.notifyDataSetChanged()

                //cartArrayList.removeAt(cartArrayList[i].position)
                //adapter.notifyItemChanged(listBean.position)
                isAvail = true


            }
        }

        return isAvail
    }

    override fun onMenuItemSelected(
        listBean: CartRestaurantsMenuModel,
        isProductVariance: Boolean,
        isRemove: Boolean
    ) {
        if (isRemove) {
            isProductAvailable(cartArrayList, listBean.foodmenu_id.toString(),listBean)
            updateTotal()
            cartArrayList.remove(listBean)

        } else {
            cartArrayList.add(listBean)
            updateTotal()
        }
        if (isProductVariance) {
           // addtocartAPI(listBean)

        } else {
            activity?.let { SharedPref.savedata(it,cartArrayList) }
            activity?.let { SharedPref.saveArrayList(cartArrayList,"data", it) }
            if (userId != null && userId != "") {
                //addtocartAPI(listBean)
            } else {
                activity?.let { Toast(it,"Please login to add products into cart ") }

                val ine = Intent(activity, LoginPage::class.java)
                ine.putExtra(SCREEN_REDIRECTION,"1")
                startActivity(ine)
                //login dialog()
                *//* cartArrayList.add(listBean)
                 updateTotal()
                 //cartArrayList.add(listB                                                                                              an)
                 if (cartArrayList.size > 0) {
                     sheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED)
                     bottomSheetForCart()
                     sheetBehavior.isHideable = false
                 }*//*
            }
        }
    }


    //when variance selected add btn visibility visible
    override fun onVarianceItemSelected(item: CartRestaurantsMenuModel) {
        varianceitem = item
        dialog!!.tvAddVariance.visibility = View.VISIBLE

        Log.e("variance", item.variation_name)

    }


    override fun onVarianceDialogSelected(
        dataBean: List<CategoryBean.MenuBean.ProductVariationBean>,
        catId: String?,
        foodmenuId: String?,
        menuName: String?,
        taxPrice: Double,tvAddToCart :TextView,tvRemove:TextView
    ) {
        openBottomSheetDialog(dataBean, catId, foodmenuId, menuName, taxPrice,tvAddToCart,tvRemove)
    }


    private fun openBottomSheetDialog(
        listBean: List<CategoryBean.MenuBean.ProductVariationBean>,
        catId: String?,
        foodmenuId: String?,
        menuName: String?,
        taxPrice: Double,tvAddToCart :TextView,tvRemove:TextView
    ) {
        val view = layoutInflater.inflate(R.layout.bottom_sheet_variance, null)
        dialog = activity?.let { BottomSheetDialog(it) }
        dialog!!.setContentView(view)
        var varainceList = listBean
        (dialog as BottomSheetDialog).tvAddVariance.visibility = View.GONE
        dialog!!.imgClose.setOnClickListener {

            (dialog as BottomSheetDialog).cancel()
        }
        dialog!!.txtMenuName.text = menuName
        dialog!!.tvAddVariance.setOnClickListener {
            Log.e("dialog", "click")
            cartArrayList.add(varianceitem)
            addtocartAPI(varianceitem,tvAddToCart,tvRemove)
            updateTotal()
            dialog!!.cancel()
        }


        dialog!!.show()

        if (listBean.size > 0) {
            val mLayoutManager = LinearLayoutManager(activity)
            dialog!!.rvVariance!!.layoutManager = mLayoutManager
            dialog!!.rvVariance!!.itemAnimator = DefaultItemAnimator()
            val varianceAdapter = activity?.let {
                ProductVarianceAdapter(
                    it,
                    varainceList,
                    this,
                    catId,
                    foodmenuId,
                    menuName,
                    taxPrice
                )
            }
            dialog!!.rvVariance.adapter = varianceAdapter


        }

    }

    private fun addtocartAPI(
        listBean: CartRestaurantsMenuModel,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {

        CustomProgressbar.showProgressBar(activity!!, false)

        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurant_id
        map["food_menu_id"] = listBean.foodmenu_id.toString()
        map["food_menu_name"] =listBean.menu_name.toString()
        map["variation_id"] = listBean.variation_id.toString()
        map["variation_name"] = listBean.variation_name.toString()
        map["user_id"] = userId

        RetrofitClientSingleton
            .getInstance()
            .addtoCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("RestaurantDetail", t.message)
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        // val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //top
                                tvAddToCart.visibility=View.GONE
                                tvRemove.visibility=View.VISIBLE
                                Toast(activity!!, "" + response.body()!!.message)
                            }
                            FAILURE -> {
                                Toast(activity!!, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })

    }*/

}
