package com.attune.zamy.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.attune.zamy.R
import com.attune.zamy.adapter.TopGroceryPaginationAdapter
import com.attune.zamy.listener.PaginationScrollListener
import com.attune.zamy.model.TopRestaurantModel
import com.attune.zamy.utils.Toast
import com.attune.zamy.utils.isNetworkAvailable
import kotlinx.android.synthetic.main.fragment_top_restaurant.view.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*

class TopGroceryFragment() : Fragment(),TopGroceryPaginationAdapter.OnItemSelectedListener  {


    lateinit var madapter: TopGroceryPaginationAdapter
    lateinit var mview: View
    var TOTAL_PAGE: Int = 0
    var isLoading: Boolean = false
    var isLastPage: Boolean = false
    val PAGE_START = 1
    var currentPage: Int = PAGE_START
    lateinit var dataModels: MutableList<TopRestaurantModel.DataBean.TopRestaurantListBean>
    lateinit var swipeLayout: SwipeRefreshLayout
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        mview = inflater.inflate(R.layout.fragment_top_grocery, null)
        initView()
        return mview
    }

    private fun initView() {
        clearPaging()
        dataModels = ArrayList()
        mview.txtTitle.text = getString(R.string.top_restaurants)
        mview.imgback.setOnClickListener {
            activity!!.onBackPressed()
        }
        swipeLayout = mview.findViewById(R.id.srGrocery)
        rvTopGroceryBind()

        if (!activity?.let { isNetworkAvailable(it) }!!) {
            activity?.let { Toast(it, getString(R.string.no_internet_connection)) }
        } else {
            getTopRestaurantDataAPI(false)
        }
        srRefresh()
    }

    private fun getTopRestaurantDataAPI(b: Boolean) {

    }

    private fun rvTopGroceryBind() {
        val mLayoutManager = LinearLayoutManager(activity)
        mview.rvTopRest!!.layoutManager = mLayoutManager
        madapter = activity?.let { TopGroceryPaginationAdapter(it, dataModels, this) }!!
        mview.rvTopRest.adapter = madapter
        mview.rvTopRest!!.addOnScrollListener(object : PaginationScrollListener(mLayoutManager) {
            override val totalPageCount: Int
                get() = TOTAL_PAGE

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                //you have to call loadmore items to get more data
                currentPage += 1
                lodeMore()
            }
        })
    }

    private fun lodeMore() {

    }

    private fun srRefresh() {
        swipeLayout.setOnRefreshListener {
            clearPaging()
            getTopRestaurantDataAPI(true)
            swipeLayout.isRefreshing = false
        }
    }

    private fun clearPaging() {
        currentPage = PAGE_START
        isLoading = false
        isLastPage = false
    }

    override fun onItemSelected(
        item: TopRestaurantModel.DataBean.TopRestaurantListBean,
        position: Int,
        adapterPostion: Int
    ) {
       /* val args = Bundle()
        args.putString(RESTAURANT_ID_PASS, item.id)
        val toFragment = RestaurantsDetailsFragment()
        toFragment.arguments = args
        fragmentManager!!
            .beginTransaction()
            .add(R.id.fragment_restaurant, toFragment, "RestaurantsDetailsFragment")
            .addToBackStack("RestaurantsDetailsFragment").commit()*/
    }
}