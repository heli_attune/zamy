package com.attune.zamy.fragment


import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.activity.LoginPage
import com.attune.zamy.adapter.DemoRestaurantDetailMenuAdapter
import com.attune.zamy.adapter.ProductVarianceAdapter
import com.attune.zamy.adapter.RestaurantsGalleryAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.interaface.RecyclerSectionItemDecoration
import com.attune.zamy.interaface.onItemVarianceSelected
import com.attune.zamy.interaface.onReadMore
import com.attune.zamy.model.*
import com.attune.zamy.model.RestaurantsMenuModel.DataBean.CategoryBean
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_variance.*
import kotlinx.android.synthetic.main.dialog_logout.*
import kotlinx.android.synthetic.main.fragment_restaurants_details.view.imgFav
import kotlinx.android.synthetic.main.fragment_restaurants_details.view.rvMenu
import kotlinx.android.synthetic.main.fragment_restaurants_details.view.tvReastoTitle
import kotlinx.android.synthetic.main.info_layout.view.*
import kotlinx.android.synthetic.main.layout_cart_bottom.view.*
import kotlinx.android.synthetic.main.layout_content_scrolling.*
import kotlinx.android.synthetic.main.layout_content_scrolling.view.*
import kotlinx.android.synthetic.main.layout_read_more.*
import kotlinx.android.synthetic.main.layout_restaurant_detail_scroll.*
import kotlinx.android.synthetic.main.layout_restaurant_detail_scroll.view.*
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set
import com.attune.zamy.model.RestaurantsMenuModel as RestaurantsMenuModel1


@Suppress("DEPRECATION")
class RestaurantsDetailsFragment : Fragment(),
    DemoRestaurantDetailMenuAdapter.AddtoCartItemClickListener,
    DemoRestaurantDetailMenuAdapter.VarianceDialogClickListener,
    onItemVarianceSelected,
    onReadMore {


    var totalRecords: Int = 0
    val isSwitched: Boolean = true
    lateinit var menudataList: List<RestaurantsMenuModel1.DataBean.CategoryBean>
    private var productListArray: ArrayList<RestaurantsMenuModel1.DataBean.CategoryBean> =
        ArrayList()
    private lateinit var adapter: DemoRestaurantDetailMenuAdapter
    var userId: String = ""
    var finalTotalValue: Double = 0.00
    private lateinit var varianceitem: CartRestaurantsMenuModel
    var dialog: Dialog? = null
    lateinit var mView: View
    lateinit var restaurant_id: String
    lateinit var tvMenu: TextView
    lateinit var tvInfo: TextView
    lateinit var tvGallary: TextView
    private lateinit var sheetBehavior: BottomSheetBehavior<LinearLayout>
    private lateinit var sheetBehaviorvariance: BottomSheetBehavior<LinearLayout>
    lateinit var txtItem: TextView
    lateinit var txtPrice: TextView
    lateinit var txtCharges: TextView
    lateinit var txtViewCart: TextView
    var cartArrayList: MutableList<CartRestaurantsMenuModel> = ArrayList<CartRestaurantsMenuModel>()
    lateinit var bottom_sheet: LinearLayout
    lateinit var bottom_sheet_variance: LinearLayout
    var orderCartModel: ArrayList<OrderCart> = ArrayList<OrderCart>()
    lateinit var orderDataPass: String
    lateinit var text_toolbar_title: TextView
    var linearLayoutManager: LinearLayoutManager? = null
    var veglinearLayoutManager: LinearLayoutManager? = null
    var gridLayoutManager: GridLayoutManager? = null
    var page: Int = 1
    var isFirst = 0

    var isFavAdded: Boolean = false
    lateinit var rvMenu: RecyclerView
    lateinit var rvVegMenu: RecyclerView
    lateinit var rvGallery: RecyclerView

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.layout_restaurant_detail_scroll, container, false)
        //initView()
        return mView
    }


    override fun onResume() {
        super.onResume()
        initView()
    }

    private fun initView() {

        val userID = SharedPref.getUserId(activity!!)
        if (userID != null && SharedPref.getUserId(activity!!) != "") {
            imgFav.visibility = View.VISIBLE
            imgFav.setOnClickListener {
                if (isFavAdded) {
                    addRemoveFavouriteAPI(restaurant_id, true)
                } else {
                    addRemoveFavouriteAPI(restaurant_id, false)
                }
            }
        } else {
            imgFav.visibility = View.INVISIBLE
        }


        val args = arguments

        if (args != null) {
            restaurant_id = args.getString(RESTAURANT_ID_PASS)!!
        }

        userId = activity?.let { SharedPref.getUserId(it) }!!
        val toolbar_layout = mView.findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)
        val toolbar = mView.findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar1)
        toolbar_layout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
        toolbar_layout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.title = ""
        }
        toolbar.setNavigationOnClickListener {
            activity!!.onBackPressed()

        }
        tvMenu = mView.findViewById(R.id.tvMenu)
        tvMenu.setOnClickListener {
            SelectOptions(true, false, false)
        }
        tvInfo = mView.findViewById(R.id.tvInfo)
        tvInfo.setOnClickListener {
            SelectOptions(false, true, false)
        }
        tvGallary = mView.findViewById(R.id.tvGallary)
        tvGallary.setOnClickListener {
            SelectOptions(false, false, true)
        }
        bottom_sheet = mView.findViewById(R.id.bottom_sheet)
        sheetBehavior = BottomSheetBehavior.from<LinearLayout>(bottom_sheet)
        if (cartArrayList.size > 0) {
            bottomSheetForCart()
            sheetBehavior.isHideable = false
        }
        bottomSheetForCart()

        SelectOptions(true, false, false)

        rvMenu = mView.findViewById(R.id.rvMenu)
        rvVegMenu = mView.findViewById(R.id.rvVegMenu)
        rvGallery = mView.findViewById(R.id.rvGallery)
        linearLayoutManager = LinearLayoutManager(activity)
        veglinearLayoutManager = LinearLayoutManager(activity)
        gridLayoutManager = GridLayoutManager(activity!!, 3, GridLayoutManager.VERTICAL, false)
        rvMenu.layoutManager = linearLayoutManager
        rvVegMenu.layoutManager = veglinearLayoutManager
        if (!activity?.let { isNetworkAvailable(it) }!!) {
            activity?.let { Toast(it, getString(R.string.network_error)) }
        } else {
            //menu items call api
            getRestaurentMenuList("")
        }



        mView.rdVeg.setOnClickListener {
            if (mView.rdVeg.isChecked) {
                getRestaurentMenuList("1")
            } else {
                getRestaurentMenuList("")
            }
        }


        mView.rvMenu.addOnScrollListener(object : RecyclerView.OnScrollListener() {

            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                super.onScrolled(recyclerView, dx, dy)
                if (dy > 0 || dy < 0 && mView.bottom_sheet.isShown) {
                }
            }

            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                if (newState == RecyclerView.SCROLL_STATE_IDLE) {

                }

                super.onScrollStateChanged(recyclerView, newState)
            }
        })


    }

    override fun onCreate(savedInstanceState: Bundle?) {
        setHasOptionsMenu(true)
        super.onCreate(savedInstanceState)
    }


    private fun SelectOptions(bMenu: Boolean, bInfo: Boolean, bGallary: Boolean) {

        if (bMenu) {
            tvMenu.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
            tvMenu.setTextColor(resources.getColor(R.color.white))
            tvInfo.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
            tvInfo.setTextColor(resources.getColor(R.color.red_ea1b25))
            tvGallary.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
            tvGallary.setTextColor(resources.getColor(R.color.red_ea1b25))
            llInfo.visibility = View.GONE
            mView.rvMenu.visibility = View.VISIBLE
            mView.bottom_sheet.visibility = View.VISIBLE
            mView.rvGallery.visibility = View.GONE

        } else if (bInfo) {
            tvMenu.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
            tvMenu.setTextColor(resources.getColor(R.color.red_ea1b25))
            tvInfo.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
            tvInfo.setTextColor(resources.getColor(R.color.white))
            tvGallary.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
            tvGallary.setTextColor(resources.getColor(R.color.red_ea1b25))
            llInfo.visibility = View.VISIBLE
            mView.rvMenu.visibility = View.GONE
            mView.bottom_sheet.visibility = View.GONE
            mView.rvGallery.visibility = View.GONE
            getRestaurentInfo()


        } else if (bGallary) {
            tvMenu.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
            tvMenu.setTextColor(resources.getColor(R.color.red_ea1b25))
            tvInfo.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
            tvInfo.setTextColor(resources.getColor(R.color.red_ea1b25))
            tvGallary.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
            tvGallary.setTextColor(resources.getColor(R.color.white))
            llInfo.visibility = View.GONE
            mView.rvMenu.visibility = View.GONE
            mView.bottom_sheet.visibility = View.GONE
            mView.rvGallery.visibility = View.VISIBLE
            getRestaurentGallery()
        }


    }

    private fun getRestaurentGallery() {
        CustomProgressbar.showProgressBar(activity, false)
        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurant_id
        RetrofitClientSingleton
            .getInstance()
            .getGallery(map)
            .enqueue(object : retrofit2.Callback<GalerryResponse> {
                override fun onFailure(call: Call<GalerryResponse>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Toast(activity!!, "Data Not Found")
                }

                override fun onResponse(
                    call: Call<GalerryResponse>,
                    response: Response<GalerryResponse>
                ) {
                    CustomProgressbar.hideProgressBar()
                    val galleryResponse = response.body()!!.data
                    if (response.isSuccessful) {
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                rvGallery.layoutManager = gridLayoutManager
                                val mAdapter =
                                    RestaurantsGalleryAdapter(activity!!, galleryResponse)
                                rvGallery.adapter = mAdapter
                            }
                            FAILURE -> {

                            }
                        }
                    } else {
                        Toast(activity!!, getString(R.string.something_went_wrong))
                    }
                }

            })

    }

    private fun getRestaurentInfo() {
        CustomProgressbar.showProgressBar(activity, false)
        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurant_id
        RetrofitClientSingleton
            .getInstance()
            .getReasturantInfo(map)
            .enqueue(object : retrofit2.Callback<ReasturantInfoModel> {
                override fun onFailure(call: Call<ReasturantInfoModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()

                }

                override fun onResponse(
                    call: Call<ReasturantInfoModel>,
                    response: Response<ReasturantInfoModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    val infoResponse = response.body()!!.data
                    if (response.isSuccessful) {
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                mView.llInfo.tvInfoAddress.text = infoResponse!!.address
                                mView.llInfo.tvInfoMobile.text = infoResponse.owner_contact_number
                                mView.llInfo.tvInfoLandline.text = infoResponse.contact_information
                                mView.llInfo.tvInfoEmail.text = infoResponse.email
                            }
                            FAILURE -> {

                            }
                        }
                    } else {
                        Toast(activity!!, getString(R.string.something_went_wrong))
                    }
                }

            })
    }


    private fun getRestaurentMenuList(vegType: String) {
        CustomProgressbar.showProgressBar(activity, false)
        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurant_id
        map["user_id"] = userId
        map["pageno"] = page.toString()
        map["veg_item"] = vegType

        RetrofitClientSingleton
            .getInstance()
            .getReastaurentMenu(map)
            .enqueue(object : retrofit2.Callback<RestaurantsMenuModel1> {
                override fun onFailure(call: Call<RestaurantsMenuModel1>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    alertDialog(activity!!, "Something want wrong")
                    activity?.let { Toast(it, t.message) }

                }

                override fun onResponse(
                    call: Call<RestaurantsMenuModel1>,
                    response: Response<RestaurantsMenuModel1>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        isFirst += 1
                        val menuList = response.body()!!.data
                        response.body()!!.data!!.category
                        //llRestaurantDetails.visibility = View.VISIBLE
                        when (response.body()!!.status) {
                            SUCCESS -> {

                                if (response.body()!!.data!!.restaurant_details.restaurant_online_status.equals(
                                        "0"
                                    )
                                ) {
                                    if (response.body()!!.data!!.restaurant_details.kitchen_timings != null) {
                                        alertDialog(
                                            activity!!,
                                            response.body()!!.data!!.restaurant_details.kitchen_timings!!
                                        )
                                    } else {
                                        alertDialog(
                                            activity!!,
                                            ""
                                        )
                                    }

                                }

                                mView.rdVeg.visibility = View.VISIBLE
                                val restaurantdata = menuList!!.restaurant_details
                                if (restaurantdata.images != null && restaurantdata.images != "") {
                                    activity?.let {
                                        Glide.with(it)
                                            .load(restaurantdata.images)
                                            .into(mView.imgRestaurant)
                                    }
                                }
                                if (restaurantdata.restaurant_favorite.equals("yes")) {
                                    isFavAdded = true
                                    mView.imgFav.setImageDrawable(activity?.let {
                                        ContextCompat.getDrawable(
                                            it, R.drawable.ic_favorite_selected_red
                                        )
                                    })
                                } else {
                                    isFavAdded = false
                                    mView.imgFav.setImageDrawable(activity?.let {
                                        ContextCompat.getDrawable(
                                            it, R.drawable.ic_favorite_unselected_red
                                        )
                                    })
                                }

                                if (restaurantdata.res_name != null && restaurantdata.res_name != "") {
                                    mView.tvReastoTitle.text = menuList.restaurant_details.res_name
                                    mView.toolbar_layout.title =
                                        menuList.restaurant_details.res_name

                                }
                                if (restaurantdata.address != null && restaurantdata.address != "") {
                                    mView.tvReastoAddress.text = restaurantdata.address
                                    Log.e("TAG", "Address" + restaurantdata.address)
                                }

                                /* tvReastoDesc.text =
                                       menuList.restaurant_details!!.area + "," + menuList.restaurant_details!!.landmark*/

                                if (menuList.category!!.size > 0) {

                                    val menuHeader = menuList


                                    rvMenu.layoutManager =
                                        LinearLayoutManager(activity)

                                    menudataList = response.body()!!.data!!.category!!

                                    /*  adapter = activity?.let {
                                          DemoRestaurantDetailMenuAdapter(
                                              it,
                                              response.body()!!.data!!.category!!,
                                              this@RestaurantsDetailsFragment,
                                              this@RestaurantsDetailsFragment, restaurant_id
                                          )
                                      }!!*/

                                    adapter = activity?.let {
                                        DemoRestaurantDetailMenuAdapter(
                                            it,
                                            response.body()!!.data,
                                            this@RestaurantsDetailsFragment,
                                            this@RestaurantsDetailsFragment,
                                            restaurant_id,
                                            this@RestaurantsDetailsFragment
                                        )
                                    }!!


                                    rvMenu.adapter = adapter

                                    if (isFirst == 1) {
                                        val sectionItemDecoration = RecyclerSectionItemDecoration(
                                            LinearLayout.LayoutParams.WRAP_CONTENT,
                                            true, getSectionCallback(menuHeader.category!!)
                                        )
                                        rvMenu.addItemDecoration(sectionItemDecoration)
                                    }


                                }
                            }
                            FAILURE -> {
                            }
                            AUTH_FAILURE -> {
                            }
                        }

                    } else {
                        mView.container.visibility = View.GONE
                        Toast(activity!!, getString(R.string.please_try_again))
                    }

                }

            })
    }


    private fun getSectionCallback(people: List<CategoryBean>): RecyclerSectionItemDecoration.SectionCallback {
        return object : RecyclerSectionItemDecoration.SectionCallback {
            override fun isSection(position: Int): Boolean {
                return position == 0 || people[position]
                    .cat_display_name !== people[position - 1]
                    .cat_display_name
            }

            override fun getSectionHeader(position: Int): CharSequence {
                return people[position]
                    .cat_display_name.toString()
            }
        }
    }

    private fun bottomSheetForCart() {

        txtItem = mView.findViewById(R.id.txtItem) as TextView
        txtPrice = mView.findViewById(R.id.txtPrice) as TextView
        txtCharges = mView.findViewById(R.id.txtItem) as TextView
        txtViewCart = mView.findViewById(R.id.txtViewCart) as TextView

        txtViewCart.setOnClickListener {
            Log.e("cart size", cartArrayList.size.toString())

            /*if (cartArrayList.size > 0) {*/
            val args = Bundle()
            args.putString(RESTAURANT_ID_PASS, restaurant_id)
            val toFragment = CartToolbarFragment()
            toFragment.arguments = args
            fragmentManager!!
                .beginTransaction()
                .replace(R.id.fragment_restaurant, toFragment, "CartToolbarFragment").commit()
            /*}*/


        }

        //bottom sheet

        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })
    }

    fun adddatatoStore() {
        val CartItemArrayModel: ArrayList<OrderCart> =
            ArrayList<OrderCart>()
        if (cartArrayList.size > 0) {
            for (i in 0 until cartArrayList.size) {

                val favoriteList = OrderCart()
                favoriteList.food_menu_id = cartArrayList[i].foodmenu_id
                favoriteList.menu_name = cartArrayList[i].menu_name
                favoriteList.variation_id = cartArrayList[i].variation_id
                favoriteList.variation_name = cartArrayList[i].variation_name
                favoriteList.qty = cartArrayList[i].qty
                favoriteList.price = cartArrayList[i].priceCart

                CartItemArrayModel.add(favoriteList)
            }
            Log.e("json", CartItemArrayModel.size.toString())


            Log.e("json", CartItemArrayModel.size.toString())
            orderCartModel = CartItemArrayModel
        }
        val data = activity?.let { SharedPref.getCartArrayList(CARTITEMS, it) }
        if (data != null) {
            if (data.size > 0) {
                data.addAll(0, orderCartModel)
                activity?.let { SharedPref.saveArrayListCart(data, CARTITEMS, it) }
            } else {
                activity?.let { SharedPref.saveArrayListCart(orderCartModel, CARTITEMS, it) }
            }
        } else {
            activity?.let { SharedPref.saveArrayListCart(orderCartModel, CARTITEMS, it) }
        }
    }


    fun updateTotal() {
        var total: Double = 0.00
        var totalprice: Double
        for (i in 0 until cartArrayList.size) {

            totalprice = cartArrayList[i].priceCart
            total = total + totalprice
        }


        finalTotalValue = total
        txtPrice.text = getString(R.string.rupees) + " " + total

        txtItem.text = "" + cartArrayList.size

    }

    private fun isProductAvailable(
        cartArrayList: MutableList<CartRestaurantsMenuModel>,
        foodmenu_id: String,
        listBean: CartRestaurantsMenuModel
    ): Boolean {

        var isAvail = false

        for (cartModel in cartArrayList) {
            //if cart arraylist have menu items that we have click check condition
            if (cartModel.foodmenu_id.equals(foodmenu_id)) {
                //if cart items have variance than check with variance id also
                cartArrayList.remove(listBean)
                updateTotal()
                Log.e("removesize", cartArrayList.size.toString())
                isAvail = true
            }
        }

        return isAvail
    }

    override fun onMenuItemSelected(
        listBean: CartRestaurantsMenuModel,
        isProductVariance: Boolean,
        isRemove: Boolean
    ) {
        if (isRemove) {
            isProductAvailable(cartArrayList, listBean.foodmenu_id.toString(), listBean)
            updateTotal()
            cartArrayList.remove(listBean)

        } else {
            cartArrayList.add(listBean)
            updateTotal()
        }
        if (isProductVariance) {
            // addtocartAPI(listBean)

        } else {
            activity?.let { SharedPref.savedata(it, cartArrayList) }
            activity?.let { SharedPref.saveArrayList(cartArrayList, "data", it) }
            if (userId != "") {
                //addtocartAPI(listBean)
            } else {
                activity?.let { Toast(it, "Please login to add products into cart ") }

                val ine = Intent(activity, LoginPage::class.java)
                ine.putExtra(SCREEN_REDIRECTION, "1")
                startActivity(ine)
            }
        }
    }


    //when variance selected add btn visibility visible
    override fun onVarianceItemSelected(item: CartRestaurantsMenuModel) {
        varianceitem = item
        dialog!!.tvAddVariance.visibility = View.VISIBLE
        Log.e("variance", item.variation_name)

    }


    override fun onVarianceDialogSelected(
        dataBean: List<CategoryBean.MenuBean.ProductVariationBean>,
        catId: String?,
        foodmenuId: String?,
        menuName: String?,
        taxPrice: Double,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {
        openBottomSheetDialog(
            dataBean,
            catId,
            foodmenuId,
            menuName,
            taxPrice,
            tvAddToCart,
            tvRemove
        )
    }


    private fun openBottomSheetDialog(
        listBean: List<CategoryBean.MenuBean.ProductVariationBean>,
        catId: String?,
        foodmenuId: String?,
        menuName: String?,
        taxPrice: Double,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {
        val view = layoutInflater.inflate(R.layout.bottom_sheet_variance, null)
        dialog = activity?.let { BottomSheetDialog(it) }
        dialog!!.setContentView(view)
        val varainceList = listBean
        (dialog as BottomSheetDialog).tvAddVariance.visibility = View.GONE
        dialog!!.imgClose.setOnClickListener {

            (dialog as BottomSheetDialog).cancel()
        }
        dialog!!.txtMenuName.text = menuName
        dialog!!.tvAddVariance.setOnClickListener {
            Log.e("dialog", "click")
            cartArrayList.add(varianceitem)
            addtocartAPI(varianceitem, tvAddToCart, tvRemove)
            updateTotal()
            dialog!!.cancel()
        }


        dialog!!.show()

        if (listBean.size > 0) {
            val mLayoutManager = LinearLayoutManager(activity)
            dialog!!.rvVariance!!.layoutManager = mLayoutManager
            dialog!!.rvVariance!!.itemAnimator = DefaultItemAnimator()
            val varianceAdapter = activity?.let {
                ProductVarianceAdapter(
                    it,
                    varainceList,
                    this,
                    catId,
                    foodmenuId,
                    menuName,
                    taxPrice
                )
            }
            dialog!!.rvVariance.adapter = varianceAdapter


        }

    }

    private fun addtocartAPI(
        listBean: CartRestaurantsMenuModel,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {

        CustomProgressbar.showProgressBar(activity!!, false)

        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurant_id
        map["food_menu_id"] = listBean.foodmenu_id.toString()
        map["food_menu_name"] = listBean.menu_name.toString()
        map["variation_id"] = listBean.variation_id.toString()
        map["variation_name"] = listBean.variation_name.toString()
        map["user_id"] = userId

        RetrofitClientSingleton
            .getInstance()
            .addtoCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("RestaurantDetail", t.message)
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        // val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //top
                                // tvAddToCart.visibility=View.GONE
                                //tvRemove.visibility=View.VISIBLE
                                Toast(activity!!, "" + response.body()!!.message)
                            }
                            FAILURE -> {
                                dialogForAddCart(
                                    response.body()!!.message.toString(),
                                    listBean,
                                    tvAddToCart,
                                    tvRemove
                                )

                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })

    }

    private fun dialogForAddCart(
        message: String,
        listBean: CartRestaurantsMenuModel,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {
        val itemDialog = Dialog(activity!!)
        itemDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        itemDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        itemDialog.setContentView(R.layout.dialog_logout)
        itemDialog.tvLogoutDialogTitle.text = message
        itemDialog.show()

        itemDialog.btn_cencle.setOnClickListener { itemDialog.dismiss() }

        itemDialog.btn_ok.setOnClickListener {
            clearCart(listBean, tvAddToCart, tvRemove)
            itemDialog.dismiss()

        }
        /* val builder = AlertDialog.Builder(activity, R.style.AlertDialogCustom)



         // Display a message on alert dialog
         builder.setMessage(message)

         // Set a positive button and its click listener on alert dialog
         builder.setPositiveButton("Yes") { dialog, which ->
             // Do something when user press the positive button
             clearCart(listBean, tvAddToCart, tvRemove)

             dialog.cancel()
         }

         builder.setNegativeButton("No") { dialog, which ->

             // Do something when user press the positive button

             dialog.cancel()
         }
         val dialog: AlertDialog = builder.create()

         // Display the alert dialog on app interface
         dialog.show()
 */

    }

    private fun clearCart(
        listBean: CartRestaurantsMenuModel,
        tvAddToCart: TextView,
        tvRemove: TextView
    ) {
        CustomProgressbar.showProgressBar(activity, false)

        val map = HashMap<String, String>()
        map["user_id"] = userId

        CustomProgressbar.showProgressBar(activity, false)
        RetrofitClientSingleton
            .getInstance()
            .clearCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        /*   val homeScreenData = response.body()*/
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //activity?.let { Toast(it, "" + response.body()!!.message) }
                                addtocartAPI(listBean, tvAddToCart, tvRemove)
                            }
                            FAILURE -> {
                                activity?.let { Toast(it, "" + response.body()!!.message) }
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("Cart clear", response.body().toString())
                }
            })
    }


    private fun addRemoveFavouriteAPI(
        id: String,
        isFav: Boolean
    ) {
        CustomProgressbar.showProgressBar(activity!!, false)
        val map = HashMap<String, String>()
        map["restaurant_id"] = id
        map["user_id"] = SharedPref.getUserId(activity!!)
        RetrofitClientSingleton
            .getInstance()
            .addRemoveFavouriteRestaurant(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()

                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    if (response.isSuccessful) {
                        CustomProgressbar.hideProgressBar()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                if (isFav) {
                                    imgFav.setImageResource(R.drawable.ic_favorite_unselected_red)
                                    isFavAdded = false
                                } else {
                                    imgFav.setImageResource(R.drawable.ic_favorite_selected_red)
                                    isFavAdded = true
                                }

                            }
                            FAILURE -> {
                                Toast(activity!!, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })
    }

    override fun readmore(longDescription: String?, menuName: String?) {
        val itemDialog = Dialog(activity!!)
        itemDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        itemDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        itemDialog.setContentView(R.layout.layout_read_more)
        itemDialog.tvReadMoreTitle.text = menuName
        itemDialog.tvReadMoreDialog.text = longDescription
        itemDialog.show()

    }
}
