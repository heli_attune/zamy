package com.attune.zamy.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RadioButton
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.activity.CheckOutActivity
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.CartListModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_confirmation.view.*
import retrofit2.Call
import retrofit2.Response


class ConfirmationFragment : Fragment() {
    private lateinit var datumList: MutableList<CartListModel.DataBean.ItemsBean>
    lateinit var confirmCartAdapter: ConfirmCartAdapter
    lateinit var userId: String
    private lateinit var mactivity: CheckOutActivity
    lateinit var mView: View
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_confirmation, container, false)
        initView()
        return mView
    }

    @SuppressLint("SetTextI18n")
    override fun setUserVisibleHint(isVisibleToUser: Boolean) {
        super.setUserVisibleHint(isVisibleToUser)
        if (isVisibleToUser) {
            if (mactivity.addressModel != null) {
                mView.tvCustName.text = mactivity.addressModel!!.name
                mView.tvFullAddress.text = mactivity.addressModel!!.address_1 +
                        "," + mactivity.addressModel!!.city +
                        "," + mactivity.addressModel!!.postcode +
                        "," + mactivity.addressModel!!.state +
                        "," + mactivity.addressModel!!.country

            }
        }
    }

    @SuppressLint("SetTextI18n")
    private fun initView() {
        userId = activity?.let { SharedPref.getUserId(it) }!!
        mactivity = activity as CheckOutActivity
        if (mactivity.addressModel != null) {
            mView.tvCustName.text = mactivity.addressModel!!.name
            mView.tvFullAddress.text =
                mactivity.addressModel!!.address_1 + " , " +
                        mactivity.addressModel!!.city + " , " +
                        mactivity.addressModel!!.postcode + " , " +
                        mactivity.addressModel!!.state + " , " +
                        mactivity.addressModel!!.country

        }

        val mLayoutManager = LinearLayoutManager(activity)
        mView.rlMyorders!!.layoutManager = mLayoutManager
        mView.rlMyorders!!.itemAnimator = DefaultItemAnimator()

        if (!activity?.let { isNetworkAvailable(it) }!!) {
            activity?.let { Toast(it, getString(R.string.network_error)) }
        } else {
            getCartData()
        }
        mView.btnConfirmationContinue.setOnClickListener {
            mactivity.getPagerPostion().currentItem = 2
        }
        // mactivity.addressModel!!.city
    }

    private fun getCartData() {
        CustomProgressbar.showProgressBar(activity, false)
        val map = HashMap<String, String>()
        map["user_id"] = userId

        RetrofitClientSingleton
            .getInstance()
            .getCartList(map)
            .enqueue(object : retrofit2.Callback<CartListModel> {
                override fun onFailure(call: Call<CartListModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    activity?.let { Toast(it, t.message) }

                }

                override fun onResponse(
                    call: Call<CartListModel>,
                    response: Response<CartListModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val menuList = response.body()!!.data
                        response.body()!!.data!!
                        val cartDataResponse = response.body()
                        //llRestaurantDetails.visibility = View.VISIBLE
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //Log.e("TAG", "${menuList!!.restaurant_details!!}")

                                datumList = cartDataResponse!!.data!!.items!!
                                confirmCartAdapter =
                                    activity?.let { ConfirmCartAdapter(it, datumList) }!!
                                mView.rlMyorders!!.adapter = confirmCartAdapter

                                confirmCartAdapter.notifyDataSetChanged()
                                mactivity.total = cartDataResponse.data!!.total_amount!!.toInt()
                                mView.tvSubTotal.text =
                                    getString(R.string.rupees) + cartDataResponse.data!!.total_amount.toString()


                            }
                            FAILURE -> {
                                activity?.let { Toast(it, cartDataResponse!!.message) }
                            }
                            AUTH_FAILURE -> {

                            }
                        }

                    }

                }

            })
    }

    inner class ConfirmCartAdapter(
        var mContext: Context,
        val cartList: MutableList<CartListModel.DataBean.ItemsBean>?
    ) :
        RecyclerView.Adapter<ConfirmCartAdapter.ViewHolder>() {
        var finalTotalValue: Double = 0.0
        lateinit var userId: String
        private var row_index: Int = -1
        var selected: RadioButton? = null


        override fun onCreateViewHolder(
            viewGroup: ViewGroup,
            viewType: Int
        ): ConfirmCartAdapter.ViewHolder {
            val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_row_confirm_cart, viewGroup, false)
            return ViewHolder(view)
        }


        override fun getItemCount(): Int {
            return cartList!!.size
        }

        fun removeAt(position: Int) {
            cartList!!.removeAt(position)
            notifyItemRemoved(position)
            //  notifyItemRangeChanged(position, cartList.size)
        }

        @SuppressLint("ResourceAsColor", "SetTextI18n")
        override fun onBindViewHolder(holder: ConfirmCartAdapter.ViewHolder, position: Int) {
            val addressData = cartList?.get(position)
            userId = SharedPref.getUserId(mContext)
            val dataList = this.cartList!![position]
            if (dataList != null) {
                holder.tvItemName.text = dataList.menu_name
            }
            if (dataList.variation_name != null) {
                holder.tvItemDetail.text = dataList.variation_name
            }
            if (dataList.price != null) {
                dataList.price = dataList.price
                holder.tvPrice.text =
                    mContext.getString(R.string.rupees) + " " + ((dataList.price!!.toDouble() * dataList.qty).toString())
                //tableData.pricetotal = tableData.price!!.toInt() * tableData.qty
                dataList.pricetotal = dataList.price!!.toDouble() * dataList.qty
                // holder.txtPrice.text = mcontext.getString(R.string.rupees)+tableData.pricetotal
            } else {
                dataList.price = 0.toString()
                holder.tvPrice.text = mContext.getString(R.string.rupees) + " " + dataList.price
            }
            if (dataList.qty != null) {
                holder.txtQty.text = dataList.qty.toString()
            }
            mactivity.restaurantId = dataList.restaurant_id.toString()
            if (dataList.menu_logo != null) {
                Glide.with(mContext)
                    .load(dataList.menu_logo)
                    .error(R.drawable.ic_place_holder)
                    .into(holder.imgItem)

            } else {
                Glide.with(mContext)
                    .load(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(holder.imgItem)
            }

        }

        private fun updateTotal() {
            // var total: Int = 0
            var total: Double = 0.00

            //var totalprice: Int
            var totalprice: Double
            for (i in 0 until cartList!!.size) {

                totalprice = cartList[i].pricetotal
                total = total + totalprice
            }


            finalTotalValue = total
            mView.tvSubTotal.text = mContext.getString(R.string.rupees) + " " + total


        }


        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var tvItemName: TextView = itemView.findViewById(R.id.tvItemName)
            var tvItemDetail: TextView = itemView.findViewById(R.id.tvItemDetail)
            var tvPrice: TextView = itemView.findViewById(R.id.tvPrice)
            var txtQty: TextView = itemView.findViewById(R.id.txtQty)
            var imgItem: ImageView = itemView.findViewById(R.id.imgItem)


        }

    }

}



