package com.attune.zamy.fragment

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.attune.zamy.R
import com.attune.zamy.adapter.StoreAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.StoreModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.google.android.material.button.MaterialButton
import com.google.android.material.textfield.TextInputEditText
import kotlinx.android.synthetic.main.fragment_calculate_distance.*
import kotlinx.android.synthetic.main.fragment_calculate_distance.view.*


class CalculateDistanceFragment : Fragment() {
    lateinit var calculateView:View
    lateinit var covidFragment: CovidFragment
    lateinit var btndelivery2NextStep:MaterialButton
    lateinit var btndelivery2Back:MaterialButton
    lateinit var edadditional: TextInputEditText

    //local
    private lateinit var user_id:String
    private lateinit var store:String
    private lateinit var item_details:String
    private lateinit var pickup_address:String
    private lateinit var pickup_pincode:String
    private lateinit var pickup_contact_no:String
    private lateinit var delivery_address:String
    private lateinit var delivery_pincode:String
    private lateinit var delivery_contact_no:String
    private lateinit var from_to_distance:String
    private lateinit var km_amount:String
    private lateinit var pickup_lattitude:String
    private lateinit var pickup_longitude:String
    private lateinit var delivery_lattitude:String
    private lateinit var delivery_longitude:String



    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
//        return inflater.inflate(R.layout.fragment_calculate_distance, container, false)
        calculateView= inflater.inflate(R.layout.fragment_calculate_distance, container, false)
        covidFragment=parentFragment as CovidFragment

        btndelivery2NextStep=calculateView.findViewById(R.id.btndelivery2NextStepFragment)
        btndelivery2Back=calculateView.findViewById(R.id.btndelivery2BackFragment)


        LocalBroadcastManager.getInstance(activity!!)
            .registerReceiver(broadCastReceiver, IntentFilter("CalculateDistance"))

        btndelivery2NextStep.setOnClickListener {
            covidFragment.getPagePosition()!!.currentItem=2
            sendBroadcast()
        }

        btndelivery2Back.setOnClickListener {
            covidFragment.getPagePosition()!!.currentItem=0
        }

        return calculateView
    }

    override fun onResume() {
        super.onResume()
        edadditional=calculateView.findViewById(R.id.edAditionalAmountFragment)
            edAditionalAmountFragment.addTextChangedListener(textWatcher)
    }

    private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
//            Toast(activity!!,"afterTextChanged")
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
        {
//            Toast(activity!!,"onTextChanged")
            if (s!!.length<1)
            {
//                edAditionalAmountFragment.isEnabled=false
                edTotalamountFragment.setText(edTotalkmamountFragment.text.toString())
            }
            else {
                val a =
                    (edAditionalAmountFragment.text.toString().toDouble() + edTotalamountFragment.text.toString()
                        .toDouble())
                edTotalamountFragment.setText(a.toString())
//                edAditionalAmountFragment.isEnabled=true
            }

        }
    }

    val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, i: Intent?) {
//            strPickupAddress= i?.getStringExtra("Pickup") ?: Log.e(MainApp.TAG, "My Value "+strPickupAddress)
//                .toString()
            user_id= i?.getStringExtra("user_id").toString()
            store= i?.getStringExtra("store").toString()
            item_details= i?.getStringExtra("item_details").toString()
            pickup_address= i?.getStringExtra("pickup_address").toString()
            pickup_pincode= i?.getStringExtra("pickup_pincode").toString()
            pickup_contact_no= i?.getStringExtra("pickup_contact_no").toString()
            delivery_address= i?.getStringExtra("delivery_address").toString()
            delivery_pincode= i?.getStringExtra("delivery_pincode").toString()
            delivery_contact_no= i?.getStringExtra("delivery_contact_no").toString()
            from_to_distance= i?.getStringExtra("from_to_distance").toString()
            km_amount= i?.getStringExtra("km_amount").toString()
            pickup_lattitude= i?.getStringExtra("pickup_lattitude").toString()
            pickup_longitude= i?.getStringExtra("pickup_longitude").toString()
            delivery_lattitude= i?.getStringExtra("delivery_lattitude").toString()
            delivery_longitude= i?.getStringExtra("delivery_longitude").toString()

            edTotalkmamountFragment.setText(km_amount)
            edTotalKMFragment.setText(from_to_distance)
            edTotalamountFragment.setText(km_amount)
//            Toast(activity!!,delivery_contact_no)
        }
    }

    private fun sendBroadcast() {
        val intent = Intent("Pass_payment")
        intent.putExtra(
            "user_id",
            user_id
        )
        intent.putExtra("store", store)
        intent.putExtra(
            "item_details",
            item_details
        )
        intent.putExtra("pickup_address", pickup_address)
        intent.putExtra("pickup_pincode", pickup_pincode)
        intent.putExtra(
            "pickup_contact_no",
            pickup_contact_no
        )
        intent.putExtra(
            "delivery_address",
            delivery_address
        )
        intent.putExtra(
            "delivery_pincode",
            delivery_pincode
        )
        intent.putExtra(
            "delivery_contact_no",
           delivery_contact_no
        )
        intent.putExtra("from_to_distance",edTotalKMFragment.text.toString())
        intent.putExtra("km_amount",edTotalkmamountFragment.text.toString())
        intent.putExtra("additional_amount",edAditionalAmountFragment.text.toString())
        intent.putExtra("total_amount",edTotalamountFragment.text.toString())
        intent.putExtra(
            "pickup_lattitude",pickup_lattitude
        )
        intent.putExtra(
            "pickup_longitude",pickup_longitude
        )
        intent.putExtra(
            "delivery_lattitude",delivery_lattitude
        )
        intent.putExtra(
            "delivery_longitude",delivery_longitude
        )
        LocalBroadcastManager.getInstance(activity!!).sendBroadcast(intent)
    }



}
