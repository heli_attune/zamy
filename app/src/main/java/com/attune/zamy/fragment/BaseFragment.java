package com.attune.zamy.fragment;

import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

public class BaseFragment extends Fragment {

    //fragments common methods
    public void pushFragments(Fragment fragment, boolean isAnimate, boolean isAdd, boolean isReplace,
                              boolean isAddToBackstack, String tag, Bundle bundle, int fl_main_container) {

        //set bundle data to fragments
        if (bundle != null) {
            fragment.setArguments(bundle);
        }


        FragmentManager fragmentManager = getFragmentManager();
        // Or: FragmentManager fragmentManager = getSupportFragmentManager() fro below 4.0 support
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

        if (getCurrentFragment(fl_main_container) != null) {
            if (getCurrentFragment(fl_main_container).getTag().equalsIgnoreCase(tag)) {
                return;
            }
        }

        //determine animation
        if (isAnimate) {

          /*  fragmentTransaction.
                    setCustomAnimations(R.animator.slide_in_left,R.animator.slide_out_right,R.animator.slide_in_right
                            ,R.animator.slide_out_left
                    );*/
        }


        if (isAdd) {
            //add fragments
            fragmentTransaction.add(fl_main_container, fragment, tag);
        } else if (isReplace) {
            //replace fragments
            fragmentTransaction.replace(fl_main_container, fragment, tag);
        } else {
        }

        //determine backstack
        if (isAddToBackstack) {
            fragmentTransaction.addToBackStack(tag);
        } else {
        }


        if (getCurrentFragment(fl_main_container) != null) {
            fragmentTransaction.hide(getCurrentFragment(fl_main_container));
        }

        //hide keyboard
//        hideKeyboard();

//        if (!isFinishing()) {
        fragmentTransaction.commitAllowingStateLoss();
//        }

    }

    public Fragment getCurrentFragment(int fl_main_container) {
        FragmentManager fm = getFragmentManager();
        if (fm != null) {
            return fm.findFragmentById(fl_main_container);
        } else {
            return null;
        }
    }

}
