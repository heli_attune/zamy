package com.attune.zamy.fragment

import android.app.Dialog
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.widget.AppCompatTextView
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.activity.CheckOutActivity
import com.attune.zamy.activity.MainActivity
import com.attune.zamy.activity.PromoCodeActivity
import com.attune.zamy.activity.VerificationMobile
import com.attune.zamy.adapter.PromoCodeDemoAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.*
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_variance.*
import kotlinx.android.synthetic.main.cartbottom_sheet.view.*
import kotlinx.android.synthetic.main.dialog_logout.*
import kotlinx.android.synthetic.main.empty_cart_layout.*
import kotlinx.android.synthetic.main.empty_cart_layout.view.*
import kotlinx.android.synthetic.main.fragment_cart.*
import kotlinx.android.synthetic.main.fragment_cart.view.*
import kotlinx.android.synthetic.main.fragment_cart.view.edPromoCode
import kotlinx.android.synthetic.main.fragment_cart.view.imgInfo
import kotlinx.android.synthetic.main.fragment_cart.view.tvCarttotal
import kotlinx.android.synthetic.main.fragment_cart.view.tvDeliveryCharge
import kotlinx.android.synthetic.main.fragment_cart.view.tvPrmoCodeDiscount
import kotlinx.android.synthetic.main.fragment_cart.view.tvProcess
import kotlinx.android.synthetic.main.fragment_cart.view.tvSubTotal
import kotlinx.android.synthetic.main.fragmrnt_toolbar.view.*
import retrofit2.Call
import retrofit2.Response


class CartFragment : androidx.fragment.app.Fragment() {

    var delivery_charges: String = "0"
    var totalValue: String = ""
    var coupanCode: String = ""
    lateinit var datumList: MutableList<CartListModel.DataBean.ItemsBean>
    var finalTotalValue: Double = 0.0
    private lateinit var rvMenu: RecyclerView
    lateinit var cartAdapter: CartAdapter
    lateinit var userId: String
    lateinit var mView: View
    var dialog: Dialog? = null
    lateinit var restaurant_id: String
    var mAdapter: PromoCodeDemoAdapter? = null
    var mPromoArrayList: ArrayList<CouponListModel.DataBean> = ArrayList()
    var linearLayoutManager: LinearLayoutManager? = null
    var first = 1
    var minimum_order = ""
    lateinit var btncnt: AppCompatTextView

    var mainActivity = MainActivity()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_cart, null)
        initView()
        mainActivity = activity as MainActivity





        mView.tvCompleteProfile.setOnClickListener {
            val mIntent = Intent(activity!!, VerificationMobile::class.java)
            mIntent.putExtra("cartfragment", 1)
            startActivity(mIntent)
        }
        return mView
    }

    private fun initView()
    {
        datumList = ArrayList()
        userId = activity?.let { SharedPref.getUserId(it) }!!
        mView.tvProcess.setOnClickListener {
            if (isNetworkAvailable(activity!!)) {
                if (totalValue.toDouble() >= minimum_order.toDouble()) {
                    val intent = Intent(activity, CheckOutActivity::class.java)
                    intent.putExtra(RESTAURANT_ID_PASS, restaurant_id)
                    activity!!.startActivity(intent)

                } else {
                    Toast(
                        activity!!,
                        "Minimum order amount ${getString(R.string.rs)} $minimum_order is required"
                    )
                }
            } else {
                Toast(activity!!, getString(R.string.network_error))
            }
        }
        linearLayoutManager = LinearLayoutManager(activity!!, LinearLayoutManager.HORIZONTAL, false)
        mView.ToolBartitle.text = "Cart"
        mView.imgback.visibility = View.GONE
        /* var cartList =activity?.let { SharedPref.getArrayListData("data", it) }
         Log.e("cartsize", cartList!!.size.toString())*/
        mView.imgDelete.setOnClickListener {
            if (datumList.size > 0)
                showCartRemoveDialog()

        }

        mView.btnCartRetry.setOnClickListener {
            bindRecyclerview()
        }

        bindRecyclerview()
        mView.imgInfo.setOnClickListener {
            val LoginIntent = Intent(activity, PromoCodeActivity::class.java)
            LoginIntent.putExtra(RESTAURANT_ID_PASS, restaurant_id)
            startActivity(LoginIntent)

        }


        mView.btnApplayPromoCode.setOnClickListener {
            if (isNetworkAvailable(activity!!)) {
                val LoginIntent = Intent(activity, PromoCodeActivity::class.java)
                LoginIntent.putExtra("CartFragment", true)
                LoginIntent.putExtra(RESTAURANT_ID_PASS, restaurant_id)
                startActivity(LoginIntent)
            } else {
                Toast(activity!!, getString(R.string.network_error))

            }

        }


        mView.btnRemovePromoCode.setOnClickListener {
            if (isNetworkAvailable(activity!!)) {
                removePromoCode()
            } else {
                Toast(activity!!, getString(R.string.network_error))
            }
        }




    }

    private fun removePromoCode() {
        //CustomProgressbar.showProgressBar(activity!!, false)

        val map = HashMap<String, String>()
        map["user_id"] = userId
        map["cart_total"] = totalValue
        map["delivery_fees"] = delivery_charges

        //CustomProgressbar.showProgressBar(context, false)
        RetrofitClientSingleton
            .getInstance()
            .removePromoCode(map)
            .enqueue(object : retrofit2.Callback<RemoveCoupanModel> {
                override fun onFailure(call: Call<RemoveCoupanModel>, t: Throwable) {
                    //CustomProgressbar.hideProgressBar()
                    Toast(activity!!, t.message)
                }

                override fun onResponse(
                    call: Call<RemoveCoupanModel>,
                    response: Response<RemoveCoupanModel>
                ) {
                    // CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                activity?.let { Toast(it, "" + response.body()!!.message) }
                                //mView.btnApplayPromoCode.text = "Apply"
                                mView.edPromoCode.setText("")
                                mView.tvPrmoCodeDiscount.text = ""
                                //mView.tvSubTotal.setText(activity!!.resources.getString(R.string.rupees) +" "+response.body()!!.data)
                                mView.btnApplayPromoCode.visibility = View.VISIBLE
                                mView.btnRemovePromoCode.visibility = View.GONE
                                getCartList()
                            }
                            FAILURE -> {
                                activity?.let { Toast(it, "" + response.body()!!.message) }
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("Cart clear", response.body()!!.message)
                }
            })

    }

    private fun bindRecyclerview() {

        val mLayoutManager = LinearLayoutManager(activity)
        mView.rvCartList!!.layoutManager = mLayoutManager
        mView.rvCartList!!.itemAnimator = DefaultItemAnimator()



        if (!activity?.let { isNetworkAvailable(it) }!!) {
            activity?.let { Toast(it, getString(R.string.network_error)) }
            mView.btnCartRetry.visibility = View.VISIBLE
            mView.tvNoInternet.visibility = View.VISIBLE
            mView.rlPromoCode.visibility = View.GONE
            mView.rvCartList.visibility = View.GONE
        } else {
            getCartList()
            mView.btnCartRetry.visibility = View.GONE
            mView.tvNoInternet.visibility = View.GONE
            mView.rlPromoCode.visibility = View.VISIBLE
            mView.rvCartList.visibility = View.VISIBLE
        }


    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (requestCode == 5) {
            if (data!!.hasExtra("coupon_name")) {
                coupanCode = data.hasExtra("coupon_name").toString()
                restaurant_id = data.hasExtra("resto_id").toString()
                Toast(activity!!, data.extras!!.getString("coupon_name"))
                edPromoCode.setText(data.extras!!.getString("coupon_name"))
                myApplyPromoCode()
            }
        }
    }

    private fun getCartList() {

        if (first <= 1) {
            CustomProgressbar.showProgressBar(activity, false)
        }
        val map = HashMap<String, String>()
        map["user_id"] = userId

        RetrofitClientSingleton
            .getInstance()
            .getCartList(map)
            .enqueue(object : retrofit2.Callback<CartListModel> {
                override fun onFailure(call: Call<CartListModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    activity?.let { Toast(it, t.message) }
                }
                override fun onResponse(
                    call: Call<CartListModel>,
                    response: Response<CartListModel>
                ) {
                    if (first > 0) {
                        CustomProgressbar.hideProgressBar()
                    }
                    if (response.isSuccessful) {

                        if (response.body()!!.data!!.user_phone.equals("")) {
                            mView.tvProcess.visibility = View.GONE
                            mView.tvCompleteProfile.visibility = View.VISIBLE

                        } else {
                            mView.tvProcess.visibility = View.VISIBLE
                            mView.tvCompleteProfile.visibility = View.GONE
                        }


                        first = first + 1
                        val menuList = response.body()!!.data
                        response.body()!!.data!!
                        //llRestaurantDetails.visibility = View.VISIBLE
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //Log.e("TAG", "${menuList!!.restaurant_details!!}")
                                //mainActivity.cartSet(response.body()!!.data!!.cart_count)

                                val cartDataResponse = response.body()
                                datumList = cartDataResponse!!.data?.items!!
                                cartAdapter = CartAdapter(datumList, activity!!)
                                totalValue = cartDataResponse.data!!.total.toString()
                                minimum_order = cartDataResponse.data!!.min_order_amount.toString()

                                delivery_charges =
                                    cartDataResponse.data!!.delivery_charge.toString()
                                mView.tvDeliveryCharge.text =
                                    activity!!.resources.getString(R.string.rupees) + delivery_charges
                                /*cartAdapter = activity?.let { CartAdapter(datumList, it) }*/
                                mView.tvCarttotal.text =
                                    getString(R.string.rupees) + cartDataResponse.data!!.total
                                mView.tvSubTotal.text =
                                    getString(R.string.rupees) + cartDataResponse.data!!.total_amount
                                mView.tvPrmoCodeDiscount.text =
                                    getString(R.string.rupees) + cartDataResponse.data!!.coupon_amount
                                mView.tvGst.text =
                                    getString(R.string.rupees) + cartDataResponse.data!!.gst
                                mView.edPromoCode.setText(cartDataResponse.data!!.coupon_code)
                                if (coupanCode != null && cartDataResponse.data!!.coupon_code != "") {
                                    // mView.btnApplayPromoCode.text = "Remove"
                                    mView.imgInfo.visibility = View.GONE
                                }
                                if (cartDataResponse.data!!.complementery!!.length > 0) {
                                    mView.txtComplementry.text =
                                        "*" + cartDataResponse.data!!.complementery
                                } else {

                                }
                                mView.rvCartList!!.adapter = cartAdapter

                                cartAdapter.notifyDataSetChanged()

                                if (datumList.isNotEmpty()) {

                                    mView.rlCartView.visibility = View.VISIBLE
                                    mView.rlPromoCode.visibility = View.VISIBLE

                                    if (mView.edPromoCode.text!!.isNotEmpty()) {
                                        mView.btnApplayPromoCode.visibility = View.GONE
                                        mView.btnRemovePromoCode.visibility = View.VISIBLE
                                    } else {
                                        mView.btnApplayPromoCode.visibility = View.VISIBLE
                                        mView.btnRemovePromoCode.visibility = View.GONE
                                    }
                                    mView.lnEmpptyCart.visibility = View.GONE

                                } else {
                                    mView.rlCartView.visibility = View.GONE
                                    mView.rlPromoCode.visibility = View.GONE
                                    mView.lnEmpptyCart.visibility = View.VISIBLE

                                   //mainActivity.cartSet(0)
                                }


                            }
                            FAILURE -> {
                                mView.rlCartView.visibility = View.GONE
                                mView.rlPromoCode.visibility = View.GONE
                                mView.lnEmpptyCart.visibility = View.VISIBLE
                                //mainActivity.cartSet(0)
                            }
                            AUTH_FAILURE -> {
                                mView.rlCartView.visibility = View.GONE
                                mView.rlPromoCode.visibility = View.GONE
                                mView.lnEmpptyCart.visibility = View.VISIBLE
                            }
                        }

                    } else {
                        mView.rlCartView.visibility = View.GONE
                        mView.rlPromoCode.visibility = View.GONE
                        mView.lnEmpptyCart.visibility = View.VISIBLE
                        //mainActivity.cartSet(0)
                    }

                }

            })
    }

    fun showCartRemoveDialog() {
        val mDialog = Dialog(activity!!)
        mDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        mDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        mDialog.setContentView(R.layout.dialog_logout)
        mDialog.tvLogoutDialogTitle.text = getString(R.string.cart_delete_message)
        mDialog.show()

        mDialog.btn_cencle.setOnClickListener { mDialog.dismiss() }

        mDialog.btn_ok.setOnClickListener {
            mDialog.dismiss()
            cartClearAPI()
        }

        /* // Display a message on alert dialog
         builder.setMessage(getString(R.string.cart_delete_message))

         // Set a positive button and its click listener on alert dialog
         builder.setPositiveButton("YES") { dialog, which ->
             // Do something when user press the positive button
             cartClearAPI()
            // mainActivity.cartSet(0)

         }
         builder.setNegativeButton("No") { dialog, which ->
             // Do something when user press the positive button
             dialog.cancel()

         }
         val dialog: AlertDialog = builder.create()*/

        // Display the alert dialog on app interface
        /*dialog!!.show()*/
    }

    private fun cartClearAPI() {
        // CustomProgressbar.showProgressBar(activity!!, false)

        val map = HashMap<String, String>()
        map["user_id"] = userId

        //CustomProgressbar.showProgressBar(context, false)
        RetrofitClientSingleton
            .getInstance()
            .clearCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    //  CustomProgressbar.hideProgressBar()
                    Toast(activity!!, t.message)
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    //  CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        /*   val homeScreenData = response.body()*/
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                datumList.clear()
                                cartAdapter.notifyDataSetChanged()
                                activity?.let { Toast(it, "" + response.body()!!.message) }
                                mView.rlCartView.visibility = View.GONE
                                mView.rlPromoCode.visibility = View.GONE
                                mView.lnEmpptyCart.visibility = View.VISIBLE
                                sendBroadCastToDetailPage()
                                getCartList()
                                /*mainActivity.cartSet(0)*/

                            }
                            FAILURE -> {
                                activity?.let { Toast(it, "" + response.body()!!.message) }
                                //mainActivity.cartSet(0)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("Cart clear", response.body().toString())
                    //mainActivity.cartSet(0)
                }
            })
    }

    override fun onResume() {
        getCartList()
        super.onResume()

        btncnt=mView.findViewById(R.id.btncnt)
//        btncnt.setText("aaa")
        btncnt.setOnClickListener {
            val mIntent = Intent(activity!!, MainActivity::class.java)
            startActivity(mIntent)
        }

        try {
            LocalBroadcastManager.getInstance(activity!!).unregisterReceiver(
                mMessageReceiver
            )
        } catch (e: Exception) {

            Log.e("TAG", "$e")
        }

        try {
            LocalBroadcastManager.getInstance(activity!!).registerReceiver(
                mMessageReceiver,
                IntentFilter(APPLY_COUPAN)
            )
        } catch (e: Exception) {

            Log.e("TAG", "$e")
        }




    }


    private fun myApplyPromoCode() {

        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(activity!!)
        map["coupon_code"] = coupanCode
        map["cart_total"] = totalValue
        map["restaurant_id"] = restaurant_id
        RetrofitClientSingleton.getInstance()
            .applyPromoCode(map)
            .enqueue(object : retrofit2.Callback<ApplyPromoCode> {
                override fun onFailure(call: Call<ApplyPromoCode>?, t: Throwable?) {
                    try {
                        Toast(activity!!, resources.getString(R.string.something_went_wrong))
                    } catch (e: Exception) {

                    }


                }

                override fun onResponse(
                    call: Call<ApplyPromoCode>?,
                    response: Response<ApplyPromoCode>?
                ) {

                    if (response!!.isSuccessful) {
                        when (response.body()?.success) {
                            SUCCESS -> {
                                val subtotal = response.body()!!.data!!.sub_total
                                val discount = response.body()!!.data!!.discount_amount
                                val total = response.body()!!.data!!.total
                                mView.tvCarttotal.text = subtotal
                                //mView.tvDeliveryCharge.setText("10")
                                // mView.tvPrmoCodeDiscount.text = discount
                                // mView.tvSubTotal.text = activity!!.resources.getString(R.string.rupees)+total
                                mView.edPromoCode.isEnabled = false
                                mView.btnRemovePromoCode.visibility = View.VISIBLE
                                mView.imgInfo.visibility = View.GONE
                                Toast(activity!!, response.body()!!.msg)

                                getCartList()

                            }
                            FAILURE -> {
                                Toast(activity!!, response.body()?.msg)
                                mView.edPromoCode.text!!.clear()
                                mView.btnApplayPromoCode.visibility = View.VISIBLE
                                mView.btnRemovePromoCode.visibility = View.GONE

                            }
                            AUTH_FAILURE -> {
                                //forceLogout(this@PromoCodeActivity)
                            }
                        }
                    } else {
                        Toast(activity!!, response.body()!!.msg)
                        mView.edPromoCode.text!!.clear()
                        mView.btnApplayPromoCode.visibility = View.VISIBLE
                        mView.btnRemovePromoCode.visibility = View.GONE
                    }

                }

            })
    }

    inner class CartAdapter(
        val cartList: MutableList<CartListModel.DataBean.ItemsBean>,
        var mcontext: Context
    ) : RecyclerView.Adapter<CartAdapter.ViewHolder>() {


        override fun onCreateViewHolder(viewGroup: ViewGroup, viewType: Int): ViewHolder {
            val view = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.row_cart_list, viewGroup, false)
            return ViewHolder(view)
        }


        override fun getItemCount(): Int {
            return cartList.size
            //return 50s
        }

        fun removeItem(position: Int) {
            cartList
        }

        fun removeAt(position: Int) {
            cartList.removeAt(position)
            notifyItemRemoved(position)
            //  notifyItemRangeChanged(position, cartList.size)
        }

        override fun onBindViewHolder(holder: ViewHolder, position: Int) {
            val dataList = this.cartList[position]
            if (dataList != null) {
                holder.txtItemName.text = dataList.menu_name
            }
            if (dataList.variation_name != null) {
                holder.tvSubTitle.text = dataList.variation_name
            }
            if (dataList.price != null) {
                dataList.price = dataList.price
                holder.tvCartPrice.text =
                    mcontext.getString(R.string.rupees) + " " + ((dataList.price!!.toDouble() * dataList.qty).toString())
                //tableData.pricetotal = tableData.price!!.toInt() * tableData.qty
                dataList.pricetotal = dataList.price!!.toDouble() * dataList.qty
                // holder.txtPrice.text = mcontext.getString(R.string.rupees)+tableData.pricetotal
            } else {
                dataList.price = 0.toString()
                holder.tvCartPrice.text = mcontext.getString(R.string.rupees) + " " + dataList.price
            }
            if (dataList.qty != null) {
                holder.txtCount.text = dataList.qty.toString()
            }
            updateTotal()
            holder.imgPlus.setOnClickListener {
                var mqty = dataList.qty + 1
                updateCartItems(
                    dataList,
                    position,
                    holder.adapterPosition,
                    holder.tvCartPrice,
                    true,
                    mqty.toString()
                )

            }

            holder.imgMinus.setOnClickListener {
                if (dataList.qty == 1) {

                } else {
                    var mqty = dataList.qty - 1
                    updateCartItems(
                        dataList,
                        position,
                        holder.adapterPosition,
                        holder.tvCartPrice,
                        false,
                        mqty.toString()
                    )
                }


            }

            if (dataList.menu_logo != null) {
                Glide.with(mcontext)
                    .load(dataList.menu_logo)
                    .error(R.drawable.ic_place_holder)
                    .into(holder.imgMenu)

            } else {
                Glide.with(mcontext)
                    .load(R.drawable.ic_place_holder)
                    .error(R.drawable.ic_place_holder)
                    .into(holder.imgMenu)
            }

            holder.imgRemove.setOnClickListener {

                val itemDialog = Dialog(activity!!)
                itemDialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
                itemDialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
                itemDialog.setContentView(R.layout.dialog_logout)
                itemDialog.tvLogoutDialogTitle.text = getString(R.string.item_remove_message)
                itemDialog.show()

                itemDialog.btn_cencle.setOnClickListener { itemDialog.dismiss() }

                itemDialog.btn_ok.setOnClickListener {
                    removeCartItem(dataList, position, holder.adapterPosition)
                    itemDialog.dismiss()

                }
                /*  //remove item api
                  val builder = AlertDialog.Builder(mcontext, R.style.AlertDialogCustom)

                  // Display a message on alert dialog
                  builder.setMessage(getString(R.string.item_remove_message))

                  // Set a positive button and its click listener on alert dialog
                  builder.setPositiveButton("Yes") { dialog, which ->
                      // Do something when user press the positive button

                      removeCartItem(dataList, position, holder.adapterPosition)
                  }
                  builder.setNegativeButton("No") { dialog, which ->
                      // Do something when user press the positive button
                      dialog.cancel()

                  }
                  val dialog: AlertDialog = builder.create()

                  // Display the alert dialog on app interface
                  dialog.show()*/

            }
            restaurant_id = dataList.restaurant_id.toString()


        }

        private fun updateTotal() {
            // var total: Int = 0
            var total: Double = 0.00

            //var totalprice: Int
            var totalprice: Double
            for (i in 0 until cartList.size) {
                totalprice = cartList[i].pricetotal
                total = total + totalprice
            }


            finalTotalValue = total
            //mView.tvSubTotal.text = getString(R.string.rupees) + " " + total
            // mView.tvCarttotal.text = getString(R.string.rupees) + " " + total


        }

        private fun updateCartItems(
            listBean: CartListModel.DataBean.ItemsBean,
            position: Int,
            adapterPosition: Int,
            tvCartPrice: TextView,
            isAddorRemove: Boolean,
            qty: String
        ): Boolean {

            CustomProgressbar.showProgressBar(activity!!, false)
            var status: Boolean = false
            val map = HashMap<String, String>()
            map["restaurant_id"] = restaurant_id
            map["food_menu_id"] = listBean.food_menu_id.toString()
            map["variation_id"] = listBean.variation_id.toString()
            map["qty"] = qty
            map["user_id"] = userId

            RetrofitClientSingleton
                .getInstance()
                .updateCart(map)
                .enqueue(object : retrofit2.Callback<CommonModel> {
                    override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                        CustomProgressbar.hideProgressBar()
                        Log.e("RestaurantDetail", t.message)
                        status = false
                    }

                    override fun onResponse(
                        call: Call<CommonModel>,
                        response: Response<CommonModel>
                    ) {
                        CustomProgressbar.hideProgressBar()
                        if (response.isSuccessful) {
                            // val homeScreenData = response.body()
                            when (response.body()!!.status) {
                                SUCCESS -> {
                                    //top
                                    Toast(activity!!, "" + response.body()!!.message)
                                    if (isAddorRemove) {
                                        var currentQTY: Int = listBean.qty.toInt()
                                        currentQTY = currentQTY + 1
                                        listBean.qty = currentQTY

                                        val revisedPrice =
                                            (listBean.price!!.toDouble() * (listBean.qty))
                                        //tableData.pricetotal = revisedPrice
                                        listBean.pricetotal = revisedPrice.toDouble()
                                        /* tvCartPrice.text =
                                             mcontext.getString(R.string.rupees) + " " + listBean.pricetotal*/
                                        cartAdapter.notifyDataSetChanged()

                                        //update total
                                        updateTotal()
                                        getCartList()
                                    } else {
                                        var currentQTY: Int = listBean.qty

                                        if (currentQTY == 1) {
                                            currentQTY = 1
                                        } else {
                                            currentQTY = currentQTY - 1
                                        }
                                        listBean.qty = currentQTY

                                        val revisedPrice =
                                            (listBean.price!!.toDouble() * (listBean.qty))
                                        //tableData.pricetotal = revisedPrice
                                        listBean.pricetotal = revisedPrice.toDouble()
                                        /*tvCartPrice.text =
                                            mcontext.getString(R.string.rupees) + " " + listBean.pricetotal*/
                                        cartAdapter.notifyDataSetChanged()
                                        //update total
                                        updateTotal()
                                        getCartList()
                                    }
                                }
                                FAILURE -> {
                                    Toast(activity!!, "" + response.body()!!.message)
                                    status = false
                                }
                                AUTH_FAILURE -> {
                                    status = false
                                }

                            }
                        }
                        status = false
                        Log.e("updatecart", response.body().toString())
                    }
                })
            return status
        }

        private fun removeCartItem(
            dataList: CartListModel.DataBean.ItemsBean,
            position: Int,
            adapterPosition: Int
        ) {

            CustomProgressbar.showProgressBar(activity!!, false)

            val map = HashMap<String, String>()
            map["restaurant_id"] = dataList.restaurant_id.toString()
            map["food_menu_id"] = dataList.food_menu_id.toString()
            map["variation_id"] = dataList.variation_id.toString()
            map["user_id"] = userId

            CustomProgressbar.showProgressBar(context, false)
            RetrofitClientSingleton
                .getInstance()
                .removeItemfromCart(map)
                .enqueue(object : retrofit2.Callback<CommonModel> {
                    override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                        CustomProgressbar.hideProgressBar()
                    }

                    override fun onResponse(
                        call: Call<CommonModel>,
                        response: Response<CommonModel>
                    ) {
                        CustomProgressbar.hideProgressBar()
                        if (response.isSuccessful) {
                            val homeScreenData = response.body()
                            when (response.body()!!.status) {
                                SUCCESS -> {
                                    removeAt(adapterPosition)
                                    sendBroadCastToDetailPage()
                                    getCartList()
                                    /* cartAdapter.notifyItemRemoved(adapterPosition)

                                     notifyItemRemoved(adapterPosition)
                                     notifyItemRangeChanged(adapterPosition, cartList!!.size)*/
                                    Toast(mcontext, "" + response.body()!!.message)

                                }
                                FAILURE -> {
                                    Toast(mcontext, "" + response.body()!!.message)
                                }
                                AUTH_FAILURE -> {
                                }

                            }
                        }
                        Log.e("HOME FRAGMENT", response.body().toString())
                    }
                })

        }

        inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
            var txtItemName = itemView.findViewById<TextView>(R.id.tvItemName)
            var tvSubTitle = itemView.findViewById<TextView>(R.id.tvSubTitle)
            var tvCartPrice = itemView.findViewById<TextView>(R.id.tvCartPrice)
            var txtCount = itemView.findViewById<TextView>(R.id.txtCount)
            var imgPlus = itemView.findViewById<ImageView>(R.id.imgPlus)
            var imgMinus = itemView.findViewById<ImageView>(R.id.imgMinus)
            var imgRemove = itemView.findViewById<ImageView>(R.id.imgRemove)
            var imgMenu = itemView.findViewById<ImageView>(R.id.imgMenu)
        }

    }

    private fun sendBroadCastToDetailPage() {
        val intent = Intent("REFRESH DETAIL")
        // You can also include some extra data.
        intent.putExtra("sucess", "1")
        LocalBroadcastManager.getInstance(activity!!).sendBroadcast(intent)

    }


    // Our handler for received Intents. This will be called whenever an Intent
    // with an action named "custom-event-name" is broadcasted.
    private val mMessageReceiver = object : BroadcastReceiver() {
        override fun onReceive(context: Context, intent: Intent) {
            coupanCode = intent.getStringExtra(PROMOCODE)
            restaurant_id = intent.getStringExtra(RESTAURANT_ID_PASS)
            mView.edPromoCode.setText(coupanCode)
            Log.d("receiver", "Got message: $coupanCode")
            mView.btnRemovePromoCode.visibility = View.VISIBLE
            mView.btnApplayPromoCode.visibility = View.GONE
            myApplyPromoCode()
        }
    }


    private fun openBottomSheetDialog(mPromoArrayList: ArrayList<CouponListModel.DataBean>) {

        val promoCodeDemoAdapter = PromoCodeDemoAdapter(activity!!, mPromoArrayList)
        val view = layoutInflater.inflate(R.layout.bottom_sheet_variance, null)
        dialog = activity?.let { BottomSheetDialog(it) }
        dialog!!.setContentView(view)
        (dialog as BottomSheetDialog).tvAddVariance.visibility = View.GONE
        dialog!!.imgClose.setOnClickListener {
            (dialog as BottomSheetDialog).cancel()
        }

        dialog!!.show()

    }


}
