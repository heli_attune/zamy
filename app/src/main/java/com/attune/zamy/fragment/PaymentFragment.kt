package com.attune.zamy.fragment


import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.attune.zamy.R
import com.attune.zamy.activity.CheckOutActivity
import com.attune.zamy.activity.MainActivity
import com.attune.zamy.activity.PaymentActivity
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.CheckoutModel
import com.attune.zamy.model.PaytmCheckoutModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.fragment_payment.view.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class PaymentFragment : Fragment() {
    lateinit var addressId: String
    var restaurant_id: String = ""
    val checkOutActivity = CheckOutActivity()
    private lateinit var mactivity: CheckOutActivity
    lateinit var userID: String
    lateinit var mView: View
    var strPayment: String = " "
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.fragment_payment, container, false)
        initView()
        return mView
    }


    private fun initView() {
        userID = activity?.let { SharedPref.getUserId(it) }!!
        mactivity = activity as CheckOutActivity

        restaurant_id = mactivity.restaurantId
        addressId = mactivity.addressModel!!.id.toString()
        mView.rlPyamentSucess.visibility = View.GONE
        mView.rlPayment.visibility = View.VISIBLE
        mView.btnPayNow.setOnClickListener {
            if (strPayment != "") {
                if (!activity?.let { isNetworkAvailable(it) }!!) {
                    activity?.let { Toast(it, getString(R.string.network_error)) }
                } else if (strPayment == "cod") {
                    doPaymentAPI()
                } else if (strPayment == "paytm") {
                    doPaymentPaytmAPI("paytm")
                } else if (strPayment == "instamojo") {
                    doPaymentPaytmAPI("instamojo")
                } else if (strPayment == "razorpay") {
                    doPaymentPaytmAPI("razorpay")
                } else {
                    Toast(activity!!, "Please select Payment method")
                }
            } else {
                Toast(activity!!, "Please select Payment method")
            }
        }
        mView.tvContinue.setOnClickListener {
            val ine = Intent(activity, MainActivity::class.java)
            startActivity(ine)
            (activity as CheckOutActivity).finish()

        }


        if (mactivity.total > 0) {
            mView.radioPaytm.visibility = View.VISIBLE
            mView.radioCashonDelivery.visibility = View.VISIBLE
        } else {
            mView.radioPaytm.visibility = View.GONE
        }



        mView.radio_group.setOnCheckedChangeListener { radioGroup, optionId ->
            run {
                when (optionId) {
                    R.id.radioCashonDelivery -> {
                        strPayment = "cod"
                        // do something when radio button 1 is selected
                    }
                    R.id.radioPaytm -> {
                        strPayment = "paytm"
                        // do something when radio button 1 is selected
                    }
                    R.id.radioInstaMojo -> {
                        strPayment = "instamojo"
                        // do something when radio button 1 is selected
                    }
                    R.id.radioRezorpay -> {
                        strPayment = "razorpay"
                    }
                    // add more cases here to handle other buttons in the RadioGroup
                }
            }
        }
    }

    private fun doPaymentAPI() {
        CustomProgressbar.showProgressBar(activity, false)
        val map = HashMap<String, String>()
        map["user_id"] = userID
        map["restaurant_id"] = restaurant_id
        map["address_id"] = addressId
        map["payment_method"] = strPayment

        RetrofitClientSingleton.getInstance().doCheckoutPayment(map).enqueue(object :
            Callback<CheckoutModel> {
            override fun onFailure(call: Call<CheckoutModel>, t: Throwable) {
                CustomProgressbar.hideProgressBar()
                //Toast(activity!!, getString(R.string.something_went_wrong))
                Toast(activity!!, "Sorry!! We are closed. See you soon.")

            }

            override fun onResponse(call: Call<CheckoutModel>, response: Response<CheckoutModel>) {
                CustomProgressbar.hideProgressBar()
                if (response.isSuccessful) {
                    val homeScreenData = response.body()
                    when (response.body()!!.status) {
                        SUCCESS -> {
                            activity?.let { Toast(it, "" + response.body()!!.message) }
                            mView.rlPyamentSucess.visibility = View.VISIBLE
                            mView.rlPayment.visibility = View.GONE
                            mView.txtOrderCode.text = "#" + homeScreenData!!.data.toString()
                        }
                        FAILURE -> {
                            activity?.let { Toast(it, "" + response.body()!!.message) }
                        }
                        AUTH_FAILURE -> {
                        }
                        RESTO_AVAILABLE -> {
                            Toast(activity!!, response.body()!!.message)
                        }

                    }
                } else {
                    activity?.let { Toast(it, getString(R.string.please_try_again)) }
                }
            }

        })
    }

    private fun doPaymentPaytmAPI(paymentType: String) {
        CustomProgressbar.showProgressBar(activity, false)
        val map = HashMap<String, String>()
        map["user_id"] = userID
        map["restaurant_id"] = restaurant_id
        map["address_id"] = addressId
        map["payment_method"] = paymentType

        RetrofitClientSingleton.getInstance().doCheckoutPaytmPayment(map).enqueue(object :
            Callback<PaytmCheckoutModel> {
            override fun onFailure(call: Call<PaytmCheckoutModel>, t: Throwable) {
                CustomProgressbar.hideProgressBar()
                activity?.let { Toast(it, getString(R.string.please_try_again)) }
            }

            override fun onResponse(
                call: Call<PaytmCheckoutModel>,
                response: Response<PaytmCheckoutModel>
            ) {
                CustomProgressbar.hideProgressBar()
                /* if (response.isSuccessful) {
                     val responsedata = response.body() as CheckoutModel
                     if (responsedata.) {
                         activity?.let { Toast(it, responsedata.message)
                             mView.rlPyamentSucess.visibility=View.VISIBLE
                             mView.rlPayment.visibility=View.GONE

                         }



                     } else {
                         activity?.let { Toast(it, responsedata.message) }
                     }
                 } */
                if (response.isSuccessful) {
                    val homeScreenData = response.body()
                    when (response.body()!!.status) {
                        SUCCESS -> {
                            var dataresponse = response.body()!!.data
                            val i = Intent(activity, PaymentActivity::class.java)
                            i.putExtra(keyOrderId, dataresponse.order_id.toString())
                            /* var strPayment=dataresponse.paytm_link+"?ORDER_ID="+dataresponse.order_id+
                                     "&CUST_ID="+dataresponse.user_id+"&TXN_AMOUNT="+ dataresponse.order_total
                             Log.e("payment link",strPayment)*/
                            i.putExtra("payment_url", dataresponse.paytm_link)
                            startActivity(i)
                            /*ORDER_ID=123&CUST_ID=1233&TXN_AMOUNT=50*/

                            activity?.let { Toast(it, "" + response.body()!!.message) }
                            /*  mView.rlPyamentSucess.visibility=View.VISIBLE
                              mView.rlPayment.visibility=View.GONE
                              mView.txtOrderCode.text= homeScreenData!!.data.toString()*/
                        }
                        FAILURE -> {
                            activity?.let { Toast(it, "" + response.body()!!.message) }
                        }
                        AUTH_FAILURE -> {
                        }

                    }
                } else {
                    activity?.let { Toast(it, getString(R.string.please_try_again)) }
                }
            }

        })
    }


}
