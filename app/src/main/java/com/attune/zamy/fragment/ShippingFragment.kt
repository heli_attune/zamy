package com.attune.zamy.fragment

import android.app.AlertDialog
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.activity.AddAddressActivity
import com.attune.zamy.activity.CheckOutActivity
import com.attune.zamy.adapter.BillingAddressAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.interaface.onItemAddressSelected
import com.attune.zamy.model.CommonModel
import com.attune.zamy.model.GetAddressModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.check_out_bottom_view.view.*
import kotlinx.android.synthetic.main.fragment_shipping.view.*
import retrofit2.Call
import retrofit2.Response

class ShippingFragment : Fragment(), onItemAddressSelected {
    override fun onAddressSelected(
        item: GetAddressModel.DataBean.AddressBookBean,
        isSelected: Boolean,
        isEdit: Boolean
    ) {
        if (isSelected) {
            addressId = item.id.toString()
//            mactivity.addressModel1= item.
            mactivity.addressModel = item

            Log.e("address", mactivity.addressModel!!.name)
        }
        if (isEdit) {
            var addressitem = item
            mactivity.addressModel = item
        }
    }

    private lateinit var mactivity: CheckOutActivity
    var addressId: String = ""
    lateinit var addressAdapter: BillingAddressAdapter
    lateinit var datumList: MutableList<GetAddressModel.DataBean.AddressBookBean>
    lateinit var userId: String
    lateinit var mView: View
    lateinit var rvAddress: RecyclerView
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        mView = inflater.inflate(R.layout.fragment_shipping, container, false)
        initView()

        return mView
    }

    private fun initView() {
        mactivity = activity as CheckOutActivity

        userId = activity?.let { SharedPref.getUserId(it) }!!
        mView.txtAddNew.setOnClickListener {
            var intent = Intent(activity, AddAddressActivity::class.java)
            intent.putExtra("add", 0)
            activity!!.startActivity(intent)
        }
        mView.btnShipingContinue.setOnClickListener {
            if (addressId != null && !addressId.isEmpty()) {
                checkAddressAPI()
            } else {
                Toast(activity!!, "Please select Shipping Address")
            }

        }
        rvAddress = mView.findViewById(R.id.rvAddress)
        rvAddress.layoutManager = LinearLayoutManager(
            activity,
            LinearLayoutManager.VERTICAL,
            false
        )


    }

    override fun onResume() {
        super.onResume()
        if (isNetworkAvailable(activity!!)) {
            getAddressAPI()
        } else {
            Toast(activity!!, getString(R.string.network_error))
        }
    }

    private fun checkAddressAPI() {
        CustomProgressbar.showProgressBar(activity, false)
        val map = HashMap<String, String>()
        //    map["restaurant_id"] = mactivity.restaurantId
        map["address_id"] = addressId


        CustomProgressbar.showProgressBar(context, false)
        RetrofitClientSingleton
            .getInstance()
            .checkAddress(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {

                                activity?.let { Toast(it, "" + response.body()!!.message) }
                                mactivity.getPagerPostion().currentItem = 1
                            }
                            FAILURE -> {
                                dialogAddress(response.body()!!.message.toString())
                                //activity?.let { Toast(it, "" + response.body()!!.message) }
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })


        /*  RetrofitClientSingleton.getInstance().checkAddress(map).enqueue(object :
              Callback<CommonModel> {
              override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                  CustomProgressbar.hideProgressBar()
                  activity?.let { Toast(it, getString(R.string.please_try_again)) }
              }

              override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                  CustomProgressbar.hideProgressBar()
                  if (response.isSuccessful) {
                      when (response.body()!!.status) {
                          SUCCESS -> {
                              //Log.e("TAG", "${menuList!!.restaurant_details!!}")
                              activity?.let { Toast(it,response.message()) }
                              mactivity.getPagerPostion().currentItem=1
                          }
                          FAILURE -> {
                              dialogAddress(response.message())
                              activity?.let { Toast(it,response.message()) }

                          }
                          AUTH_FAILURE -> {

                          }
                      }

                  }
              }

          })*/

    }

    private fun dialogAddress(message: String) {
        val builder = AlertDialog.Builder(activity, R.style.AlertDialogCustom)

        // Display a message on alert dialog
        builder.setMessage(message)

        // Set a positive button and its click listener on alert dialog
        builder.setPositiveButton("Ok") { dialog, which ->
            // Do something when user press the positive button

            dialog.cancel()
        }

        val dialog: AlertDialog = builder.create()

        // Display the alert dialog on app interface
        dialog.show()


    }

    private fun getAddressAPI() {
        CustomProgressbar.showProgressBar(activity, false)
        val map = HashMap<String, String>()
        map["user_id"] = userId

        RetrofitClientSingleton
            .getInstance()
            .getAddress(map)
            .enqueue(object : retrofit2.Callback<GetAddressModel> {
                override fun onFailure(call: Call<GetAddressModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    activity?.let { Toast(it, t.message) }
                    mView.btnShipingContinue.visibility = View.GONE
                }

                override fun onResponse(
                    call: Call<GetAddressModel>,
                    response: Response<GetAddressModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val menuList = response.body()!!.data
                        response.body()!!.data!!
                        //llRestaurantDetails.visibility = View.VISIBLE
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //Log.e("TAG", "${menuList!!.restaurant_details!!}")
                                val cartDataResponse = response.body()
                                datumList = cartDataResponse!!.data!!.address_book!!
                                val categoryRoomModel = cartDataResponse.data
                                addressAdapter = BillingAddressAdapter(
                                    activity!!,
                                    datumList,
                                    this@ShippingFragment
                                )
                                //addressAdapter = activity?.let { BillingAddressAdapter(it,datumList,this@ShippingFragment) }!!
                                //addressAdapter = activity?.let { AddressAdapter(it,datumList,this) }!!
                                mView.rvAddress!!.adapter = addressAdapter
                                addressAdapter.notifyDataSetChanged()

                                if (datumList.size > 0) {
                                    mView.btnShipingContinue.visibility = View.VISIBLE
                                } else {
                                    mView.btnShipingContinue.visibility = View.GONE
                                }

                            }
                            FAILURE -> {
                                mView.btnShipingContinue.visibility = View.GONE
                            }
                            AUTH_FAILURE -> {
                                mView.btnShipingContinue.visibility = View.GONE
                            }
                        }

                    }

                }

            })
    }


}
