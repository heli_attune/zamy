package com.attune.zamy.extracode


import android.app.Dialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.LinearLayoutManager
import com.attune.zamy.R
import com.attune.zamy.adapter.ProductVarianceAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.fragment.CartToolbarFragment
import com.attune.zamy.interaface.RecyclerSectionItemDecoration
import com.attune.zamy.interaface.onItemVarianceSelected
import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.CommonModel
import com.attune.zamy.model.OrderCart
import com.attune.zamy.model.RestaurantsMenuModel
import com.attune.zamy.model.RestaurantsMenuModel.DataBean.CategoryBean
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import com.google.android.material.bottomsheet.BottomSheetDialog
import kotlinx.android.synthetic.main.bottom_sheet_variance.*
import kotlinx.android.synthetic.main.colapse_tolbar.imgRestaurant
import kotlinx.android.synthetic.main.fragment_restaurants_details.*
import kotlinx.android.synthetic.main.fragment_restaurants_details.tvReastoTitle
import kotlinx.android.synthetic.main.layout_restaurant_detail_scroll.*
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set


@Suppress("DEPRECATION")
class RestaurantsDetailsDataCartFragment : Fragment(),
    DemoRestaurantDetailDataCartMenuAdapter.AddtoCartItemClickListener,
    DemoRestaurantDetailDataCartMenuAdapter.VarianceDialogClickListener,
    onItemVarianceSelected {
    override fun onMenuItemSelected(
        listBean: CartRestaurantsMenuModel,
        isProductVariance: Boolean,
        isRemove: Boolean
    ) {
        if (isRemove) {
            isProductAvailable(cartArrayList, listBean.foodmenu_id.toString(),listBean)

        } else {

        }
        if (isProductVariance) {


        } else {
            activity?.let { SharedPref.savedata(it,cartArrayList) }
            activity?.let { SharedPref.saveArrayList(cartArrayList,"data", it) }
            if (userId != null && userId != "") {
                addtocartAPI(listBean)
            } else {
                cartArrayList.add(listBean)
                updateTotal()
                //cartArrayList.add(listBean)
                if (cartArrayList.size > 0) {
                    sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
                    bottomSheetForCart()
                    sheetBehavior.isHideable = false
                }
            }
        }
    }


    //when variance selected add btn visibility visible
    override fun onVarianceItemSelected(item: CartRestaurantsMenuModel) {
        varianceitem = item
        dialog!!.tvAddVariance.visibility = View.VISIBLE

        Log.e("variance", item.variation_name)

    }


    override fun onVarianceDialogSelected(
        dataBean: List<CategoryBean.MenuBean.ProductVariationBean>,
        catId: String?,
        foodmenuId: String?,
        menuName: String?,
        taxPrice: Double
    ) {
        openBottomSheetDialog(dataBean, catId, foodmenuId, menuName, taxPrice)
    }


    private fun openBottomSheetDialog(
        listBean: List<CategoryBean.MenuBean.ProductVariationBean>,
        catId: String?,
        foodmenuId: String?,
        menuName: String?,
        taxPrice: Double
    ) {
        val view = layoutInflater.inflate(R.layout.bottom_sheet_variance, null)
        dialog = activity?.let { BottomSheetDialog(it) }
        dialog!!.setContentView(view)
        var varainceList = listBean
        (dialog as BottomSheetDialog).tvAddVariance.visibility = View.GONE
        dialog!!.imgClose.setOnClickListener {

            (dialog as BottomSheetDialog).cancel()
        }
        dialog!!.txtMenuName.text = menuName
        dialog!!.tvAddVariance.setOnClickListener {
            Log.e("dialog", "click")
            cartArrayList.add(varianceitem)
            updateTotal()
            dialog!!.cancel()
        }


        dialog!!.show()

        if (listBean.size > 0) {
            val mLayoutManager = LinearLayoutManager(activity)
            dialog!!.rvVariance!!.layoutManager = mLayoutManager
            dialog!!.rvVariance!!.itemAnimator = DefaultItemAnimator()
            val varianceAdapter = activity?.let {
                ProductVarianceAdapter(
                    it,
                    varainceList,
                    this,
                    catId,
                    foodmenuId,
                    menuName,
                    taxPrice
                )
            }
            dialog!!.rvVariance.adapter = varianceAdapter


        }

    }

    private fun addtocartAPI(listBean: CartRestaurantsMenuModel) {

        CustomProgressbar.showProgressBar(activity!!, false)

        val map = HashMap<String, String>()
        map["restaurant_id"] = "3"
        map["food_menu_id"] = listBean.foodmenu_id.toString()
        map["food_menu_name"] = listBean.menu_name.toString()
        map["user_id"] = "4"

        RetrofitClientSingleton
            .getInstance()
            .addtoCart(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("RestaurantDetail", t.message)
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        // val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //top
                                Toast(activity!!, "" + response.body()!!.message)
                            }
                            FAILURE -> {
                                Toast(activity!!, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })

    }

    lateinit var menudataList: List<RestaurantsMenuModel.DataBean.CategoryBean>
    private lateinit var adapter: DemoRestaurantDetailDataCartMenuAdapter
    private lateinit var userId: String
    var finalTotalValue: Double = 0.00
    private lateinit var varianceitem: CartRestaurantsMenuModel
    var dialog: Dialog? = null
    lateinit var mView: View
    lateinit var restaurant_id: String
    lateinit var tvMenu: TextView
    lateinit var tvInfo: TextView
    lateinit var tvGallary: TextView
    private lateinit var sheetBehavior: BottomSheetBehavior<LinearLayout>
    private lateinit var sheetBehaviorvariance: BottomSheetBehavior<LinearLayout>
    lateinit var txtItem: TextView
    lateinit var txtPrice: TextView
    lateinit var txtCharges: TextView
    lateinit var txtViewCart: TextView
    var cartArrayList: MutableList<CartRestaurantsMenuModel> = ArrayList<CartRestaurantsMenuModel>()
    lateinit var bottom_sheet: LinearLayout
    lateinit var bottom_sheet_variance: LinearLayout
    var orderCartModel: ArrayList<OrderCart> = ArrayList<OrderCart>()
    lateinit var orderDataPass: String

    override fun onCreateView(

        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        mView = inflater.inflate(R.layout.layout_restaurant_detail_scroll, container, false)
        initView()


        return mView
    }

    private fun initView() {
        val args = arguments

        if(args!=null)
        {
            restaurant_id = args.getString(RESTAURANT_ID_PASS)!!
        }

        userId = activity?.let { SharedPref.getUserId(it) }!!
        //  restaurant_id = intent.getStringExtra(RESTAURANT_ID_PASS) ?: "0"
        val toolbar_layout = mView.findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)
        var toolbar = mView.findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar1)
        toolbar_layout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
        toolbar_layout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.title = "xyz"
        }
        toolbar.setNavigationOnClickListener {
            activity!!.onBackPressed()

        }
        tvMenu = mView.findViewById(R.id.tvMenu)
        tvMenu.setOnClickListener {
            SelectOptions(true, false, false)
        }
        tvInfo = mView.findViewById(R.id.tvInfo)
        tvInfo.setOnClickListener {
            SelectOptions(false, true, false)
        }
        tvGallary = mView.findViewById(R.id.tvGallary)
        tvGallary.setOnClickListener {
            SelectOptions(false, false, true)
        }
        bottom_sheet = mView.findViewById(R.id.bottom_sheet)
        sheetBehavior = BottomSheetBehavior.from<LinearLayout>(bottom_sheet)
//        bottom_sheet_variance = mView.findViewById(R.id.bottom_sheet_variance)
        //      sheetBehaviorvariance = BottomSheetBehavior.from<LinearLayout>(bottom_sheet_variance)

        sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        if (cartArrayList.size > 0) {
            sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            bottomSheetForCart()
            sheetBehavior.isHideable = false
        }
        bottomSheetForCart()

        SelectOptions(true, false, false)

        if (!activity?.let { isNetworkAvailable(it) }!!) {
            activity?.let { Toast(it, getString(R.string.network_error)) }
        } else {
            //menu items call api
            getRestaurentMenuList()
        }

    }

    private fun SelectOptions(bMenu: Boolean, bInfo: Boolean, bGallary: Boolean) {

        if (bMenu) {
            tvMenu.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
            tvMenu.setTextColor(resources.getColor(R.color.red_ea1b25))
            tvInfo.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
            tvInfo.setTextColor(resources.getColor(R.color.white))
            tvGallary.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
            tvGallary.setTextColor(resources.getColor(R.color.white))

        } else if (bInfo) {
            tvMenu.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
            tvMenu.setTextColor(resources.getColor(R.color.white))
            tvInfo.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
            tvInfo.setTextColor(resources.getColor(R.color.red_ea1b25))
            tvGallary.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
            tvGallary.setTextColor(resources.getColor(R.color.white))
        } else if (bGallary) {
            tvMenu.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
            tvMenu.setTextColor(resources.getColor(R.color.white))
            tvInfo.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_button_fill)
            tvInfo.setTextColor(resources.getColor(R.color.white))
            tvGallary.background =
                ContextCompat.getDrawable(this.activity!!, R.drawable.red_border_button)
            tvGallary.setTextColor(resources.getColor(R.color.red_ea1b25))
        }


    }

    private fun getRestaurentMenuList() {

        CustomProgressbar.showProgressBar(activity, false)
        val map = HashMap<String, String>()
        map["restaurant_id"] = "3"

        RetrofitClientSingleton
            .getInstance()
            .getReastaurentMenu(map)
            .enqueue(object : retrofit2.Callback<RestaurantsMenuModel> {
                override fun onFailure(call: Call<RestaurantsMenuModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    activity?.let { Toast(it, t.message) }

                }

                override fun onResponse(
                    call: Call<RestaurantsMenuModel>,
                    response: Response<RestaurantsMenuModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val menuList = response.body()!!.data
                        response.body()!!.data!!.category
                        //llRestaurantDetails.visibility = View.VISIBLE
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //Log.e("TAG", "${menuList!!.restaurant_details!!}")
                                var restaurantdata = menuList!!.restaurant_details
                                if (restaurantdata.images != null && restaurantdata.images != "") {
                                    activity?.let {
                                        Glide.with(it)
                                            .load(restaurantdata.images)
                                            .into(imgRestaurant)
                                    }
                                }
                                if (restaurantdata.res_name != null && restaurantdata.res_name != "") {
                                    tvReastoTitle.text = menuList.restaurant_details.res_name
                                    toolbar_layout.title =
                                        menuList.restaurant_details.res_name
                                }
                                /*tvReastoDesc.text =
                                      menuList.restaurant_details!!.area + "," + menuList.restaurant_details!!.landmark*/


                                if (menuList.category!!.size > 0) {
                                    val menuHeader = menuList


                                    rvMenu.layoutManager =
                                        LinearLayoutManager(activity)
                                    /*  val adapter = response.body()!!.data!!.category?.let {
                                          DemoRestaurantDetailDataCartMenuAdapter(
                                              this!!.activity,
                                              it,
                                              activity
                                          )
                                      }*/
                                    /* val  adapter = activity?.let {
                                         DemoRestaurantDetailDataCartMenuAdapter(
                                             it, response.body()!!.data!!.category!!,this)
                                     }*/
                                    menudataList = response.body()!!.data!!.category!!

                                    adapter = activity?.let {
                                        DemoRestaurantDetailDataCartMenuAdapter(
                                            it,
                                            response.body()!!.data!!.category!!,
                                            this@RestaurantsDetailsDataCartFragment,
                                            this@RestaurantsDetailsDataCartFragment
                                        )
                                    }!!

                                    rvMenu.adapter = adapter

                                    val sectionItemDecoration = RecyclerSectionItemDecoration(
                                        LinearLayout.LayoutParams.WRAP_CONTENT,
                                        true, getSectionCallback(menuHeader.category!!)

                                    )

                                    rvMenu.addItemDecoration(sectionItemDecoration)
                                }



                            }
                            FAILURE -> {

                            }
                            AUTH_FAILURE -> {

                            }
                        }

                    }

                }

            })
    }

    private fun getSectionCallback(people: List<CategoryBean>): RecyclerSectionItemDecoration.SectionCallback {
        return object : RecyclerSectionItemDecoration.SectionCallback {
            override fun isSection(position: Int): Boolean {
                return position == 0 || people[position]
                    .cat_display_name !== people[position - 1]
                    .cat_display_name
            }

            override fun getSectionHeader(position: Int): CharSequence {
                return people[position]
                    .cat_display_name.toString()
            }
        }
    }

    private fun bottomSheetForCart() {

        txtItem = mView.findViewById(R.id.txtItem) as TextView
        txtPrice = mView.findViewById(R.id.txtPrice) as TextView
        txtCharges = mView.findViewById(R.id.txtItem) as TextView
        txtViewCart = mView.findViewById(R.id.txtViewCart) as TextView

        txtViewCart.setOnClickListener {
            Log.e("cart size", cartArrayList.size.toString())
            if (cartArrayList.size > 0) {
                val manager = fragmentManager
                val transaction = manager!!.beginTransaction()
                transaction.add(R.id.fragment_restaurant,
                    CartToolbarFragment()
                )
                transaction.addToBackStack(CartToolbarFragment().toString())
                transaction.commit()
                txtItem.text = cartArrayList.size.toString() + " Items"
                adddatatoStore()
                //addDatatoJson()
            }


        }

        //bottom sheet

        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })
    }

    fun adddatatoStore(){
        var CartItemArrayModel: ArrayList<OrderCart> =
            ArrayList<OrderCart>()
        if (cartArrayList.size > 0) {
            for (i in 0 until cartArrayList.size) {

                val favoriteList = OrderCart()
                favoriteList.food_menu_id = cartArrayList[i].foodmenu_id
                favoriteList.menu_name = cartArrayList[i].menu_name
                favoriteList.variation_id = cartArrayList[i].variation_id
                favoriteList.variation_name = cartArrayList[i].variation_name
                favoriteList.qty = cartArrayList[i].qty
                favoriteList.price = cartArrayList[i].priceCart

                CartItemArrayModel.add(favoriteList)
            }
            Log.e("json", CartItemArrayModel.size.toString())


            Log.e("json", CartItemArrayModel.size.toString())
            orderCartModel=CartItemArrayModel
        }
        var data = activity?.let { SharedPref.getCartArrayList(CARTITEMS, it) }
        if (data != null) {
            if (data.size > 0) {
                data.addAll(0,orderCartModel)
                activity?.let { SharedPref.saveArrayListCart(data, CARTITEMS, it) }
            }else{
                activity?.let { SharedPref.saveArrayListCart(orderCartModel, CARTITEMS, it) }
            }
        } else {
            activity?.let { SharedPref.saveArrayListCart(orderCartModel, CARTITEMS, it) }
        }
    }
    /*private fun addDatatoJson() {

        var orderCart = OrderCart()
        val orders = OrderCart.OrderDataArrayBean()
        orders.restaurant_id = restaurant_id
        orders.user_id ="4"
        orders.sub_total= finalTotalValue.toInt()
        orderCart.order_data_array = orders
        var CartItemArrayModel: ArrayList<OrderCart.OrderDataArrayBean.FoodMenuDataBean> =
            ArrayList<OrderCart.OrderDataArrayBean.FoodMenuDataBean>()
        if (cartArrayList.size > 0) {
            for (i in 0 until cartArrayList.size) {

                val favoriteList = OrderCart.OrderDataArrayBean.FoodMenuDataBean()
                favoriteList.food_menu_id = cartArrayList[i].foodmenu_id
                favoriteList.menu_name = cartArrayList[i].menu_name
                favoriteList.variation_id = cartArrayList[i].variation_id
                favoriteList.variation_name = cartArrayList[i].variation_name
                favoriteList.qty = cartArrayList[i].qty
                favoriteList.price = cartArrayList[i].priceCart

                CartItemArrayModel.add(favoriteList)
            }
            Log.e("json", CartItemArrayModel.size.toString())
            orderCart.order_data_array!!.food_menu_data = CartItemArrayModel

            Log.e("json", CartItemArrayModel.size.toString())
        }

        orderCartModel.add(orderCart)
        //activity?.let { SharedPref.savedata(it,orderCartModel) }
        var data = activity?.let { SharedPref.getCartArrayList(CARTITEMS, it) }
        if (data != null) {
            if (data.size > 0) {
                data.addAll(0,orderCartModel)
                activity?.let { SharedPref.saveArrayListCart(data, CARTITEMS, it) }
            }else{
                activity?.let { SharedPref.saveArrayListCart(orderCartModel, CARTITEMS, it) }
            }
        } else {
            activity?.let { SharedPref.saveArrayListCart(orderCartModel, CARTITEMS, it) }
        }
        //saveArrayList(testData,"abcd",this@MenuPage)
        Log.e("json", orderCartModel.size.toString())
        val args = Bundle()
        args.putString(RESTAURANT_ID_PASS,restaurant_id)
        val toFragment = CartToolbarFragment()
        toFragment.setArguments(args)
        fragmentManager!!
            .beginTransaction()
            .add(R.id.fragment_restaurant, toFragment,"CartToolbarFragment")
            .addToBackStack("CartToolbarFragment").commit()
      *//*  if (cartArrayList.size > 0) {
            try {
                val mainobject = JSONObject()

                val subjsonobject = JSONObject()
                //once order will be updated pass order id

                subjsonobject.put("user_id", "4")

                subjsonobject.put("restaurant_id", "3")
                // subjsonobject.put("lastName", "Ameen")


                //if you want to modify some value just do like this.
                //subjsonobject.put("postalCode", 88888)


                var jsonParam1: JSONObject?
                val jsonArray = JSONArray()
                for (i in 0 until cartArrayList.size) {
                    jsonParam1 = JSONObject()
                    try {
                        jsonParam1.put("food_menu_id", cartArrayList[i].foodmenu_id)
                        jsonParam1.put("menu_name", cartArrayList[i].menu_name)
                        jsonParam1.put("variation_name", cartArrayList[i].variation_name)
                        jsonParam1.put("variation_id", cartArrayList[i].variation_id)
                        jsonParam1.put("price", cartArrayList[i].price)
                        jsonParam1.put("qty", cartArrayList[i].qty)


                    } catch (e: JSONException) {
                        // TODO Auto-generated catch block
                        e.printStackTrace()
                    }

                    jsonArray.put(jsonParam1)
                }
                subjsonobject.put("food_menu_data", jsonArray)


                mainobject.put("order_data_array", subjsonobject)

                Log.d("json", mainobject.toString())
                orderDataPass = mainobject.toString()


            } catch (e: JSONException) {
                e.printStackTrace()
            }

        } else {
            //Toast(activity, "Please Select Items for orders!")
        }*//*


    }*/


    fun updateTotal() {
        // var total: Int = 0
        var total: Double = 0.00

        //var totalprice: Int
        var totalprice: Double
        for (i in 0 until cartArrayList.size) {

            totalprice = cartArrayList[i].priceCart
            total = total + totalprice
        }


        finalTotalValue = total
        txtPrice.text = getString(R.string.rupees) + " " + total

        txtItem.text = "" + cartArrayList.size

    }

    private fun isProductAvailable(
        cartArrayList: MutableList<CartRestaurantsMenuModel>,
        foodmenu_id: String,
        listBean: CartRestaurantsMenuModel
    ): Boolean {

        var isAvail = false

        for (cartModel in cartArrayList) {
            //if cart arraylist have menu items that we have click check condition
            if (cartModel.foodmenu_id.equals(foodmenu_id)) {
                //if cart items have variance than check with variance id also
                cartArrayList.remove(listBean)

                updateTotal()
                Log.e("removesize",cartArrayList.size.toString())

                //adapter.notifyDataSetChanged()

                //cartArrayList.removeAt(cartArrayList[i].position)
                //adapter.notifyItemChanged(listBean.position)
                isAvail = true


            }
        }

        return isAvail
    }



}
