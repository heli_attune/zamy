package com.attune.zamy.extracode

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.RestaurantsMenuModel


class DemoRestaurantDetailDataCartMenuAdapter(
    internal var mContext: Context,
    internal var mArrayListString: List<RestaurantsMenuModel.DataBean.CategoryBean>,
    internal var itemSelectedListener: AddtoCartItemClickListener,
    internal var itemVariationListener: VarianceDialogClickListener

) : RecyclerView.Adapter<DemoRestaurantDetailDataCartMenuAdapter.Holder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): Holder {
        val view = LayoutInflater.from(mContext)
            .inflate(R.layout.restaurant_detail_item_row_child, parent, false)
        return Holder(view)
    }

    override fun onBindViewHolder(holder: Holder, position: Int) {
        val categoryBean = mArrayListString[position]
        holder.linearLayout.removeAllViews()

        holder.mTextView.text = categoryBean.cat_display_name


        for (i in 0 until categoryBean.menu!!.size) {

            val view = LayoutInflater.from(mContext)
                .inflate(R.layout.restaurant_detail_item_row_child, null)
            //   TextView textView = new TextView(mContext);
            val tvSubTitle = view.findViewById<TextView>(R.id.tvSubTitle)
            val tvItemName = view.findViewById<TextView>(R.id.tvItemName)
            val tvDeliveryTime = view.findViewById<TextView>(R.id.tvDeliveryTime)
            val tvPrice = view.findViewById<TextView>(R.id.tvPrice)
            holder.linearLayout.addView(view)
            val rlheader = view.findViewById<RelativeLayout>(R.id.rlheader)
            rlheader.visibility = View.GONE
            val tvCustom = view.findViewById<TextView>(R.id.tvCustom)
            val lnCart = view.findViewById<LinearLayout>(R.id.lnCart)
            lnCart.visibility = View.GONE
            val tvAddToCart = view.findViewById<TextView>(R.id.tvAddToCart)
            val tvRemove = view.findViewById<TextView>(R.id.tvRemove)
            tvAddToCart.visibility = View.VISIBLE
            var lnPrice = view.findViewById<LinearLayout>(R.id.lnPrice)


            tvCustom.setOnClickListener {
                var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> = ArrayList<CartRestaurantsMenuModel>()
                val favouriteModel = CartRestaurantsMenuModel()
                favouriteModel.cat_id =categoryBean.menu!![i].cat_id
                favouriteModel.foodmenu_id =categoryBean.menu!![i].foodmenu_id
                favouriteModel.menu_name =categoryBean.menu!![i].menu_name
                favouriteModel.qty =1
                favouriteModel.tax_price =categoryBean.menu!![i].tax_price.toString()
                if(categoryBean.menu!![i].reduced_price!=null && categoryBean.menu!![i].reduced_price>0){
                    favouriteModel.priceCart =categoryBean.menu!![i].reduced_price.toDouble()
                }else if(categoryBean.menu!![i].price!=null && categoryBean.menu!![i].price>0){
                    favouriteModel.priceCart =categoryBean.menu!![i].price.toDouble()
                }else{
                    favouriteModel.priceCart =0.0
                }

                favouriteModel.isSelected=false
                favouriteModel.position= holder.adapterPosition
                CartItemArrayModel.add(favouriteModel)
                if(categoryBean.menu!![i].product_variation!!.isNotEmpty()){
                    // itemSelectedListener.onMenuItemSelected(favouriteModel,true)
                    itemVariationListener.onVarianceDialogSelected(categoryBean.menu!![i].product_variation!!,categoryBean.cat_id,
                        categoryBean.menu!![i].foodmenu_id,
                        categoryBean.menu!![i].menu_name,
                        categoryBean.menu!![i].tax_price)
                }else{
                    itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)

                }
                tvAddToCart.visibility= View.GONE

                tvRemove.visibility=View.VISIBLE
            }
            lnPrice.setOnClickListener {

            }
            tvAddToCart.setOnClickListener {

                var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> = ArrayList<CartRestaurantsMenuModel>()
                val favouriteModel = CartRestaurantsMenuModel()
                favouriteModel.cat_id =categoryBean.menu!![i].cat_id
                favouriteModel.foodmenu_id =categoryBean.menu!![i].foodmenu_id
                favouriteModel.menu_name =categoryBean.menu!![i].menu_name
                favouriteModel.qty =1
                favouriteModel.tax_price =categoryBean.menu!![i].tax_price.toString()
                if(categoryBean.menu!![i].reduced_price!=null && categoryBean.menu!![i].reduced_price>0){
                    favouriteModel.priceCart =categoryBean.menu!![i].reduced_price.toDouble()
                }else if(categoryBean.menu!![i].price!=null && categoryBean.menu!![i].price>0){
                    favouriteModel.priceCart =categoryBean.menu!![i].price.toDouble()
                }else{
                    favouriteModel.priceCart =0.0
                }
                favouriteModel.isSelected=false
                favouriteModel.position= holder.adapterPosition
                CartItemArrayModel.add(favouriteModel)
                if(categoryBean.menu!![i].product_variation!!.isNotEmpty()){
                    // itemSelectedListener.onMenuItemSelected(favouriteModel,true)
                    itemVariationListener.onVarianceDialogSelected(categoryBean.menu!![i].product_variation!!,categoryBean.cat_id,
                        categoryBean.menu!![i].foodmenu_id,
                        categoryBean.menu!![i].menu_name,
                        categoryBean.menu!![i].tax_price)
                }else{
                    itemSelectedListener.onMenuItemSelected(favouriteModel,false,false)

                }
                tvAddToCart.visibility= View.GONE

                tvRemove.visibility=View.VISIBLE
            }
            tvRemove.setOnClickListener {
                tvAddToCart.visibility = View.VISIBLE
                tvRemove.visibility = View.GONE
                var CartItemArrayModel: ArrayList<CartRestaurantsMenuModel> = ArrayList<CartRestaurantsMenuModel>()
                val favouriteModel = CartRestaurantsMenuModel()
                favouriteModel.cat_id =categoryBean.menu!![i].cat_id
                favouriteModel.foodmenu_id =categoryBean.menu!![i].foodmenu_id
                favouriteModel.menu_name =categoryBean.menu!![i].menu_name
                favouriteModel.qty =1
                favouriteModel.tax_price =categoryBean.menu!![i].tax_price.toString()
                favouriteModel.priceCart =categoryBean.menu!![i].tax_price
                favouriteModel.position= position
                favouriteModel.isSelected=false
                favouriteModel.position= holder.adapterPosition
                CartItemArrayModel.add(favouriteModel)
                //item removefrom cartaarylist
                itemSelectedListener.onMenuItemSelected(favouriteModel,false,true)

            }
            if (categoryBean.isSelected){
                tvRemove.visibility = View.VISIBLE
                tvAddToCart.visibility=View.GONE
            }else{
                tvRemove.visibility = View.GONE
                tvAddToCart.visibility=View.VISIBLE
            }


            //if product variance is available show custom button
            if (categoryBean.menu!![i].product_variation!!.size > 0) {
                tvCustom.visibility = View.VISIBLE
            } else {
                tvCustom.visibility = View.GONE
            }
            if (i == 0) {

            } else if (i == categoryBean.menu!!.size - 1) {
                rlheader.visibility = View.VISIBLE

            }

            if (categoryBean.menu!![i].long_description != null && categoryBean.menu!![i].long_description !== "") {
                tvSubTitle.text = categoryBean.menu!![i].long_description

            }
            if (categoryBean.menu!![i].menu_name != null && categoryBean.menu!![i].menu_name !== "") {
                tvItemName.text = categoryBean.menu!![i].menu_name
            }
            if (categoryBean.menu!![i].minimum_preparation_time != null && categoryBean.menu!![i].minimum_preparation_time !== "") {
                tvDeliveryTime.text = categoryBean.menu!![i].minimum_preparation_time
            }
            if (categoryBean.menu!![i].reduced_price != null) {
                tvPrice.text =
                    mContext.resources.getString(R.string.rupees) + categoryBean.menu!![i].reduced_price
            }else if(categoryBean.menu!![i].price!=null){
                tvPrice.text =
                    mContext.resources.getString(R.string.rupees) + categoryBean.menu!![i].price
            }


        }
    }
    interface AddtoCartItemClickListener {

        fun onMenuItemSelected(listBean: CartRestaurantsMenuModel,isProductVariance :Boolean,isRemove :Boolean)

    }

    interface VarianceDialogClickListener {

        fun onVarianceDialogSelected(
            dataBean: List<RestaurantsMenuModel.DataBean.CategoryBean.MenuBean.ProductVariationBean>,
            catId: String?,
            foodmenuId: String?,
            menuName: String?,
            taxPrice: Double
        )

    }
    override fun getItemCount(): Int {
        return mArrayListString.size
    }

    inner class Holder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        internal var mTextView: TextView
        internal var tvSubTitle: TextView
        internal var tvItemName: TextView
        internal var tvDeliveryTime: TextView
        internal var linearLayout: LinearLayout

        init {
            mTextView = itemView.findViewById(R.id.tvItemName)
            tvItemName = itemView.findViewById(R.id.tvItemName)
            tvSubTitle = itemView.findViewById(R.id.tvSubTitle)
            tvDeliveryTime = itemView.findViewById(R.id.tvDeliveryTime)
            linearLayout = itemView.findViewById(R.id.llMainChild)


        }
    }




}