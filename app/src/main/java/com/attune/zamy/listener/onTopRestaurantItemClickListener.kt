package com.attune.zamy.listener

import com.attune.zamy.model.TopRestaurantModel

interface onTopRestaurantItemClickListener {
    fun onItemSelected(listBean: TopRestaurantModel.DataBean.TopRestaurantListBean)
}