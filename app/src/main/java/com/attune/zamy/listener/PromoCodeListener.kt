package com.attune.zamy.listener

import com.attune.zamy.model.CouponListModel


interface PromoCodeListener {
    fun onTermsClick(pos:Int ,promoDetail: CouponListModel.DataBean)
    fun onPromoRadioOnClick(pos:Int ,promoDetail: CouponListModel.DataBean)
}