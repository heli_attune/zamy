package com.attune.zamy.model

class AddAddressModel {

    /**
     * status : 1
     * message : Address book updated successfully.
     * data : {"user_id":"3","address_type":"home","other_address_type":"","name":"abc1","email":"abc1@gmail.com","phone":"23452343411","shipping_area":"ahmedabad","address_1":"ahmedabad","address_2":"ahmedabad","country":"india","state":"gujarat","city":"ahmedabad","postcode":"111111","address_lat":"22.9977913","address_lng":"72.5086396","status":1,"created_date":"2019-10-21 07:33:41"}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * user_id : 3
         * address_type : home
         * other_address_type :
         * name : abc1
         * email : abc1@gmail.com
         * phone : 23452343411
         * shipping_area : ahmedabad
         * address_1 : ahmedabad
         * address_2 : ahmedabad
         * country : india
         * state : gujarat
         * city : ahmedabad
         * postcode : 111111
         * address_lat : 22.9977913
         * address_lng : 72.5086396
         * status : 1
         * created_date : 2019-10-21 07:33:41
         */

        var user_id: String? = null
        var address_type: String? = null
        var other_address_type: String? = null
        var name: String? = null
        var email: String? = null
        var phone: String? = null
        var shipping_area: String? = null
        var address_1: String? = null
        var address_2: String? = null
        var country: String? = null
        var state: String? = null
        var city: String? = null
        var postcode: String? = null
        var address_lat: String? = null
        var address_lng: String? = null
        var status: Int = 0
        var created_date: String? = null
    }
}
