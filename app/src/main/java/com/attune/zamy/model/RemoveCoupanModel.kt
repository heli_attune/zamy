package com.attune.zamy.model

class RemoveCoupanModel {
    /**
     * status : 1
     * message : Coupon code remove Successfully.
     * data : 220
     */

    /* {
        "status": 1,
            "message": "Coupon code remove Successfully.",
            "data": 220
    }*/

    var status: Int = 0
    var message: String? = null
    var data: Int = 0
}
