package com.attune.zamy.model;

public class PaytmCheckoutModel {

    /**
     * status : 1
     * message : Order received.
     * data : {"order_id":89,"user_id":"5","order_total":875,"paytm_link":"http://client.attuneinfocom.com/PaytmPaymentGateway/pgRedirect.php"}
     */

    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * order_id : 89
         * user_id : 5
         * order_total : 875
         * paytm_link : http://client.attuneinfocom.com/PaytmPaymentGateway/pgRedirect.php
         */

     /*   private int order_id;
        private String user_id;
        private int order_total;*/
        private String paytm_link;
        private int order_id;
        public int getOrder_id() {
            return order_id;
        }

        public void setOrder_id(int order_id) {
            this.order_id = order_id;
        }
        /*
                public String getUser_id() {
                    return user_id;
                }

                public void setUser_id(String user_id) {
                    this.user_id = user_id;
                }

                public int getOrder_total() {
                    return order_total;
                }

                public void setOrder_total(int order_total) {
                    this.order_total = order_total;
                }
        */
        public String getPaytm_link() {
            return paytm_link;
        }

        public void setPaytm_link(String paytm_link) {
            this.paytm_link = paytm_link;
        }
    }
}
