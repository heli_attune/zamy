package com.attune.zamy.model

class HomeScreenModel {

    /**
     * status : 1
     * message : success
     * data : {"top_restaurant_list":
     * [
     * {"id":"3",
     * "res_name":"Hop Meal Restaurant",
     * "res_alias":"hop-meal-restaurant-makarba-ahmedabad-380051",
     * "logo":"http://pos.zamy.in/uploads/kitchen/logo/2d6cb3c59baf576d3f101b376800a4b6.png",
     * "images":"http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg",
     * "landmark":"Prahlad Nagar",
     * "area":"Makarba",
     * "approx_delivery_time":"52 MINS","approx_cost":"\u20b9500 FOR TWO",
     * "service_type":"Indian, Chinese, Fast Food","total_orders":"259","total_restaurant_orders":"259"},
     * {"id":"10",
     * "res_name":"Healthy Restaurant",
     * "res_alias":"healthy-restaurant-makarba-ahmedabad-380055",
     * "logo":"http://pos.zamy.in/uploads/kitchen/logo/4de066608e5d7e80b8bcc5a8600714f7.png",
     * "images":"http://pos.zamy.in/uploads/kitchen/images/e3e69325f34f2f7f7600c41b0d632364.jpg",
     * "landmark":"Makarba","area":"Makarba","approx_delivery_time":"","approx_cost":"","service_type":"","total_orders":"117","total_restaurant_orders":"117"},
     * {"id":"9","res_name":"Bun Kabab","res_alias":"bun-kabab-makarba-ahmedabad-380055","logo":"http://pos.zamy.in/uploads/kitchen/logo/08982faeb1b5babdeb0f7bb243ab72e2.png","images":"http://pos.zamy.in/uploads/kitchen/images/8a25363cc6c59e224910b1816b26b151.jpg","landmark":"makrba","area":"Makarba","approx_delivery_time":"","approx_cost":"","service_type":"","total_orders":"112","total_restaurant_orders":"112"},
     * {"id":"20","res_name":"Kitchen Badshah","res_alias":"kitchen-badshah-sarkhej-ahmedabad-382210","logo":"http://pos.zamy.in/uploads/kitchen/logo/4cd5200a365a66658270a017b29d01b4.jpg","images":"http://pos.zamy.in/uploads/kitchen/images/ff5765694ecb478a6e9857c013fe2c90.jpg","landmark":"Near Qadri Party Plot","area":"Sarkhej","approx_delivery_time":"30 Mins","approx_cost":"550","service_type":"Indian, Chinese, Veg. and Much more","total_orders":"14","total_restaurant_orders":"14"},
     * {"id":"27","res_name":"Tawa IceCream","res_alias":"tawa-icecream-sarkhej-ahmedabad-380051","logo":"http://pos.zamy.in/uploads/kitchen/logo/00c9fc9bda8b6eeea2924984d9790132.png","images":"http://pos.zamy.in/uploads/kitchen/images/57c0bfac9765afb20e93b7279cb798b6.png","landmark":"Al-Burooj","area":"Sarkhej","approx_delivery_time":"30","approx_cost":"250","service_type":"Tawa IceCream","total_orders":"5","total_restaurant_orders":"5"},
     * {"id":"17","res_name":"Fish Express","res_alias":"fish-express-sarkhej-ahmedabad-380055","logo":"http://pos.zamy.in/uploads/kitchen/logo/52c6d764f12cfd5358a372d9b3970535.png","images":"http://pos.zamy.in/uploads/kitchen/images/fe093b27f275b38a79122d39a8b579e0.png","landmark":"Near Amber Tower","area":"Sarkhej","approx_delivery_time":"45 Minits","approx_cost":"650","service_type":"North Indian, Seafood","total_orders":"2","total_restaurant_orders":"2"},
     * {"id":"32","res_name":"New ZK Restaurant","res_alias":"new-zk-restaurant-sarkhej-ahmedabad-380051","logo":"http://pos.zamy.in/uploads/kitchen/logo/f465e1b703bc627ee05970cbb7cb46a7.jpg","images":"http://pos.zamy.in/uploads/kitchen/images/9eada5be547072da8f24637d8c1d357d.jpg","landmark":"Opposite Gandhi Hall","area":"Sarkhej","approx_delivery_time":"30","approx_cost":"500","service_type":"Mughlai, North Indian, Chinese","total_orders":"1","total_restaurant_orders":"1"},
     * {"id":"31","res_name":"Al-Heraa","res_alias":"al-heraa-sarkhej-ahmedabad-380055","logo":"http://pos.zamy.in/uploads/kitchen/logo/3b1e6630f41ad04179b4a87505b2e3eb.jpg","images":"http://pos.zamy.in/uploads/kitchen/images/921dc78e9f7a898c8c159e8840c5cac9.jpg","landmark":"Gujarat","area":"Sarkhej","approx_delivery_time":"45","approx_cost":"600","service_type":"Chinese ","total_orders":"1","total_restaurant_orders":"1"},
     * {"id":"36","res_name":"Arabic Flavour  Center","res_alias":"arabic-flavour-center-juhapura-ahmedabad-380051","logo":"http://pos.zamy.in/uploads/kitchen/logo/76462e1e6161712c68e068cd759bfd8d.jpg","images":"http://pos.zamy.in/uploads/kitchen/images/91f9541ff37fb95decded241b344dfaa.jpg","landmark":"sarkhej","area":"Juhapura","approx_delivery_time":"","approx_cost":"","service_type":"Fast Food","total_orders":"1","total_restaurant_orders":"1"},{"id":"15","res_name":"Magic Chicken - Sahrkhej","res_alias":"magic-chicken--sahrkhej-sarkhej-ahmedabad-380055","logo":"http://pos.zamy.in/uploads/kitchen/logo/f2bb0ca21998c90297a7a78f204aca43.jpg","images":"http://pos.zamy.in/uploads/kitchen/images/bc628c31ef6eb6b40e6bfb0f55643597.jpg","landmark":"Opposite Amber Tower","area":"Sarkhej","approx_delivery_time":"45 Minits","approx_cost":"600","service_type":"Chicken Shawarama, Chicken Grill, Rolls, Shawarma Roll, Chicken Roll, Pita Bread, Al Faham Chicken","total_orders":"0","total_restaurant_orders":"0"}],"fav_food_list":[{"menu_name":"BBQ COMBO 2","short_code":"BBQ COMBO 2","order_type":"1,2,3","menu_logo":"http://pos.zamy.in/uploads/FoodMenu/BBQ_COMBO_2.jpg","price":null,"reduced_price":null,"long_description":"","restaurant_id":"3","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-makarba-ahmedabad-380051","kitchen_image":"http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"BBQ COMBO 1","short_code":"BBQ COMBO 1","order_type":"1,2,3","menu_logo":"http://pos.zamy.in/uploads/FoodMenu/BBQ_COMBO_1.jpg","price":null,"reduced_price":null,"long_description":"","restaurant_id":"3","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-makarba-ahmedabad-380051","kitchen_image":"http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Chicken Club Sandwich","short_code":"chicken club sandwic","order_type":"1,2,3","menu_logo":"http://pos.zamy.in/uploads/FoodMenu/sandwitch.jpg","price":null,"reduced_price":null,"long_description":"","restaurant_id":"3","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-makarba-ahmedabad-380051","kitchen_image":"http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Chicken Tikka Sandwich","short_code":"chicken tikka sandwi","order_type":"1,2,3","menu_logo":"http://pos.zamy.in/uploads/FoodMenu/Chicken_Tikka_Sandwich.jpg","price":null,"reduced_price":null,"long_description":"","restaurant_id":"3","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-makarba-ahmedabad-380051","kitchen_image":"http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Chicken Human Sandwich","short_code":"chicken human sandwi","order_type":"1,2,3","menu_logo":"http://pos.zamy.in/uploads/FoodMenu/Chicken_Human_Sandwich.jpg","price":null,"reduced_price":null,"long_description":"","restaurant_id":"3","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-makarba-ahmedabad-380051","kitchen_image":"http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Chicken Junglee Sandwich","short_code":"chicken junglee sand","order_type":"1,2,3","menu_logo":"http://pos.zamy.in/uploads/FoodMenu/Chicken-junglee.jpg","price":null,"reduced_price":null,"long_description":"","restaurant_id":"3","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-makarba-ahmedabad-380051","kitchen_image":"http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Hawaiian Chicken Sandwich","short_code":"hawaiian chicken san","order_type":"1,2,3","menu_logo":"http://pos.zamy.in/uploads/FoodMenu/download22.jpeg","price":null,"reduced_price":null,"long_description":"","restaurant_id":"3","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-makarba-ahmedabad-380051","kitchen_image":"http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Egg Masala Sandwich","short_code":"egg masala sandwich","order_type":"1,2,3","menu_logo":"http://pos.zamy.in/uploads/FoodMenu/EGG_MASALA_SANDWICH.jpg","price":null,"reduced_price":null,"long_description":"","restaurant_id":"3","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-makarba-ahmedabad-380051","kitchen_image":"http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Egg Sandwich Plain","short_code":"egg sandwich plain","order_type":"1,2,3","menu_logo":"http://pos.zamy.in/uploads/FoodMenu/EGG_SANDWICH_PLAIN.jpg","price":null,"reduced_price":null,"long_description":"","restaurant_id":"3","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-makarba-ahmedabad-380051","kitchen_image":"http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Mexican Chicken Sandwich","short_code":"mexican chicken sand","order_type":"1,2,3","menu_logo":"http://pos.zamy.in/uploads/FoodMenu/Mexican_Sandwich.jpg","price":null,"reduced_price":null,"long_description":"","restaurant_id":"3","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-makarba-ahmedabad-380051","kitchen_image":"http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"}],"category_list":[{"cat_id":"5","cat_name":"OFFER-490","cat_display_name":"OFFER-490","cat_logo":"http://pos.zamy.in/uploads/FoodCategory/"},{"cat_id":"6","cat_name":"Beverages","cat_display_name":"Beverages","cat_logo":"http://pos.zamy.in/uploads/FoodCategory/"},{"cat_id":"7","cat_name":"Mocktail","cat_display_name":"Mocktail","cat_logo":"http://pos.zamy.in/uploads/FoodCategory/"},{"cat_id":"8","cat_name":"Fresh Juice","cat_display_name":"Fresh Juice","cat_logo":"http://pos.zamy.in/uploads/FoodCategory/"},{"cat_id":"9","cat_name":"Soups","cat_display_name":"Soups","cat_logo":"http://pos.zamy.in/uploads/FoodCategory/"},{"cat_id":"10","cat_name":"Salad","cat_display_name":"Salad","cat_logo":"http://pos.zamy.in/uploads/FoodCategory/"},{"cat_id":"11","cat_name":"Raita","cat_display_name":"Raita","cat_logo":"http://pos.zamy.in/uploads/FoodCategory/"},{"cat_id":"12","cat_name":"Papad","cat_display_name":"Papad","cat_logo":"http://pos.zamy.in/uploads/FoodCategory/"},{"cat_id":"13","cat_name":"Veg. Dry","cat_display_name":"Veg. Dry","cat_logo":"http://pos.zamy.in/uploads/FoodCategory/"},{"cat_id":"14","cat_name":"Murg. Dry","cat_display_name":"Murg. Dry","cat_logo":"http://pos.zamy.in/uploads/FoodCategory/"}],"popular_this_month":[{"res_name":"Healthy Restaurant","kitchen_id":"10","images":"http://pos.zamy.in/uploads/FoodMenu/3272dd8077416d31202df1d431f42c8c.jpg","landmark":"Makarba","area":"Makarba","menu_name":"Full Veg. Thali","short_code":"Full Veg. Thali","price":"100.00","product_variation":[]},{"res_name":"Hop Meal Restaurant","kitchen_id":"3","images":"http://pos.zamy.in/uploads/FoodMenu/BUTTERMILK.jpg","landmark":"Prahlad Nagar","area":"Makarba","menu_name":" Buttermilk","short_code":" Buttermilk","price":"30.00","product_variation":[]},{"res_name":"Hop Meal Restaurant","kitchen_id":"3","images":"http://pos.zamy.in/uploads/FoodMenu/Veg_Thali-min.JPG","landmark":"Prahlad Nagar","area":"Makarba","menu_name":"Fix Lunch \u2013 Veg","short_code":"Fix Lunch \u2013 Veg","price":"130.00","product_variation":[]},{"res_name":"Bun Kabab","kitchen_id":"9","images":"http://pos.zamy.in/uploads/FoodMenu/bc978645130c75fef36075b1ffe4d5c9.jpeg","landmark":"makrba","area":"Makarba","menu_name":"Mutton Kabab Indian","short_code":"Mutton Kabab Indian","price":"160-190","product_variation":[{"variation_id":"18","variation_name":"Cheese","variation_price":"175","variation_sale_price":"175","tax_price":175,"cart_variation_id":"no"},{"variation_id":"19","variation_name":"Double Cheese","variation_price":"190","variation_sale_price":"190","tax_price":190,"cart_variation_id":"no"},{"variation_id":"25","variation_name":"Regular","variation_price":"160","variation_sale_price":"160","tax_price":160,"cart_variation_id":"no"}]},{"res_name":"Hop Meal Restaurant","kitchen_id":"3","images":"http://pos.zamy.in/uploads/FoodMenu/Non_Veg_Thali-min8.JPG","landmark":"Prahlad Nagar","area":"Makarba","menu_name":"Fix Lunch \u2013 Non Veg","short_code":"Fix Lunch \u2013 Non Veg","price":"180.00","product_variation":[]},{"res_name":"Hop Meal Restaurant","kitchen_id":"3","images":"http://pos.zamy.in/uploads/FoodMenu/PLAIN_LASSI_SALTY.jpg","landmark":"Prahlad Nagar","area":"Makarba","menu_name":" Plain Lassi Salty","short_code":" Plain Lassi Salty","price":"70.00","product_variation":[]},{"res_name":"Hop Meal Restaurant","kitchen_id":"3","images":"http://pos.zamy.in/uploads/FoodMenu/DRYFRUIT_KHAJANA_LASSI.jpg","landmark":"Prahlad Nagar","area":"Makarba","menu_name":"Dryfruit Khajana Lassi","short_code":"Dryfruit Khajana Lassi","price":"140.00","product_variation":[]},{"res_name":"Bun Kabab","kitchen_id":"9","images":"http://pos.zamy.in/uploads/FoodMenu/ae39bc7d38679897b1fed43f759d8cb5.jpg","landmark":"makrba","area":"Makarba","menu_name":"Chicken Kabab Mexican","short_code":"Chicken Kabab Mexican","price":"150-180","product_variation":[{"variation_id":"18","variation_name":"Cheese","variation_price":"165","variation_sale_price":"165","tax_price":165,"cart_variation_id":"no"},{"variation_id":"19","variation_name":"Double Cheese","variation_price":"180","variation_sale_price":"180","tax_price":180,"cart_variation_id":"no"},{"variation_id":"25","variation_name":"Regular","variation_price":"150","variation_sale_price":"150","tax_price":150,"cart_variation_id":"no"}]},{"res_name":"Bun Kabab","kitchen_id":"9","images":"http://pos.zamy.in/uploads/FoodMenu/b247ebeb83efd3840e8659112809de3d.jpg","landmark":"makrba","area":"Makarba","menu_name":"Mutton Kabab Mexican","short_code":"Mutton Kabab Mexican","price":"175-205","product_variation":[{"variation_id":"18","variation_name":"Cheese","variation_price":"190","variation_sale_price":"190","tax_price":190,"cart_variation_id":"no"},{"variation_id":"19","variation_name":"Double Cheese","variation_price":"205","variation_sale_price":"205","tax_price":205,"cart_variation_id":"no"},{"variation_id":"25","variation_name":"Regular","variation_price":"175","variation_sale_price":"175","tax_price":175,"cart_variation_id":"no"}]},{"res_name":"Kitchen Badshah","kitchen_id":"20","images":"http://pos.zamy.in/uploads/FoodMenu/IMG_20191031_1707121.jpg","landmark":"Near Qadri Party Plot","area":"Sarkhej","menu_name":"Chapati","short_code":"Chapati","price":"5.00","product_variation":[]}]}
     */
    var status = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        val cart_count = 0
        var top_restaurant_list: List<TopRestaurantListBean>? =
            null
        var fav_food_list: List<FavFoodListBean>? =
            null
        var category_list: List<CategoryListBean>? =
            null
        var popular_this_month: List<PopularThisMonthBean>? =
            null
        var coupon_list: List<CouponListBean>? = null

        class TopRestaurantListBean {
            /**
             * id : 3
             * res_name : Hop Meal Restaurant
             * res_alias : hop-meal-restaurant-makarba-ahmedabad-380051
             * logo : http://pos.zamy.in/uploads/kitchen/logo/2d6cb3c59baf576d3f101b376800a4b6.png
             * images : http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg
             * landmark : Prahlad Nagar
             * area : Makarba
             * approx_delivery_time : 52 MINS
             * approx_cost : ₹500 FOR TWO
             * service_type : Indian, Chinese, Fast Food
             * total_orders : 259
             * total_restaurant_orders : 259
             */
            var id: String? = null
            var res_name: String? = null
            var res_alias: String? = null
            var logo: String? = null
            var images: String? = null
            var landmark: String? = null
            var area: String? = null
            var approx_delivery_time: String? = null
            var approx_cost: String? = null
            var service_type: String? = null
            var total_orders: String? = null
            var total_restaurant_orders: String? = null

        }

        class FavFoodListBean {
            /**
             * menu_name : BBQ COMBO 2
             * short_code : BBQ COMBO 2
             * order_type : 1,2,3
             * menu_logo : http://pos.zamy.in/uploads/FoodMenu/BBQ_COMBO_2.jpg
             * price : null
             * reduced_price : null
             * long_description :
             * restaurant_id : 3
             * res_name : Hop Meal Restaurant
             * res_alias : hop-meal-restaurant-makarba-ahmedabad-380051
             * kitchen_image : http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg
             * address : Sarkhej, Ahmedabad, Gujarat, India
             * country : 1
             * state : 1
             * city : 2
             * approx_cost : ₹500 FOR TWO
             * pure_veg : 0
             * approx_delivery_time : 52 MINS
             * service_type : Indian, Chinese, Fast Food
             */
            var menu_name: String? = null
            var short_code: String? = null
            var order_type: String? = null
            var menu_logo: String? = null
            var price: String? = null
            var reduced_price: String? = null
            var long_description: String? = null
            var restaurant_id: String? = null
            var res_name: String? = null
            var res_alias: String? = null
            var kitchen_image: String? = null
            var address: String? = null
            var country: String? = null
            var state: String? = null
            var city: String? = null
            var approx_cost: String? = null
            var pure_veg: String? = null
            var approx_delivery_time: String? = null
            var service_type: String? = null

        }

        class CategoryListBean {
            /**
             * cat_id : 5
             * cat_name : OFFER-490
             * cat_display_name : OFFER-490
             * cat_logo : http://pos.zamy.in/uploads/FoodCategory/
             */
            var cat_id: String? = null
            var cat_name: String? = null
            var cat_display_name: String? = null
            var cat_logo: String? = null

        }

        class PopularThisMonthBean {
            /**
             * var foodmenu_id: String? = null
             * res_name : Healthy Restaurant
             * kitchen_id : 10
             * images : http://pos.zamy.in/uploads/FoodMenu/3272dd8077416d31202df1d431f42c8c.jpg
             * landmark : Makarba
             * area : Makarba
             * menu_name : Full Veg. Thali
             * short_code : Full Veg. Thali
             * price : 100.00
             *restaurant_online_status: "1"
             * product_variation : []
             */
            var cart: String? = null
            var food_menu_id: String? = null
            var res_name: String? = null
            var kitchen_id: String? = null
            var images: String? = null
            var landmark: String? = null
            var area: String? = null
            var restaurant_online_status: String? = null
            var menu_name: String? = null
            var short_code: String? = null
            var price: String? = null
            var product_variation: List<ProductVariationBean>? = null

            class ProductVariationBean {
                /**
                 * variation_id : 1
                 * variation_name : Regular
                 * variation_price : 1200
                 * variation_sale_price : 1190
                 */

                var variation_id: String? = null
                var variation_name: String? = null
                var variation_price: String? = null
                var variation_sale_price: String? = null
                var selected: Boolean = false
                var tax_price: String? = null
            }
        }

        class CouponListBean {
            /**
             * id : 6
             * title : OFF100
             * coupon_code : OFF100
             * description :
             * coupon_image : https://zamy.in/pos.zamy.in/uploads/coupon_image/fc547cb01f59d15a39c4003727151bea.png
             * min_spend : 0
             * max_spend : 0
             * usage_limit_per_coupon : 0
             * usage_limit_per_user : 1
             * discount : 100
             * dis_type : fix
             * start_date : 2019-11-12
             * end_date : 2019-12-31
             * get_usage_limit : 0
             * get_usage_limit_per_user : 0
             * restaurant : 3,9
             */
            var id: String? = null
            var title: String? = null
            var coupon_code: String? = null
            var description: String? = null
            var coupon_image: String? = null
            var min_spend: String? = null
            var max_spend: String? = null
            var usage_limit_per_coupon: String? = null
            var usage_limit_per_user: String? = null
            var discount: String? = null
            var dis_type: String? = null
            var start_date: String? = null
            var end_date: String? = null
            var get_usage_limit: String? = null
            var get_usage_limit_per_user: String? = null
            var restaurant: String? = null

        }
    }
}
