package com.attune.zamy.model

class CheckVersionModel {
    /**
     * success : 1
     * message :
     * data : {"version":"1.0"}
     */
    var status = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * version : 1.0
         */
        var version: String? = null
        var id: Int = 0
        var android: String? = null
        var ios: String? = null

    }
}