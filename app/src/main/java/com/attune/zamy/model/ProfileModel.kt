package com.attune.zamy.model

class ProfileModel {

    /**
     * status : 1
     * message : Get user profile successfully.
     * data : {"id":"4","phone":"9510805727","name":"ruchi shahu","email":"ruchi.shahu@attuneww.com","password":"$2y$10$GeAHBmSZ1U25WK/QkS/Ny.qLkXqWfGr96jSFv3UQzffhuvMe0PJgm","location":"","country":"","state":"","city":"","profile":"","status":"1","created_date":"2019-10-04 02:02:01"}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {

        /**
         * id : 4
         * phone : 9510805727
         * name : ruchi shahu
         * profile_pic: "http:\/\/pos.zamy.in\/uploads\/users\/profile\/",
         * email : ruchi.shahu@attuneww.com
         * password : $2y$10$GeAHBmSZ1U25WK/QkS/Ny.qLkXqWfGr96jSFv3UQzffhuvMe0PJgm
         * location :
         * country :
         * state :
         * city :
         * profile :
         * status : 1
         * created_date : 2019-10-04 02:02:01
         */

        var id: String? = null
        var phone: String? = null
        var profile_pic: String? = null
        var name: String? = null
        var email: String? = null
        var password: String? = null
        var location: String? = null
        var country: String? = null
        var state: String? = null
        var city: String? = null
        var profile: String? = null
        var status: String? = null
        var created_date: String? = null
        var refer_by_code : String? = null

    }
}
