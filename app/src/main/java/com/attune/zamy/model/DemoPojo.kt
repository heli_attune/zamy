package com.attune.zamy.model

class DemoPojo {

    /**
     * status : 1
     * message :
     * data : {"total_rows":4,"total_pages":1,"list":[{"id":"3","res_name":"Hop Meal Restaurant","images":"http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg","logo":"http://pos.zamy.in/uploads/kitchen/logo/2d6cb3c59baf576d3f101b376800a4b6.png","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","latitude":"23","longitude":"72","contact_information":"23562","pure_veg":"0"},{"id":"10","res_name":"Healthy Restaurant","images":"http://pos.zamy.in/uploads/kitchen/images/e3e69325f34f2f7f7600c41b0d632364.jpg","logo":"http://pos.zamy.in/uploads/kitchen/logo/4de066608e5d7e80b8bcc5a8600714f7.png","service_type":"","address":"K-27, 28, Al Burooj Tower, 132 ft. Makarba Road Makarba, Ahmedabad - 380055","area":"Makarba","landmark":"Makarba","zipcode":"380055","latitude":"23","longitude":"73","contact_information":"2147483647","pure_veg":"0"},{"id":"16","res_name":"Kitchen1","images":"http://pos.zamy.in/uploads/kitchen/images/","logo":"http://pos.zamy.in/uploads/kitchen/logo/","service_type":"Indian, Chinese, Fast Food","address":"45/Mahavirnagar Society, Opp-Divyaprabha aprtment, Beside- Sigyan high school\r\nViratnagar road, Odhav","area":"Makarba","landmark":"Kataria","zipcode":"382415","latitude":"23","longitude":"73","contact_information":"2147483647","pure_veg":"0"},{"id":"17","res_name":"Fish Express","images":"http://pos.zamy.in/uploads/kitchen/images/fe093b27f275b38a79122d39a8b579e0.png","logo":"http://pos.zamy.in/uploads/kitchen/logo/52c6d764f12cfd5358a372d9b3970535.png","service_type":"North Indian, Seafood","address":"Near Amber Tower, Opposite Bank of India, Juhapura, Sarkhej, Ahmedabad","area":"Sarkhej","landmark":"Near Amber Tower","zipcode":"380055","latitude":"23","longitude":"72","contact_information":"99786","pure_veg":"0"}]}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * total_rows : 4
         * total_pages : 1
         * list : [{"id":"3","res_name":"Hop Meal Restaurant","images":"http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg","logo":"http://pos.zamy.in/uploads/kitchen/logo/2d6cb3c59baf576d3f101b376800a4b6.png","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","latitude":"23","longitude":"72","contact_information":"23562","pure_veg":"0"},{"id":"10","res_name":"Healthy Restaurant","images":"http://pos.zamy.in/uploads/kitchen/images/e3e69325f34f2f7f7600c41b0d632364.jpg","logo":"http://pos.zamy.in/uploads/kitchen/logo/4de066608e5d7e80b8bcc5a8600714f7.png","service_type":"","address":"K-27, 28, Al Burooj Tower, 132 ft. Makarba Road Makarba, Ahmedabad - 380055","area":"Makarba","landmark":"Makarba","zipcode":"380055","latitude":"23","longitude":"73","contact_information":"2147483647","pure_veg":"0"},{"id":"16","res_name":"Kitchen1","images":"http://pos.zamy.in/uploads/kitchen/images/","logo":"http://pos.zamy.in/uploads/kitchen/logo/","service_type":"Indian, Chinese, Fast Food","address":"45/Mahavirnagar Society, Opp-Divyaprabha aprtment, Beside- Sigyan high school\r\nViratnagar road, Odhav","area":"Makarba","landmark":"Kataria","zipcode":"382415","latitude":"23","longitude":"73","contact_information":"2147483647","pure_veg":"0"},{"id":"17","res_name":"Fish Express","images":"http://pos.zamy.in/uploads/kitchen/images/fe093b27f275b38a79122d39a8b579e0.png","logo":"http://pos.zamy.in/uploads/kitchen/logo/52c6d764f12cfd5358a372d9b3970535.png","service_type":"North Indian, Seafood","address":"Near Amber Tower, Opposite Bank of India, Juhapura, Sarkhej, Ahmedabad","area":"Sarkhej","landmark":"Near Amber Tower","zipcode":"380055","latitude":"23","longitude":"72","contact_information":"99786","pure_veg":"0"}]
         */

        var total_rows: Int = 0
        var total_pages: Int = 0
        var list: List<ListBean>? = null

        class ListBean {
            /**
             * id : 3
             * res_name : Hop Meal Restaurant
             * images : http://pos.zamy.in/uploads/kitchen/images/2e677408b2573d10982c42ae9ffbbd39.jpg
             * logo : http://pos.zamy.in/uploads/kitchen/logo/2d6cb3c59baf576d3f101b376800a4b6.png
             * service_type : Indian, Chinese, Fast Food
             * address : Sarkhej, Ahmedabad, Gujarat, India
             * area : Makarba
             * landmark : Prahlad Nagar
             * zipcode : 380051
             * latitude : 23
             * longitude : 72
             * contact_information : 23562
             * pure_veg : 0
             */

            var id: String? = null
            var res_name: String? = null
            var images: String? = null
            var logo: String? = null
            var service_type: String? = null
            var address: String? = null
            var area: String? = null
            var landmark: String? = null
            var zipcode: String? = null
            var latitude: String? = null
            var longitude: String? = null
            var contact_information: String? = null
            var pure_veg: String? = null
        }
    }
}
