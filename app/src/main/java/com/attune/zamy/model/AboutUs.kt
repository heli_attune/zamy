package com.attune.zamy.model

class AboutUs {
    /**
     * status : 1
     * message : Get about us.
     * data : {"id":"1","title_1":"STAY HOME STAY SAFE","desc_1":"For customer, Zamy is your local online ordering and delivery partner. We will take care of all your outside food, grocery, medicine & other pickup/drop needs. We will do pickup from your favorte store and do safe delivery at your door steps. We will give you more discount and benefits from our business partners to maker our relationship win-win for everyone.","img_1":"safe.jpg","title_2":"Become Digital","desc_2":"For business partner, Zamy will take care of all your online ordering, delivery and digital marketing need. We will setup your online store for online ordering and do safe product delivery to your customer doorsteps.  We will do Digital Marketing of your business and products to bring more business from our digital channels. As a part of Digital Marketing, we will do Content creation, Content publishing, Content promotion, Social Media marketing, SMS marketing, On-site marketing, Visual marketing and product promotion to bring more customer and to make win-win business relationship.","img_2":"register.jpg","title_3":" Be a Boss","desc_3":"For delivery partner, Zamy will provide platform to become a boss of your own time and start earning when ever you want. Zamy will give you complete freedom and flexibility, You can select your location and time to earn while you are doing delivery of your order. Become part of our growing network where everyone can win. \r\nDelivery Partner Registration for Free","img_3":"delivery.jpg","link_1":"dfsdf","link_2":"dfsdf","link_3":"dfsdf"}
     */
    var status = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * id : 1
         * title_1 : STAY HOME STAY SAFE
         * desc_1 : For customer, Zamy is your local online ordering and delivery partner. We will take care of all your outside food, grocery, medicine & other pickup/drop needs. We will do pickup from your favorte store and do safe delivery at your door steps. We will give you more discount and benefits from our business partners to maker our relationship win-win for everyone.
         * img_1 : safe.jpg
         * title_2 : Become Digital
         * desc_2 : For business partner, Zamy will take care of all your online ordering, delivery and digital marketing need. We will setup your online store for online ordering and do safe product delivery to your customer doorsteps.  We will do Digital Marketing of your business and products to bring more business from our digital channels. As a part of Digital Marketing, we will do Content creation, Content publishing, Content promotion, Social Media marketing, SMS marketing, On-site marketing, Visual marketing and product promotion to bring more customer and to make win-win business relationship.
         * img_2 : register.jpg
         * title_3 :  Be a Boss
         * desc_3 : For delivery partner, Zamy will provide platform to become a boss of your own time and start earning when ever you want. Zamy will give you complete freedom and flexibility, You can select your location and time to earn while you are doing delivery of your order. Become part of our growing network where everyone can win.
         * Delivery Partner Registration for Free
         * img_3 : delivery.jpg
         * link_1 : dfsdf
         * link_2 : dfsdf
         * link_3 : dfsdf
         */
        var id: String? = null
        var title_1: String? = null
        var desc_1: String? = null
        var img_1: String? = null
        var title_2: String? = null
        var desc_2: String? = null
        var img_2: String? = null
        var title_3: String? = null
        var desc_3: String? = null
        var img_3: String? = null
        var image_url: String? = null
        var link_1: String? = null
        var link_2: String? = null
        var link_3: String? = null

    }
}