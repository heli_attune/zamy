package com.attune.zamy.model

class StoreModel {

    /**
    {
    "status": 1,
    "message": "Get store successfully.",
    "data": [{"id": "54","res_name": "Al Baik","status": "1"},{"id": "19","res_name": "Bombay Biryani","status": "1"},{"id": "79","res_name": "Burhanpur Jalebi","status": "1"},{"id": "56","res_name": "Easy Healthcare","status": "1"},{"id": "57","res_name": "HOP Grocery","status": "1"},{"id": "3","res_name": "Hop Meal Restaurant","status": "1"},{"id": "77","res_name": "Khamosh Vegetable","status": "1"},{"id": "20","res_name": "Kitchen Badshah","status": "1"},{"id": "75","res_name": "N G Grocery","status": "1"},{"id": "26","res_name": "Saniyaben Khakhrawala","status": "1"},{"id": "58","res_name": "Sehat Food","status": "1"},{"id": "80","res_name": "Tasty Point","status": "1"}]}
     */

    var status: Int = 0
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
        "id": "80",
        "res_name": "Tasty Point",
        "status": "1"
         */

        var id: String? = null
        var res_name: String? = null
        var status: String? = null

    }
}
