package com.attune.zamy.model

class TicketListingConversationModel {

    /**
     * status : 1
     * message : Get All Conversation .
     * data : [{"ticketID":"1","ticketChatID":"72","ticketSenderID":"3","ticketReceiverID":"1","order_id":"72","ticketTitle":"3","subject":"ddsl","ticketBody":"SSLKS","ticketAttachment":"","ticketDateTime":"2019-11-01 02:28:46","status":"1","ticketstatus":"close"},{"ticketID":"4","ticketChatID":"72","ticketSenderID":"1","ticketReceiverID":"3","order_id":"72","ticketTitle":"3","subject":"ddsl","ticketBody":"damdskmadmamamdm","ticketAttachment":"","ticketDateTime":"2019-11-01 02:28:46","status":"1","ticketstatus":"close"},{"ticketID":"6","ticketChatID":"72","ticketSenderID":"3","ticketReceiverID":"1","order_id":"72","ticketTitle":"3","subject":"ddsl","ticketBody":"ok","ticketAttachment":"","ticketDateTime":"2019-11-01 02:28:46","status":"1","ticketstatus":"close"}]
     */

    var status: Int = 0
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * ticketID : 1
         * ticketChatID : 72
         * ticketSenderID : 3
         * ticketReceiverID : 1
         * order_id : 72
         * ticketTitle : 3
         * subject : ddsl
         * ticketBody : SSLKS
         * ticketAttachment :
         * ticketDateTime : 2019-11-01 02:28:46
         * status : 1
         * ticketstatus : close
         */

        var ticketID: String? = null
        var ticketChatID: String? = null
        var ticketSenderID: String? = null
        var ticketReceiverID: String? = null
        var order_id: String? = null
        var ticketTitle: String? = null
        var subject: String? = null
        var ticketBody: String? = null
        var ticketAttachment: String? = null
        var ticketDateTime: String? = null
        var status: String? = null
        var ticketstatus: String? = null
    }
}
