package com.attune.zamy.model

class UserModel {

    /**
     * status : true
     * message : User login successful.
     * data : {"id":"5","phone":"8320701963","name":"john","email":"mustaq@mailinator.com","password":"$2y$10$5YX/sbH8x/ra9.A8.sF2WucIqCsuLZIzbyKGoy6h8YuVdIcjSFey.","location":"","status":"1","created_date":"2019-10-04 02:21:26"}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * id : 5
         * phone : 8320701963
         * name : john
         * email : mustaq@mailinator.com
         * password : $2y$10$5YX/sbH8x/ra9.A8.sF2WucIqCsuLZIzbyKGoy6h8YuVdIcjSFey.
         * location :
         * status : 1
         * created_date : 2019-10-04 02:21:26
         */

        var id: String? = null
        var phone: String? = null
        var name: String? = null
        var email: String? = null
        var password: String? = null
        var location: String? = null
        var status: String? = null
        var created_date: String? = null
        var fire_token: String? = null
    }
}
