package com.attune.zamy.model

class CustomerIssueModel {

    /**
     * status : 1
     * message : Get issue successfully.
     * data : [{"id":"1","title":"Payment Issue","status":"1"},{"id":"2","title":"Food Issue","status":"1"},{"id":"3","title":"Delivery Issue","status":"1"},{"id":"4","title":"Other","status":"1"}]
     */

    var status: Int = 0
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * id : 1
         * title : Payment Issue
         * status : 1
         */

        var id: String? = null
        var title: String? = null
        var status: String? = null
    }
}
