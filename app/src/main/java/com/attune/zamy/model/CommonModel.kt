package com.attune.zamy.model

class CommonModel {


    /**
     * success : 0
     * message : This email is already in use.Please try a different one!
     */

    var success: Int ?=0
    var status:Int ?=0
    var message: String? = null
    var data: Any? = null
}
