package com.attune.zamy.model

class CalculateDistanceModel {
    /**
    "status": 1,
    "message": "Get distance successfully.",
    "data": {"latitudeFrom": 22.9958968,"longitudeFrom": 72.5173455,"latitudeTo": 23.0267556,"longitudeTo": 72.6008286,"amount": 92.1,"distance": 9.21}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
        "latitudeFrom": 22.9958968,
        "longitudeFrom": 72.5173455,
        "latitudeTo": 23.0267556,
        "longitudeTo": 72.6008286,
        "amount": 92.1,
        "distance": 9.21
         */

        var latitudeFrom: String? = null
        var longitudeFrom: String? = null
        var latitudeTo: String? = null
        var longitudeTo: String? = null
        var amount: String? = null
        var distance: String? = null

    }
}
