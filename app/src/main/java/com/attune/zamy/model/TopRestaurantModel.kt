package com.attune.zamy.model

class TopRestaurantModel {

    /**
     * status : 1
     * message : success
     * data : {"total_rows":4,"total_pages":1,"top_restaurant_list":[{"id":"2","res_name":"Hop Meal Cafe","total_payable":null,"type":"2","email":"attune_HO @gmail.com","images":"http://192.168.15.23/hopmeal_pos/uploads/kitchen/images/","logo":"","address":"45454","area":"ahm","landmark":"ahm","zipcode":"45345345","country":"1","state":"4","city":"7","currency_format":"2"},{"id":"8","res_name":"Prabhu Parlour","total_payable":null,"type":"2","email":"rahul.satvara@attuneww.com","images":"http://192.168.15.23/hopmeal_pos/uploads/kitchen/images/","logo":"","address":"Sahibaug, Ahmedabad, Gujarat, India","area":"Sahibaugh","landmark":"NA","zipcode":"380016","country":"1","state":"1","city":"2","currency_format":"2"},{"id":"7","res_name":"HopMeal Cafe","total_payable":null,"type":"1","email":"test@test.com","images":"http://192.168.15.23/hopmeal_pos/uploads/kitchen/images/women-cat.jpg","logo":"","address":"makarba","area":"makarba","landmark":"","zipcode":"380001","country":"1","state":"1","city":"2","currency_format":"3"},{"id":"3","res_name":"Hop Meal Restaurant","total_payable":null,"type":"1","email":"hopmeal12@gmial.com","images":"http://192.168.15.23/hopmeal_pos/uploads/kitchen/images/logo-300x83.png","logo":"fact-icon13.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Satellite","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","currency_format":"2"}]}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * total_rows : 4
         * total_pages : 1
         * top_restaurant_list : [{"id":"2","res_name":"Hop Meal Cafe","total_payable":null,"type":"2","email":"attune_HO @gmail.com","images":"http://192.168.15.23/hopmeal_pos/uploads/kitchen/images/","logo":"","address":"45454","area":"ahm","landmark":"ahm","zipcode":"45345345","country":"1","state":"4","city":"7","currency_format":"2"},{"id":"8","res_name":"Prabhu Parlour","total_payable":null,"type":"2","email":"rahul.satvara@attuneww.com","images":"http://192.168.15.23/hopmeal_pos/uploads/kitchen/images/","logo":"","address":"Sahibaug, Ahmedabad, Gujarat, India","area":"Sahibaugh","landmark":"NA","zipcode":"380016","country":"1","state":"1","city":"2","currency_format":"2"},{"id":"7","res_name":"HopMeal Cafe","total_payable":null,"type":"1","email":"test@test.com","images":"http://192.168.15.23/hopmeal_pos/uploads/kitchen/images/women-cat.jpg","logo":"","address":"makarba","area":"makarba","landmark":"","zipcode":"380001","country":"1","state":"1","city":"2","currency_format":"3"},{"id":"3","res_name":"Hop Meal Restaurant","total_payable":null,"type":"1","email":"hopmeal12@gmial.com","images":"http://192.168.15.23/hopmeal_pos/uploads/kitchen/images/logo-300x83.png","logo":"fact-icon13.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Satellite","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","currency_format":"2"}]
         */

        var total_rows: Int = 0
        var total_pages: Int = 0
        var top_restaurant_list: List<TopRestaurantListBean>? = null

        class TopRestaurantListBean {
            /**
             * id : 2
             * res_name : Hop Meal Cafe
             * total_payable : null
             * type : 2
             * email : attune_HO @gmail.com
             * images : http://192.168.15.23/hopmeal_pos/uploads/kitchen/images/
             * logo :
             * address : 45454
             * area : ahm
             * landmark : ahm
             * zipcode : 45345345
             * country : 1
             * state : 4
             * city : 7
             * currency_format : 2
             */

            var id: String? = null
            var res_name: String? = null
            var total_payable: Any? = null
            var type: String? = null
            var email: String? = null
            var images: String? = null
            var logo: String? = null
            var address: String? = null
            var area: String? = null
            var landmark: String? = null
            var zipcode: String? = null
            var country: String? = null
            var state: String? = null
            var city: String? = null
            var currency_format: String? = null
            var service_type: String? = null
            var favourite_flag: String? = null
            var approx_delivery_time: String? = null
            var total_fav_count: String? = null
        }
    }
}
