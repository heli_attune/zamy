package com.attune.zamy.model

class CustomerSupportList {

    /**
     * status : 1
     * message : Get customer_support_ticket Details.
     * data : [{"ticketID":"1","ticketChatID":"72","ticketSenderID":"3","ticketReceiverID":"1","order_id":"72","ticketTitle":"3","subject":"ddsl","ticketBody":"SSLKS","ticketAttachment":"","ticketDateTime":"2019-11-01 02:28:46","status":"1","ticketstatus":"close"},{"ticketID":"2","ticketChatID":"50","ticketSenderID":"3","ticketReceiverID":"1","order_id":"50","ticketTitle":"3","subject":"slksam","ticketBody":"become late come","ticketAttachment":"","ticketDateTime":"2019-10-25 01:56:22","status":"1","ticketstatus":"open"},{"ticketID":"3","ticketChatID":"62","ticketSenderID":"3","ticketReceiverID":"1","order_id":"62","ticketTitle":"3","subject":"SLLLL","ticketBody":"ssdd65","ticketAttachment":"","ticketDateTime":"2019-10-25 02:54:34","status":"1","ticketstatus":"open"},{"ticketID":"6","ticketChatID":"72","ticketSenderID":"3","ticketReceiverID":"1","order_id":"72","ticketTitle":"3","subject":"ddsl","ticketBody":"ok","ticketAttachment":"","ticketDateTime":"2019-11-01 02:28:46","status":"1","ticketstatus":"close"}]
     */

    var status: Int = 0
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * ticketID : 1
         * ticketChatID : 72
         * ticketSenderID : 3
         * ticketReceiverID : 1
         * order_id : 72
         * ticketTitle : 3
         * subject : ddsl
         * ticketBody : SSLKS
         * ticketAttachment :
         * ticketDateTime : 2019-11-01 02:28:46
         * status : 1
         * ticketstatus : close
         */

        var ticketID: String? = null
        var ticketChatID: String? = null
        var ticketSenderID: String? = null
        var ticketReceiverID: String? = null
        var order_id: String? = null
        var ticketTitle: String? = null
        var subject: String? = null
        var ticketBody: String? = null
        var ticketAttachment: String? = null
        var ticketDateTime: String? = null
        var status: String? = null
        var ticketstatus: String? = null
    }
}
