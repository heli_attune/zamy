package com.attune.zamy.model

import java.io.Serializable

class GetAddressModel :Serializable{

    /**
     * status : 1
     * message : Get address book successfully.
     * data : {"address_book":[{"id":"10","user_id":"5","address_type":"home","other_address_type":"","name":"abc1","email":"abc1@gmail.com","phone":"2345234341","shipping_area":"ahmedabad","address_1":"ahmedabad","address_2":"ahmedabad","country":"india","state":"gujarat","city":"ahmedabad","postcode":"111111","address_lat":"22.9977913","address_lng":"72.5086396","status":"1","created_date":"2019-10-23 22:28:47","updated_date":"2019-10-24 05:28:47"},{"id":"11","user_id":"5","address_type":"Other","other_address_type":"","name":"","email":"","phone":"","shipping_area":"","address_1":"","address_2":"","country":"","state":"","city":"","postcode":"0","address_lat":"0","address_lng":"0","status":"1","created_date":"2019-10-23 22:29:37","updated_date":"2019-10-24 05:29:37"},{"id":"12","user_id":"5","address_type":"Other","other_address_type":"","name":"","email":"","phone":"","shipping_area":"","address_1":"","address_2":"","country":"","state":"","city":"","postcode":"0","address_lat":"0","address_lng":"0","status":"1","created_date":"2019-10-23 22:29:37","updated_date":"2019-10-24 05:29:37"}]}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean :Serializable {
        var address_book: MutableList<AddressBookBean>? = null

        class AddressBookBean :Serializable{
            /**
             * id : 10
             * user_id : 5
             * address_type : home
             * other_address_type :
             * name : abc1
             * email : abc1@gmail.com
             * phone : 2345234341
             * shipping_area : ahmedabad
             * address_1 : ahmedabad
             * address_2 : ahmedabad
             * country : india
             * state : gujarat
             * city : ahmedabad
             * postcode : 111111
             * address_lat : 22.9977913
             * address_lng : 72.5086396
             * status : 1
             * created_date : 2019-10-23 22:28:47
             * updated_date : 2019-10-24 05:28:47
             */

            var id: String? = null
            var user_id: String? = null
            var address_type: String? = null
            var other_address_type: String? = null
            var name: String? = null
            var email: String? = null
            var phone: String? = null
            var shipping_area: String? = null
            var address_1: String? = null
            var address_2: String? = null
            var country: String? = null
            var state: String? = null
            var city: String? = null
            var postcode: String? = null
            var address_lat: String? = null
            var address_lng: String? = null
            var status: String? = null
            var created_date: String? = null
            var updated_date: String? = null
        }
    }
}
