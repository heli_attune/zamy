package com.attune.zamy.model

class DeliveryResponseModel
{
    /**
    {
    "status": 1,
    "message": "Order received.",
    "data": 48

    {
    "status": 1,
    "message": "Order received.",
    "data": {
    "order_id": 51,
    "paytm_link": "https://zamy.in/paytm/payby_pickup_drop_paytm?ORDER_ID=51&CUST_ID=4&TXN_AMOUNT="
    }
    }
    }
     */

    private var status: Int = 0
    private var message: String? = null
    private var data: DeliveryResponseModel.DataBean? = null

    fun getStatus(): Int {
        return status
    }

    fun setStatus(status: Int) {
        this.status = status
    }

    fun getMessage(): String? {
        return message
    }

    fun setMessage(message: String?) {
        this.message = message
    }

    fun getData(): DeliveryResponseModel.DataBean? {
        return data
    }

    fun setData(data: DeliveryResponseModel.DataBean?) {
        this.data = data
    }

    public class DataBean {
        /**
         * order_id : 89
         * user_id : 5
         * order_total : 875
         * paytm_link : http://client.attuneinfocom.com/PaytmPaymentGateway/pgRedirect.php
         */
        /*   private int order_id;
        private String user_id;
        private int order_total;*/
        private var paytm_link: String? = null
        private var order_id = 0
        fun getOrder_id(): Int {
            return order_id
        }

        fun setOrder_id(order_id: Int) {
            this.order_id = order_id
        }

        fun getPaytm_link(): String? {
            return paytm_link
        }

        fun setPaytm_link(paytm_link: String?) {
            this.paytm_link = paytm_link
        }
    }
}