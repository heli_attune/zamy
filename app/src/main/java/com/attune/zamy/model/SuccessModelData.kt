package com.attune.zamy.model

class SuccessModelData {
    var success: Int = 0
    var message: String? = null
    var data: ArrayList<DataBean>? = null

    class DataBean {

    }
}
