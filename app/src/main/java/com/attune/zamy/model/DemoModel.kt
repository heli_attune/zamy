package com.attune.zamy.model

class DemoModel {


    /**
     * status : 1
     * message : success
     * data : {"top_restaurant_list":[{"id":"3","res_name":"Hop Meal Restaurant","total_payable":null,"type":"1","email":"hopmeal12@gmial.com","images":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","logo":"fact-icon13.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Satellite","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","approx_delivery_time":"52 MINS","currency_format":"2","service_type":"Indian, Chinese, Fast Food","favourite_flag":"yes"},{"id":"9","res_name":"Bun Kabab","total_payable":null,"type":"2","email":"info@bunkabab.in","images":"http://zamy.in/uploads/kitchen/images/","logo":"","address":"K-27, 28, Al Burooj Tower, 132 ft. Makarba Road Makarba, Ahmedabad - 380055","area":"makrba","landmark":"makrba","zipcode":"380055","country":"1","state":"1","city":"2","approx_delivery_time":"","currency_format":"2","service_type":"","favourite_flag":"yes"},{"id":"7","res_name":"HopMeal Cafe","total_payable":null,"type":"1","email":"test@test.com","images":"http://zamy.in/uploads/kitchen/images/women-cat.jpg","logo":"","address":"makarba","area":"makarba","landmark":"","zipcode":"380001","country":"1","state":"1","city":"2","approx_delivery_time":"42 MINS","currency_format":"3","service_type":"Indian, Chinese, Fast Food","favourite_flag":"yes"},{"id":"8","res_name":"Prabhu Parlour","total_payable":null,"type":"2","email":"rahul.satvara@attuneww.com","images":"http://zamy.in/uploads/kitchen/images/","logo":"","address":"Sahibaug, Ahmedabad, Gujarat, India","area":"Sahibaugh","landmark":"NA","zipcode":"380016","country":"1","state":"1","city":"2","approx_delivery_time":"NA","currency_format":"2","service_type":"RES","favourite_flag":"yes"},{"id":"2","res_name":"Hop Meal Cafe","total_payable":null,"type":"2","email":"attune_HO @gmail.com","images":"http://zamy.in/uploads/kitchen/images/","logo":"","address":"45454","area":"ahm","landmark":"ahm","zipcode":"45345345","country":"1","state":"4","city":"7","approx_delivery_time":"","currency_format":"2","service_type":"","favourite_flag":"yes"},{"id":"10","res_name":"Healthy Restaurant","total_payable":null,"type":"2","email":"info@healthy.restaurant","images":"http://zamy.in/uploads/kitchen/images/","logo":"","address":"K-27, 28, Al Burooj Tower, 132 ft. Makarba Road Makarba, Ahmedabad - 380055","area":"Makarba","landmark":"Makarba","zipcode":"380055","country":"1","state":"1","city":"2","approx_delivery_time":"","currency_format":"2","service_type":"","favourite_flag":"no"}],"fav_food_list":[{"menu_name":"Test","short_code":"test","order_type":"2","menu_logo":"http://zamy.in/uploads/FoodMenu/badge13.jpg","price":"150.00","reduced_price":"140.00","long_description":"","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-satellite-ahmedabad-380051","kitchen_image":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"fries","short_code":"fries","order_type":"1,2,3","menu_logo":"http://zamy.in/uploads/FoodMenu/clickcart.png","price":null,"reduced_price":null,"long_description":"","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-satellite-ahmedabad-380051","kitchen_image":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Mojito","short_code":"mojito","order_type":"1,2,3","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"100.00","reduced_price":"0.00","long_description":"","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-satellite-ahmedabad-380051","kitchen_image":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Blue Heaven","short_code":"blue heaven","order_type":"1,2,3","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"100.00","reduced_price":"0.00","long_description":"","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-satellite-ahmedabad-380051","kitchen_image":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Melon Blush","short_code":"melon blush","order_type":"1,2,3","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"100.00","reduced_price":"0.00","long_description":"","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-satellite-ahmedabad-380051","kitchen_image":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Litchi Limca","short_code":"litchi limca","order_type":"1,2,3","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"100.00","reduced_price":"0.00","long_description":"","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-satellite-ahmedabad-380051","kitchen_image":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Pina Colada","short_code":"pina colada","order_type":"1,2,3","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"100.00","reduced_price":"0.00","long_description":"","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-satellite-ahmedabad-380051","kitchen_image":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Orange Cooler","short_code":"orange cooler","order_type":"1,2,3","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"100.00","reduced_price":"0.00","long_description":"","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-satellite-ahmedabad-380051","kitchen_image":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Fruit Pupch","short_code":"fruit pupch","order_type":"1,2,3","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"100.00","reduced_price":"0.00","long_description":"","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-satellite-ahmedabad-380051","kitchen_image":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"},{"menu_name":"Orange Juice","short_code":"1","order_type":"1,2,3","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"100.00","reduced_price":"0.00","long_description":"","res_name":"Hop Meal Restaurant","res_alias":"hop-meal-restaurant-satellite-ahmedabad-380051","kitchen_image":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","country":"1","state":"1","city":"2","approx_cost":"\u20b9500 FOR TWO","pure_veg":"0","approx_delivery_time":"52 MINS","service_type":"Indian, Chinese, Fast Food"}],"category_list":[{"cat_id":"5","cat_name":"OFFER-490","cat_display_name":"OFFER-490","cat_logo":"http://zamy.in/uploads/FoodCategory/"},{"cat_id":"6","cat_name":"Beverages","cat_display_name":"Beverages","cat_logo":"http://zamy.in/uploads/FoodCategory/"},{"cat_id":"7","cat_name":"Mocktail","cat_display_name":"Mocktail","cat_logo":"http://zamy.in/uploads/FoodCategory/"},{"cat_id":"8","cat_name":"Fresh Juice","cat_display_name":"Fresh Juice","cat_logo":"http://zamy.in/uploads/FoodCategory/"},{"cat_id":"9","cat_name":"Soups","cat_display_name":"Soups","cat_logo":"http://zamy.in/uploads/FoodCategory/"},{"cat_id":"10","cat_name":"Salad","cat_display_name":"Salad","cat_logo":"http://zamy.in/uploads/FoodCategory/"},{"cat_id":"11","cat_name":"Raita","cat_display_name":"Raita","cat_logo":"http://zamy.in/uploads/FoodCategory/"},{"cat_id":"12","cat_name":"Papad","cat_display_name":"Papad","cat_logo":"http://zamy.in/uploads/FoodCategory/"},{"cat_id":"13","cat_name":"Veg. Dry","cat_display_name":"Veg. Dry","cat_logo":"http://zamy.in/uploads/FoodCategory/"},{"cat_id":"14","cat_name":"Murg. Dry","cat_display_name":"Murg. Dry","cat_logo":"http://zamy.in/uploads/FoodCategory/"}],"popular_this_month":[{"id":"1","food_menu_id":"13","menu_name":"Dry Fruit Lassi","short_code":"dry fruit lassi","online_display_name":"","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"140.00","reduced_price":"0.00","variation_id":"0","qty":"2","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Satellite","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"10","food_menu_id":"4","menu_name":"fries","short_code":"fries","online_display_name":"fries","menu_logo":"http://zamy.in/uploads/FoodMenu/clickcart.png","price":null,"reduced_price":null,"variation_id":"1","qty":"2","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Satellite","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"3","food_menu_id":"3","menu_name":"Chicken Chilli Dry-","short_code":"chicken chilli dry","online_display_name":"","menu_logo":"http://zamy.in/uploads/FoodMenu/pizza4.jpg","price":"220.00","reduced_price":"0.00","variation_id":"0","qty":"1","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Satellite","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"7","food_menu_id":"3","menu_name":"Kaju Lassi","short_code":"kaju lassi","online_display_name":"","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"110.00","reduced_price":"0.00","variation_id":"0","qty":"1","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Satellite","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"10","food_menu_id":"3","menu_name":"Rose Lassi","short_code":"rose lassi","online_display_name":"","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"80.00","reduced_price":"0.00","variation_id":"0","qty":"2","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Satellite","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"12","food_menu_id":"3","menu_name":"Butter Milk-","short_code":"butter milk-","online_display_name":"","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"30.00","reduced_price":"0.00","variation_id":"0","qty":"1","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Satellite","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"16","food_menu_id":"3","menu_name":"Orange Juice","short_code":"1","online_display_name":"","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"100.00","reduced_price":"0.00","variation_id":"0","qty":"2","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Satellite","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"1","food_menu_id":"2","menu_name":"Litchi Limca","short_code":"litchi limca","online_display_name":"","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"100.00","reduced_price":"0.00","variation_id":"0","qty":"2","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Satellite","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"2","food_menu_id":"2","menu_name":"Butter Milk","short_code":"butter milk","online_display_name":"","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"30.00","reduced_price":"0.00","variation_id":"0","qty":"1","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Satellite","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"2","food_menu_id":"2","menu_name":"Soft Drink","short_code":"222","online_display_name":"","menu_logo":"http://zamy.in/uploads/FoodMenu/","price":"40.00","reduced_price":"0.00","variation_id":"0","qty":"2","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"http://zamy.in/uploads/kitchen/images/logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Satellite","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"}]}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        var top_restaurant_list: List<TopRestaurantListBean>? = null
        var fav_food_list: List<FavFoodListBean>? = null
        var category_list: List<CategoryListBean>? = null
        var popular_this_month: List<PopularThisMonthBean>? = null

        class TopRestaurantListBean {
            /**
             * id : 3
             * res_name : Hop Meal Restaurant
             * total_payable : null
             * type : 1
             * email : hopmeal12@gmial.com
             * images : http://zamy.in/uploads/kitchen/images/logo-300x83.png
             * logo : fact-icon13.png
             * address : Sarkhej, Ahmedabad, Gujarat, India
             * area : Satellite
             * landmark : Prahlad Nagar
             * zipcode : 380051
             * country : 1
             * state : 1
             * city : 2
             * approx_delivery_time : 52 MINS
             * currency_format : 2
             * service_type : Indian, Chinese, Fast Food
             * favourite_flag : yes
             */

            var id: String? = null
            var res_name: String? = null
            var total_payable: Any? = null
            var type: String? = null
            var email: String? = null
            var images: String? = null
            var logo: String? = null
            var address: String? = null
            var area: String? = null
            var landmark: String? = null
            var zipcode: String? = null
            var country: String? = null
            var state: String? = null
            var city: String? = null
            var approx_delivery_time: String? = null
            var currency_format: String? = null
            var service_type: String? = null
            var favourite_flag: String? = null
        }

        class FavFoodListBean {
            /**
             * menu_name : Test
             * short_code : test
             * order_type : 2
             * menu_logo : http://zamy.in/uploads/FoodMenu/badge13.jpg
             * price : 150.00
             * reduced_price : 140.00
             * long_description :
             * res_name : Hop Meal Restaurant
             * res_alias : hop-meal-restaurant-satellite-ahmedabad-380051
             * kitchen_image : http://zamy.in/uploads/kitchen/images/logo-300x83.png
             * address : Sarkhej, Ahmedabad, Gujarat, India
             * country : 1
             * state : 1
             * city : 2
             * approx_cost : ₹500 FOR TWO
             * pure_veg : 0
             * approx_delivery_time : 52 MINS
             * service_type : Indian, Chinese, Fast Food
             */

            var menu_name: String? = null
            var short_code: String? = null
            var order_type: String? = null
            var menu_logo: String? = null
            var price: String? = null
            var reduced_price: String? = null
            var long_description: String? = null
            var res_name: String? = null
            var res_alias: String? = null
            var kitchen_image: String? = null
            var address: String? = null
            var country: String? = null
            var state: String? = null
            var city: String? = null
            var approx_cost: String? = null
            var pure_veg: String? = null
            var approx_delivery_time: String? = null
            var service_type: String? = null
        }

        class CategoryListBean {
            /**
             * cat_id : 5
             * cat_name : OFFER-490
             * cat_display_name : OFFER-490
             * cat_logo : http://zamy.in/uploads/FoodCategory/
             */

            var cat_id: String? = null
            var cat_name: String? = null
            var cat_display_name: String? = null
            var cat_logo: String? = null
        }

        class PopularThisMonthBean {
            /**
             * id : 1
             * food_menu_id : 13
             * menu_name : Dry Fruit Lassi
             * short_code : dry fruit lassi
             * online_display_name :
             * menu_logo : http://zamy.in/uploads/FoodMenu/
             * price : 140.00
             * reduced_price : 0.00
             * variation_id : 0
             * qty : 2
             * kitchen_id : 3
             * res_name : Hop Meal Restaurant
             * service_type : Indian, Chinese, Fast Food
             * logo : fact-icon13.png
             * images : http://zamy.in/uploads/kitchen/images/logo-300x83.png
             * address : Sarkhej, Ahmedabad, Gujarat, India
             * area : Satellite
             * landmark : Prahlad Nagar
             * zipcode : 380051
             * country : 1
             * state : 1
             * city : 2
             */

            var id: String? = null
            var food_menu_id: String? = null
            var menu_name: String? = null
            var short_code: String? = null
            var online_display_name: String? = null
            var menu_logo: String? = null
            var price: String? = null
            var reduced_price: String? = null
            var variation_id: String? = null
            var qty: String? = null
            var kitchen_id: String? = null
            var res_name: String? = null
            var service_type: String? = null
            var logo: String? = null
            var images: String? = null
            var address: String? = null
            var area: String? = null
            var landmark: String? = null
            var zipcode: String? = null
            var country: String? = null
            var state: String? = null
            var city: String? = null
        }
    }
}
