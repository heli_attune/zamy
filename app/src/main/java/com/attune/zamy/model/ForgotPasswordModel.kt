package com.attune.zamy.model

class ForgotPasswordModel {

    /**
     * status : true
     * message : Password sent to your email address. if not display in inbox then check spam folder!
     * data : successful
     */

    var status: Int = 0
    var message: String? = null
    var data: String? = null
}
