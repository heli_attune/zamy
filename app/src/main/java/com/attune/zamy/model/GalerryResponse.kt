package com.attune.zamy.model

class GalerryResponse {

    /**
     * status : 1
     * message : Get restaurant info successfully.
     * data : {"gallery":["http://pos.zamy.in/uploads/kitchen/gallery/3609349c56fddc95e66ba5056b553c93.jpg","http://pos.zamy.in/uploads/kitchen/gallery/53e48ca173f19522a2323bcd42885dec.jpg","http://pos.zamy.in/uploads/kitchen/gallery/7e0dcf6b4e679d850c51c4fbb11a2264.jpg"]}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        var gallery: List<String>? = null
    }
}
