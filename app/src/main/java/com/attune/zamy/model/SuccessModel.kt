package com.attune.zamy.model

class SuccessModel {
    var status: Int = 0
    var message: String? = null
    var data: Any? = null
}
