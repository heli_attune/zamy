package com.attune.zamy.model

class ShippingAreaModel {
    /**
     * status : 1
     * message : Get shipping area successfully.
     * data : [{"area":"S. G. Highway between Sarkhej to Pakwan cross road","zipcode":"380054","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Corporate road","zipcode":"380015","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Prahlad nagar","zipcode":"380015","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Anand nagar","zipcode":"380015","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Makarba","zipcode":"380051","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Ramdev nagar","zipcode":"380015","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Iscon cross road","zipcode":"380015","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Shivranjani","zipcode":"380015","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Jodhpur","zipcode":"380015","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Shyamal","zipcode":"380015","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Bodakdev","zipcode":"380054","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Manekbaug","zipcode":"380006","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Vasna","zipcode":"382460","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Jivraj Park","zipcode":"380051","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Vejalpur","zipcode":"380051","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Vishala","zipcode":"380055","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Juhapura","zipcode":"380055","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Sarkhej","zipcode":"382210","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Ujala Circle","zipcode":"382210","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Shantipura","zipcode":"382210","city":"Ahmedabad","state":"Gujarat","country":"IN"},{"area":"Paldi Anjali Cross Road","zipcode":"380007","city":"Ahmedabad","state":"Gujarat","country":"IN"}]
     */

    var status: Int = 0
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * area : S. G. Highway between Sarkhej to Pakwan cross road
         * zipcode : 380054
         * city : Ahmedabad
         * state : Gujarat
         * country : IN
         */

        var area: String? = null
        var zipcode: String? = null
        var city: String? = null
        var state: String? = null
        var country: String? = null
    }
}
