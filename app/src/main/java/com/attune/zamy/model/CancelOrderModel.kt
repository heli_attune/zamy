package com.attune.zamy.model

class CancelOrderModel {

    /**
     * status : 1
     * message : Cancel order Successfully.
     * data : true
     */

    var status: Int = 0
    var message: String? = null
    var isData: Boolean = false
}
