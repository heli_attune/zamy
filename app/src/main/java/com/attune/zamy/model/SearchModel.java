package com.attune.zamy.model;

import java.util.List;

public class SearchModel {

    /**
     * status : 1
     * message : Get search result.
     * data : {"total_rows":14,"total_pages":2,"search_data":[{"foodmenu_id":"296","menu_name":"Italian Pizza","short_code":"76","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Veg Pizza"},{"foodmenu_id":"295","menu_name":"Margherita Pizza","short_code":"77","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Veg Pizza"},{"foodmenu_id":"294","menu_name":"Paneer Capsicum Pizza","short_code":"79","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Veg Pizza"},{"foodmenu_id":"293","menu_name":"Mexican Wave Hot And Fire Pizza","short_code":"80","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Veg Pizza"},{"foodmenu_id":"292","menu_name":"Chilli Garlic Pizza","short_code":"81","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Veg Pizza"},{"foodmenu_id":"291","menu_name":"Pineapple Pizza","short_code":"82","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Veg Pizza"},{"foodmenu_id":"290","menu_name":"Chicken Monteale Pizza","short_code":"85","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Non Veg. Pizza"},{"foodmenu_id":"289","menu_name":"Chicken Pesto Pizza","short_code":"86","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Non Veg. Pizza"},{"foodmenu_id":"288","menu_name":"Chicken Chatinal Pizza","short_code":"87","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Non Veg. Pizza"},{"foodmenu_id":"287","menu_name":"Spicy Chicken Pizza","short_code":"88","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Non Veg. Pizza"}]}
     */

    private int status;
    private String message;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * total_rows : 14
         * total_pages : 2
         * search_data : [{"foodmenu_id":"296","menu_name":"Italian Pizza","short_code":"76","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Veg Pizza"},{"foodmenu_id":"295","menu_name":"Margherita Pizza","short_code":"77","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Veg Pizza"},{"foodmenu_id":"294","menu_name":"Paneer Capsicum Pizza","short_code":"79","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Veg Pizza"},{"foodmenu_id":"293","menu_name":"Mexican Wave Hot And Fire Pizza","short_code":"80","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Veg Pizza"},{"foodmenu_id":"292","menu_name":"Chilli Garlic Pizza","short_code":"81","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Veg Pizza"},{"foodmenu_id":"291","menu_name":"Pineapple Pizza","short_code":"82","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Veg Pizza"},{"foodmenu_id":"290","menu_name":"Chicken Monteale Pizza","short_code":"85","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Non Veg. Pizza"},{"foodmenu_id":"289","menu_name":"Chicken Pesto Pizza","short_code":"86","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Non Veg. Pizza"},{"foodmenu_id":"288","menu_name":"Chicken Chatinal Pizza","short_code":"87","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Non Veg. Pizza"},{"foodmenu_id":"287","menu_name":"Spicy Chicken Pizza","short_code":"88","online_display_name":"","menu_logo":"","price":"0.00","reduced_price":"0.00","kitchen_id":"3","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","logo":"fact-icon13.png","images":"logo-300x83.png","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2","cat_name":"Non Veg. Pizza"}]
         */

        private int total_rows;
        private int total_pages;
        private List<SearchDataBean> search_data;

        public int getTotal_rows() {
            return total_rows;
        }

        public void setTotal_rows(int total_rows) {
            this.total_rows = total_rows;
        }

        public int getTotal_pages() {
            return total_pages;
        }

        public void setTotal_pages(int total_pages) {
            this.total_pages = total_pages;
        }

        public List<SearchDataBean> getSearch_data() {
            return search_data;
        }

        public void setSearch_data(List<SearchDataBean> search_data) {
            this.search_data = search_data;
        }

        public static class SearchDataBean {
            /**
             * foodmenu_id : 296
             * menu_name : Italian Pizza
             * short_code : 76
             * online_display_name :
             * menu_logo :
             * price : 0.00
             * reduced_price : 0.00
             * kitchen_id : 3
             * res_name : Hop Meal Restaurant
             * service_type : Indian, Chinese, Fast Food
             * logo : fact-icon13.png
             * images : logo-300x83.png
             * address : Sarkhej, Ahmedabad, Gujarat, India
             * area : Makarba
             * landmark : Prahlad Nagar
             * zipcode : 380051
             * country : 1
             * state : 1
             * city : 2
             * cat_name : Veg Pizza
             */

            private String foodmenu_id;
            private String menu_name;
            private String short_code;
            private String online_display_name;
            private String menu_logo;
            private String price;
            private String reduced_price;
            private String kitchen_id;
            private String res_name;
            private String service_type;
            private String logo;
            private String images;
            private String address;
            private String area;
            private String landmark;
            private String zipcode;
            private String country;
            private String state;
            private String city;
            private String cat_name;

            public String getFoodmenu_id() {
                return foodmenu_id;
            }

            public void setFoodmenu_id(String foodmenu_id) {
                this.foodmenu_id = foodmenu_id;
            }

            public String getMenu_name() {
                return menu_name;
            }

            public void setMenu_name(String menu_name) {
                this.menu_name = menu_name;
            }

            public String getShort_code() {
                return short_code;
            }

            public void setShort_code(String short_code) {
                this.short_code = short_code;
            }

            public String getOnline_display_name() {
                return online_display_name;
            }

            public void setOnline_display_name(String online_display_name) {
                this.online_display_name = online_display_name;
            }

            public String getMenu_logo() {
                return menu_logo;
            }

            public void setMenu_logo(String menu_logo) {
                this.menu_logo = menu_logo;
            }

            public String getPrice() {
                return price;
            }

            public void setPrice(String price) {
                this.price = price;
            }

            public String getReduced_price() {
                return reduced_price;
            }

            public void setReduced_price(String reduced_price) {
                this.reduced_price = reduced_price;
            }

            public String getKitchen_id() {
                return kitchen_id;
            }

            public void setKitchen_id(String kitchen_id) {
                this.kitchen_id = kitchen_id;
            }

            public String getRes_name() {
                return res_name;
            }

            public void setRes_name(String res_name) {
                this.res_name = res_name;
            }

            public String getService_type() {
                return service_type;
            }

            public void setService_type(String service_type) {
                this.service_type = service_type;
            }

            public String getLogo() {
                return logo;
            }

            public void setLogo(String logo) {
                this.logo = logo;
            }

            public String getImages() {
                return images;
            }

            public void setImages(String images) {
                this.images = images;
            }

            public String getAddress() {
                return address;
            }

            public void setAddress(String address) {
                this.address = address;
            }

            public String getArea() {
                return area;
            }

            public void setArea(String area) {
                this.area = area;
            }

            public String getLandmark() {
                return landmark;
            }

            public void setLandmark(String landmark) {
                this.landmark = landmark;
            }

            public String getZipcode() {
                return zipcode;
            }

            public void setZipcode(String zipcode) {
                this.zipcode = zipcode;
            }

            public String getCountry() {
                return country;
            }

            public void setCountry(String country) {
                this.country = country;
            }

            public String getState() {
                return state;
            }

            public void setState(String state) {
                this.state = state;
            }

            public String getCity() {
                return city;
            }

            public void setCity(String city) {
                this.city = city;
            }

            public String getCat_name() {
                return cat_name;
            }

            public void setCat_name(String cat_name) {
                this.cat_name = cat_name;
            }
        }
    }
}
