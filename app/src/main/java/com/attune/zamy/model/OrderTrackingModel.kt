package com.attune.zamy.model

import java.io.Serializable

class OrderTrackingModel : Serializable {

    /**
     * status : 1
     * message : success
     * data : {"order":{"delivery_boy_name":"Delivery boy 1","delivery_boy_profile_pic":"c08d37891621a7f7eb180385a769f4ff.jpg","delivery_boy_phone":"9724407927","delivery_boy_latitude":"22.993844090982332","delivery_boy_longitude":"72.49884727417724","delivery_boy_angle":"","customer_latitude":"22.9977913","customer_longitude":"72.5086396","customer_address":"Siddhivinayak tower, Off S.g Hignway, MakarbaMakarba, Ahmedabad, Gujarat-380051, India","job_accepted_status":"Rejected"}}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean :Serializable{
        /**
         * order : {"delivery_boy_name":"Delivery boy 1","delivery_boy_profile_pic":"c08d37891621a7f7eb180385a769f4ff.jpg","delivery_boy_phone":"9724407927","delivery_boy_latitude":"22.993844090982332","delivery_boy_longitude":"72.49884727417724","delivery_boy_angle":"","customer_latitude":"22.9977913","customer_longitude":"72.5086396","customer_address":"Siddhivinayak tower, Off S.g Hignway, MakarbaMakarba, Ahmedabad, Gujarat-380051, India","job_accepted_status":"Rejected"}
         */

        var order: OrderBean? = null

        class OrderBean : Serializable {
            /**
             * delivery_boy_name : Delivery boy 1
             * delivery_boy_profile_pic : c08d37891621a7f7eb180385a769f4ff.jpg
             * delivery_boy_phone : 9724407927
             * delivery_boy_latitude : 22.993844090982332
             * delivery_boy_longitude : 72.49884727417724
             * delivery_boy_angle :
             * customer_latitude : 22.9977913
             * customer_longitude : 72.5086396
             * customer_address : Siddhivinayak tower, Off S.g Hignway, MakarbaMakarba, Ahmedabad, Gujarat-380051, India
             * job_accepted_status : Rejected,Accepted,Assigned,On the way,Delivered
             */

            var delivery_boy_name: String? = null
            var delivery_boy_profile_pic: String? = null
            var delivery_boy_phone: String? = null
            var delivery_boy_latitude: String? = null
            var delivery_boy_longitude: String? = null
            var delivery_boy_angle: String? = null
            var customer_latitude: String? = null
            var customer_longitude: String? = null
            var customer_address: String? = null
            var job_accepted_status: String? = null
        }
    }
}
