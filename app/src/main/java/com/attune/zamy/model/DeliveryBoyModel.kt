package com.attune.zamy.model

class DeliveryBoyModel {


    /**
     * status : 1
     * message :
     * data : {"id":"210","full_name":"Delivery boy 1","phone":"9724407927","latitude":"22.993566","longitude":"72.4989596","angle":"44.48127"}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * id : 210
         * full_name : Delivery boy 1
         * phone : 9724407927
         * latitude : 22.993566
         * longitude : 72.4989596
         * angle : 44.48127
         */

        var id: String? = null
        var full_name: String? = null
        var phone: String? = null
        var latitude: String? = null
        var longitude: String? = null
        var angle: String? = null
        var profile_pic: String? = null
    }
}
