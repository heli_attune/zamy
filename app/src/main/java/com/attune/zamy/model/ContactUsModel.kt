package com.attune.zamy.model

class ContactUsModel {


    /**
     * status : 1
     * message : Insert data successfully.
     * data : [{"id":"5","name":"test","email":"test@gmail.com","subject":"test","msg":"testing","status":"1"}]
     */

    var status: Int = 0
    var message: String? = null
    var data: List<DataBean>? = null

    class DataBean {
        /**
         * id : 5
         * name : test
         * email : test@gmail.com
         * subject : test
         * msg : testing
         * status : 1
         */

        var id: String? = null
        var name: String? = null
        var email: String? = null
        var subject: String? = null
        var msg: String? = null
        var status: String? = null
    }
}
