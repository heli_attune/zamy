package com.attune.zamy.model;

import java.util.List;

public class EstimatedTimeModel {

    /**
     * geocoded_waypoints : [{"geocoder_status":"OK","place_id":"ChIJoQLM88KaXjkRNrUDKVB4Xrs","types":["street_address"]},{"geocoder_status":"OK","place_id":"ChIJGXQB0CSbXjkRbIVqgxKEEqY","types":["route"]}]
     * routes : [{"bounds":{"northeast":{"lat":23.0196867,"lng":72.5049563},"southwest":{"lat":22.9948716,"lng":72.498003}},"copyrights":"Map data Â©2018 Google","legs":[{"distance":{"text":"3.0 km","value":3026},"duration":{"text":"7 mins","value":394},"end_address":"Sarkhej - Gandhinagar Hwy, Spring Valley, Mumatpura, Ahmedabad, Gujarat 380054, India","end_location":{"lat":23.0196867,"lng":72.5049563},"start_address":"A-421/1, Kataria Automobiles Rd, Makarba, Ahmedabad, Gujarat 380051, India","start_location":{"lat":22.9948716,"lng":72.4992407},"steps":[{"distance":{"text":"36 m","value":36},"duration":{"text":"1 min","value":12},"end_location":{"lat":22.9951121,"lng":72.4990033},"html_instructions":"Head <b>northwest<\/b> toward <b>Kataria Automobiles Rd<\/b><div style=\"font-size:0.9em\">Restricted usage road<\/div>","polyline":{"points":"}djkCg_oyLWVWV"},"start_location":{"lat":22.9948716,"lng":72.4992407},"travel_mode":"DRIVING"},{"distance":{"text":"0.1 km","value":137},"duration":{"text":"1 min","value":42},"end_location":{"lat":22.9961277,"lng":72.49976529999999},"html_instructions":"Turn <b>right<\/b> onto <b>Kataria Automobiles Rd<\/b>","maneuver":"turn-right","polyline":{"points":"mfjkCw}nyLIEo@_@QK_CgB"},"start_location":{"lat":22.9951121,"lng":72.4990033},"travel_mode":"DRIVING"},{"distance":{"text":"0.1 km","value":99},"duration":{"text":"1 min","value":28},"end_location":{"lat":22.9967199,"lng":72.49910910000001},"html_instructions":"Turn <b>left<\/b> at <b>Makarba Sarkhej Roza Rd<\/b>","maneuver":"turn-left","polyline":{"points":"yljkCqboyLYdAEDEFEDIDMDg@TIF"},"start_location":{"lat":22.9961277,"lng":72.49976529999999},"travel_mode":"DRIVING"},{"distance":{"text":"0.1 km","value":104},"duration":{"text":"1 min","value":28},"end_location":{"lat":22.9969693,"lng":72.49813340000001},"html_instructions":"Turn <b>left<\/b> at the 1st cross street toward <b>SG Hwy Service Rd<\/b>","maneuver":"turn-left","polyline":{"points":"opjkCm~nyLGXAJKp@[jB"},"start_location":{"lat":22.9967199,"lng":72.49910910000001},"travel_mode":"DRIVING"},{"distance":{"text":"49 m","value":49},"duration":{"text":"1 min","value":21},"end_location":{"lat":22.9973834,"lng":72.4982843},"html_instructions":"Turn <b>right<\/b> at the 1st cross street onto <b>SG Hwy Service Rd<\/b>","maneuver":"turn-right","polyline":{"points":"arjkCixnyL]CICQEIGMG"},"start_location":{"lat":22.9969693,"lng":72.49813340000001},"travel_mode":"DRIVING"},{"distance":{"text":"15 m","value":15},"duration":{"text":"1 min","value":6},"end_location":{"lat":22.9974308,"lng":72.4981471},"html_instructions":"Turn <b>left<\/b> toward <b>Sarkhej - Gandhinagar Hwy<\/b>","maneuver":"turn-left","polyline":{"points":"stjkCgynyLIX"},"start_location":{"lat":22.9973834,"lng":72.4982843},"travel_mode":"DRIVING"},{"distance":{"text":"2.6 km","value":2586},"duration":{"text":"4 mins","value":257},"end_location":{"lat":23.0196867,"lng":72.5049563},"html_instructions":"Turn <b>right<\/b> at the 1st cross street onto <b>Sarkhej - Gandhinagar Hwy<\/b><div style=\"font-size:0.9em\">Pass by KAMDAR CARZ (on the right in 350&nbsp;m)<\/div>","maneuver":"turn-right","polyline":{"points":"}tjkCmxnyLI\\yLiCaLaCsJsBOCwImBkFgA_FaA}A]GAEC_ASoRgEMCsAWcE}@gJoB{Cs@aHyAoGsAcB_@wBe@AA"},"start_location":{"lat":22.9974308,"lng":72.4981471},"travel_mode":"DRIVING"}],"traffic_speed_entry":[],"via_waypoint":[]}],"overview_polyline":{"points":"}djkCg_oyLo@n@y@e@qCsB_@jAKLWJq@\\Id@g@|Cg@G[MMGIXI\\yLiCuWuFsX{FkDw@}RkEwGuAcOcDqPmD}EgA"},"summary":"Sarkhej - Gandhinagar Hwy","warnings":[],"waypoint_order":[]}]
     * status : OK
     */

    private String status;
    private List<GeocodedWaypointsBean> geocoded_waypoints;
    private List<RoutesBean> routes;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<GeocodedWaypointsBean> getGeocoded_waypoints() {
        return geocoded_waypoints;
    }

    public void setGeocoded_waypoints(List<GeocodedWaypointsBean> geocoded_waypoints) {
        this.geocoded_waypoints = geocoded_waypoints;
    }

    public List<RoutesBean> getRoutes() {
        return routes;
    }

    public void setRoutes(List<RoutesBean> routes) {
        this.routes = routes;
    }

    public static class GeocodedWaypointsBean {
        /**
         * geocoder_status : OK
         * place_id : ChIJoQLM88KaXjkRNrUDKVB4Xrs
         * types : ["street_address"]
         */

        private String geocoder_status;
        private String place_id;
        private List<String> types;

        public String getGeocoder_status() {
            return geocoder_status;
        }

        public void setGeocoder_status(String geocoder_status) {
            this.geocoder_status = geocoder_status;
        }

        public String getPlace_id() {
            return place_id;
        }

        public void setPlace_id(String place_id) {
            this.place_id = place_id;
        }

        public List<String> getTypes() {
            return types;
        }

        public void setTypes(List<String> types) {
            this.types = types;
        }
    }

    public static class RoutesBean {
        /**
         * bounds : {"northeast":{"lat":23.0196867,"lng":72.5049563},"southwest":{"lat":22.9948716,"lng":72.498003}}
         * copyrights : Map data Â©2018 Google
         * legs : [{"distance":{"text":"3.0 km","value":3026},"duration":{"text":"7 mins","value":394},"end_address":"Sarkhej - Gandhinagar Hwy, Spring Valley, Mumatpura, Ahmedabad, Gujarat 380054, India","end_location":{"lat":23.0196867,"lng":72.5049563},"start_address":"A-421/1, Kataria Automobiles Rd, Makarba, Ahmedabad, Gujarat 380051, India","start_location":{"lat":22.9948716,"lng":72.4992407},"steps":[{"distance":{"text":"36 m","value":36},"duration":{"text":"1 min","value":12},"end_location":{"lat":22.9951121,"lng":72.4990033},"html_instructions":"Head <b>northwest<\/b> toward <b>Kataria Automobiles Rd<\/b><div style=\"font-size:0.9em\">Restricted usage road<\/div>","polyline":{"points":"}djkCg_oyLWVWV"},"start_location":{"lat":22.9948716,"lng":72.4992407},"travel_mode":"DRIVING"},{"distance":{"text":"0.1 km","value":137},"duration":{"text":"1 min","value":42},"end_location":{"lat":22.9961277,"lng":72.49976529999999},"html_instructions":"Turn <b>right<\/b> onto <b>Kataria Automobiles Rd<\/b>","maneuver":"turn-right","polyline":{"points":"mfjkCw}nyLIEo@_@QK_CgB"},"start_location":{"lat":22.9951121,"lng":72.4990033},"travel_mode":"DRIVING"},{"distance":{"text":"0.1 km","value":99},"duration":{"text":"1 min","value":28},"end_location":{"lat":22.9967199,"lng":72.49910910000001},"html_instructions":"Turn <b>left<\/b> at <b>Makarba Sarkhej Roza Rd<\/b>","maneuver":"turn-left","polyline":{"points":"yljkCqboyLYdAEDEFEDIDMDg@TIF"},"start_location":{"lat":22.9961277,"lng":72.49976529999999},"travel_mode":"DRIVING"},{"distance":{"text":"0.1 km","value":104},"duration":{"text":"1 min","value":28},"end_location":{"lat":22.9969693,"lng":72.49813340000001},"html_instructions":"Turn <b>left<\/b> at the 1st cross street toward <b>SG Hwy Service Rd<\/b>","maneuver":"turn-left","polyline":{"points":"opjkCm~nyLGXAJKp@[jB"},"start_location":{"lat":22.9967199,"lng":72.49910910000001},"travel_mode":"DRIVING"},{"distance":{"text":"49 m","value":49},"duration":{"text":"1 min","value":21},"end_location":{"lat":22.9973834,"lng":72.4982843},"html_instructions":"Turn <b>right<\/b> at the 1st cross street onto <b>SG Hwy Service Rd<\/b>","maneuver":"turn-right","polyline":{"points":"arjkCixnyL]CICQEIGMG"},"start_location":{"lat":22.9969693,"lng":72.49813340000001},"travel_mode":"DRIVING"},{"distance":{"text":"15 m","value":15},"duration":{"text":"1 min","value":6},"end_location":{"lat":22.9974308,"lng":72.4981471},"html_instructions":"Turn <b>left<\/b> toward <b>Sarkhej - Gandhinagar Hwy<\/b>","maneuver":"turn-left","polyline":{"points":"stjkCgynyLIX"},"start_location":{"lat":22.9973834,"lng":72.4982843},"travel_mode":"DRIVING"},{"distance":{"text":"2.6 km","value":2586},"duration":{"text":"4 mins","value":257},"end_location":{"lat":23.0196867,"lng":72.5049563},"html_instructions":"Turn <b>right<\/b> at the 1st cross street onto <b>Sarkhej - Gandhinagar Hwy<\/b><div style=\"font-size:0.9em\">Pass by KAMDAR CARZ (on the right in 350&nbsp;m)<\/div>","maneuver":"turn-right","polyline":{"points":"}tjkCmxnyLI\\yLiCaLaCsJsBOCwImBkFgA_FaA}A]GAEC_ASoRgEMCsAWcE}@gJoB{Cs@aHyAoGsAcB_@wBe@AA"},"start_location":{"lat":22.9974308,"lng":72.4981471},"travel_mode":"DRIVING"}],"traffic_speed_entry":[],"via_waypoint":[]}]
         * overview_polyline : {"points":"}djkCg_oyLo@n@y@e@qCsB_@jAKLWJq@\\Id@g@|Cg@G[MMGIXI\\yLiCuWuFsX{FkDw@}RkEwGuAcOcDqPmD}EgA"}
         * summary : Sarkhej - Gandhinagar Hwy
         * warnings : []
         * waypoint_order : []
         */

        private BoundsBean bounds;
        private String copyrights;
        private OverviewPolylineBean overview_polyline;
        private String summary;
        private List<LegsBean> legs;
        private List<?> warnings;
        private List<?> waypoint_order;

        public BoundsBean getBounds() {
            return bounds;
        }

        public void setBounds(BoundsBean bounds) {
            this.bounds = bounds;
        }

        public String getCopyrights() {
            return copyrights;
        }

        public void setCopyrights(String copyrights) {
            this.copyrights = copyrights;
        }

        public OverviewPolylineBean getOverview_polyline() {
            return overview_polyline;
        }

        public void setOverview_polyline(OverviewPolylineBean overview_polyline) {
            this.overview_polyline = overview_polyline;
        }

        public String getSummary() {
            return summary;
        }

        public void setSummary(String summary) {
            this.summary = summary;
        }

        public List<LegsBean> getLegs() {
            return legs;
        }

        public void setLegs(List<LegsBean> legs) {
            this.legs = legs;
        }

        public List<?> getWarnings() {
            return warnings;
        }

        public void setWarnings(List<?> warnings) {
            this.warnings = warnings;
        }

        public List<?> getWaypoint_order() {
            return waypoint_order;
        }

        public void setWaypoint_order(List<?> waypoint_order) {
            this.waypoint_order = waypoint_order;
        }

        public static class BoundsBean {
            /**
             * northeast : {"lat":23.0196867,"lng":72.5049563}
             * southwest : {"lat":22.9948716,"lng":72.498003}
             */

            private NortheastBean northeast;
            private SouthwestBean southwest;

            public NortheastBean getNortheast() {
                return northeast;
            }

            public void setNortheast(NortheastBean northeast) {
                this.northeast = northeast;
            }

            public SouthwestBean getSouthwest() {
                return southwest;
            }

            public void setSouthwest(SouthwestBean southwest) {
                this.southwest = southwest;
            }

            public static class NortheastBean {
                /**
                 * lat : 23.0196867
                 * lng : 72.5049563
                 */

                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }

            public static class SouthwestBean {
                /**
                 * lat : 22.9948716
                 * lng : 72.498003
                 */

                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }
        }

        public static class OverviewPolylineBean {
            /**
             * points : }djkCg_oyLo@n@y@e@qCsB_@jAKLWJq@\Id@g@|Cg@G[MMGIXI\yLiCuWuFsX{FkDw@}RkEwGuAcOcDqPmD}EgA
             */

            private String points;

            public String getPoints() {
                return points;
            }

            public void setPoints(String points) {
                this.points = points;
            }
        }

        public static class LegsBean {
            /**
             * distance : {"text":"3.0 km","value":3026}
             * duration : {"text":"7 mins","value":394}
             * end_address : Sarkhej - Gandhinagar Hwy, Spring Valley, Mumatpura, Ahmedabad, Gujarat 380054, India
             * end_location : {"lat":23.0196867,"lng":72.5049563}
             * start_address : A-421/1, Kataria Automobiles Rd, Makarba, Ahmedabad, Gujarat 380051, India
             * start_location : {"lat":22.9948716,"lng":72.4992407}
             * steps : [{"distance":{"text":"36 m","value":36},"duration":{"text":"1 min","value":12},"end_location":{"lat":22.9951121,"lng":72.4990033},"html_instructions":"Head <b>northwest<\/b> toward <b>Kataria Automobiles Rd<\/b><div style=\"font-size:0.9em\">Restricted usage road<\/div>","polyline":{"points":"}djkCg_oyLWVWV"},"start_location":{"lat":22.9948716,"lng":72.4992407},"travel_mode":"DRIVING"},{"distance":{"text":"0.1 km","value":137},"duration":{"text":"1 min","value":42},"end_location":{"lat":22.9961277,"lng":72.49976529999999},"html_instructions":"Turn <b>right<\/b> onto <b>Kataria Automobiles Rd<\/b>","maneuver":"turn-right","polyline":{"points":"mfjkCw}nyLIEo@_@QK_CgB"},"start_location":{"lat":22.9951121,"lng":72.4990033},"travel_mode":"DRIVING"},{"distance":{"text":"0.1 km","value":99},"duration":{"text":"1 min","value":28},"end_location":{"lat":22.9967199,"lng":72.49910910000001},"html_instructions":"Turn <b>left<\/b> at <b>Makarba Sarkhej Roza Rd<\/b>","maneuver":"turn-left","polyline":{"points":"yljkCqboyLYdAEDEFEDIDMDg@TIF"},"start_location":{"lat":22.9961277,"lng":72.49976529999999},"travel_mode":"DRIVING"},{"distance":{"text":"0.1 km","value":104},"duration":{"text":"1 min","value":28},"end_location":{"lat":22.9969693,"lng":72.49813340000001},"html_instructions":"Turn <b>left<\/b> at the 1st cross street toward <b>SG Hwy Service Rd<\/b>","maneuver":"turn-left","polyline":{"points":"opjkCm~nyLGXAJKp@[jB"},"start_location":{"lat":22.9967199,"lng":72.49910910000001},"travel_mode":"DRIVING"},{"distance":{"text":"49 m","value":49},"duration":{"text":"1 min","value":21},"end_location":{"lat":22.9973834,"lng":72.4982843},"html_instructions":"Turn <b>right<\/b> at the 1st cross street onto <b>SG Hwy Service Rd<\/b>","maneuver":"turn-right","polyline":{"points":"arjkCixnyL]CICQEIGMG"},"start_location":{"lat":22.9969693,"lng":72.49813340000001},"travel_mode":"DRIVING"},{"distance":{"text":"15 m","value":15},"duration":{"text":"1 min","value":6},"end_location":{"lat":22.9974308,"lng":72.4981471},"html_instructions":"Turn <b>left<\/b> toward <b>Sarkhej - Gandhinagar Hwy<\/b>","maneuver":"turn-left","polyline":{"points":"stjkCgynyLIX"},"start_location":{"lat":22.9973834,"lng":72.4982843},"travel_mode":"DRIVING"},{"distance":{"text":"2.6 km","value":2586},"duration":{"text":"4 mins","value":257},"end_location":{"lat":23.0196867,"lng":72.5049563},"html_instructions":"Turn <b>right<\/b> at the 1st cross street onto <b>Sarkhej - Gandhinagar Hwy<\/b><div style=\"font-size:0.9em\">Pass by KAMDAR CARZ (on the right in 350&nbsp;m)<\/div>","maneuver":"turn-right","polyline":{"points":"}tjkCmxnyLI\\yLiCaLaCsJsBOCwImBkFgA_FaA}A]GAEC_ASoRgEMCsAWcE}@gJoB{Cs@aHyAoGsAcB_@wBe@AA"},"start_location":{"lat":22.9974308,"lng":72.4981471},"travel_mode":"DRIVING"}]
             * traffic_speed_entry : []
             * via_waypoint : []
             */

            private DistanceBean distance;
            private DurationBean duration;
            private String end_address;
            private EndLocationBean end_location;
            private String start_address;
            private StartLocationBean start_location;
            private List<StepsBean> steps;
            private List<?> traffic_speed_entry;
            private List<?> via_waypoint;

            public DistanceBean getDistance() {
                return distance;
            }

            public void setDistance(DistanceBean distance) {
                this.distance = distance;
            }

            public DurationBean getDuration() {
                return duration;
            }

            public void setDuration(DurationBean duration) {
                this.duration = duration;
            }

            public String getEnd_address() {
                return end_address;
            }

            public void setEnd_address(String end_address) {
                this.end_address = end_address;
            }

            public EndLocationBean getEnd_location() {
                return end_location;
            }

            public void setEnd_location(EndLocationBean end_location) {
                this.end_location = end_location;
            }

            public String getStart_address() {
                return start_address;
            }

            public void setStart_address(String start_address) {
                this.start_address = start_address;
            }

            public StartLocationBean getStart_location() {
                return start_location;
            }

            public void setStart_location(StartLocationBean start_location) {
                this.start_location = start_location;
            }

            public List<StepsBean> getSteps() {
                return steps;
            }

            public void setSteps(List<StepsBean> steps) {
                this.steps = steps;
            }

            public List<?> getTraffic_speed_entry() {
                return traffic_speed_entry;
            }

            public void setTraffic_speed_entry(List<?> traffic_speed_entry) {
                this.traffic_speed_entry = traffic_speed_entry;
            }

            public List<?> getVia_waypoint() {
                return via_waypoint;
            }

            public void setVia_waypoint(List<?> via_waypoint) {
                this.via_waypoint = via_waypoint;
            }

            public static class DistanceBean {
                /**
                 * text : 3.0 km
                 * value : 3026
                 */

                private String text;
                private int value;

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public int getValue() {
                    return value;
                }

                public void setValue(int value) {
                    this.value = value;
                }
            }

            public static class DurationBean {
                /**
                 * text : 7 mins
                 * value : 394
                 */

                private String text;
                private int value;

                public String getText() {
                    return text;
                }

                public void setText(String text) {
                    this.text = text;
                }

                public int getValue() {
                    return value;
                }

                public void setValue(int value) {
                    this.value = value;
                }
            }

            public static class EndLocationBean {
                /**
                 * lat : 23.0196867
                 * lng : 72.5049563
                 */

                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }

            public static class StartLocationBean {
                /**
                 * lat : 22.9948716
                 * lng : 72.4992407
                 */

                private double lat;
                private double lng;

                public double getLat() {
                    return lat;
                }

                public void setLat(double lat) {
                    this.lat = lat;
                }

                public double getLng() {
                    return lng;
                }

                public void setLng(double lng) {
                    this.lng = lng;
                }
            }

            public static class StepsBean {
                /**
                 * distance : {"text":"36 m","value":36}
                 * duration : {"text":"1 min","value":12}
                 * end_location : {"lat":22.9951121,"lng":72.4990033}
                 * html_instructions : Head <b>northwest</b> toward <b>Kataria Automobiles Rd</b><div style="font-size:0.9em">Restricted usage road</div>
                 * polyline : {"points":"}djkCg_oyLWVWV"}
                 * start_location : {"lat":22.9948716,"lng":72.4992407}
                 * travel_mode : DRIVING
                 * maneuver : turn-right
                 */

                private DistanceBeanX distance;
                private DurationBeanX duration;
                private EndLocationBeanX end_location;
                private String html_instructions;
                private PolylineBean polyline;
                private StartLocationBeanX start_location;
                private String travel_mode;
                private String maneuver;

                public DistanceBeanX getDistance() {
                    return distance;
                }

                public void setDistance(DistanceBeanX distance) {
                    this.distance = distance;
                }

                public DurationBeanX getDuration() {
                    return duration;
                }

                public void setDuration(DurationBeanX duration) {
                    this.duration = duration;
                }

                public EndLocationBeanX getEnd_location() {
                    return end_location;
                }

                public void setEnd_location(EndLocationBeanX end_location) {
                    this.end_location = end_location;
                }

                public String getHtml_instructions() {
                    return html_instructions;
                }

                public void setHtml_instructions(String html_instructions) {
                    this.html_instructions = html_instructions;
                }

                public PolylineBean getPolyline() {
                    return polyline;
                }

                public void setPolyline(PolylineBean polyline) {
                    this.polyline = polyline;
                }

                public StartLocationBeanX getStart_location() {
                    return start_location;
                }

                public void setStart_location(StartLocationBeanX start_location) {
                    this.start_location = start_location;
                }

                public String getTravel_mode() {
                    return travel_mode;
                }

                public void setTravel_mode(String travel_mode) {
                    this.travel_mode = travel_mode;
                }

                public String getManeuver() {
                    return maneuver;
                }

                public void setManeuver(String maneuver) {
                    this.maneuver = maneuver;
                }

                public static class DistanceBeanX {
                    /**
                     * text : 36 m
                     * value : 36
                     */

                    private String text;
                    private int value;

                    public String getText() {
                        return text;
                    }

                    public void setText(String text) {
                        this.text = text;
                    }

                    public int getValue() {
                        return value;
                    }

                    public void setValue(int value) {
                        this.value = value;
                    }
                }

                public static class DurationBeanX {
                    /**
                     * text : 1 min
                     * value : 12
                     */

                    private String text;
                    private int value;

                    public String getText() {
                        return text;
                    }

                    public void setText(String text) {
                        this.text = text;
                    }

                    public int getValue() {
                        return value;
                    }

                    public void setValue(int value) {
                        this.value = value;
                    }
                }

                public static class EndLocationBeanX {
                    /**
                     * lat : 22.9951121
                     * lng : 72.4990033
                     */

                    private double lat;
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }

                public static class PolylineBean {
                    /**
                     * points : }djkCg_oyLWVWV
                     */

                    private String points;

                    public String getPoints() {
                        return points;
                    }

                    public void setPoints(String points) {
                        this.points = points;
                    }
                }

                public static class StartLocationBeanX {
                    /**
                     * lat : 22.9948716
                     * lng : 72.4992407
                     */

                    private double lat;
                    private double lng;

                    public double getLat() {
                        return lat;
                    }

                    public void setLat(double lat) {
                        this.lat = lat;
                    }

                    public double getLng() {
                        return lng;
                    }

                    public void setLng(double lng) {
                        this.lng = lng;
                    }
                }
            }
        }
    }
}
