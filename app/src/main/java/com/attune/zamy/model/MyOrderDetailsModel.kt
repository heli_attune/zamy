package com.attune.zamy.model

import java.io.Serializable

class MyOrderDetailsModel :Serializable{
    /**
     * status : 1
     * message : Get order Details.
     * data : {"order":{"id":"210","kitchens_status":"Food Prepared","restaurant_id":"10","customer_name":"Rahul Satvara","shipping_name":"Rahul Vitthalbhai Satvara","shipping_email":"rahul.sathvara@gmail.com","shipping_phone":"972440792","shipping_address":"Siddhivinayak tower, Off S.g Hignway, MakarbaMakarba, Ahmedabad, Gujarat-380051, India","shipping_lat":"22.9977913","shipping_lng":"72.5086396","total_items":"1","order_total":"100","sub_total":"100","delivery_charge":"10","payment_method":"paytm","order_status":"processing","created_date":"2019-11-04 00:23:11","res_name":"Healthy Restaurant","service_type":"","customer_id":"3","order_item":[{"foodmenu_id":"637","menu_name":"Full Veg. Thali","variation_name":"","qty":"1","subtotal":"100","menu_logo":"download45.jpeg","swiggy_logo":"download46.jpeg","choice":"1"}],"order_tracking":{"delivery_boy_name":"Delivery boy 1","delivery_boy_profile_pic":"c08d37891621a7f7eb180385a769f4ff.jpg","delivery_boy_phone":"9724407927","delivery_boy_latitude":"22.993845602001493","delivery_boy_longitude":"72.49886178145476","delivery_boy_angle":"","customer_latitude":"22.9977913","customer_longitude":"72.5086396","customer_address":"Siddhivinayak tower, Off S.g Hignway, MakarbaMakarba, Ahmedabad, Gujarat-380051, India","job_accepted_status":"On the way"}}}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * order : {"id":"210","kitchens_status":"Food Prepared","restaurant_id":"10","customer_name":"Rahul Satvara","shipping_name":"Rahul Vitthalbhai Satvara","shipping_email":"rahul.sathvara@gmail.com","shipping_phone":"972440792","shipping_address":"Siddhivinayak tower, Off S.g Hignway, MakarbaMakarba, Ahmedabad, Gujarat-380051, India","shipping_lat":"22.9977913","shipping_lng":"72.5086396","total_items":"1","order_total":"100","sub_total":"100","delivery_charge":"10","payment_method":"paytm","order_status":"processing","created_date":"2019-11-04 00:23:11","res_name":"Healthy Restaurant","service_type":"","customer_id":"3","order_item":[{"foodmenu_id":"637","menu_name":"Full Veg. Thali","variation_name":"","qty":"1","subtotal":"100","menu_logo":"download45.jpeg","swiggy_logo":"download46.jpeg","choice":"1"}],"order_tracking":{"delivery_boy_name":"Delivery boy 1","delivery_boy_profile_pic":"c08d37891621a7f7eb180385a769f4ff.jpg","delivery_boy_phone":"9724407927","delivery_boy_latitude":"22.993845602001493","delivery_boy_longitude":"72.49886178145476","delivery_boy_angle":"","customer_latitude":"22.9977913","customer_longitude":"72.5086396","customer_address":"Siddhivinayak tower, Off S.g Hignway, MakarbaMakarba, Ahmedabad, Gujarat-380051, India","job_accepted_status":"On the way"}}
         */

        var order: OrderBean? = null

        class OrderBean :Serializable{
            /**
             * id : 210
             * kitchens_status : Food Prepared
             * restaurant_id : 10
             * customer_name : Rahul Satvara
             * shipping_name : Rahul Vitthalbhai Satvara
             * shipping_email : rahul.sathvara@gmail.com
             * shipping_phone : 972440792
             * shipping_address : Siddhivinayak tower, Off S.g Hignway, MakarbaMakarba, Ahmedabad, Gujarat-380051, India
             * shipping_lat : 22.9977913
             * shipping_lng : 72.5086396
             * total_items : 1
             * order_total : 100
             * sub_total : 100
             * delivery_charge : 10
             * payment_method : paytm
             * order_status : processing
             * created_date : 2019-11-04 00:23:11
             * res_name : Healthy Restaurant
             * service_type :
             * customer_id : 3
             * order_item : [{"foodmenu_id":"637","menu_name":"Full Veg. Thali","variation_name":"","qty":"1","subtotal":"100","menu_logo":"download45.jpeg","swiggy_logo":"download46.jpeg","choice":"1"}]
             * order_tracking : {"delivery_boy_name":"Delivery boy 1","delivery_boy_profile_pic":"c08d37891621a7f7eb180385a769f4ff.jpg","delivery_boy_phone":"9724407927","delivery_boy_latitude":"22.993845602001493","delivery_boy_longitude":"72.49886178145476","delivery_boy_angle":"","customer_latitude":"22.9977913","customer_longitude":"72.5086396","customer_address":"Siddhivinayak tower, Off S.g Hignway, MakarbaMakarba, Ahmedabad, Gujarat-380051, India","job_accepted_status":"On the way"}
             */

            var id: String? = null
            var kitchens_status: String? = null
            var restaurant_id: String? = null
            var customer_name: String? = null
            var shipping_name: String? = null
            var shipping_email: String? = null
            var shipping_phone: String? = null
            var shipping_address: String? = null
            var shipping_lat: String? = null
            var shipping_lng: String? = null
            var total_items: String? = null
            var order_total: String? = null
            var sub_total: String? = null
            var delivery_charge: String? = null
            var discount_amount:String?=null
            var payment_method: String? = null
            var order_status: String? = null
            var created_date: String? = null
            var res_name: String? = null
            var service_type: String? = null
            var customer_id: String? = null
            var gst: String? = null
            var order_tracking: OrderTrackingBean? = null
            var order_item: List<OrderItemBean>? = null

            class OrderTrackingBean :Serializable{
                /**
                 * delivery_boy_name : Delivery boy 1
                 * delivery_boy_profile_pic : c08d37891621a7f7eb180385a769f4ff.jpg
                 * delivery_boy_phone : 9724407927
                 * delivery_boy_latitude : 22.993845602001493
                 * delivery_boy_longitude : 72.49886178145476
                 * delivery_boy_angle :
                 * customer_latitude : 22.9977913
                 * customer_longitude : 72.5086396
                 * customer_address : Siddhivinayak tower, Off S.g Hignway, MakarbaMakarba, Ahmedabad, Gujarat-380051, India
                 * job_accepted_status : On the way
                 */

                var delivery_boy_name: String? = null
                var delivery_boy_profile_pic: String? = null
                var delivery_boy_phone: String? = null
                var delivery_boy_latitude: String? = null
                var delivery_boy_longitude: String? = null
                var delivery_boy_angle: String? = null
                var customer_latitude: String? = null
                var customer_longitude: String? = null
                var customer_address: String? = null
                var job_accepted_status: String? = null
            }

            class OrderItemBean :Serializable{
                /**
                 * foodmenu_id : 637
                 * menu_name : Full Veg. Thali
                 * variation_name :
                 * qty : 1
                 * subtotal : 100
                 * menu_logo : download45.jpeg
                 * swiggy_logo : download46.jpeg
                 * choice : 1
                 */

                var foodmenu_id: String? = null
                var menu_name: String? = null
                var variation_name: String? = null
                var qty: String? = null
                var subtotal: String? = null
                var menu_logo: String? = null
                var swiggy_logo: String? = null
                var choice: String? = null
            }
        }
    }
}
