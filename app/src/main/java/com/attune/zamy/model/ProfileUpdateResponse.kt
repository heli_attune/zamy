package com.attune.zamy.model

class ProfileUpdateResponse {

    /**
     * status : 1
     * message : The profile info has been updated successfully.
     * data : {"id":"4","phone":"9510805727","name":"ruchi shahu","email":"ruchi.shahu@attuneww.com","password":"$2y$10$uLF7Vgje9KN23Kvb/sYNLe2CECsWYjCErtBoKzdAyGL6Jil4N9VdG","current_area":"","location":"Makarba","country":"","state":"","city":"","profile":"http://pos.zamy.in/uploads/users/profile/2309Attune-Training.jpg","device_type":"a","fire_token":"","status":"1","created_date":"2019-10-04 02:02:01"}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * id : 4
         * phone : 9510805727
         * name : ruchi shahu
         * email : ruchi.shahu@attuneww.com
         * password : $2y$10$uLF7Vgje9KN23Kvb/sYNLe2CECsWYjCErtBoKzdAyGL6Jil4N9VdG
         * current_area :
         * location : Makarba
         * country :
         * state :
         * city :
         * profile : http://pos.zamy.in/uploads/users/profile/2309Attune-Training.jpg
         * device_type : a
         * fire_token :
         * status : 1
         * created_date : 2019-10-04 02:02:01
         */

        var id: String? = null
        var phone: String? = null
        var name: String? = null
        var email: String? = null
        var password: String? = null
        var current_area: String? = null
        var location: String? = null
        var country: String? = null
        var state: String? = null
        var city: String? = null
        var profile: String? = null
        var device_type: String? = null
        var fire_token: String? = null
        var status: String? = null
        var created_date: String? = null
    }
}
