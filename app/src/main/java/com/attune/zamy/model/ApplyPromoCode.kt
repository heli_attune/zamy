package com.attune.zamy.model

class ApplyPromoCode {

    /**
     * success : 0
     * msg : Invalid coupon code
     * data : {"sub_total":"","discount_amount":"","total":"200"}
     */

    var success: Int = 0
    var msg: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * sub_total :
         * discount_amount :
         * total : 200
         */

        var sub_total: String? = null
        var discount_amount: String? = null
        var total: String? = null
    }
}
