package com.attune.zamy.model

class ReasturantInfoModel {


    /**
     * status : 1
     * message : Get restaurant info successfully.
     * data : {"id":"10","address":"K-27, 28, Al Burooj Tower, 132 ft. Makarba Road Makarba, Ahmedabad - 380055","city":"Ahmedabad","area":"Makarba","landmark":"Makarba","zipcode":"380055","owner_contact_number":"2147483647","contact_information":"2147483647","manager_contact_number":"","biller_name":"","biller_contact_number":""}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * id : 10
         * address : K-27, 28, Al Burooj Tower, 132 ft. Makarba Road Makarba, Ahmedabad - 380055
         * city : Ahmedabad
         * email:''
         * area : Makarba
         * landmark : Makarba
         * zipcode : 380055
         * owner_contact_number : 2147483647
         * contact_information : 2147483647
         * manager_contact_number :
         * biller_name :
         * biller_contact_number :
         */

        var id: String? = null
        var address: String? = null
        var email: String? = null
        var city: String? = null
        var area: String? = null
        var landmark: String? = null
        var zipcode: String? = null
        var owner_contact_number: String? = null
        var contact_information: String? = null
        var manager_contact_number: String? = null
        var biller_name: String? = null
        var biller_contact_number: String? = null
    }
}
