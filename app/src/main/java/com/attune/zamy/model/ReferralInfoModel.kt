package com.attune.zamy.model

class ReferralInfoModel {

    /**
     * status : 1
     * message : Get user profile successfully.
     * data : {"earning_point":"50","cover_image":"","main_image":"","offer":"","reffaral_code":"seksfkjsbdfjhsdf","offer_validity":"Valid till 2019-11-13 05:45:48","invite_message":"Register on HOPMeal with Referral Code seksfkjsbdfjhsdf and earn Rs. 50 off on your next order above Rs. 200 at HOPMeal Cafe. Download HOPMeal from"}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * earning_point : 50
         * cover_image :
         * main_image :
         * offer :
         * reffaral_code : seksfkjsbdfjhsdf
         * offer_validity : Valid till 2019-11-13 05:45:48
         * invite_message : Register on HOPMeal with Referral Code seksfkjsbdfjhsdf and earn Rs. 50 off on your next order above Rs. 200 at HOPMeal Cafe. Download HOPMeal from
         */

        var earning_point: String? = null
        var cover_image: String? = null
        var main_image: String? = null
        var offer: String? = null
        var reffaral_code: String? = null
        var offer_validity: String? = null
        var invite_message: String? = null
    }
}
