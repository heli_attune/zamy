package com.attune.zamy.model

class CouponListModel {
    /**
     * status : 1
     * message : Coupon apply successfully.
     * data : [{"id":"4","title":"off101","coupon_code":"off101","description":"","min_spend":"100","max_spend":"500","usage_limit_per_coupon":"0","usage_limit_per_user":"0","discount":"101","dis_type":"fix","start_date":"2019-06-17","end_date":"2019-11-25","get_usage_limit":"0","get_usage_limit_per_user":"0","get_individual_use":"","get_used_by":",3","get_usage_count":"1","status":"1"},{"id":"5","title":"asdasd","coupon_code":"asdsad","description":"","min_spend":"0","max_spend":"0","usage_limit_per_coupon":"0","usage_limit_per_user":"0","discount":"12","dis_type":"percentage","start_date":"2019-06-15","end_date":"2019-11-30","get_usage_limit":"0","get_usage_limit_per_user":"0","get_individual_use":"","get_used_by":"","get_usage_count":"0","status":"1"},{"id":"6","title":"off10","coupon_code":"off10","description":"","min_spend":"250","max_spend":"1000","usage_limit_per_coupon":"0","usage_limit_per_user":"1","discount":"10","dis_type":"percentage","start_date":"2019-10-22","end_date":"2019-11-30","get_usage_limit":"0","get_usage_limit_per_user":"0","get_individual_use":"","get_used_by":"","get_usage_count":"0","status":"1"},{"id":"7","title":"off22","coupon_code":"off22","description":"","min_spend":"10","max_spend":"20","usage_limit_per_coupon":"5","usage_limit_per_user":"6","discount":"10","dis_type":"percentage","start_date":"2019-10-30","end_date":"2019-11-30","get_usage_limit":"0","get_usage_limit_per_user":"0","get_individual_use":"","get_used_by":"","get_usage_count":"0","status":"1"}]
     */

    var status: Int = 0
    var message: String? = null
    var data: ArrayList<DataBean>? = null



    class DataBean {
        /**
         * id : 4
         * title : off101
         * coupon_code : off101
         * description :
         * min_spend : 100
         * max_spend : 500
         * usage_limit_per_coupon : 0
         * usage_limit_per_user : 0
         * discount : 101
         * dis_type : fix
         * start_date : 2019-06-17
         * end_date : 2019-11-25
         * get_usage_limit : 0
         * get_usage_limit_per_user : 0
         * get_individual_use :
         * get_used_by : ,3
         * get_usage_count : 1
         * status : 1
         */

        var id: String? = null
        var title: String? = null
        var coupon_code: String? = null
        var description: String? = null
        var min_spend: String? = null
        var max_spend: String? = null
        var usage_limit_per_coupon: String? = null
        var usage_limit_per_user: String? = null
        var discount: String? = null
        var dis_type: String? = null
        var start_date: String? = null
        var end_date: String? = null
        var get_usage_limit: String? = null
        var get_usage_limit_per_user: String? = null
        var get_individual_use: String? = null
        var get_used_by: String? = null
        var get_usage_count: String? = null
        var status: String? = null
    }
}
