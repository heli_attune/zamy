package com.attune.zamy.model
class MyOrderListModel {


    /**
     * status : 1
     * message : Get order List.
     * data : {"total_rows":16,"total_pages":2,"list":[{"id":"50","restaurant_id":"3","shipping_name":"abc1","payment_method":"paytm","order_total":"380","kitchens_status":"Food Prepared","order_status":"processing","created_date":"2019-10-21 04:03:27","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"2","restaurant_id":"3","shipping_name":"Rahul","payment_method":"paytm","order_total":"190","kitchens_status":"Cancelled","order_status":"processing","created_date":"2019-10-10 05:44:50","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"4","restaurant_id":"3","shipping_name":"Rahul","payment_method":"paytm","order_total":"190","kitchens_status":"Waiting for Acceptance","order_status":"processing","created_date":"2019-10-10 06:29:01","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"5","restaurant_id":"3","shipping_name":"Rahul","payment_method":"paytm","order_total":"190","kitchens_status":"Waiting for Acceptance","order_status":"processing","created_date":"2019-10-10 06:29:47","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"6","restaurant_id":"3","shipping_name":"Rahul","payment_method":"paytm","order_total":"190","kitchens_status":"Waiting for Acceptance","order_status":"processing","created_date":"2019-10-10 06:31:53","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"49","restaurant_id":"3","shipping_name":"abc1","payment_method":"paytm","order_total":"380","kitchens_status":"Waiting for Acceptance","order_status":"pending","created_date":"2019-10-21 04:02:49","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"47","restaurant_id":"9","shipping_name":"Rahul","payment_method":"cod","order_total":"50","kitchens_status":"Waiting for Acceptance","order_status":"processing","created_date":"2019-10-18 02:44:00","res_name":"Bun Kabab","service_type":"","address":"K-27, 28, Al Burooj Tower, 132 ft. Makarba Road Makarba, Ahmedabad - 380055","area":"Makarba","landmark":"makrba","zipcode":"380055","country":"1","state":"1","city":"2"},{"id":"48","restaurant_id":"9","shipping_name":"abc1","payment_method":"cod","order_total":"250","kitchens_status":"Waiting for Acceptance","order_status":"processing","created_date":"2019-10-21 03:54:19","res_name":"Bun Kabab","service_type":"","address":"K-27, 28, Al Burooj Tower, 132 ft. Makarba Road Makarba, Ahmedabad - 380055","area":"Makarba","landmark":"makrba","zipcode":"380055","country":"1","state":"1","city":"2"},{"id":"46","restaurant_id":"9","shipping_name":"Rahul","payment_method":"paytm","order_total":"50","kitchens_status":"Waiting for Acceptance","order_status":"pending","created_date":"2019-10-18 02:43:19","res_name":"Bun Kabab","service_type":"","address":"K-27, 28, Al Burooj Tower, 132 ft. Makarba Road Makarba, Ahmedabad - 380055","area":"Makarba","landmark":"makrba","zipcode":"380055","country":"1","state":"1","city":"2"},{"id":"45","restaurant_id":"3","shipping_name":"Rahul","pa 10-25 14:05:08.638 4316-4677/com.attune.zamy D/OkHttp: yment_method":"cod","order_total":"455","kitchens_status":"Waiting for Acceptance","order_status":"processing","created_date":"2019-10-18 02:30:33","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"}]}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBean? = null

    class DataBean {
        /**
         * total_rows : 16
         * total_pages : 2
         * list : [{"id":"50","restaurant_id":"3","shipping_name":"abc1","payment_method":"paytm","order_total":"380","kitchens_status":"Food Prepared","order_status":"processing","created_date":"2019-10-21 04:03:27","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"2","restaurant_id":"3","shipping_name":"Rahul","payment_method":"paytm","order_total":"190","kitchens_status":"Cancelled","order_status":"processing","created_date":"2019-10-10 05:44:50","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"4","restaurant_id":"3","shipping_name":"Rahul","payment_method":"paytm","order_total":"190","kitchens_status":"Waiting for Acceptance","order_status":"processing","created_date":"2019-10-10 06:29:01","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"5","restaurant_id":"3","shipping_name":"Rahul","payment_method":"paytm","order_total":"190","kitchens_status":"Waiting for Acceptance","order_status":"processing","created_date":"2019-10-10 06:29:47","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"6","restaurant_id":"3","shipping_name":"Rahul","payment_method":"paytm","order_total":"190","kitchens_status":"Waiting for Acceptance","order_status":"processing","created_date":"2019-10-10 06:31:53","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"49","restaurant_id":"3","shipping_name":"abc1","payment_method":"paytm","order_total":"380","kitchens_status":"Waiting for Acceptance","order_status":"pending","created_date":"2019-10-21 04:02:49","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"},{"id":"47","restaurant_id":"9","shipping_name":"Rahul","payment_method":"cod","order_total":"50","kitchens_status":"Waiting for Acceptance","order_status":"processing","created_date":"2019-10-18 02:44:00","res_name":"Bun Kabab","service_type":"","address":"K-27, 28, Al Burooj Tower, 132 ft. Makarba Road Makarba, Ahmedabad - 380055","area":"Makarba","landmark":"makrba","zipcode":"380055","country":"1","state":"1","city":"2"},{"id":"48","restaurant_id":"9","shipping_name":"abc1","payment_method":"cod","order_total":"250","kitchens_status":"Waiting for Acceptance","order_status":"processing","created_date":"2019-10-21 03:54:19","res_name":"Bun Kabab","service_type":"","address":"K-27, 28, Al Burooj Tower, 132 ft. Makarba Road Makarba, Ahmedabad - 380055","area":"Makarba","landmark":"makrba","zipcode":"380055","country":"1","state":"1","city":"2"},{"id":"46","restaurant_id":"9","shipping_name":"Rahul","payment_method":"paytm","order_total":"50","kitchens_status":"Waiting for Acceptance","order_status":"pending","created_date":"2019-10-18 02:43:19","res_name":"Bun Kabab","service_type":"","address":"K-27, 28, Al Burooj Tower, 132 ft. Makarba Road Makarba, Ahmedabad - 380055","area":"Makarba","landmark":"makrba","zipcode":"380055","country":"1","state":"1","city":"2"},{"id":"45","restaurant_id":"3","shipping_name":"Rahul","pa 10-25 14:05:08.638 4316-4677/com.attune.zamy D/OkHttp: yment_method":"cod","order_total":"455","kitchens_status":"Waiting for Acceptance","order_status":"processing","created_date":"2019-10-18 02:30:33","res_name":"Hop Meal Restaurant","service_type":"Indian, Chinese, Fast Food","address":"Sarkhej, Ahmedabad, Gujarat, India","area":"Makarba","landmark":"Prahlad Nagar","zipcode":"380051","country":"1","state":"1","city":"2"}]
         */

        var total_rows: Int = 0
        var total_pages: Int = 0
        var list: List<ListBean>? = null

        class ListBean {
            /**
             * id : 50
             * restaurant_id : 3
             * shipping_name : abc1
             * payment_method : paytm
             * order_total : 380
             * kitchens_status : Food Prepared
             * order_status : processing
             * created_date : 2019-10-21 04:03:27
             * res_name : Hop Meal Restaurant
             * service_type : Indian, Chinese, Fast Food
             * address : Sarkhej, Ahmedabad, Gujarat, India
             * area : Makarba
             * landmark : Prahlad Nagar
             * zipcode : 380051
             * country : 1
             * state : 1
             * city : 2
             * pa 10-25 14:05:08.638 4316-4677/com.attune.zamy D/OkHttp: yment_method : cod
             */

            var id: String? = null
            var restaurant_id: String? = null
            var shipping_name: String? = null
            var payment_method: String? = null
            var order_total: String? = null
            var kitchens_status: String? = null
            var order_status: String? = null
            var created_date: String? = null
            var res_name: String? = null
            var service_type: String? = null
            var address: String? = null
            var area: String? = null
            var landmark: String? = null
            var zipcode: String? = null
            var country: String? = null
            var state: String? = null
            var city: String? = null
            var logo:String?=null
            var images:String?=null
        }
    }
}
