package com.attune.zamy.model

import com.google.gson.annotations.SerializedName

import java.io.Serializable

class OrderCart : Serializable {
    /**
     * order_data_array : {"order_id":"0","waiter_id":"2","customer_id":0,"sub_total":1190,"table_id":"30","order_date":"2019/09/02","order_time":"15:12","outlet_id":"3","no_of_person":"1","total_items":2,"food_menu_data":[{"foodmenu_id":"1","menu_name":"Khaliya","variation_id":"1","qty":1,"menu_unit_price":1190,"menu_price_without_discount":"1200","menu_price_with_discount":"1190"},{"foodmenu_id":"3","menu_name":"Kaju Lassi","variation_id":"1","qty":2,"menu_unit_price":0,"menu_price_without_discount":"90","menu_price_with_discount":"0"}]}
     */

    /*var order_data_array: OrderDataArrayBean? = null

    class OrderDataArrayBean : Serializable {
        */
    /**
     * order_id : 0
     * waiter_id : 2
     * customer_id : 0
     * sub_total : 1190
     * table_id : 30
     * order_date : 2019/09/02
     * order_time : 15:12
     * outlet_id : 3
     * no_of_person : 1
     * total_items : 2
     * food_menu_data : [{"foodmenu_id":"1","menu_name":"Khaliya","variation_id":"1","qty":1,"menu_unit_price":1190,"menu_price_without_discount":"1200","menu_price_with_discount":"1190"},{"foodmenu_id":"3","menu_name":"Kaju Lassi","variation_id":"1","qty":2,"menu_unit_price":0,"menu_price_without_discount":"90","menu_price_with_discount":"0"}]
     *//*

        var restaurant_id: String? = null
        var user_id: String? = null
        var sub_total: Int = 0
        var food_menu_data: List<FoodMenuDataBean>? = null

        class FoodMenuDataBean : Serializable {*/
    /**
     * foodmenu_id : 1
     * menu_name : Khaliya
     * variation_id : 1
     * qty : 1
     * menu_unit_price : 1190
     * menu_price_without_discount : 1200
     * menu_price_with_discount : 1190
     */

    var food_menu_id: String? = null
    var menu_name: String? = null
    var variation_id: String? = null
    var variation_name: String? = null
    var qty: Int = 0
    var pricetotal: Double = 0.00
    var price: Double = 0.0
    var restaurant_id: String? = null
    var user_id: String? = null
    var sub_total: Int = 0


    companion object {
        private const val serialVersionUID = -7060210544600464481L
    }


}
