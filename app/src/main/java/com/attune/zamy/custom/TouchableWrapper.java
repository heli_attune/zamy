package com.attune.zamy.custom;

import android.content.Context;
import android.os.SystemClock;
import android.view.MotionEvent;
import android.widget.FrameLayout;

public class TouchableWrapper extends FrameLayout {
    private static final long SCROLL_TIME = 200;
    private boolean isTouch = false;
    private long lastTouched = 0;
    private UpdateMapAfterUserInteraction updateMapAfterUserInteraction;

    public interface UpdateMapAfterUserInteraction {
        void onUpdateMapAfterUserInteraction();
    }

    public TouchableWrapper(Context context) {
        super(context);
        try {
            this.updateMapAfterUserInteraction = (UpdateMapAfterUserInteraction) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement UpdateMapAfterUserInteraction");
        }
    }

    public boolean dispatchTouchEvent(MotionEvent ev) {
        switch (ev.getAction()) {
            case  MotionEvent.ACTION_DOWN:
                this.lastTouched = SystemClock.uptimeMillis();
                this.isTouch = true;
                break;
            case MotionEvent.ACTION_UP:
                this.isTouch = false;
                long now = SystemClock.uptimeMillis();
                this.updateMapAfterUserInteraction.onUpdateMapAfterUserInteraction();
                break;
            default:
                this.isTouch = true;
                break;
        }
        return super.dispatchTouchEvent(ev);
    }
}
