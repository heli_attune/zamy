package com.attune.zamy.custom


import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.attune.zamy.fragment.CalculateDistanceFragment
import com.attune.zamy.fragment.PickUpAddressFragment
import com.attune.zamy.fragment.PickUpDropPaymentFragment

class AdapterDropAndDelivery(supportFragmentManager: FragmentManager) :
    FragmentPagerAdapter(supportFragmentManager) {


    private val mFragmentList = ArrayList<Fragment>()
    private val mFragmentTitleList = ArrayList<String>()

    override fun getItem(position: Int): Fragment {
        return when (position) {
            0 -> PickUpAddressFragment()
            1 -> CalculateDistanceFragment()
            2 -> PickUpDropPaymentFragment()
            else -> null!!
        }
    }

    override fun getPageTitle(position: Int): CharSequence? {
        return mFragmentTitleList[position]
    }

    override fun getCount(): Int {
        return mFragmentList.size
    }

    fun addFragment(fragment: Fragment, title: String) {
        mFragmentList.add(fragment)
        mFragmentTitleList.add(title)
    }
}
