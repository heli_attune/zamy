package com.attune.zamy.custom;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import com.attune.zamy.R;

public class SearchProgressBar extends Dialog {
    private static SearchProgressBar mCustomProgressbar;
    private SearchProgressBar searchprogressbar;
    private DialogInterface.OnDismissListener mOnDissmissListener;

    private SearchProgressBar(Context context) {
        super(context);
        setContentView(R.layout.search_progressbar);
    }

    public SearchProgressBar(Context context, Boolean instance) {
        super(context);
        searchprogressbar = new SearchProgressBar(context);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        if (mOnDissmissListener != null) {
            mOnDissmissListener.onDismiss(this);
        }
    }

    public static void showSearchProgressBar(Context context, boolean cancelable) {
        showSearchProgressBar(context, cancelable, null);
    }

    public static void showSearchProgressBar(Context context, boolean cancelable, String message) {
        if (mCustomProgressbar != null && mCustomProgressbar.isShowing()) {
            mCustomProgressbar.cancel();
        }
        mCustomProgressbar = new SearchProgressBar(context);
        mCustomProgressbar.setCancelable(cancelable);
        mCustomProgressbar.show();

    }

    public static void showSearchProgressBar(Context context, DialogInterface.OnDismissListener listener) {

        if (mCustomProgressbar != null && mCustomProgressbar.isShowing()) {
            mCustomProgressbar.cancel();
        }
        mCustomProgressbar = new SearchProgressBar(context);
        mCustomProgressbar.setListener(listener);
        mCustomProgressbar.setCancelable(Boolean.TRUE);
        mCustomProgressbar.show();
    }

    public static void hideSearchProgressBar() {
        if (mCustomProgressbar != null) {
            mCustomProgressbar.dismiss();
        }
    }

    private void setListener(DialogInterface.OnDismissListener listener) {
        mOnDissmissListener = listener;

    }

    public static void showListViewBottomProgressBar(View view) {
        if (mCustomProgressbar != null) {
            mCustomProgressbar.dismiss();
        }

        view.setVisibility(View.VISIBLE);
    }

    public static void hideListViewBottomProgressBar(View view) {
        if (mCustomProgressbar != null) {
            mCustomProgressbar.dismiss();
        }

        view.setVisibility(View.GONE);
    }

    public void showSearchProgress(Context context, boolean cancelable, String message) {

        if (searchprogressbar != null && searchprogressbar.isShowing()) {
            searchprogressbar.cancel();
        }
        searchprogressbar.setCancelable(cancelable);
        searchprogressbar.show();
    }
}
