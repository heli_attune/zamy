package com.attune.zamy.interaface

import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.RestaurantsMenuModel

interface onItemVarianceSelected {
        fun onVarianceItemSelected(
            item: CartRestaurantsMenuModel
        )
    }
