package com.attune.zamy.interaface

import com.attune.zamy.model.MyOrderDetailsModel

interface onReturnItemClick {
    fun onItemReturn(
        id: String?,
        foodmenuId: String?,
        qty: String?,
        orderItemBean: MyOrderDetailsModel.DataBean.OrderBean.OrderItemBean
    )

}