package com.attune.zamy.interaface

import android.text.BoringLayout
import com.attune.zamy.model.AddAddressModel
import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.GetAddressModel
import com.attune.zamy.model.RestaurantsMenuModel

interface onItemAddressSelected {
        fun onAddressSelected(
            item: GetAddressModel.DataBean.AddressBookBean,
            isSelected :Boolean, isEdit:Boolean
        )
    }
