package com.attune.zamy.coroutines.dataclass

import com.google.gson.annotations.SerializedName
data class HomeScreenDataClass(
    @SerializedName("data")
    val data: Data,
    @SerializedName("message")
    val message: String, // success
    @SerializedName("status")
    val status: Int // 1
) {
    data class Data(
        @SerializedName("cart_count")
        val cartCount: Int, // 0
        @SerializedName("coupon_list")
        val couponList: List<Coupon>,
        @SerializedName("fav_food_list")
        val favFoodList: List<FavFood>,
        @SerializedName("popular_this_month")
        val popularThisMonth: List<PopularThisMonth>,
        @SerializedName("top_restaurant_list")
        val topRestaurantList: List<TopRestaurant>
    ) {
        data class Coupon(
            @SerializedName("coupon_code")
            val couponCode: String, // RP20
            @SerializedName("coupon_image")
            val couponImage: String, // https://zamy.in/pos.zamy.in/uploads/coupon_image/b615a5e12995659fe60827a21f018909.png
            @SerializedName("id")
            val id: String // 13
        )
        data class FavFood(
            @SerializedName("long_description")
            val longDescription: String,
            @SerializedName("menu_logo")
            val menuLogo: String, // https://zamy.in/pos.zamy.in/uploads/FoodMenu/Small/33a0bd580d2111f1babe2d020ec30cc4.jpeg
            @SerializedName("menu_name")
            val menuName: String, // Fish strips (6pcs)
            @SerializedName("order_type")
            val orderType: String, // 1,2,3,4
            @SerializedName("price")
            val price: String, // 190.00
            @SerializedName("reduced_price")
            val reducedPrice: String, // 0.00
            @SerializedName("res_name")
            val resName: String, // Al Baik
            @SerializedName("restaurant_id")
            val restaurantId: String, // 54
            @SerializedName("short_code")
            val shortCode: String, // Fish strips (6pcs)
            @SerializedName("swiggy_logo")
            val swiggyLogo: String // 33a0bd580d2111f1babe2d020ec30cc4.jpeg
        )
        data class PopularThisMonth(
            @SerializedName("area")
            val area: String, // Sarkhej
            @SerializedName("cart")
            val cart: String, // no
            @SerializedName("food_menu_id")
            val foodMenuId: String, // 2637
            @SerializedName("food_type")
            val foodType: String, // 2
            @SerializedName("images")
            val images: String, // https://zamy.in/pos.zamy.in/uploads/FoodMenu/Small/9f42002fadc288bba66cb12747a58391.jpg
            @SerializedName("kitchen_id")
            val kitchenId: String, // 54
            @SerializedName("landmark")
            val landmark: String, // makrba
            @SerializedName("menu_name")
            val menuName: String, // chi. shawarma pizza
            @SerializedName("price")
            val price: String, // 170-230
            @SerializedName("product_variation")
            val productVariation: List<ProductVariation>,
            @SerializedName("res_name")
            val resName: String, // Al Baik
            @SerializedName("restaurant_online_status")
            val restaurantOnlineStatus: String, // 0
            @SerializedName("short_code")
            val shortCode: String, // chi. shawarma pizza
            @SerializedName("variation_product")
            val variationProduct: String // yes
        ) {
            data class ProductVariation(
                @SerializedName("cart_variation_id")
                val cartVariationId: String, // no
                @SerializedName("tax_price")
                val taxPrice: Int, // 170
                @SerializedName("variation_id")
                val variationId: String, // 141
                @SerializedName("variation_name")
                val variationName: String, // Regular
                @SerializedName("variation_price")
                val variationPrice: String, // 170
                @SerializedName("variation_sale_price")
                val variationSalePrice: String // 0
            )
        }
        data class TopRestaurant(
            @SerializedName("approx_cost")
            val approxCost: String,
            @SerializedName("approx_delivery_time")
            val approxDeliveryTime: String,
            @SerializedName("area")
            val area: String, // Makarba
            @SerializedName("id")
            val id: String, // 3
            @SerializedName("images")
            val images: String, // https://zamy.in/pos.zamy.in/uploads/kitchen/images/0578699362893e70e456c89d4e0cbf21.jpg
            @SerializedName("landmark")
            val landmark: String, // Al-burooj Tower
            @SerializedName("logo")
            val logo: String, // https://zamy.in/pos.zamy.in/uploads/kitchen/logo/d758305b813f3012a0486f56d6089b0e.png
            @SerializedName("res_alias")
            val resAlias: String, // hop-meal-restaurant-makarba-ahmedabad-390051
            @SerializedName("res_name")
            val resName: String, // Hop Meal Restaurant
            @SerializedName("restaurant_online_status")
            val restaurantOnlineStatus: String, // 0
            @SerializedName("service_type")
            val serviceType: String, // Indian,Chinees,Non-veg
            @SerializedName("total_orders")
            val totalOrders: String, // 772
            @SerializedName("total_restaurant_orders")
            val totalRestaurantOrders: String // 772
        )
    }
}