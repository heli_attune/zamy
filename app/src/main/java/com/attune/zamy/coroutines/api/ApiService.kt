package com.attune.zamy.coroutines.api

import com.attune.zamy.coroutines.dataclass.HomeScreenDataClass
import io.reactivex.Single

interface ApiService {

    fun getUsers(): Single<List<HomeScreenDataClass.Data.TopRestaurant>>

}