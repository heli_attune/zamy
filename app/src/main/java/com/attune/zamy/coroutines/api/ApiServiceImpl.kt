package com.attune.zamy.coroutines.api

import com.attune.zamy.coroutines.dataclass.HomeScreenDataClass
import com.attune.zamy.retrofitClient.BASEURL
import com.attune.zamy.retrofitClient.HOME_SCREEN
import com.rx2androidnetworking.Rx2AndroidNetworking
import io.reactivex.Single

class ApiServiceImpl : ApiService {
    override fun getUsers(): Single<List<HomeScreenDataClass.Data.TopRestaurant>> {

        return Rx2AndroidNetworking.get(BASEURL + HOME_SCREEN)
            .build()
            .getObjectListSingle(HomeScreenDataClass.Data.TopRestaurant::class.java)
    }

}