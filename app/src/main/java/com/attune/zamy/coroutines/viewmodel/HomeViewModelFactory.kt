package com.attune.zamy.coroutines.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.attune.zamy.coroutines.apiHelper.ApiHelper
import com.attune.zamy.coroutines.repository.HomeRepository

class HomeViewModelFactory (
    private val apiHelper: ApiHelper
):ViewModelProvider.Factory {
    override fun <T : ViewModel?> create(modelClass: Class<T>): T {
        if (modelClass.isAssignableFrom(HomeViewModel::class.java)){
            return HomeViewModel(HomeRepository(apiHelper)) as T
        }
        throw IllegalArgumentException("Unknown Class Name ")
    }

}