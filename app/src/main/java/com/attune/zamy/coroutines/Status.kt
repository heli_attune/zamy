package com.attune.zamy.coroutines

enum class Status {
    SUCCESS,
    ERROR,
    LOADING
}