package com.attune.zamy.activity

import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.attune.zamy.R
import com.attune.zamy.adapter.DemoRestaurantDetailMenuAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.interaface.RecyclerSectionItemDecoration
import com.attune.zamy.model.CartRestaurantsMenuModel
import com.attune.zamy.model.RestaurantsMenuModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import com.google.android.material.appbar.CollapsingToolbarLayout
import com.google.android.material.bottomsheet.BottomSheetBehavior
import kotlinx.android.synthetic.main.colapse_tolbar.imgRestaurant
import kotlinx.android.synthetic.main.colapse_tolbar.tvReastoTitle
import kotlinx.android.synthetic.main.fragment_restaurants_details.*
import kotlinx.android.synthetic.main.layout_cart_bottom.*
import kotlinx.android.synthetic.main.layout_restaurant_detail_scroll.*
import retrofit2.Call
import retrofit2.Response

class RestaurantDetailPage : AppCompatActivity(),
    DemoRestaurantDetailMenuAdapter.AddtoCartItemClickListener{
    override fun onMenuItemSelected(
        listBean: CartRestaurantsMenuModel,
        isProductVariance: Boolean,
        isRemove: Boolean
    ) {

    }



    lateinit var restaurant_id: String
    lateinit var tvMenu: TextView
    lateinit var tvInfo: TextView
    lateinit var tvGallary: TextView
    private lateinit var sheetBehavior: BottomSheetBehavior<LinearLayout>
    lateinit var txtItem: TextView
    lateinit var txtPrice: TextView
    lateinit var txtCharges: TextView
    lateinit var txtViewCart: TextView
    var cartArrayList: MutableList<CartRestaurantsMenuModel> = ArrayList<CartRestaurantsMenuModel>()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.layout_restaurant_detail_scroll)
        initView()


    }

    private fun initView() {
        restaurant_id = intent.getStringExtra(RESTAURANT_ID_PASS) ?: "0"
        val toolbar_layout = findViewById<CollapsingToolbarLayout>(R.id.toolbar_layout)
        var toolbar = findViewById<androidx.appcompat.widget.Toolbar>(R.id.toolbar1)
        toolbar_layout.setExpandedTitleTextAppearance(R.style.ExpandedAppBar)
        toolbar_layout.setCollapsedTitleTextAppearance(R.style.CollapsedAppBar)
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            toolbar.title = "xyz"
        }
        toolbar.setNavigationOnClickListener {
            finish()
        }
        tvMenu = findViewById(R.id.tvMenu)
        tvMenu.setOnClickListener {
            SelectOptions(true, false, false)
        }
        tvInfo = findViewById(R.id.tvInfo)
        tvInfo.setOnClickListener {
            SelectOptions(false, true, false)
        }
        tvGallary = findViewById(R.id.tvGallary)
        tvGallary.setOnClickListener {
            SelectOptions(false, false, true)
        }

        SelectOptions(true, false, false)

        if (!isNetworkAvailable(this@RestaurantDetailPage)) {
            Toast(this@RestaurantDetailPage, getString(R.string.network_error))
        } else {
            //menu items call api
            getRestaurentMenuList()
        }
    }

    private fun SelectOptions(bMenu: Boolean, bInfo: Boolean, bGallary: Boolean) {

        if (bMenu) {
            tvMenu.background = ContextCompat.getDrawable(this, R.drawable.red_border_button)
            tvMenu.setTextColor(resources.getColor(R.color.red_ea1b25))
            tvInfo.background = ContextCompat.getDrawable(this, R.drawable.red_button_fill)
            tvInfo.setTextColor(resources.getColor(R.color.white))
            tvGallary.background = ContextCompat.getDrawable(this, R.drawable.red_button_fill)
            tvGallary.setTextColor(resources.getColor(R.color.white))

        } else if (bInfo) {
            tvMenu.background = ContextCompat.getDrawable(this, R.drawable.red_button_fill)
            tvMenu.setTextColor(resources.getColor(R.color.white))
            tvInfo.background = ContextCompat.getDrawable(this, R.drawable.red_border_button)
            tvInfo.setTextColor(resources.getColor(R.color.red_ea1b25))
            tvGallary.background = ContextCompat.getDrawable(this, R.drawable.red_button_fill)
            tvGallary.setTextColor(resources.getColor(R.color.white))
        } else {
            tvMenu.background = ContextCompat.getDrawable(this, R.drawable.red_button_fill)
            tvMenu.setTextColor(resources.getColor(R.color.white))
            tvInfo.background = ContextCompat.getDrawable(this, R.drawable.red_button_fill)
            tvInfo.setTextColor(resources.getColor(R.color.white))
            tvGallary.background = ContextCompat.getDrawable(this, R.drawable.red_border_button)
            tvGallary.setTextColor(resources.getColor(R.color.red_ea1b25))
        }
        sheetBehavior = BottomSheetBehavior.from<LinearLayout>(bottom_sheet)
        sheetBehavior.state = BottomSheetBehavior.STATE_HIDDEN
        if(cartArrayList.size>0){
            sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            bottomSheetForCart()
        }


    }

    private fun bottomSheetForCart() {

        txtItem = findViewById<TextView>(R.id.txtItem)
        txtPrice = findViewById<TextView>(R.id.txtPrice)
        txtCharges = findViewById<TextView>(R.id.txtItem)
        txtViewCart = findViewById<TextView>(R.id.txtViewCart)

        txtViewCart.setOnClickListener {
            Log.e("cart size",cartArrayList.size.toString())
            /*val intent= Intent(activity, RestaurantDetailPage::class.java)
            intent.putExtra(RESTAURANT_ID_PASS,item.id)
            startActivity(intent)*/
            /*if (!isNetworkAvailable(this@EditKOTMenuPage)) {
                Toast(this@EditKOTMenuPage, getString(R.string.network_error))
            } else {
                if (cartArrayList.size > 0) {

                    tableOrderAPI()
                } else {
                    Toast(this@EditKOTMenuPage, "Please Select Items for orders!")
                }
            }*/

        }

        //bottom sheet

        /**
         * bottom sheet state change listener
         * we are changing button text when sheet changed state
         * */
        sheetBehavior.setBottomSheetCallback(object : BottomSheetBehavior.BottomSheetCallback() {
            override fun onStateChanged(bottomSheet: View, newState: Int) {
                when (newState) {
                    BottomSheetBehavior.STATE_HIDDEN -> {
                    }
                    BottomSheetBehavior.STATE_EXPANDED -> {
                    }
                    BottomSheetBehavior.STATE_COLLAPSED -> {
                    }
                    BottomSheetBehavior.STATE_DRAGGING -> {
                    }
                    BottomSheetBehavior.STATE_SETTLING -> {
                    }
                }
            }

            override fun onSlide(bottomSheet: View, slideOffset: Float) {
            }
        })
    }

    private fun expandCloseSheet() {
        if (sheetBehavior.state != BottomSheetBehavior.STATE_EXPANDED) {
            sheetBehavior.state = BottomSheetBehavior.STATE_EXPANDED
            //btBottomSheet.text = "Close Bottom Sheet"
        } else {
            sheetBehavior.state = BottomSheetBehavior.STATE_COLLAPSED
            //btBottomSheet.text = "Expand Bottom Sheet"
        }
    }

    private fun getRestaurentMenuList() {

        CustomProgressbar.showProgressBar(this@RestaurantDetailPage, false)
        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurant_id

        RetrofitClientSingleton
            .getInstance()
            .getReastaurentMenu(map)
            .enqueue(object : retrofit2.Callback<RestaurantsMenuModel> {
                override fun onFailure(call: Call<RestaurantsMenuModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Toast(this@RestaurantDetailPage, t.message)

                }

                override fun onResponse(
                    call: Call<RestaurantsMenuModel>,
                    response: Response<RestaurantsMenuModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val menuList = response.body()!!.data
                        val passMenu = response.body()!!.data!!.category
                        //llRestaurantDetails.visibility = View.VISIBLE
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                //Log.e("TAG", "${menuList!!.restaurant_details!!}")
                                if (menuList!!.restaurant_details != null) {
                                    var restaurantdata = menuList.restaurant_details
                                    if (restaurantdata.images != null && restaurantdata.images != "") {
                                        Glide.with(this@RestaurantDetailPage)
                                            .load(restaurantdata.images)
                                            .into(imgRestaurant)
                                    }
                                    if (restaurantdata.res_name != null && restaurantdata.res_name != "") {
                                        tvReastoTitle.text = menuList.restaurant_details.res_name
                                        toolbar_layout.title =
                                            menuList.restaurant_details.res_name
                                    }


                                    if (menuList.category!!.size > 0) {
                                        val menuHeader = menuList
                                        val rvOrientation =
                                            LinearLayoutManager(this@RestaurantDetailPage)

                                        rvMenu.layoutManager =
                                            LinearLayoutManager(this@RestaurantDetailPage)
                                       /* val adapter = response.body()!!.data!!.category?.let {
                                            DemoRestaurantDetailMenuAdapter(
                                                this@RestaurantDetailPage,
                                                it)
                                        }*/
                                        /*response.body()!!.data!!.category?.let {
                                            DemoRestaurantDetailMenuAdapter(
                                                this@RestaurantDetailPage,this@RestaurantDetailPage
                                                it
                                            )
                                        }*/
                                        //rvMenu.adapter = adapter

                                        val sectionItemDecoration = RecyclerSectionItemDecoration(
                                            LinearLayout.LayoutParams.WRAP_CONTENT,
                                            true, getSectionCallback(menuHeader.category!!)

                                        )

                                        rvMenu.addItemDecoration(sectionItemDecoration)
                                    }
                                }
/*
                                tvReastoDesc.text =
                                      menuList.restaurant_details!![0].area + "," + menuList.restaurant_details!![0].landmark
*/


                            }
                            FAILURE -> {

                            }
                            AUTH_FAILURE -> {

                            }
                        }

                    }

                }

            })
    }

    private fun getSectionCallback(people: List<RestaurantsMenuModel.DataBean.CategoryBean>): RecyclerSectionItemDecoration.SectionCallback {
        return object : RecyclerSectionItemDecoration.SectionCallback {
            override fun isSection(position: Int): Boolean {
                return position == 0 || people[position]
                    .cat_display_name !== people[position - 1]
                    .cat_display_name
            }

            override fun getSectionHeader(position: Int): CharSequence {
                return people[position]
                    .cat_display_name.toString()
            }
        }
    }



}
