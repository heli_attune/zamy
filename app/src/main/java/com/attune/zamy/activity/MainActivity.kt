package com.attune.zamy.activity

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.drawable.LayerDrawable
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.attune.zamy.R
import com.attune.zamy.fragment.*
import com.attune.zamy.model.SuccessModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.firebase.iid.FirebaseInstanceId
import com.hopmeal.android.utils.cartCount
import com.hopmeal.android.utils.keyFirebaseToken
import kotlinx.android.synthetic.main.activity_main.*
import retrofit2.Call
import retrofit2.Response
import java.util.*


class MainActivity : AppCompatActivity() {

    //private var cartItem: Int = 0
    private lateinit var floatingActionButton: FloatingActionButton
    private lateinit var toolbar: androidx.appcompat.widget.Toolbar
    private lateinit var cartBadge: LayerDrawable

    lateinit var restaurantId: String

    //    public var navigation: BottomNavigationView = null
    private lateinit var userId: String
    var fragment: Fragment? = null

    private val mOnNavigationItemSelectedListener =
        BottomNavigationView.OnNavigationItemSelectedListener { item ->
            when (item.itemId) {
                R.id.ic_navigation_home -> {
                        fragment = HomeFragment()

                }
                R.id.ic_navigation_search -> {
                    fragment = SearchDishFragment()
                }
                R.id.ic_navigation_covid -> {
//                     val intent = Intent(
//                         applicationContext,
//                         DeliveryActivity::class.java
//                     )
//                     startActivity(intent)
                    fragment = CovidFragment()
                }
                R.id.ic_navigation_grocery -> {
                    fragment = GroceryFragment()
                }
                R.id.ic_navigation_profile -> {
                    if (userId != "") {
                        fragment = AccountFragment()
                    } else {
                        val intent = Intent(applicationContext, LoginPage::class.java)
                        startActivity(intent)
                        finish()
                    }
                }

            }
            loadFragment(fragment)

        }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())

        setContentView(R.layout.activity_main)
        toolbar = findViewById(R.id.cartCustomToolbar)
        setSupportActionBar(toolbar)

        initView()

        LocalBroadcastManager.getInstance(this).registerReceiver(
            mMessageReceiver,
            IntentFilter("update_data")
        )
//        Log.e("fdslkjflkdsf",MainApp.actContext().toString())

//        Fabric.with(this, Crashlytics())
        //throw RuntimeException("This is a crash");
    }

    fun getrestaurantId(): String {
        return restaurantId
    }

    fun setrestaurantId(restaurantId: String) {
        this.restaurantId = restaurantId
    }

    private val mMessageReceiver: BroadcastReceiver = object : BroadcastReceiver() {
        override fun onReceive(
            context: Context?,
            intent: Intent
        ) {
            val navHostFragment =
                supportFragmentManager.fragments
            if (navHostFragment.size > 0) {
                val accountFragment = navHostFragment[2] as AccountFragment
                accountFragment.getUserProfile(false)
            }
        }
    }

    private fun initView() {
        floatingActionButton = findViewById(R.id.fabCart)
        fabCart.setOnClickListener {
            val intent = Intent(this@MainActivity, CartActivity::class.java)
            startActivity(intent)
        }

        updateFirebaseToken()
        userId = SharedPref.getUserId(this@MainActivity)
        loadFragment(HomeFragment())
        navigation.setOnNavigationItemSelectedListener(mOnNavigationItemSelectedListener)

        Log.e("TAG", "InitView ")
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        Log.e("TAG", "OnCreateMenu")
        menuInflater.inflate(R.menu.cart_menu, menu)
        for (i in 0 until menu!!.size()) {
            val item = menu.getItem(i)
            if (item.itemId == R.id.badge) {
                cartBadge = item.icon as LayerDrawable
                setBadgeCount(
                    this,
                    cartBadge,
                    SharedPref.getIntValue(this@MainActivity, cartCount).toString()
                )
            }
        }
        return super.onCreateOptionsMenu(menu)
    }


    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId) {
            R.id.badge -> {
                val intent = Intent(this, TopRestaurantsPage::class.java)
                intent.putExtra("PageName", "ViewCart")
                intent.putExtra(RESTAURANT_ID_PASS, 0)
                startActivity(intent)
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }


/*
    fun cartSet(cartItem: Int) {
        if (cartItem != 0) {
            Log.e("TAG", "CartSet ")
            val menuId = toolbar.menu.getItem(0).itemId
            val badge = navigation.getOrCreateBadge(menuId)
            badge.number = cartItem
            badge.backgroundColor = resources.getColor(R.color.colorPrimary)
            badge.badgeTextColor = resources.getColor(R.color.white)
            badge.maxCharacterCount = 2
            badge.isVisible = true
        } else {
            val menuId = navigation.menu.getItem(2).itemId
            val badge = navigation.getOrCreateBadge(menuId)
            badge.isVisible = false
        }
    }

    fun getCount(): Int {
        return cartItem
    }

    fun setCount(cartCount: Int) {
        cartItem = cartCount
    }*/

    fun hideToolbar() {
        toolbar.visibility = View.GONE
    }

    fun showToolbar() {
        toolbar.visibility = View.VISIBLE
    }

    fun updateCartBadge(cartCount: Int) {
        try {
            if (::cartBadge.isInitialized) {
                setBadgeCount(
                    this@MainActivity,
                    cartBadge,
                    cartCount.toString()
                    //SharedPref.getIntValue(this@MainActivity, cartCount).toString()
                )
            } else {
                Log.e("TAG", "updateCartBadge >> cartBadge not initilize")
            }
        } catch (e: Exception) {
            Log.e("TAG", "updateCartBadge >> $e")
        }
    }

//6490

    fun setBadgeCount(context: Context, icon: LayerDrawable, count: String) {
        val badge: BadgeDrawable
        val reuse = icon.findDrawableByLayerId(R.id.ic_badge)
        badge = if (reuse != null && reuse is BadgeDrawable) {
            reuse
        } else {
            BadgeDrawable(context)
        }
        badge.setCount(count)
        icon.mutate()
        icon.setDrawableByLayerId(R.id.ic_badge, badge)

    }
    /*private fun setMargins(
        view: View,
        left: Int,
        top: Int,
        right: Int,
        bottom: Int
    ) {
        if (view.layoutParams is ViewGroup.MarginLayoutParams) {
            val p: ViewGroup.MarginLayoutParams =
                view.layoutParams as ViewGroup.MarginLayoutParams
            p.setMargins(left, top, right, bottom)
            view.requestLayout()
        }
    }*/

    private fun loadFragment(fragment: Fragment?): Boolean {
        if (fragment != null) {
            supportFragmentManager
                .beginTransaction()
                .remove(fragment)
                .replace(R.id.fragment_container, fragment)
                .addToBackStack(null)
                .commit()
            return true
        }
        return false
    }


    override fun onBackPressed() {
        finish()
        finishAffinity()

    }

    /*private fun getListener(): FragmentManager.OnBackStackChangedListener? {
        return FragmentManager.OnBackStackChangedListener {
            val manager: FragmentManager = supportFragmentManager
            val backStackEntryCount: Int = manager.backStackEntryCount
            if (backStackEntryCount == 0) {
                finish()
            }
            val fragment: Fragment = manager.fragments[backStackEntryCount - 1]
            fragment.onResume()
        }
    }*/

    private fun updateFirebaseToken() {
        if (FirebaseInstanceId.getInstance().token != null) {
            val map = HashMap<String, String>()
            map["fire_token"] = FirebaseInstanceId.getInstance().token!!
            map["user_id"] = SharedPref.getUserId(this)
            map["device_type"] = "a"

            RetrofitClientSingleton.getInstance()
                .updateFirebaseToken(map).enqueue(object : retrofit2.Callback<SuccessModel> {
                    override fun onFailure(call: Call<SuccessModel>, t: Throwable) {
                        Log.e("TAG", "updateFirebaseToken t: >> $t")
                    }

                    override fun onResponse(
                        call: Call<SuccessModel>,
                        response: Response<SuccessModel>
                    ) {
                        if (response.isSuccessful)
                            when (response.body()?.status) {
                                SUCCESS -> {
                                    Log.d("TAG", "updateFirebaseToken : update >> success")
                                    SharedPref.setBooleanValue(
                                        this@MainActivity,
                                        keyFirebaseToken,
                                        true
                                    )
                                }

                                FAILURE -> {
                                    Log.d(
                                        "TAG",
                                        "updateFirebaseToken : update >> ${response.body()!!.message}"
                                    )
                                }
                                else -> {
                                    Log.d(
                                        "TAG",
                                        "updateFirebaseToken : update >> ${response.body()!!.message}"
                                    )
                                }
                            }
                    }
                })
        }
    }


    /* internal fun BottomNavigationView.checkItem(actionId: Int) {
         menu.findItem(actionId)?.isChecked = true
     }
 */
}
