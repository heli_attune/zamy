package com.attune.zamy.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.provider.Settings
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.UserModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.common.api.ApiException
import com.google.gson.Gson
import com.hopmeal.android.utils.RC_SIGN_IN
import com.hopmeal.android.utils.isGmaillogin
import kotlinx.android.synthetic.main.activity_login.*
import retrofit2.Call
import retrofit2.Response


class LoginPage : AppCompatActivity() {

    private lateinit var screenName: String
    private lateinit var strEmail: String
    private lateinit var strPassword: String
    var androidID = ""

    //private var callbackManager = CallbackManager.Factory.create()!!
    @SuppressLint("HardwareIds")
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        setContentView(R.layout.activity_login)
        androidID = Settings.Secure.getString(contentResolver, Settings.Secure.ANDROID_ID)
        Log.e("TAG", "Device$androidID")
        initView()

    }

    override fun onBackPressed() {
        super.onBackPressed()
        finish()
        finishAffinity()
    }

    private fun initView() {
        val intent = intent

        screenName = intent.getStringExtra(SCREEN_REDIRECTION) ?: "0"
        // validation()
        btnLogin.setOnClickListener {
            validation()
            /*  val ine = Intent(this, MainActivity::class.java)
              startActivity(ine)*/
        }
        tvLoginForgotPassword.setOnClickListener {

        }
        llSignUp.setOnClickListener {
            val ine = Intent(this, SignUpPage::class.java)
            ine.putExtra(SCREEN_REDIRECTION, screenName)
            startActivity(ine)
            finish()
        }
        tvLoginForgotPassword.setOnClickListener {
            val ine = Intent(this, ForgotPasswordPage::class.java)
            startActivity(ine)
        }

        txtSkip.setOnClickListener {
            val ine = Intent(this, MainActivity::class.java)
            startActivity(ine)
        }


        tilLoginName.editText?.doOnTextChanged { text, start, count, after ->
            if (text!!.isNotEmpty()) {
                tilLoginName.error = ""
            }
        }


        tilLoginPassword.editText?.doOnTextChanged { text, start, count, after ->
            if (text!!.isNotEmpty()) {
                tilLoginPassword.error = ""
            }
        }
    }


    private fun validation() {
        if (edloginemail?.text.toString().trim().isEmpty()) {
            tilLoginName.error = getString(R.string.username_or_email)
        } else if (edLoginpassword?.text.toString().trim().isEmpty()) {
            tilLoginPassword.error = getString(R.string.please_enter_password)
        } else if(edLoginpassword?.text!!.length<6){
            tilLoginPassword.error = "Password must be at least 6 character"
        }else {
            strEmail = edloginemail?.text.toString()
            strPassword = edLoginpassword?.text.toString()
            if (!isNetworkAvailable(this@LoginPage)) {
                Toast(this@LoginPage, getString(R.string.network_error))
            } else {
                doLogin()
            }
        }
        /* strEmail =  "john"
         strPassword =  "123"
         doLogin()
 */
    }

    /**Login Call**/
    private fun doLogin() {
        CustomProgressbar.showProgressBar(this, false)
        val map = HashMap<String, String>()
        map["email"] = strEmail
        map["password"] = strPassword
        RetrofitClientSingleton.getInstance().loginAPI(map)
            .enqueue(object : retrofit2.Callback<UserModel> {
                override fun onFailure(call: Call<UserModel>, t: Throwable) {
                    Toast(this@LoginPage, t.message)
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(
                    call: Call<UserModel>,
                    response: Response<UserModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    val userDataResponse = response.body()
                    if (userDataResponse != null) {
                        when (userDataResponse.status) {
                            SUCCESS -> {
                                val userBean = userDataResponse.data
                                userBean?.let {
                                    SharedPref.saveLogin(
                                        this@LoginPage,
                                        userDataResponse.data!!
                                    )
                                    if (screenName != null && screenName != "" && screenName.equals(
                                            "1"
                                        )
                                    ) {
                                        finish()
                                    } else {
                                        val LoginIntent =
                                            Intent(this@LoginPage, MainActivity::class.java)
                                        startActivity(LoginIntent)
//                                        finish()
                                    }
                                }
                                Toast(this@LoginPage, userDataResponse.message!!)
                            }
                            FAILURE -> {
                                Toast(this@LoginPage, userDataResponse.message!!)
                            }
                            else -> {
                                //toast(this@LoginPage, R.string.something_went_wrong)
                            }
                        }
                    }
                }
            })
    }


    /** Google Login**/
    fun googleLogin(view: View) {
        if (isNetworkAvailable(this)) {
            initializeGoogleSignIn()
            val signInIntent = getGoogleSignInClient(this).signInIntent
            startActivityForResult(signInIntent, RC_SIGN_IN)
        } else
            Toast(this, getString(R.string.network_error))
    }


    /**Social Login**/
    private fun doSocialLogin(
        map: java.util.HashMap<String, String>,
        isGmail: Boolean,
        isFb: Boolean
    ) {
        CustomProgressbar.showProgressBar(this, false)
        RetrofitClientSingleton.getInstance().getSocialLogin(map)
            .enqueue(object : retrofit2.Callback<UserModel> {
                override fun onFailure(call: Call<UserModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Toast(this@LoginPage, t.message)
                }

                override fun onResponse(
                    call: Call<UserModel>,
                    response: Response<UserModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val userDataResponse = response.body()
                        when (response.body()?.status) {
                            SUCCESS -> {
                                val userBean = userDataResponse!!.data
                                userBean?.let {
                                    SharedPref.saveLogin(
                                        this@LoginPage,
                                        userDataResponse.data!!
                                    )
                                }
                                if (isFb) {
                                    //    SharedPref.setFBLogin(this@LoginPage, isFblogin, true)
                                } else if (isGmail) {
                                    SharedPref.setGmaillLogin(
                                        this@LoginPage,
                                        isGmaillogin,
                                        true
                                    )
                                }
                                val intent = Intent(this@LoginPage, MainActivity::class.java)
                                startActivity(intent)

                                Toast(this@LoginPage, userDataResponse.message!!)

                            }
                            FAILURE -> {
                                Toast(this@LoginPage, userDataResponse!!.message!!)
                                /*  if (AccessToken.getCurrentAccessToken() != null) {
                                      LoginManager.getInstance().logOut()
                                  }*/
                                signOut()
                            }
                            AUTH_FAILURE -> {
                                Toast(this@LoginPage, getString(R.string.something_went_wrong))
                                /*    if (AccessToken.getCurrentAccessToken() != null) {
                                        LoginManager.getInstance().logOut()
                                    }
                                    signOut()*/

                            }
                        }
                    } else {
                        Toast(this@LoginPage, getString(R.string.something_went_wrong))
                        /* if (AccessToken.getCurrentAccessToken() != null) {
                             LoginManager.getInstance().logOut()
                         }*/
                        signOut()
                    }
                }
            })
    }

    /**Activity Result**/
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RC_SIGN_IN) {
            val task = GoogleSignIn.getSignedInAccountFromIntent(data)

            try {
                val account = task.getResult(ApiException::class.java)
                val map = java.util.HashMap<String, String>()
                Log.e("TAG", "Google Data : " + Gson().toJson(account))

                map["username"] = account!!.givenName.toString()
                /*map["password"] = account.id.toString()*/
                map["email"] = account.email.toString()
                //map["profile"] = account.photoUrl.toString()
                map["deviceID"] = androidID
                map["mobile"] = ""
                map["type"] = "google"
                doSocialLogin(map, true, false)
                /*  deviceID : 3434ere
                    username:test
                    email:test
                    mobile:9510805727
                    type:'google'*/

            } catch (e: ApiException) {
                Log.w("TAG", "signInResult:failed code=" + e.statusCode)
            }
        } else {
            //callbackManager.onActivityResult(requestCode, resultCode, data)
        }
    }
}
