package com.attune.zamy.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.adapter.FavouriteRestaurantAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.FavouriteReastaurantModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.activity_favourite.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import kotlinx.android.synthetic.main.no_data_layout.*
import kotlinx.android.synthetic.main.no_data_layout.view.*
import retrofit2.Call
import retrofit2.Response

class FavouriteActivity : AppCompatActivity() {

    lateinit var favAdapter: FavouriteRestaurantAdapter
    var linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    var isLastPageProduct = false
    var isLoadingProducts = false
    var totalRecords = 0
    var page: Int = 1
    private var favList: ArrayList<FavouriteReastaurantModel.DataBean.ListBean> =
        ArrayList()
    private var productArray: FavouriteReastaurantModel.DataBean? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_favourite)
        customtoolbar.txtTitle.text = "Favourite"
        customtoolbar.imgback.setOnClickListener {
            finish()
        }
        initLoading()
        rvFavouriteRestaurantsList.addOnScrollListener(recyclerViewOnScrollListener)
    }

    private fun initLoading() {

        favAdapter = FavouriteRestaurantAdapter(this@FavouriteActivity, favList)

        myFavSwip.setOnRefreshListener {
            if (!isNetworkAvailable(this@FavouriteActivity)) {

            } else {
                getfavourite()
            }

            myFavSwip.isRefreshing = false
        }

        getfavourite()

    }

    private fun getfavourite() {
        CustomProgressbar.showProgressBar(this, false)
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(this)
        map["page"] = page.toString()
        RetrofitClientSingleton
            .getInstance()
            .getFavouriteReasturantList(map)
            .enqueue(object : retrofit2.Callback<FavouriteReastaurantModel> {
                override fun onFailure(call: Call<FavouriteReastaurantModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    llParentNoData.visibility = View.VISIBLE
                    llParentNoData.icNoFavourite.visibility = View.VISIBLE
                    llParentNoData.txtNodata.text = "You have not favorites yet....."
                    Log.e("TAG", t.message)
                }

                override fun onResponse(
                    call: Call<FavouriteReastaurantModel>,
                    response: Response<FavouriteReastaurantModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        isLoadingProducts = false
                        // if (page == 1) favList.clear()
                        val apiResponse = response.body()!!.data
                        when (response.body()!!.status) {
                            SUCCESS -> {

                                if (apiResponse != null) {
                                    totalRecords = response.body()!!.data!!.total_rows
                                    productArray = apiResponse

                                    if (totalRecords > 0) {
                                        rvFavouriteRestaurantsList.visibility = View.VISIBLE
                                        /* favAdapter =
                                             FavouriteRestaurantAdapter(
                                                 this@FavouriteActivity,
                                                 apiResponse.list as MutableList<FavouriteReastaurantModel.DataBean.ListBean>?
                                             )*/
                                        favAdapter.addAll(apiResponse.list!!)
                                        rvFavouriteRestaurantsList.layoutManager =
                                            linearLayoutManager
                                        rvFavouriteRestaurantsList!!.adapter = favAdapter
                                    } else {
                                        if (page == 1)
                                            llParentNoData.visibility = View.VISIBLE
                                        icNoFavourite.visibility = View.VISIBLE
                                        txtNodata.text = getString(R.string.favourite_not_add)

                                    }
                                    if (totalRecords <= favAdapter.itemCount) {
                                        isLastPageProduct = true
                                    }
                                } else {
                                    favAdapter.clear()
                                }
                            }
                            FAILURE -> {
                                Toast(this@FavouriteActivity, response.body()!!.message)
                                llParentNoData.visibility = View.VISIBLE
                                icNoFavourite.visibility = View.VISIBLE
                            }
                            AUTH_FAILURE -> {

                            }
                        }
                    } else {
                        llParentNoData.visibility = View.VISIBLE
                        llParentNoData.icNoFavourite.visibility = View.VISIBLE
                        llParentNoData.txtNodata.text = getString(R.string.favourite_not_add)
                        rvFavouriteRestaurantsList.visibility = View.GONE
                    }

                }

            })

    }

    /**Scroll RecycleView**/
    private
    val recyclerViewOnScrollListener = object : RecyclerView.OnScrollListener() {
        override fun onScrolled(
            recyclerView: RecyclerView,
            dx: Int,
            dy: Int
        ) {
            super.onScrolled(recyclerView, dx, dy)
            val visibleItemCount = linearLayoutManager.childCount
            val firstVisibleItemPosition =
                linearLayoutManager.findFirstVisibleItemPosition()
            val totalItemCount = favAdapter.itemCount
            if (!isLoadingProducts && !isLastPageProduct) {
                if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount) {
                    loadMoreProducts()
                }
            }
        }
    }

    private fun loadMoreProducts() {
        isLoadingProducts = true
        page += 1
        if (!isNetworkAvailable(this)) {
            Toast(
                this@FavouriteActivity,
                getString(R.string.something_went_wrong)
            )
        } else {
            android.os.Handler().postDelayed({
                getfavourite()
            }, 1000)
        }

    }
}
