package com.attune.zamy.activity

import android.os.Bundle

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

import com.attune.zamy.R

class Demo {
    /*val currentFragment: Fragment?
        get() {
            val fm = getSupportFragmentManager()
            return if (fm != null) {
                fm!!.findFragmentById(R.id.fl_main_container)
            } else {
                null
            }
        }

    //fragments common methods
    fun pushFragments(
        fragment: Fragment, isAnimate: Boolean, isAdd: Boolean, isReplace: Boolean,
        isAddToBackstack: Boolean, tag: String, bundle: Bundle?
    ) {

        if (bundle != null) {
            fragment.arguments = bundle
        }


        val fragmentManager = getSupportFragmentManager()
        // Or: FragmentManager fragmentManager = getSupportFragmentManager() fro below 4.0 support
        val fragmentTransaction = fragmentManager.beginTransaction()

        if (currentFragment != null) {
            if (currentFragment!!.tag!!.equals(tag, ignoreCase = true)) {
                return
            }
        }

        //determine animation
        if (isAnimate) {

            *//*  fragmentTransaction.
                    setCustomAnimations(R.animator.slide_in_left,R.animator.slide_out_right,R.animator.slide_in_right
                            ,R.animator.slide_out_left
                    );*//*
            fragmentTransaction.setCustomAnimations(
                R.anim.enter,
                R.anim.exit,
                R.anim.pop_enter,
                R.anim.pop_exit
            )
        }


        if (isAdd) {
            //add fragments
            fragmentTransaction.add(R.id.fl_main_container, fragment, tag)
        } else if (isReplace) {
            //replace fragments
            fragmentTransaction.replace(R.id.fl_main_container, fragment, tag)
        } else {
        }

        //determine backstack
        if (isAddToBackstack) {
            fragmentTransaction.addToBackStack(tag)
        } else {
        }


        //hide keyboard
        hideKeyboard()

        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment!!)
        }

        if (!isFinishing()) {
            fragmentTransaction.commitAllowingStateLoss()
        }
    }*/
}
