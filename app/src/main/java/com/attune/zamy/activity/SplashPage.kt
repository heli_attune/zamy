package com.attune.zamy.activity

import android.content.Intent
import android.os.Bundle
import android.os.Handler
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.R
import com.attune.zamy.utils.SharedPref
import com.crashlytics.android.Crashlytics

class SplashPage : AppCompatActivity() {
    private lateinit var userId: String
    private val SPLASH_TIME_OUT: Long = 2000

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_splash_page)

        initView()
        logUser()
        //throw  RuntimeException("This is a crash by Umar Shaik")
    }

    private fun initView() {
        Handler().postDelayed({
            userId = SharedPref.getUserId(this)
            if (userId != "") {
                startActivity(Intent(this, MainActivity::class.java))
                finish()
            } else {
                startActivity(Intent(this, LoginPage::class.java))
                finish()
            }
        }, SPLASH_TIME_OUT)
    }

    fun logUser() {
        Crashlytics.setUserIdentifier(SharedPref.getUserId(this))
        Crashlytics.setUserEmail(SharedPref.getUserEmail(this))
        Crashlytics.setUserName(SharedPref.getUserName(this))
    }
}
