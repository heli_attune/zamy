package com.attune.zamy.activity

import android.Manifest
import android.content.DialogInterface
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.LinearLayoutManager
import com.attune.zamy.R
import com.attune.zamy.adapter.TicketsListingAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.interaface.callClick
import com.attune.zamy.model.CustomerSupportList
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.activity_customer_support.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.no_data_layout.*
import retrofit2.Call
import retrofit2.Response
import java.util.*

class CustomerSupportActivity : AppCompatActivity(), callClick {
    var linearLayoutManager: LinearLayoutManager? = null
    private var mAdapter: TicketsListingAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        setContentView(R.layout.activity_customer_support)
        setView()
    }

    private fun setView() {
        toolbarSet()
        linearLayoutManager = LinearLayoutManager(this)
        listtickets.layoutManager = linearLayoutManager

        mAdapter = TicketsListingAdapter(this, this@CustomerSupportActivity)
        listtickets.adapter = mAdapter


        if (isNetworkAvailable(this)) {
            getTicketList()
        } else {
        }
    }

    private fun toolbarSet() {
        imgback.setOnClickListener {
            onBackPressed()
        }
        txtTitle.text = "Customer Support"
    }

    private fun getTicketList() {
        CustomProgressbar.showProgressBar(
            this
            , false
        )
        val map = HashMap<String, String>()
        map["ticketSenderID"] = SharedPref.getUserId(this)
        RetrofitClientSingleton.getInstance().getCustomerSUPPORTList(map)
            .enqueue(object : retrofit2.Callback<CustomerSupportList> {
                override fun onFailure(call: Call<CustomerSupportList>?, t: Throwable?) {
                    CustomProgressbar.hideProgressBar()
                    Toast(this@CustomerSupportActivity, getString(R.string.something_went_wrong))
                }

                override fun onResponse(
                    call: Call<CustomerSupportList>?,
                    response: Response<CustomerSupportList>?
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response!!.isSuccessful) {
                        val ticketList = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                if (ticketList!!.data != null) {
                                    if (ticketList.data!!.size > 0) {
                                        mAdapter!!.setList(ticketList.data!!)
                                    } else {
                                        showNoDataLayout(
                                            llParentNoData,
                                            getString(R.string.no_data_fnd)
                                        )
                                    }
                                } else {
                                    showNoDataLayout(
                                        llParentNoData,
                                        getString(R.string.no_data_fnd)
                                    )
                                }
                            }
                            FAILURE -> {
                                Toast(this@CustomerSupportActivity, ticketList!!.message)
                            }
                            AUTH_FAILURE -> {
                                //forceLogout(activity!!)
                            }
                        }
                    }
                }

            })

    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>, grantResults: IntArray
    ) {
        Log.d("in fragment on request", "Permission callback called-------")
        when (requestCode) {
            1001 -> {

                val perms = HashMap<String, Int>()
                // Initialize the map with both permissions
                perms[Manifest.permission.CALL_PHONE] = PackageManager.PERMISSION_GRANTED
                // Fill with actual results from user
                if (grantResults.isNotEmpty()) {
                    for (i in permissions.indices)
                        perms[permissions[i]] = grantResults[i]
                    // Check for both permissions
                    if (perms[Manifest.permission.CALL_PHONE] == PackageManager.PERMISSION_GRANTED
                    ) {
                        print("Storage permissions are required")
                        val intentChrage = Intent(Intent.ACTION_CALL)
                        val phone="8448440802"
                        intent.data = Uri.parse("tel:$phone")
                        if (ActivityCompat.checkSelfPermission(
                                this,
                                Manifest.permission.CALL_PHONE
                            ) != PackageManager.PERMISSION_GRANTED
                        ) {
                            // TODO: Consider calling
                            //    ActivityCompat#requestPermissions
                            // here to request the missing permissions, and then overriding
                            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                            //                                          int[] grantResults)
                            // to handle the case where the user grants the permission. See the documentation
                            // for ActivityCompat#requestPermissions for more details.
                            return
                        }
                        else {
                            startActivity(intentChrage)
                        }
                    } else {
                        Log.d(
                            "in fragment on request",
                            "Some permissions are not granted ask again "
                        )
                        //permission is denied (this is the first time, when "never ask again" is not checked) so ask again explaining the usage of permission
                        //                        // shouldShowRequestPermissionRationale will return true
                        //show the dialog or snackbar saying its necessary and try again otherwise proceed with setup.
                        if (ActivityCompat.shouldShowRequestPermissionRationale(
                                this,
                                Manifest.permission.CALL_PHONE
                            )
                        ) {
                            showDialogOK("Call  permission is required for this app",
                                DialogInterface.OnClickListener { dialog, which ->
                                    when (which) {
                                        DialogInterface.BUTTON_POSITIVE -> checkAndRequestPermissions()
                                        DialogInterface.BUTTON_NEGATIVE -> {
                                        }
                                    }// proceed with logic by disabling the related features or quit the app.
                                })
                        } else {
                            Toast(this, "Go to settings and enable permissions")
                            //                            //proceed with logic by disabling the related features or quit the app.
                        }//permission is denied (and never ask again is  checked)
                        //shouldShowRequestPermissionRationale will return false
                    }
                }
            }
        }

    }

    private fun checkAndRequestPermissions(): Boolean {
        val call = ContextCompat.checkSelfPermission(this@CustomerSupportActivity, Manifest.permission.CALL_PHONE)
        val listPermissionsNeeded = ArrayList<String>()
        if (call != PackageManager.PERMISSION_GRANTED) {
            listPermissionsNeeded.add(Manifest.permission.CALL_PHONE)
        }

        if (listPermissionsNeeded.isNotEmpty()) {
            ActivityCompat.requestPermissions(
                this,
                listPermissionsNeeded.toTypedArray(),
                1
            )
            return false
        }
        return true
    }

    fun showDialogOK(message: String, okListener: DialogInterface.OnClickListener) {
        AlertDialog.Builder(this)
            .setMessage(message)
            .setPositiveButton("OK", okListener)
            .setNegativeButton("Cancel", okListener)
            .create()
            .show()
    }

    override fun callClick() {
        if (checkAndRequestPermissions()) {
            val intent = Intent(Intent.ACTION_CALL)
            val phone="8448440802"
            intent.data = Uri.parse("tel:$phone")
            if (ActivityCompat.checkSelfPermission(
                    this,
                    Manifest.permission.CALL_PHONE
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                ActivityCompat.requestPermissions(this,  arrayOf(Manifest.permission.CALL_PHONE),1);
            }
            else {
                this.startActivity(intent)
            }
        }
    }
}
