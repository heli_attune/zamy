package com.attune.zamy.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.R

class OptionActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_option)
    }
}
