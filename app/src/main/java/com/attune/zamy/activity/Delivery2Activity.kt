package com.attune.zamy.activity

import android.content.Intent
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.R
import com.attune.zamy.utils.Toast
import kotlinx.android.synthetic.main.activity_delivery2.*
import kotlinx.android.synthetic.main.title_toolbar.*

class Delivery2Activity :AppCompatActivity()
{

    override fun onCreate(savedInstanceState: Bundle?)
    {

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery2)
        titleToolbar.text = getString(R.string.area_distance_calculation)

        val i = intent!!
        var user_id=i.getStringExtra("user_id")
        var store=i.getStringExtra("store")
        var item_details=i.getStringExtra("item_details")
        var pickup_address=i.getStringExtra("pickup_address")
        var pickup_pincode=i.getStringExtra("pickup_pincode")
        var pickup_contact_no=i.getStringExtra("pickup_contact_no")
        var delivery_address=i.getStringExtra("delivery_address")
        var delivery_pincode=i.getStringExtra("delivery_pincode")
        var delivery_contact_no=i.getStringExtra("delivery_contact_no")
        var from_to_distance=i.getStringExtra("from_to_distance")
        var km_amount=i.getStringExtra("km_amount")
        var pickup_lattitude=i.getStringExtra("pickup_lattitude")
        var pickup_longitude=i.getStringExtra("pickup_longitude")
        var delivery_lattitude=i.getStringExtra("delivery_lattitude")
        var delivery_longitude=i.getStringExtra("delivery_longitude")
//        Toast(applicationContext,user_id+store+item_details+pickup_address+pickup_pincode+pickup_contact_no+delivery_address+delivery_pincode+delivery_contact_no)
        edTotalkmamount.setText(km_amount)
        edTotalKM.setText(from_to_distance)
        edTotalamount.setText(km_amount)


        edAditionalAmount.addTextChangedListener(textWatcher)
//        edTotalamount.setOnClickListener {
//
//
////            tvTotalamount.text=(""+edAditionalAmount.text.toString().toDouble()+tvTotalamount.text.toString().toDouble())
//            Log.e("test",""+edAditionalAmount.text.toString()+"..."+edTotalamount.text.toString())
////            Log.e("test",""+edAditionalAmount.text.toString().toDouble()+tvTotalAmount.text.toString().toDouble())
//        }

        btndelivery2Back.setOnClickListener {
            val intent= Intent(applicationContext,DeliveryActivity::class.java)
            startActivity(intent)
            finish()
        }

        btndelivery2NextStep.setOnClickListener {
            val intentnext= Intent(applicationContext,DeliveryPaymentActivity::class.java)
            intentnext.putExtra("user_id", user_id)
            intentnext.putExtra("store",store)
            intentnext.putExtra("item_details",item_details)
            intentnext.putExtra("pickup_address",pickup_address)
            intentnext.putExtra("pickup_pincode",pickup_pincode)
            intentnext.putExtra("pickup_contact_no",pickup_contact_no)
            intentnext.putExtra("pickup_lattitude",pickup_lattitude)
            intentnext.putExtra("pickup_longitude",pickup_longitude)
            intentnext.putExtra("delivery_address",delivery_address)
            intentnext.putExtra("delivery_pincode",delivery_pincode)
            intentnext.putExtra("delivery_contact_no",delivery_contact_no)
            intentnext.putExtra("from_to_distance",edTotalKM.text.toString())
            intentnext.putExtra("km_amount",edTotalkmamount.text.toString())
            intentnext.putExtra("additional_amount",edAditionalAmount.text.toString())
            intentnext.putExtra("total_amount",edTotalamount.text.toString())
            intentnext.putExtra("delivery_lattitude",delivery_lattitude)
            intentnext.putExtra("delivery_longitude",delivery_longitude)
            startActivity(intentnext)
            finish()
        }
    }

   private val textWatcher = object : TextWatcher {
        override fun afterTextChanged(s: Editable?) {
        }

        override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
        }

        override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int)
        {
            if (s!!.length<1)
            {
                edTotalamount.isEnabled=false
                edTotalamount.setText(edTotalkmamount.text.toString())
            }
            else {
                val a =
                    (edAditionalAmount.text.toString().toDouble() + edTotalkmamount.text.toString()
                        .toDouble())
                edTotalamount.setText(a.toString())
            }
        }
    }
}