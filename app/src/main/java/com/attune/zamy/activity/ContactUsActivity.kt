package com.attune.zamy.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.ContactUsModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.activity_contact_us.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import kotlinx.android.synthetic.main.no_data_layout.*
import retrofit2.Call
import retrofit2.Response

class ContactUsActivity : AppCompatActivity() {


    var strName: String = ""
    var strEmail: String = ""
    var strSubject: String = ""
    var strMessage: String = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_contact_us)
        customtoolbar.txtTitle.text = "Contact Us"
        customtoolbar.imgback.setOnClickListener {
            finish()
        }
        initLoading()
    }

    private fun initLoading() {
        btnContactSubmit.setOnClickListener {
            when {
                etContactName.text!!.isEmpty() -> {
                    Toast(this, "Please Enter Name")
                }
                etContactEmail.text!!.isEmpty() -> {
                    Toast(this, "Please Enter Email")
                }
                etContactSubject.text!!.isEmpty() -> {
                    Toast(this, "Please Enter Subject")
                }
                etContactMessage.text!!.isEmpty() -> {
                    Toast(this, "Please Enter Message")
                }
                else -> {
                    strName = etContactName.text.toString()
                    strEmail = etContactEmail.text.toString()
                    strSubject = etContactSubject.text.toString()
                    strMessage = etContactMessage.text.toString()
                    if (isNetworkAvailable(this@ContactUsActivity)) {
                        getContactUs()
                        llContactUs.visibility = View.VISIBLE
                    } else {
                        showNoDataLayout(llParentNoData, "No Data Found")
                    }

                }

            }
        }

    }


    private fun getContactUs() {
        CustomProgressbar.showProgressBar(this@ContactUsActivity, false)
        val map = HashMap<String, String>()
        map["name"] = strName
        map["email"] = strEmail
        map["subject"] = strSubject
        map["msg"] = strMessage

        RetrofitClientSingleton
            .getInstance()
            .contactUs(map)
            .enqueue(object : retrofit2.Callback<ContactUsModel> {
                override fun onFailure(call: Call<ContactUsModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("TAG", t.message.toString())
                }

                override fun onResponse(
                    call: Call<ContactUsModel>,
                    response: Response<ContactUsModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                Toast(this@ContactUsActivity, response.body()!!.message)
                                val intent =
                                    Intent(this@ContactUsActivity, MainActivity::class.java)
                                startActivity(intent)
                            }
                            FAILURE -> {

                            }
                            AUTH_FAILURE -> {

                            }
                        }
                    }
                }

            })
    }
}
