package com.attune.zamy.activity

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.attune.zamy.R
import com.attune.zamy.fragment.CartToolbarFragment
import com.attune.zamy.model.CartListModel
import com.attune.zamy.utils.SharedPref
import kotlinx.android.synthetic.main.cartbottom_sheet.*


class CartActivity :Activity(){

    private lateinit var restaurant_id: String
    lateinit var datumList: MutableList<CartListModel.DataBean.ItemsBean>
    var finalTotalValue: Double = 0.0
    private lateinit var rvMenu: RecyclerView
    lateinit var cartAdapter: CartToolbarFragment.CartAdapter
    lateinit var userId: String
    lateinit var mView: View
    var coupanCode: String = ""
    var totalValue: String = ""
    var delivery_charges: String = "0"
    var first = 1
    var userPhone = ""
    var minimum_order = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_cart_toolbar)
        userId = SharedPref.getUserId(this@CartActivity)
        initView()
        if (edPromoCode.text!!.isNotEmpty()) {
            ApplyPromoCode.visibility = View.GONE
            RemovePromoCode.visibility = View.VISIBLE
        } else {
            ApplyPromoCode.visibility = View.VISIBLE
            RemovePromoCode.visibility = View.GONE
        }
    }

    private fun initView() {
        datumList = ArrayList()
        tvProfile.setOnClickListener {
            if (userPhone != "") {
                val intent = Intent(this, CheckOutActivity::class.java)

                this.startActivity(intent)
            } else {
                val intent = Intent(this, VerificationMobile::class.java)
                this.startActivity(intent)
            }
        }

    }

}