package com.attune.zamy.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import androidx.viewpager.widget.ViewPager
import com.attune.zamy.R
import com.attune.zamy.fragment.ConfirmationFragment
import com.attune.zamy.fragment.PaymentFragment
import com.attune.zamy.fragment.ShippingFragment
import com.attune.zamy.model.GetAddressModel
import com.attune.zamy.utils.RESTAURANT_ID_PASS
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.layout_toolbar.*
import java.util.*


class CheckOutActivity : AppCompatActivity() {
    lateinit var viewPager: ViewPager
    var addressModel: GetAddressModel.DataBean.AddressBookBean? = null
    var restaurantId: String = ""
    var total: Int = 0
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_check_out)
        initView()
    }


    private fun initView() {
        val intent = intent

        restaurantId = intent.getStringExtra(RESTAURANT_ID_PASS) ?: "0"
        txtTitle.text = "Checkout"
        imgback.setOnClickListener {
            onBackPressed()
            finish()
        }
        viewPager = findViewById(R.id.checkOutViewpager)
        setupViewPager(viewPager)
        // Set Tabs inside Toolbar
        val tabs = findViewById<TabLayout>(R.id.checkOut_tabs)
        tabs.setupWithViewPager(viewPager)
        tabs.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabReselected(tab: TabLayout.Tab?) {
                viewPager.currentItem = tab!!.position
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {

            }

            override fun onTabSelected(tab: TabLayout.Tab?) {

            }

        })

    }


    private fun setupViewPager(viewPager: ViewPager) {
        val adapter = AdapterCheckOutTab(supportFragmentManager)
        adapter.addFragment(ShippingFragment(), "Address")
        adapter.addFragment(ConfirmationFragment(), "Confirmation")
        adapter.addFragment(PaymentFragment(), "Payment")
        viewPager.adapter = adapter
    }

    fun getPagerPostion(): ViewPager {
        if (null == viewPager) {
            viewPager = findViewById(R.id.checkOutViewpager)

        }
        return viewPager
    }

    internal class AdapterCheckOutTab(manager: FragmentManager) : FragmentPagerAdapter(manager) {
        private val mFragmentList = ArrayList<Fragment>()
        private val mFragmentTitleList = ArrayList<String>()

        override fun getItem(position: Int): Fragment {
            return mFragmentList[position]
        }

        override fun getCount(): Int {
            return mFragmentList.size
        }

        fun addFragment(fragment: Fragment, title: String) {
            mFragmentList.add(fragment)
            mFragmentTitleList.add(title)
        }

        override fun getPageTitle(position: Int): CharSequence? {
            return mFragmentTitleList[position]
        }
    }
}
