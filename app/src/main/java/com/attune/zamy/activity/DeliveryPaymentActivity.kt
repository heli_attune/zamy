package com.attune.zamy.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.DeliveryResponseModel
import com.attune.zamy.model.PaytmCheckoutModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.activity_delivery_payment.*
import kotlinx.android.synthetic.main.title_toolbar.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class DeliveryPaymentActivity : AppCompatActivity() {
    lateinit var user_id: String
    lateinit var store: String
    lateinit var item_details: String
    lateinit var pickup_address: String
    lateinit var pickup_pincode: String
    lateinit var pickup_lattitude: String
    lateinit var pickup_longitude: String
    lateinit var pickup_contact_no: String
    lateinit var delivery_address: String
    lateinit var delivery_pincode: String
    lateinit var delivery_lattitude: String
    lateinit var delivery_longitude: String
    lateinit var delivery_contact_no: String
    lateinit var from_to_distance: String
    lateinit var km_amount: String
    var payment_method: String="Paytm"
    lateinit var additional_amount: String
    lateinit var total_amount: String


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery_payment)
        titleToolbar.text = "Select Payment Method"

        val i = intent!!
        user_id = i.getStringExtra("user_id")
        store = i.getStringExtra("store")
        item_details = i.getStringExtra("item_details")
        pickup_address = i.getStringExtra("pickup_address")
        pickup_pincode = i.getStringExtra("pickup_pincode")
        pickup_contact_no = i.getStringExtra("pickup_contact_no")
        delivery_address = i.getStringExtra("delivery_address")
        delivery_pincode = i.getStringExtra("delivery_pincode")
        delivery_contact_no = i.getStringExtra("delivery_contact_no")
        from_to_distance = i.getStringExtra("from_to_distance")
        km_amount = i.getStringExtra("km_amount")
        pickup_lattitude = i.getStringExtra("pickup_lattitude")
        pickup_longitude = i.getStringExtra("pickup_longitude")
        delivery_lattitude = i.getStringExtra("delivery_lattitude")
        delivery_longitude = i.getStringExtra("delivery_longitude")
        additional_amount = i.getStringExtra("additional_amount")
        total_amount = i.getStringExtra("total_amount")
//        payment_method="cod"
//        Toast(applicationContext,user_id+store+item_details+pickup_address+pickup_pincode+pickup_contact_no+delivery_address+delivery_pincode+delivery_contact_no)

        Deliveryradio_group.setOnCheckedChangeListener { radioGroup, optionId ->
            run {
                when (optionId) {
                    R.id.DeliveryradioPaytm -> {
                        payment_method = "paytm"

                    }
                    R.id.DeliveryradioInstaMojo -> {
                        payment_method = "instamojo"

                    }
                    R.id.DeliveryradioRezorpay -> {
                        payment_method = "razorpay"
                    }

                }
            }
        }

        btndeliverypaymentBack.setOnClickListener {
            val intent = Intent(applicationContext, Delivery2Activity::class.java)
            startActivity(intent)
            finish()
        }

        btndeliverySubmit.setOnClickListener {
            callPickupDropAddApi()
        }
    }

    private fun doPaymentPaytmAPI(paymentType: String) {
        CustomProgressbar.showProgressBar(this@DeliveryPaymentActivity, false)
        val map = HashMap<String, String>()
        map["user_id"] = user_id
//        map["restaurant_id"] = restaurant_id
//        map["address_id"] = addressId
        map["payment_method"] = paymentType

        RetrofitClientSingleton.getInstance().doCheckoutPaytmPayment(map).enqueue(object :
            Callback<PaytmCheckoutModel> {
            override fun onFailure(call: Call<PaytmCheckoutModel>, t: Throwable) {
                CustomProgressbar.hideProgressBar()
                this@DeliveryPaymentActivity?.let {
                    Toast(
                        it,
                        getString(R.string.please_try_again)
                    )
                }
            }

            override fun onResponse(
                call: Call<PaytmCheckoutModel>,
                response: Response<PaytmCheckoutModel>
            ) {
                CustomProgressbar.hideProgressBar()
                /* if (response.isSuccessful) {
                     val responsedata = response.body() as CheckoutModel
                     if (responsedata.) {
                         activity?.let { Toast(it, responsedata.message)
                             mView.rlPyamentSucess.visibility=View.VISIBLE
                             mView.rlPayment.visibility=View.GONE

                         }



                     } else {
                         activity?.let { Toast(it, responsedata.message) }
                     }
                 } */
                if (response.isSuccessful) {
                    val homeScreenData = response.body()
                    when (response.body()!!.status) {
                        SUCCESS -> {
                            var dataresponse = response.body()!!.data
                            val i =
                                Intent(this@DeliveryPaymentActivity, PaymentActivity::class.java)
                            i.putExtra(keyOrderId, dataresponse.order_id.toString())
                            /* var strPayment=dataresponse.paytm_link+"?ORDER_ID="+dataresponse.order_id+
                                     "&CUST_ID="+dataresponse.user_id+"&TXN_AMOUNT="+ dataresponse.order_total
                             Log.e("payment link",strPayment)*/
                            i.putExtra("payment_url", dataresponse.paytm_link)
                            startActivity(i)
                            /*ORDER_ID=123&CUST_ID=1233&TXN_AMOUNT=50*/

                            this@DeliveryPaymentActivity?.let {
                                Toast(
                                    it,
                                    "" + response.body()!!.message
                                )
                            }
                            /*  mView.rlPyamentSucess.visibility=View.VISIBLE
                              mView.rlPayment.visibility=View.GONE
                              mView.txtOrderCode.text= homeScreenData!!.data.toString()*/
                        }
                        FAILURE -> {
                            this@DeliveryPaymentActivity?.let {
                                Toast(
                                    it,
                                    "" + response.body()!!.message
                                )
                            }
                        }
                        AUTH_FAILURE -> {
                        }

                    }
                } else {
                    this@DeliveryPaymentActivity?.let {
                        Toast(
                            it,
                            getString(R.string.please_try_again)
                        )
                    }
                }
            }

        })
    }

    private fun callPickupDropAddApi() {
        CustomProgressbar.showProgressBar(this, false)
        val map = HashMap<String, String>()
        map["user_id"] = user_id
        map["store"] = "ai baik"
        map["item_details"] = item_details
        map["pickup_address"] = pickup_address
        map["pickup_pincode"] = pickup_pincode
        map["pickup_lattitude"] = pickup_lattitude
        map["pickup_longitude"] = pickup_longitude
        map["pickup_contact_no"] = pickup_contact_no
        map["delivery_address"] = delivery_address
        map["delivery_pincode"] = delivery_pincode
        map["delivery_lattitude"] = delivery_lattitude
        map["delivery_longitude"] = delivery_longitude
        map["delivery_contact_no"] = delivery_contact_no
        map["from_to_distance"] = from_to_distance
        map["payment_method"] = payment_method
        map["km_amount"] = km_amount
        map["additional_amount"] = additional_amount
        map["total_amount"] = total_amount
        RetrofitClientSingleton.getInstance().PickupDropAddApi(map)
            .enqueue(object : retrofit2.Callback<DeliveryResponseModel> {
                override fun onFailure(call: Call<DeliveryResponseModel>, t: Throwable) {
                    Toast(this@DeliveryPaymentActivity, t.message)
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(
                    call: Call<DeliveryResponseModel>,
                    response: Response<DeliveryResponseModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    val DeliveryResponse = response.body()
                    if (DeliveryResponse != null) {
                        when (DeliveryResponse.getStatus()) {
                            SUCCESS -> {
                                val userBean = DeliveryResponse.getData()
                                userBean?.let {

                                }
                                Toast(
                                    this@DeliveryPaymentActivity,
                                    DeliveryResponse.getMessage()!!
                                )
                                val intent = Intent(applicationContext, PaymentActivity::class.java)
                                intent.putExtra(
                                    keyOrderId,
                                    DeliveryResponse.getData()?.getOrder_id()
                                )
                                intent.putExtra(
                                    "payment_url",
                                    DeliveryResponse.getData()?.getPaytm_link()
                                )
//                                Toast(this@DeliveryPaymentActivity, DeliveryResponse.getData()?.getOrder_id()!!+","+DeliveryResponse.getData()?.getOrder_id()!!)
                                startActivity(intent)
                            }
                            FAILURE -> {
                                Toast(this@DeliveryPaymentActivity, DeliveryResponse.getMessage()!!)
                            }
                            else -> {
                                //toast(this@LoginPage, R.string.something_went_wrong)
                            }
                        }
                    }
                }
            })
    }
}