package com.attune.zamy.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.util.Log
import android.view.InflateException
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.attune.zamy.R
import com.attune.zamy.custom.TouchableWrapper
import com.attune.zamy.utils.Toast
import com.attune.zamy.utils.showDialogPermissionRational
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.LatLng
import com.hopmeal.android.utils.REQUEST_LOCATION
import com.hopmeal.android.utils.keyLat
import com.hopmeal.android.utils.keyLng
import kotlinx.android.synthetic.main.activity_maps.*
import kotlinx.android.synthetic.main.layout_restaurant_detail_scroll.*

class MapsActivity : AppCompatActivity(),
    TouchableWrapper.UpdateMapAfterUserInteraction,
    View.OnClickListener,
    OnMapReadyCallback,
    GoogleMap.OnCameraMoveListener,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener {


    val TAG = javaClass.simpleName
    private var centerLocation = LatLng(0.0, 0.0)
    private val currentAddress = ""
    private var mMap: GoogleMap? = null
    private var zoomLevelOfMap = 18.0f

    private lateinit var mGoogleApiClient: GoogleApiClient

    private lateinit var locationManager: LocationManager
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationSettingsRequest: LocationSettingsRequest.Builder

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        /*setContentView(R.layout.activity_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
            .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)*/

        setContentView(R.layout.activity_maps)
        setSupportActionBar(toolbar1)
/*        supportActionBar!!.setHomeButtonEnabled(true)
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)*/
        btnConfirm.setOnClickListener(this)
        setMapFragment()
    }

    override fun onClick(v: View) {
            if (v.id == R.id.btnConfirm) {
                if (centerLocation != LatLng(0.0, 0.0)) {
                    val intent = Intent()
                    intent.putExtra(keyLat, centerLocation.latitude)
                    intent.putExtra(keyLng, centerLocation.longitude)
                    intent.putExtra("address", currentAddress)
                    setResult(Activity.RESULT_OK, intent)
                    finish()
                } else {
                    Toast(this, "Please add valid location")
                }
            }
    }

    private fun setMapFragment() {
        try {
            (supportFragmentManager.findFragmentById(R.id.map)
                    as SupportMapFragment).getMapAsync(this)
        } catch (e: InflateException) {
            Log.e(this.TAG, "setMapFragment$e")
        }
    }

    override fun onMapReady(googleMap: GoogleMap) {
        /*  mMap = googleMap

          // Add a marker in Sydney and move the camera
          val sydney = LatLng(-34.0, 151.0)
          mMap!!.addMarker(MarkerOptions().position(sydney).title("Marker in Sydney"))
          mMap!!.moveCamera(CameraUpdateFactory.newLatLng(sydney))*/

        try {
            mMap = googleMap
            startLocationUpdates()
//            checkLocationPermission()
        } catch (e: Exception) {
            Log.e(TAG, "onMapReady >> $e")
        }
        this.mMap!!.setOnCameraMoveListener(this@MapsActivity)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                checkLocationPermission()
                startLocationUpdates()
            } else {
                showDialogPermissionRational(this, "Go to settings and allow location permission")
            }
        }
    }

    private fun enableGps() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build()
        mGoogleApiClient.connect()

        initLocationSetting()
    }

    private fun initLocationSetting() {
        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = (1 * 1000).toLong()
        locationRequest.fastestInterval = (1 * 1000).toLong()

        locationSettingsRequest =
            LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        initResults()
    }

    override fun onUpdateMapAfterUserInteraction() {
        Log.d(
            TAG,
            "onUpdateMapAfterUserInteraction: centerLocation >> " + centerLocation.latitude + ", " + centerLocation.longitude
        )
    }

    override fun onCameraMove() {
        try {
            zoomLevelOfMap = mMap!!.cameraPosition.zoom
            centerLocation = mMap!!.cameraPosition.target

//            btnConfirm.visibility = View.GONE
//            tvAddress.visibility = View.GONE
        } catch (e: Exception) {
            Log.e(TAG, "onCameraMove $e")
        }
    }

    override fun onConnected(p0: Bundle?) {

    }

    override fun onConnectionSuspended(p0: Int) {

    }

    override fun onConnectionFailed(p0: ConnectionResult) {

    }

    private fun initResults() {
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 1000
        locationRequest.fastestInterval = 1000
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
        val result = LocationServices.getSettingsClient(this)
            .checkLocationSettings(builder.build())
        result.addOnCompleteListener { task ->
            try {
                val response = task.getResult(ApiException::class.java)
                //request for locations

            } catch (exception: ApiException) {
                when (exception.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                        try {
                            // Cast to a resolvable exception.
                            val resolvable = exception as ResolvableApiException
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(this@MapsActivity, REQUEST_LOCATION)
                        } catch (e: IntentSender.SendIntentException) {
                            // Ignore the error.
                        } catch (e: ClassCastException) {
                            // Ignore, should be an impossible error.
                        }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }// Location settings are not satisfied. However, we have no way to fix the
                // settings so we won't show the dialog.
            }
        }
    }

    private fun startLocationUpdates() {

        val permissions = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        // Create the location request to start receiving updates
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
//        mLocationRequest.interval = UPDATE_INTERVAL
//        mLocationRequest.fastestInterval = FASTEST_INTERVAL

        mLocationRequest.interval = (1 * 1000).toLong()
        mLocationRequest.fastestInterval = (1 * 1000).toLong()
        // Create LocationSettingsRequest object using location request
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        val locationSettingsRequest = builder.build()

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        val settingsClient = LocationServices.getSettingsClient(this)
        settingsClient.checkLocationSettings(locationSettingsRequest)

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // new Google API SDK v11 uses getFusedLocationProviderClient(this)
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(this, permissions, REQUEST_LOCATION)
            } else {
                if (mMap != null) {
                    mMap!!.isMyLocationEnabled = true
                    mMap!!.uiSettings.isMyLocationButtonEnabled = true
                }
                LocationServices.getFusedLocationProviderClient(this)
                    .requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper())
            }
        } else {
            enableGps()
        }

    }

    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
//            super.onLocationResult(locationResult)

            for (location in locationResult!!.locations) {
                Log.d(TAG, "Location: " + location.latitude + " " + location.longitude)
                centerLocation = LatLng(location.latitude, location.longitude)
                val cameraUpdate = CameraUpdateFactory.newLatLngZoom(centerLocation, zoomLevelOfMap)
                mMap!!.moveCamera(CameraUpdateFactory.newLatLng(centerLocation))
                mMap!!.animateCamera(cameraUpdate)

//                onUpdateMapAfterUserInteraction()

                removeLocationUpdate()
                return
            }
        }
    }

    private fun removeLocationUpdate() {
        LocationServices.getFusedLocationProviderClient(this@MapsActivity)
            .removeLocationUpdates(mLocationCallback)
    }


    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val states = LocationSettingsStates.fromIntent(data)
        when (requestCode) {
            REQUEST_LOCATION -> when (resultCode) {
                Activity.RESULT_OK -> {
//                    if (mMap != null)
//                        checkLocationPermission()
                    startLocationUpdates()
                    // All required changes were successfully made
//                    startLocationService()
                }
                Activity.RESULT_CANCELED ->
                    finish()
                else -> {
                }
            }
        }
    }
}
