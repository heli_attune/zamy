package com.attune.zamy.activity

import android.Manifest
import android.annotation.SuppressLint
import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Address
import android.location.Geocoder
import android.location.Location
import android.location.LocationManager
import android.os.Bundle
import android.os.Looper
import android.text.TextUtils
import android.util.Log
import android.view.InflateException
import android.view.View
import android.widget.AdapterView
import android.widget.Spinner
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.attune.zamy.R
import com.attune.zamy.adapter.SpinnerProductCategoryAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.custom.TouchableWrapper
import com.attune.zamy.model.AddAddressModel
import com.attune.zamy.model.GetAddressModel
import com.attune.zamy.model.ShippingAreaModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.google.android.gms.common.ConnectionResult
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.maps.CameraUpdateFactory
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.OnMapReadyCallback
import com.google.android.gms.maps.SupportMapFragment
import com.google.android.gms.maps.model.BitmapDescriptorFactory
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import com.google.android.gms.maps.model.MarkerOptions
import com.hopmeal.android.utils.REQUEST_LOCATION
import kotlinx.android.synthetic.main.activity_address.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set


class AddAddressActivity : AppCompatActivity(),
    TouchableWrapper.UpdateMapAfterUserInteraction,
    OnMapReadyCallback,
    GoogleMap.OnCameraMoveListener,
    GoogleMap.OnMarkerClickListener,
    GoogleApiClient.ConnectionCallbacks,
    GoogleApiClient.OnConnectionFailedListener,
    LocationListener {
    lateinit var firstname: String
    lateinit var telephone: String
    lateinit var address1: String
    lateinit var address2: String
    var city: String = "Ahmedabad"
    var state: String = "Gujarat"
    var country: String = "India"
    lateinit var postcode: String
    lateinit var strEmail: String
    var shippingLat = 0.0
    var shippingLng = 0.0
    var shippingArea: String? = null
    lateinit var userId: String
    var isEdit: Boolean = false
    var addID: String = ""
    lateinit var mAddressObject: GetAddressModel.DataBean.AddressBookBean

    var centerLocation = LatLng(0.0, 0.0)
    private val currentAddress = ""
    private var mMap: GoogleMap? = null
    private var zoomLevelOfMap = 18.0f

    private lateinit var mGoogleApiClient: GoogleApiClient


    private lateinit var locationManager: LocationManager
    private lateinit var locationRequest: LocationRequest
    private lateinit var locationSettingsRequest: LocationSettingsRequest.Builder


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        setContentView(R.layout.activity_address)
        userId = SharedPref.getUserId(this)
        setMapFragment()
        rbHome.isChecked = true
        customtoolbar.imgback.setOnClickListener {
            finish()
        }

        if (intent.extras!!["edit"] == 1) {
            customtoolbar.txtTitle.text = "Edit Address"
            isEdit = true
            val get = intent.getSerializableExtra("AddressModel")
            Log.e("TAG", "AddressModel$get")
            mAddressObject = get as GetAddressModel.DataBean.AddressBookBean
            etProfileFullName.setText(mAddressObject.name)
            edAddress1.setText(mAddressObject.address_1)
            edAddress2.setText(mAddressObject.address_2)
            edCity.setText(mAddressObject.city)
            edState.setText(mAddressObject.state)
            edCountry.setText(mAddressObject.country)
            edPincode.setText(mAddressObject.postcode)
            edProfileMobile.setText(mAddressObject.phone)
            edProfileEmail.setText(mAddressObject.email)
            addID = mAddressObject.id.toString()
            Log.e("TAG", "shipping_area" + mAddressObject.shipping_area)
            Log.e("TAG", "id" + mAddressObject.id)

            when {
                mAddressObject.address_type == "home" -> rbHome.isChecked = true
                mAddressObject.address_type == "work" -> rbWork.isChecked = true
                mAddressObject.address_type == "other" -> rbOther.isChecked = true
            }
            initView()

        } else if (intent.extras!!["add"] == 0) {
            //Toast(this, "No Edit")
            isEdit = false
            initView()
            customtoolbar.txtTitle.text = "Add Address"
        }


    }


    override fun onResume() {
        super.onResume()
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            ActivityCompat.requestPermissions(
                this,
                arrayOf(Manifest.permission.ACCESS_FINE_LOCATION),
                PERMISSION_REQUEST_ACCESS_FINE_LOCATION
            )

        }
    }


    fun initView() {
        btnSaveAddress.setOnClickListener {
            checkValidations()
        }

        btnConfirm.setOnClickListener {
            if (centerLocation != LatLng(0.0, 0.0)) {
                shippingLat = centerLocation.latitude
                shippingLng = centerLocation.longitude
                Log.e("TAG", "Confirm:---------${shippingLat}  ${shippingLng}")
            } else {
                Toast(this, "Please add valid location")
            }
        }
        /*imgLocation.setOnClickListener {
            val intent = Intent(this, MapsActivity::class.java)
            startActivityForResult(intent, ADD_LOCATION)
        }*/

        if (isNetworkAvailable(this)) {
            getAreaList()
        }
        edCity.isEnabled = false
        edCountry.isEnabled = false
        edState.isEnabled = false
        edPincode.isEnabled = false
    }

    private fun getAreaList() {
        CustomProgressbar.showProgressBar(this, false)
        RetrofitClientSingleton
            .getInstance()
            .getShippingArea()
            .enqueue(object : retrofit2.Callback<ShippingAreaModel> {
                override fun onFailure(call: Call<ShippingAreaModel>, t: Throwable) {
                    Log.e("TAG", t.message)
                }

                override fun onResponse(
                    call: Call<ShippingAreaModel>,
                    response: Response<ShippingAreaModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    val shippingAreaList = response.body()!!.data
                    if (shippingAreaList != null) {
                        Log.e("TAG", "$shippingArea")
                        if (response.isSuccessful) {
                            when (response.body()!!.status) {

                                SUCCESS -> {
                                    val adapter = SpinnerProductCategoryAdapter(
                                        this@AddAddressActivity,
                                        shippingAreaList as ArrayList<ShippingAreaModel.DataBean>
                                    )
                                    spShippingArea.adapter = adapter


                                    /**Set item at position using predefine**/
                                    if (isEdit) {
                                        spShippingArea.setSelection(
                                            getIndex(
                                                spShippingArea,
                                                mAddressObject.shipping_area.toString(),
                                                shippingAreaList
                                            )
                                        )
                                    }
                                    /**Set spinner item **/
                                    spShippingArea.onItemSelectedListener =
                                        object : AdapterView.OnItemSelectedListener {
                                            override fun onNothingSelected(parent: AdapterView<*>?) {

                                            }

                                            override fun onItemSelected(
                                                parent: AdapterView<*>?,
                                                view: View?,
                                                position: Int,
                                                id: Long
                                            ) {
                                                shippingArea =
                                                    shippingAreaList[position].area.toString()
                                                val city = shippingAreaList[position].city
                                                val state = shippingAreaList[position].state
                                                val country = shippingAreaList[position].country
                                                val pincode = shippingAreaList[position].zipcode
                                                edCity.setText(city)
                                                edState.setText(state)
                                                edCountry.setText(country)
                                                edPincode.setText(pincode)


                                            }

                                        }


                                }
                                FAILURE -> {

                                }
                                AUTH_FAILURE -> {

                                }
                            }
                        }
                    } else {
                        Toast(this@AddAddressActivity, getString(R.string.something_went_wrong))
                    }
                }

            })
    }

    /**Set item at position in spinner**/
    fun getIndex(
        spinner: Spinner,
        myString: String,
        shippingAreaList: ArrayList<ShippingAreaModel.DataBean>
    ): Int {
        for (i in 0 until shippingAreaList.size) {
            val databean = spinner.getItemAtPosition(i) as ShippingAreaModel.DataBean
            if (databean.area.equals(myString, ignoreCase = true)) {
                return i
            }
        }
        return 0
    }


    /**Validation++-++-**/
    private fun checkValidations() {
        if (etProfileFullName.text.toString().trim().isEmpty()) {
            Toast(this, getString(R.string.enter_first_name))
        } else if (edProfileMobile.text!!.isEmpty()) {
            Toast(this, "Please enter mobile")
        } else if (edProfileMobile.text!!.length < 10) {
            Toast(this, "Please enter valid mobile")
        } /*else if (edProfileEmail.text!!.isEmpty()) {
            Toast(this, "Please enter email")
        } else if (!isValidMail(edProfileEmail.text.toString().trim())) {
            Toast(this, getString(R.string.valid_email))
        }*/ else if (edAddress1.text.toString().trim().isEmpty()) {
            Toast(this, getString(R.string.enter_street))
        } else if (edAddress2.text.toString().trim().isEmpty()) {
            Toast(this, getString(R.string.enter_street1))
        } else if (edPincode.text!!.length < 6) {
            Toast(this, "Please enter valid pincode")
        } else {
            if (!isNetworkAvailable(this)) {
                Toast(this, getString(R.string.network_error))
            } else {
                firstname = etProfileFullName.text.toString()
                address1 = edAddress1.text.toString()
                address2 = edAddress2.text.toString()
                postcode = edPincode.text.toString()
                telephone = edProfileMobile.text.toString()
                strEmail = edProfileEmail.text.toString()

                if (addID == "") {
                    if (shippingLat != 0.0 && shippingLng != 0.0) {
                        addShippingAddress()
                    } else {
                        Toast(this, "Please Confirm Location")
                    }

                } else {
                    editShippingAddress(
                        mAddressObject.id!!,
                        mAddressObject.address_lat,
                        mAddressObject.address_lng
                    )
                }
            }
        }
    }

    private fun editShippingAddress(id: String, addressLat: String?, addressLng: String?) {
        CustomProgressbar.showProgressBar(this, false)
        val tag = when {
            rbHome.isChecked -> "home"
            rbWork.isChecked -> "work"
            else -> "other"
        }
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(this)
        map["name"] = firstname
        map["address_1"] = address1
        map["address_2"] = address2
        map["city"] = city
        map["state"] = state
        map["shipping_area"] = shippingArea!!
        map["postcode"] = postcode
        map["country"] = country
        map["phone"] = telephone
        map["email"] = " "
        map["address_lat"] = addressLat.toString()
        map["address_lng"] = addressLng.toString()
        map["address_type"] = tag
        map["address_id"] = id



        RetrofitClientSingleton
            .getInstance()
            .addAddres(map)
            .enqueue(object : retrofit2.Callback<AddAddressModel> {
                override fun onFailure(call: Call<AddAddressModel>, t: Throwable) {
                    Toast(this@AddAddressActivity, t.message!!)
                }

                override fun onResponse(
                    call: Call<AddAddressModel>,
                    response: Response<AddAddressModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val successModel = response.body()
                        when (response.body()?.status) {
                            SUCCESS -> {
                                Toast(this@AddAddressActivity, successModel!!.message!!)
                                finish()
                            }
                            FAILURE -> {
                                Toast(this@AddAddressActivity, successModel!!.message!!)
                            }
                            /*AUTH_FAILURE -> {
                                forceLogout(this@AddAddressActivity)
                            }*/
                        }
                    }
                }

            })
    }

    private fun addShippingAddress() {
        CustomProgressbar.showProgressBar(this, false)
        val tag = when {
            rbHome.isChecked -> "home"
            rbWork.isChecked -> "work"
            else -> "other"
        }
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(this)
        map["name"] = firstname
        map["address_1"] = address1
        map["address_2"] = address2
        map["city"] = city
        map["state"] = state
        map["shipping_area"] = shippingArea!!
        map["postcode"] = postcode
        map["country"] = country
        map["phone"] = telephone
        map["email"] = " "
        map["address_lat"] = shippingLat.toString()
        map["address_lng"] = shippingLng.toString()
        map["address_type"] = tag


        RetrofitClientSingleton
            .getInstance()
            .addAddres(map)
            .enqueue(object : retrofit2.Callback<AddAddressModel> {
                override fun onFailure(call: Call<AddAddressModel>, t: Throwable) {
                    Toast(this@AddAddressActivity, t.message!!)
                }

                override fun onResponse(
                    call: Call<AddAddressModel>,
                    response: Response<AddAddressModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val successModel = response.body()
                        when (response.body()?.status) {
                            SUCCESS -> {
                                Toast(this@AddAddressActivity, successModel!!.message!!)
                                finish()
                            }
                            FAILURE -> {
                                Toast(this@AddAddressActivity, successModel!!.message!!)
                            }
                            /*AUTH_FAILURE -> {
                                forceLogout(this@AddAddressActivity)
                            }*/
                        }
                    }
                }

            })
    }

    @SuppressLint("MissingSuperCall")
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        val states = LocationSettingsStates.fromIntent(data)
        when (requestCode) {
            REQUEST_LOCATION -> when (resultCode) {
                Activity.RESULT_OK -> {
//                    if (mMap != null)
//                        checkLocationPermission()
                    startLocationUpdates()
                    // All required changes were successfully made
//                    startLocationService()
                }
                Activity.RESULT_CANCELED ->
                    finish()
                else -> {
                }
            }
        }
    }

    fun isValidMail(mailString: String): Boolean {
        return !TextUtils.isEmpty(mailString) && android.util.Patterns.EMAIL_ADDRESS.matcher(
            mailString
        ).matches()
    }


    //map

    private fun setMapFragment() {
        try {
            (supportFragmentManager.findFragmentById(R.id.map)
                    as SupportMapFragment).getMapAsync(this)
        } catch (e: InflateException) {
            Log.e("TAG", "setMapFragment$e")
        }
    }


    override fun onUpdateMapAfterUserInteraction() {
        Log.d(
            "TAG",
            "onUpdateMapAfterUserInteraction: centerLocation >> " + centerLocation.latitude + ", " + centerLocation.longitude
        )
    }

    override fun onMapReady(googleMap: GoogleMap) {
        try {
            mMap = googleMap
            mMap!!.addMarker(
                MarkerOptions()
                    .position(LatLng(centerLocation.latitude, centerLocation.longitude))
                    .title("Marker")
                    .draggable(true)
                    .snippet("Hello")
                    .icon(
                        BitmapDescriptorFactory
                            .defaultMarker(BitmapDescriptorFactory.HUE_RED)
                    )
            )
            mMap!!.setOnMarkerDragListener(object : GoogleMap.OnMarkerDragListener {
                override fun onMarkerDragEnd(marker: Marker?) {
                    var geocoder: Geocoder = Geocoder(this@AddAddressActivity)

                    var lat1 = marker!!.position
                    var addreList: List<Address>? = null

                    try {
                        addreList = geocoder.getFromLocation(lat1.latitude, lat1.longitude, 1)
                        val mAddress = addreList[0]
                        marker.title = mAddress.locality

                    } catch (e: Exception) {
                        Log.e("TAG", e.message)
                    }

                    Log.d(
                        "System out",
                        "onMarkerDragEnd..." + marker.position.latitude + "..." + marker.position.longitude
                    )

                }

                override fun onMarkerDragStart(p0: Marker?) {
                    Log.d(
                        "System out",
                        "onMarkerDragStart..." + p0!!.position.latitude + "..." + p0.position.longitude
                    )
                }

                override fun onMarkerDrag(p0: Marker?) {
                    Log.d(
                        "System out",
                        "onMarkerDragDrag..." + p0!!.position.latitude + "..." + p0.position.longitude
                    )
                }

            })
            mMap!!.setOnMarkerClickListener(this@AddAddressActivity)
            startLocationUpdates()


//            checkLocationPermission()
        } catch (e: Exception) {
            Log.e("TAG", "onMapReady >> $e")
        }
        this.mMap!!.setOnCameraMoveListener(this@AddAddressActivity)
    }

    override fun onRequestPermissionsResult(
        requestCode: Int, permissions: Array<String>,
        grantResults: IntArray
    ) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                checkLocationPermission()
                startLocationUpdates()
            } else {
                showDialogPermissionRational(this, "Go to settings and allow location permission")
            }
        }
    }

    private fun enableGps() {
        mGoogleApiClient = GoogleApiClient.Builder(this)
            .addApi(LocationServices.API)
            .addConnectionCallbacks(this)
            .addOnConnectionFailedListener(this)
            .build()
        mGoogleApiClient.connect()

        initLocationSetting()
    }

    private fun initLocationSetting() {
        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = (1 * 1000).toLong()
        locationRequest.fastestInterval = (1 * 1000).toLong()

        locationSettingsRequest =
            LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        initResults()
    }

    private fun initResults() {
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 1000
        locationRequest.fastestInterval = 1000
        val builder = LocationSettingsRequest.Builder()
            .addLocationRequest(locationRequest)
        val result = LocationServices.getSettingsClient(this)
            .checkLocationSettings(builder.build())
        result.addOnCompleteListener { task ->
            try {
                val response = task.getResult(ApiException::class.java)
                //request for locations

            } catch (exception: ApiException) {
                when (exception.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                        try {
                            // Cast to a resolvable exception.
                            val resolvable = exception as ResolvableApiException
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(
                                this@AddAddressActivity,
                                REQUEST_LOCATION
                            )
                        } catch (e: IntentSender.SendIntentException) {
                            // Ignore the error.
                        } catch (e: ClassCastException) {
                            // Ignore, should be an impossible error.
                        }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }// Location settings are not satisfied. However, we have no way to fix the
                // settings so we won't show the dialog.
            }
        }
    }


    private fun startLocationUpdates() {

        val permissions = arrayOf(
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION
        )

        // Create the location request to start receiving updates
        val mLocationRequest = LocationRequest()
        mLocationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
//        mLocationRequest.interval = UPDATE_INTERVAL
//        mLocationRequest.fastestInterval = FASTEST_INTERVAL

        mLocationRequest.interval = (1 * 1000).toLong()
        mLocationRequest.fastestInterval = (1 * 1000).toLong()
        // Create LocationSettingsRequest object using location request
        val builder = LocationSettingsRequest.Builder()
        builder.addLocationRequest(mLocationRequest)
        val locationSettingsRequest = builder.build()

        // Check whether location settings are satisfied
        // https://developers.google.com/android/reference/com/google/android/gms/location/SettingsClient
        val settingsClient = LocationServices.getSettingsClient(this)
        settingsClient.checkLocationSettings(locationSettingsRequest)

        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager

        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // new Google API SDK v11 uses getFusedLocationProviderClient(this)
            if (ContextCompat.checkSelfPermission(
                    this,
                    Manifest.permission.ACCESS_FINE_LOCATION
                ) != PackageManager.PERMISSION_GRANTED
            ) {
                ActivityCompat.requestPermissions(this, permissions, REQUEST_LOCATION)
            } else {
                if (mMap != null) {
                    mMap!!.isMyLocationEnabled = true
                    mMap!!.uiSettings.isMyLocationButtonEnabled = true
                }
                LocationServices.getFusedLocationProviderClient(this)
                    .requestLocationUpdates(mLocationRequest, mLocationCallback, Looper.myLooper())
            }
        } else {
            enableGps()
        }

    }

    //get currant location
    private val mLocationCallback = object : LocationCallback() {
        override fun onLocationResult(locationResult: LocationResult?) {
//            super.onLocationResult(locationResult)

            for (location in locationResult!!.locations) {
                Log.d("TAG", "Location: " + location.latitude + " " + location.longitude)
                centerLocation = LatLng(location.latitude, location.longitude)
                val cameraUpdate = CameraUpdateFactory.newLatLngZoom(centerLocation, zoomLevelOfMap)
                mMap!!.moveCamera(CameraUpdateFactory.newLatLng(centerLocation))
                mMap!!.animateCamera(cameraUpdate)

                onUpdateMapAfterUserInteraction()
                removeLocationUpdate()
                return
            }
        }
    }

    private fun removeLocationUpdate() {
        try {
            LocationServices.getFusedLocationProviderClient(this@AddAddressActivity)
                .removeLocationUpdates(mLocationCallback)
        } catch (e: java.lang.Exception) {

        }

    }

    override fun onCameraMove() {
        try {
            zoomLevelOfMap = mMap!!.cameraPosition.zoom
            centerLocation = mMap!!.cameraPosition.target
        } catch (e: Exception) {
            Log.e("TAG", "onCameraMove $e")
        }
    }


    override fun onConnected(p0: Bundle?) {

    }

    override fun onConnectionSuspended(p0: Int) {
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    override fun onLocationChanged(location: Location?) {
        Log.e("TAG", "${location!!.latitude} ${location.longitude}")

    }

    companion object {
        private const val PERMISSION_REQUEST_ACCESS_FINE_LOCATION = 100
    }

    override fun onMarkerClick(p0: Marker?): Boolean {
        return true
    }

    /* override fun onMarkerDragEnd(marker: Marker?) {
         *//*marker!!.title = marker.position.toString()
        marker.showInfoWindow()
        marker.alpha = 0.5f*//*

    }

    override fun onMarkerDragStart(marker: Marker?) {


    }

    override fun onMarkerDrag(marker: Marker?) {

    }*/
}
