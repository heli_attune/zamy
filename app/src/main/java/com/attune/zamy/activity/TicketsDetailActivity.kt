package com.attune.zamy.activity

import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.attune.zamy.R
import com.attune.zamy.adapter.TicketsConversionAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.SuccessModelData
import com.attune.zamy.model.TicketListingConversationModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.activity_ticket_detail.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap


class TicketsDetailActivity : AppCompatActivity(), View.OnClickListener {

    private val TAG = TicketsDetailActivity::class.java.simpleName
    private var orderId: String = ""
    private var suborderId: String = ""
    private var ticket_status: String = ""
    private var chat_id: String = ""
    private var ticketsConversion = ArrayList<TicketListingConversationModel.DataBean>()
    private var linearLayoutManager: LinearLayoutManager? = null
    private var mAdapter: TicketsConversionAdapter? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_ticket_detail)

        /*setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)*/
        setView()
    }

    private fun setView() {
        orderId = intent.extras!!.getString(keyOrderId).toString()
        chat_id = intent.extras!!.getString("chat_id").toString()
        ticket_status = intent.extras!!.getString("ticket_status").toString()
        if (ticket_status.equals("Close")) {
            etMessage.isFocusable = false
            etMessage.isFocusableInTouchMode = false // user touches widget on phone with touch screen
            etMessage.isClickable = false
            etMessage.keyListener = null

            etMessage.setOnClickListener {

                Toast(this, getString(R.string.closed_ticket_reply))
            }
        }
     //   supportActionBar?.title = "Ticket No $chat_id"
        imgback.setOnClickListener {
            onBackPressed()
        }
        txtTitle.text="Ticket No $chat_id"

        rlSend.setOnClickListener(this)

        linearLayoutManager = LinearLayoutManager(this)
        lv_conversations.layoutManager = linearLayoutManager
        lv_conversations.setHasFixedSize(true)

        mAdapter = TicketsConversionAdapter(this)
        lv_conversations.adapter = mAdapter
        if (isNetworkAvailable(this)) {
            getChatConversation()
        } else {
            Toast(this,getString(R.string.network_error))
        }

        swipeContainerTicketDetail!!.setOnRefreshListener {
            // Your code to refresh the list here.
            // Make sure you call swipeContainer.setRefreshing(false)
            // once the network request has completed successfully.

            if (isNetworkAvailable(this)) {
                getChatConversation()
            } else {
                Toast(this,getString(R.string.network_error))
            }

            swipeContainerTicketDetail!!.isRefreshing = false
        }


        rel_mail.addSoftKeyboardLsner(object : SoftKeyboardLsnedRelativeLayout.SoftKeyboardLsner {
            override fun onSoftKeyboardShow() {
                Log.d("SoftKeyboard", "Soft keyboard shown")
                lv_conversations.scrollToPosition(ticketsConversion.size - 1)


            }

            override fun onSoftKeyboardHide() {
                Log.d("SoftKeyboard", "Soft keyboard hidden")


            }
        })
    }

    private fun getChatConversation() {
        CustomProgressbar.showProgressBar(this
            , false)
        val map = HashMap<String, String>()
       // map["user_id"] = SharedPref.getUserId(this)
        map["ticketChatID"] = chat_id

        RetrofitClientSingleton.getInstance().getcustomerConversation(map)
                .enqueue(object : retrofit2.Callback<TicketListingConversationModel> {
                    override fun onFailure(call: Call<TicketListingConversationModel>?, t: Throwable?) {
                        CustomProgressbar.hideProgressBar()
                        Toast(this@TicketsDetailActivity,getString(R.string.something_went_wrong))
                    }

                    override fun onResponse(call: Call<TicketListingConversationModel>?, response: Response<TicketListingConversationModel>?) {
                        CustomProgressbar.hideProgressBar()
                        if (response!!.isSuccessful) {
                            when (response.body()!!.status) {
                                SUCCESS -> {
                                    ticketsConversion = (response.body()!!.data as ArrayList<TicketListingConversationModel.DataBean>?)!!
                                    setAdapter()
                                }
                                FAILURE -> {
                                    Toast(this@TicketsDetailActivity, response.body()!!.message)
                                }
                                AUTH_FAILURE -> {
                                   // forceLogout(this@TicketsDetailActivity)
                                }
                            }
                        } else {
                            Toast(this@TicketsDetailActivity,getString(R.string.something_went_wrong))
                        }
                    }

                })
    }

    private fun setAdapter() {
        mAdapter!!.setList(ticketsConversion)
        lv_conversations.scrollToPosition(ticketsConversion.size - 1)
    }



    override fun onClick(v: View) {
        when (v.id) {
            R.id.rlSend -> {
                if (etMessage.text.toString().trim().isNotEmpty()) {
                    sendMessage()
                } else {
                }

            }
        }
    }

    private fun sendMessage() {
        replyToTicket(etMessage.text.toString())


        val obj = TicketListingConversationModel.DataBean()
        obj.ticketBody = etMessage.text.toString()
        obj.ticketSenderID = "2"
        obj.ticketDateTime = "Just now"
        ticketsConversion.add(obj)
        etMessage.setText("")
        setAdapter()

    }

    private fun replyToTicket(msg: String) {
        val map = HashMap<String, String>()
        map["ticketSenderID"] = SharedPref.getUserId(this)
        map["ticketChatID"] = chat_id
        map["description"] = msg

        RetrofitClientSingleton.getInstance().replyTicket(map)
                .enqueue(object : retrofit2.Callback<SuccessModelData> {
                    override fun onFailure(call: Call<SuccessModelData>?, t: Throwable?) {
                        Toast(this@TicketsDetailActivity,getString(R.string.something_went_wrong))
                    }

                    override fun onResponse(call: Call<SuccessModelData>?, response: Response<SuccessModelData>?) {
                        if (response?.isSuccessful!!) {
                            when (response.body()!!.success) {
                                SUCCESS -> {
                                    Log.d(TAG, response.body()!!.message)

                                }
                                FAILURE -> {
                                    Log.d(TAG, response.body()!!.message)
                                }
                                AUTH_FAILURE -> {
                                   // forceLogout(this@TicketsDetailActivity)
                                }
                            }
                        }
                    }

                })
    }
}
