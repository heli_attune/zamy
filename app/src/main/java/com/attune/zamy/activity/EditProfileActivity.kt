package com.attune.zamy.activity

import android.Manifest
import android.app.Activity
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.content.pm.PackageManager
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Environment
import android.provider.MediaStore
import android.util.Log
import android.view.View
import android.view.Window
import androidx.appcompat.app.AppCompatActivity
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.core.widget.doOnTextChanged
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.ProfileModel
import com.attune.zamy.model.ProfileUpdateResponse
import com.attune.zamy.model.SuccessModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import com.hopmeal.android.utils.*
import com.theartofdev.edmodo.cropper.CropImage
import com.theartofdev.edmodo.cropper.CropImageView
import kotlinx.android.synthetic.main.activity_edit_profile.*
import kotlinx.android.synthetic.main.dialog_choose_image.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.Response
import java.io.File

class EditProfileActivity : AppCompatActivity() {


    private lateinit var strName: String
    private lateinit var strMobile: String
    private lateinit var strReferBy: String
    private lateinit var strEmail: String
    private var strOldPassword: String = ""
    private var strNewPassword: String = ""
    val context: Context? = null
    private var mImageFile: File? = null
    var profileImageUri = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        setContentView(R.layout.activity_edit_profile)

        customtoolbar.txtTitle.text = "Edit Profile"

        customtoolbar.imgback.setOnClickListener {
            finish()
        }
        etProfileEmail.isEnabled = false




        initLoading()
    }

    private fun initLoading() {
        getUserProfileApi(true)
        cbPassword.setOnCheckedChangeListener { _, _ ->
            if (llPassword.isShown) {
                llPassword.visibility = View.GONE
            } else {
                llPassword.visibility = View.VISIBLE
            }
        }
        image_account.setOnClickListener {
            checkCameraAndStoragePermissions()
        }

        imgCamera.setOnClickListener {
            checkCameraAndStoragePermissions()
        }
        btnUpdateProfile.setOnClickListener {
            if (isNetworkAvailable(this)) {
                updateProfileValidation()
            }
        }

        textInputLayoutMobile.editText?.doOnTextChanged { text, start, count, after ->
            if (text!!.length < 10) {
                textInputLayoutMobile.error = getString(R.string.enter_valid_mobile)
            } else {
                textInputLayoutMobile.error = ""
                textInputLayoutMobile.setEndIconDrawable(R.drawable.ic_check_green)
                textInputLayoutMobile.boxStrokeColor = ContextCompat.getColor(this, R.color.green)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    textInputLayoutMobile.hintTextColor = getColorStateList(R.color.green)
                }
            }
        }
        textInputLayoutName.editText?.doOnTextChanged { text, start, count, after ->
            if (text!!.isEmpty()) {
                textInputLayoutName.error = getString(R.string.enter_full_name)
            } else {
                textInputLayoutName.error = ""
            }
        }

        tilCurrentPassword.editText?.doOnTextChanged { text, start, count, after ->
            if (text!!.isEmpty()) {
                tilCurrentPassword.error = getString(R.string.please_enter_password)
            }
            if (text.length < 6) {
                tilCurrentPassword.error = "Password must be 6 characters"
            }
            if (text.length > 7) {

            }
        }
    }


    /**Update Validation**/
    private fun updateProfileValidation() {

        if (etProfileName.text.toString().trim().isEmpty()) {
            //Toast(this@EditProfileActivity, getString(R.string.enter_first_name))
            textInputLayoutName.error = getString(R.string.please_enter_username)
            return
        } else if (!isValidText(etProfileName.text.toString().trim())) {
            textInputLayoutName.error = getString(R.string.enter_valid_first_name)
            return
        } else if (etProfileMobile.text.toString().trim().length < 10) {
            textInputLayoutMobile.error = getString(R.string.enter_valid_mobile)
            return
        } else if (llPassword.isShown) {

            when {
                edCurrentPassword.text.toString().trim().isEmpty() -> {
                    tilCurrentPassword.error = getString(R.string.enter_current_pwd)
                    return
                }
                edNewPassword.text.toString().trim().isEmpty() -> {
                    tilNewPassword.error = getString(R.string.enter_new_pwd)
                    return
                }
                edNewPassword.text.toString().trim().length < 6 -> {
                    tilNewPassword.error = getString(R.string.enterpass_lentgh)
                    return
                }
                edConfirmPassword.text.toString().trim().isEmpty() -> {
                    tilConfirmPassword.error = getString(R.string.enter_confirm_pwd)
                    return
                }
                edNewPassword.text.toString().trim() != edConfirmPassword.text.toString()
                    .trim() -> {
                    tilNewPassword.error = getString(R.string.change_pwd_desnt_match)
                    tilConfirmPassword.error = getString(R.string.change_pwd_desnt_match)
                    return
                }
            }
        }

        strName = etProfileName.text.toString()
        strEmail = etProfileEmail.text.toString()
        strMobile = etProfileMobile.text.toString()
        strReferBy = edReferalCode.text.toString()

        if (llPassword.isShown) {
            strOldPassword = edCurrentPassword.text.toString()
            strNewPassword = edNewPassword.text.toString()
        } else {
            strOldPassword = ""
            strNewPassword = ""
        }

        if (!isNetworkAvailable(this@EditProfileActivity)) {
            Toast(this@EditProfileActivity, getString(R.string.network_error))
        } else {

            Log.e("TAG", "Update Profile")
            updateProfile()
        }
    }


    private fun updateProfile() {

        CustomProgressbar.showProgressBar(this@EditProfileActivity, false)
        val map = HashMap<String, RequestBody>()
        map["user_id"] = getStringRequestBody(SharedPref.getUserId(this@EditProfileActivity))
        map["name"] = getStringRequestBody(strName)
        map["phone"] = getStringRequestBody(strMobile)
        map["email"] = getStringRequestBody(strEmail)
        map["old_password"] = getStringRequestBody(strOldPassword)
        map["password"] = getStringRequestBody(strNewPassword)
        if (edReferalCode.isEnabled) {
            map["refferal_by"] = getStringRequestBody(strReferBy)
        } else {

            map["refferal_by"] = getStringRequestBody("")
        }

        if (mImageFile != null)
            map["profile\"; filename=\"" + mImageFile!!.name + "\""] =
                getImageRequestBody(mImageFile!!)

        RetrofitClientSingleton
            .getInstance()
            .UpdateUserProfile(map)
            .enqueue(object : retrofit2.Callback<ProfileUpdateResponse> {
                override fun onFailure(call: Call<ProfileUpdateResponse>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("TAG", t.message)

                }

                override fun onResponse(
                    call: Call<ProfileUpdateResponse>,
                    response: Response<ProfileUpdateResponse>
                ) {
                    CustomProgressbar.hideProgressBar()
                    val successModel = response.body()
                    when (response.body()?.status) {
                        SUCCESS -> {
                            SharedPref.setStringValue(
                                this@EditProfileActivity,
                                keyFirstName,
                                strName
                            )

                            SharedPref.setStringValue(
                                this@EditProfileActivity,
                                keyEmail,
                                strEmail
                            )
                            SharedPref.setStringValue(
                                this@EditProfileActivity,
                                keyMobile,
                                strMobile
                            )
                            SharedPref.setStringValue(this@EditProfileActivity, keyEmail, strEmail)
                            updateData()
                            Toast(this@EditProfileActivity, successModel!!.message!!)
                            finish()
                        }
                        FAILURE -> {
                            Toast(this@EditProfileActivity, successModel!!.message!!)
                        }
                        /*AUTH_FAILURE -> {
                            forceLogout(this@AddAddressActivity)
                        }*/
                    }

                }

            })


    }

    private fun updateData() {
        val intent = Intent("update_data")
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
    }

    private fun getUserProfileApi(b: Boolean) {
        if (b) {
            CustomProgressbar.showProgressBar(this, false)
        }
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(this)
        RetrofitClientSingleton
            .getInstance()
            .getUserProfile(map)
            .enqueue(object : retrofit2.Callback<ProfileModel> {
                override fun onFailure(call: Call<ProfileModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("TAG", t.message)
                }

                override fun onResponse(
                    call: Call<ProfileModel>,
                    response: Response<ProfileModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                val profileRespo = response.body()!!.data
                                if (profileRespo != null) {
                                    etProfileName.setText(profileRespo.name)
                                    etProfileMobile.setText(profileRespo.phone)
                                    etProfileEmail.setText(profileRespo.email)

                                    if (profileRespo.refer_by_code != null) {
                                        edReferalCode.isEnabled = false
                                        tilReferCode.isFocusable = false
                                        edReferalCode.setText(profileRespo.refer_by_code)
                                    }
                                    if (profileRespo.refer_by_code == "") {
                                        edReferalCode.isEnabled = true
                                        tilReferCode.isFocusable = true
                                    }

                                    if (profileRespo.profile_pic != null && profileRespo.profile_pic != "") {
                                        Glide.with(this@EditProfileActivity)
                                            .load(profileRespo.profile_pic)
                                            .placeholder(R.drawable.ic_user_placeholder)
                                            .into(image_account)
                                    }


                                } else {
                                    Toast(
                                        this@EditProfileActivity,
                                        getString(R.string.something_went_wrong)
                                    )
                                }
                            }
                            FAILURE -> {

                            }
                            AUTH_FAILURE -> {

                            }
                        }
                    }

                }

            })
    }


    /**Permission**/
    private fun checkCameraAndStoragePermissions() {
        val permissions = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
        )
        if (!hasPermissions(this, permissions)) {
            ActivityCompat.requestPermissions(this, permissions, CAMERA_PERMISSION_CODE)
        } else {
            chooseCameraOrGallery()
        }
    }

    /**Open Camera and Gallery Popup**/
    private fun chooseCameraOrGallery() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.setContentView(R.layout.dialog_choose_image)
        dialog.setTitle("")

        val camera = dialog.camera
        val gallery = dialog.gallery
        val remove = dialog.remove

        camera.setOnClickListener {
            dialog.dismiss()
            val mIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
            val file = File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg")
            val uri =
                FileProvider.getUriForFile(this, "com.attune.zamy.android.provider", file)
            mIntent.putExtra(MediaStore.EXTRA_OUTPUT, uri)
            startActivityForResult(mIntent, REQUEST_CAMERA_IMAGE)
        }

        gallery.setOnClickListener {
            dialog.dismiss()
            val intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI)
            startActivityForResult(
                Intent.createChooser(intent, "Select File"),
                REQUEST_GALLERY_IMAGE
            )
        }
        remove.setOnClickListener {
            deactiveProfile()
            dialog.dismiss()
        }

        dialog.show()
    }

    private fun deactiveProfile() {
        CustomProgressbar.showProgressBar(this, false)
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(this)
        RetrofitClientSingleton
            .getInstance()
            .DeactiveProfilePicture()
            .enqueue(object : retrofit2.Callback<SuccessModel> {
                override fun onFailure(call: Call<SuccessModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Toast(this@EditProfileActivity, getString(R.string.something_went_wrong))
                    Log.e("SAM", "onFailure: $t")
                }

                override fun onResponse(
                    call: Call<SuccessModel>,
                    response: Response<SuccessModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    getUserProfileApi(false)
                    Toast(this@EditProfileActivity, getString(R.string.remove_profile))
                }

            })
    }

    /**Permission Result**/
    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<String>,
        grantResults: IntArray
    ) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        Log.e("Edit Profile", "$requestCode , $permissions, $grantResults")
        //Checking the request code of our request
        if (requestCode == CAMERA_PERMISSION_CODE) {
            //If permission is granted
            if (grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                chooseCameraOrGallery()
            } else {
                //Displaying another toast if permission is not granted
                Toast(this, getString(R.string.camera_permission))
            }
        }
    }

    /**Activity Result**/
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        Log.d(
            "Edit Profile",
            "onActivityResult : requestCode:$requestCode , resultCode:$resultCode"
        )

        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CAMERA_IMAGE -> if (resultCode == Activity.RESULT_OK) {

                val file = File(Environment.getExternalStorageDirectory(), "MyPhoto.jpg")
                val uri = FileProvider.getUriForFile(
                    this,
                    "com.attune.zamy.android" + ".provider",
                    file
                )

                CropImage.activity(uri)
                    .setAspectRatio(1, 1)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this)
            }

            REQUEST_GALLERY_IMAGE -> if (resultCode == Activity.RESULT_OK) {
                val selectedImage = data!!.data

                //Crop gallery image
                CropImage.activity(selectedImage!!)
                    .setAspectRatio(1, 1)
                    .setGuidelines(CropImageView.Guidelines.ON)
                    .start(this)
            }
            CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE -> if (resultCode == Activity.RESULT_OK) {
                val result = CropImage.getActivityResult(data)
                val resultUri = result.uri

                val uri = Uri.parse(resultUri.toString())
                mImageFile = File(uri.path)

                profileImageUri = mImageFile!!.absolutePath

                //Crop camera image
                Log.d("EditProfile", "profileImageUri >> $profileImageUri")

                Glide.with(applicationContext)
                    .load(profileImageUri)
                    .into(image_account)
            }
        }
    }

}
