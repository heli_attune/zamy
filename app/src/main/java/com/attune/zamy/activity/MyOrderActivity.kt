package com.attune.zamy.activity

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout
import com.attune.zamy.R
import com.attune.zamy.adapter.MyOrderPaginationAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.interaface.onItemOrderListClick
import com.attune.zamy.listener.PaginationScrollListener
import com.attune.zamy.model.MyOrderListModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.activity_my_order.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import kotlinx.android.synthetic.main.no_data_layout.*
import retrofit2.Call
import retrofit2.Response

class MyOrderActivity : AppCompatActivity(), onItemOrderListClick {


    var TOTAL_PAGE: Int = 0
    var isLoading: Boolean = false
    var isLastPage: Boolean = false
    val PAGE_START = 1
    var currentPage: Int = PAGE_START
    private lateinit var madapter: MyOrderPaginationAdapter
    lateinit var swipeLayout: SwipeRefreshLayout
    lateinit var dataModels: MutableList<MyOrderListModel.DataBean.ListBean>
    val linearLayoutManager =
        LinearLayoutManager(this@MyOrderActivity, LinearLayoutManager.VERTICAL, false)

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        setContentView(R.layout.activity_my_order)
        customtoolbar.txtTitle.text = "My Order"
        customtoolbar.imgback.setOnClickListener {
            finish()
        }
        initLoading()

    }

    private fun initLoading() {
        //getMyOrder(false)
        clearPaging()
        dataModels = ArrayList()
        swipeLayout = findViewById(R.id.sr)
        rvBind()
        if (!isNetworkAvailable(this@MyOrderActivity)) {
            Toast(this, getString(R.string.no_internet_connection))
        } else {
            getMyOrder(false)
        }
        srRefresh()


    }

    //swipe to refresh layout
    private fun srRefresh() {
        swipeLayout.setOnRefreshListener {
            clearPaging()
            getMyOrder(true)
            swipeLayout.isRefreshing = false
        }
        /*swipeLayout.setOnRefreshListener {
            clearPaging()
            if (!activity?.let { isNetworkAvailable(it) }!!) {
                activity?.let { Toast(it, getString(R.string.no_internet_connection)) }
            } else {
                getTopRestaurantDataAPI()
            }
            swipeLayout.setRefreshing(false)
        }*/
    }

    private fun clearPaging() {
        currentPage = PAGE_START
        isLoading = false
        isLastPage = false
    }

    private fun rvBind() {
        val mLayoutManager = LinearLayoutManager(this)
        rvMyOrderList.layoutManager = mLayoutManager
        madapter = MyOrderPaginationAdapter(this, dataModels, this)
        rvMyOrderList.adapter = madapter
        rvMyOrderList.addOnScrollListener(object : PaginationScrollListener(mLayoutManager) {
            override val totalPageCount: Int
                get() = TOTAL_PAGE

            override fun isLastPage(): Boolean {
                return isLastPage
            }

            override fun isLoading(): Boolean {
                return isLoading
            }

            override fun loadMoreItems() {
                isLoading = true
                currentPage += 1
                lodeMore()
            }
        })

    }

    private fun getMyOrder(isrefresh: Boolean) {
        if (!isrefresh) {
            CustomProgressbar.showProgressBar(this, false)
        }
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(this)
        map["pageno"] = currentPage.toString()


        RetrofitClientSingleton
            .getInstance()
            .getMyOrderList(map)
            .enqueue(object : retrofit2.Callback<MyOrderListModel> {
                override fun onFailure(call: Call<MyOrderListModel>, t: Throwable) {
                    Log.e("TAG", t.message)
                    CustomProgressbar.hideProgressBar()
                    showNoDataLayout(llParentNoData, "No Data Found")
                }

                override fun onResponse(
                    call: Call<MyOrderListModel>,
                    response: Response<MyOrderListModel>
                ) {
                    rvMyOrderList.visibility = View.VISIBLE
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val dataList = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                val dataListModels = dataList!!.data!!.list
                                if (dataListModels!!.isNotEmpty()) {

                                    madapter.removeAllItem()
                                    madapter.addAllItem(dataListModels)
                                    TOTAL_PAGE = dataList.data!!.total_pages.toInt()
                                    isLoading = false
                                    if (currentPage < TOTAL_PAGE) {
                                        madapter.addLoading()
                                    } else {
                                        isLastPage = true
                                    }
                                } else {
                                    madapter.removeAllItem()
                                }

                            }
                            FAILURE -> {
                                Toast(this@MyOrderActivity, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                }

            })
    }

    fun lodeMore() {

        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(this)
        map["pageno"] = currentPage.toString()


        RetrofitClientSingleton
            .getInstance()
            .getMyOrderList(map)
            .enqueue(object : retrofit2.Callback<MyOrderListModel> {
                override fun onFailure(call: Call<MyOrderListModel>, t: Throwable) {
                    Log.e("TAG", t.message)
                    showNoDataLayout(llParentNoData, "No Data Found")
                }

                override fun onResponse(
                    call: Call<MyOrderListModel>,
                    response: Response<MyOrderListModel>
                ) {
                    rvMyOrderList.visibility = View.VISIBLE


                    if (response.isSuccessful) {
                        val homeScreenData = response.body()
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                val dataList = homeScreenData!!.data!!.list
                                if (dataList != null && dataList.size > 0) {

                                    madapter.removeLoading()
                                    isLoading = false
                                    madapter.addAllItem(dataList)

                                    if (currentPage != TOTAL_PAGE) {
                                        madapter.addLoading()
                                    } else {
                                        isLastPage = true
                                    }
                                }
                            }
                            FAILURE -> {
                                Toast(this@MyOrderActivity, "" + response.body()!!.message)
                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("HOME FRAGMENT", response.body().toString())
                }
            })

    }

    /**Single item click**/
    override fun myOrderClick(id: String?) {
        val intent = Intent(this, OrderDetailActivity::class.java)
        intent.putExtra("order_id", id)
        startActivity(intent)
    }

}
