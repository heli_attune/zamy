package com.attune.zamy.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.ForgotPasswordModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.FAILURE
import com.attune.zamy.utils.SUCCESS
import com.attune.zamy.utils.Toast
import com.attune.zamy.utils.isNetworkAvailable
import kotlinx.android.synthetic.main.activity_forgot_password.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ForgotPasswordPage : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_forgot_password)

        initView()

    }

    private fun initView() {

        btnSubmit.setOnClickListener {
            validation()
        }

    }

    private fun validation() {
        if (edtEmail.text.toString().trim().isEmpty()) {
            Toast(this@ForgotPasswordPage, getString(R.string.please_enter_username_email))
        } else {
            if (!isNetworkAvailable(this@ForgotPasswordPage)) {
                Toast(this@ForgotPasswordPage, getString(R.string.no_internet_connection))
            } else {
                forgotPasswordAPI()
            }
        }
    }

    private fun forgotPasswordAPI() {

        CustomProgressbar.showProgressBar(this, false)
        val map = HashMap<String, String>()
        map["email"] = edtEmail.text.toString().trim()

        RetrofitClientSingleton.getInstance().forgotPasswrodAPI(map).enqueue(object :
            Callback<ForgotPasswordModel> {
            override fun onFailure(call: Call<ForgotPasswordModel>, t: Throwable) {
                CustomProgressbar.hideProgressBar()
                Toast(this@ForgotPasswordPage, getString(R.string.please_try_again))
            }

            override fun onResponse(
                call: Call<ForgotPasswordModel>,
                response: Response<ForgotPasswordModel>
            ) {
                CustomProgressbar.hideProgressBar()
                if (response.isSuccessful) {
                    val responsedata = response.body() as ForgotPasswordModel
                    /*if (response.body()!!.status) {
                        Toast(this@ForgotPasswordPage, responsedata.message)
                        val intent= Intent(this@ForgotPasswordPage,LoginPage::class.java)
                        startActivity(intent)
                        finish()
                    } else {
                        Toast(this@ForgotPasswordPage, responsedata.message)
                    }*/

                    when (response.body()!!.status) {
                        SUCCESS -> {
                            Toast(this@ForgotPasswordPage, responsedata.message)
                            val intent = Intent(this@ForgotPasswordPage, LoginPage::class.java)
                            startActivity(intent)
                            finish()
                        }
                        FAILURE -> {
                            Toast(this@ForgotPasswordPage, responsedata.message)
                        }
                    }
                } else {
                    Toast(this@ForgotPasswordPage, getString(R.string.please_try_again))
                }
            }

        })
    }
}
