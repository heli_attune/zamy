package com.attune.zamy.activity

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.CommonModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.SharedPref
import com.attune.zamy.utils.Toast
import com.attune.zamy.utils.getDeviceID
import kotlinx.android.synthetic.main.activity_verification_mobile.*
import kotlinx.android.synthetic.main.fragmrnt_toolbar.*
import retrofit2.Call
import retrofit2.Response

class VerificationMobile : AppCompatActivity() {

    var passPage = ""
    var userMobile = ""
    lateinit var deviceId: String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_verification_mobile)
        imgback.visibility = View.VISIBLE
        txtTitle.text = "Complete Your Profile"


        imgback.setOnClickListener {
            finish()
        }

        //deviceId = Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)

        deviceId = getDeviceID(this@VerificationMobile)

        if (intent.extras!!["cartfragment"] != null) {
            passPage = "cartfragment"
        } else {
            passPage = "cartfragmenttoolbar"
        }

        initView()
    }

    private fun initView() {

        btnOtp.setOnClickListener {
            if (edtmobileNo.text.toString().trim().isEmpty()) {
                Toast(this@VerificationMobile, getString(R.string.please_enter_mobileno))
            } else if (edtmobileNo.text!!.length < 10) {
                Toast(this@VerificationMobile, "Please Enter Valid Mobile")
            } else {
                requestOTPAPI()
            }
        }

        btnUpdate.setOnClickListener {
            if (edtOTP.text!!.isNotEmpty()) {
                verifyOTPAPI()
            } else {
                Toast(this@VerificationMobile, "Please Enter OTP")
            }
        }

        /*imgback.setOnClickListener {
            if (passPage.equals("cart")) {
                val intent = Intent(this, CartFragment::class.java)
                startActivity(intent)
            } else {
                val intent = Intent(this, CartToolbarFragment::class.java)
                startActivity(intent)
            }
            finish()
        }*/

        btnResend.setOnClickListener {
            edtOTP.text!!.clear()
            requestOTPAPI()
        }
    }


    fun requestOTPAPI() {
        CustomProgressbar.showProgressBar(this, false)
        val map = HashMap<String, String>()
        map["deviceID"] = deviceId
        map["phone"] = edtmobileNo.text.toString()
        RetrofitClientSingleton
            .getInstance()
            .mobileOTPWithGoogle(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val responsedata = response.body() as CommonModel
                        if (responsedata.status == 1) {
                            Toast(this@VerificationMobile, responsedata.message)
                            edtOTP.visibility = View.VISIBLE
                            edtmobileNo.isEnabled = false
                            btnResend.visibility = View.VISIBLE
                            userMobile = edtmobileNo.text.toString()
                            btnUpdate.visibility = View.VISIBLE
                        } else {
                            Toast(this@VerificationMobile, responsedata.message)
                            edtmobileNo.isEnabled = true
                        }
                    } else {
                        Toast(this@VerificationMobile, "" + response.body()!!.message)
                        edtmobileNo.isEnabled = true

                    }
                }

            })


    }


    private fun verifyOTPAPI() {
        CustomProgressbar.showProgressBar(this, false)
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(this@VerificationMobile)
        map["phone"] = userMobile
        map["OTP"] = edtOTP.text.toString().trim()
        map["deviceID"] = deviceId

        RetrofitClientSingleton
            .getInstance()
            .updateMobileNumberWithGoogle(map)
            .enqueue(object : retrofit2.Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val responsedata = response.body() as CommonModel
                        if (responsedata.status == 1) {
                            finish()
                        } else {
                            Toast(this@VerificationMobile, responsedata.message)
                        }
                    } else {
                        Toast(this@VerificationMobile, "" + response.body()!!.message)

                    }
                }

            })
        /*phone:9510805727
        user_id:4
        deviceID:4543636
        OTP:9630*/

    }
}
