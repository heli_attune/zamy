package com.attune.zamy.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.R
import com.attune.zamy.adapter.SpinnerIssueCategoryAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.CustomerIssueModel
import com.attune.zamy.model.SuccessModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.activity_create_ticket.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.set

class CreateTicketActivity : AppCompatActivity(), View.OnClickListener,
    AdapterView.OnItemSelectedListener {
    lateinit var categoryIssue: String
    var categoryListArray = ArrayList<CustomerIssueModel.DataBean>()
    private var orderId: String = ""
    private var suborderId: String = ""
    var titleArray: ArrayList<String> = ArrayList<String>()
    private var strTitle: String = ""
    private var strTitleId: String = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_create_ticket)

        val intent = intent
        orderId = intent.getStringExtra("orderId") ?: "0"
        customtoolbar.txtTitle.text = "Create Ticket"
        customtoolbar.imgback.setOnClickListener {
            finish()
        }
        btnComposeTicket.setOnClickListener(this)


        if (isNetworkAvailable(this)) {
            callTicketCategoryApi()
        } else {


        }
        spinnerTitle.onItemSelectedListener = this
    }

    private fun callTicketCategoryApi() {
        CustomProgressbar.showProgressBar(this
            , false)
        RetrofitClientSingleton.getInstance().getCustomerCategoryIssueList()
            .enqueue(object : retrofit2.Callback<CustomerIssueModel> {
                override fun onFailure(call: Call<CustomerIssueModel>?, t: Throwable?) {
                    CustomProgressbar.hideProgressBar()
                    Toast(this@CreateTicketActivity,getString(R.string.something_went_wrong))
                }

                override fun onResponse(call: Call<CustomerIssueModel>?, response: Response<CustomerIssueModel>?) {
                    CustomProgressbar.hideProgressBar()
                    if (response!!.isSuccessful) {
                        val categoryList = response.body()
                        var shippingAreaList = response.body()!!.data
                        when (response.body()?.status) {
                            SUCCESS -> {
                               /* if (categoryList!!.data != null) {
                                    if (categoryList.data!!.size > 0) {
                                        categoryListArray = (categoryList.data as ArrayList<CustomerIssueModel.DataBean>?)!!
                                        for (item in categoryListArray.indices) {
                                            titleArray.add(categoryListArray[item].title!!)
                                            val dataAdapter = ArrayAdapter(this@CreateTicketActivity, android.R.layout.simple_spinner_item, titleArray)
                                            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)
                                            spinnerTitle.adapter = dataAdapter
                                        }

                                    }
                                }*/
                                val adapter = SpinnerIssueCategoryAdapter(
                                    this@CreateTicketActivity,
                                    shippingAreaList as java.util.ArrayList<CustomerIssueModel.DataBean>
                                )
                                spinnerTitle.adapter = adapter


                                /**Set item at position using predefine**/
                               /* if (isEdit) {
                                    spShippingArea.setSelection(
                                        getIndex(
                                            spShippingArea,
                                            mAddressObject.shipping_area.toString(),
                                            shippingAreaList
                                        )
                                    )
                                }*/
                                /**Set spinner item **/
                                spinnerTitle.onItemSelectedListener =
                                    object : AdapterView.OnItemSelectedListener {
                                        override fun onNothingSelected(parent: AdapterView<*>?) {

                                        }

                                        override fun onItemSelected(
                                            parent: AdapterView<*>?,
                                            view: View?,
                                            position: Int,
                                            id: Long
                                        ) {
                                            categoryIssue =
                                                shippingAreaList[position].title.toString()
                                            strTitleId= shippingAreaList[position].id.toString()

                                        }

                                    }

                            }
                            FAILURE -> {
                                Toast(this@CreateTicketActivity, categoryList!!.message)
                            }
                            AUTH_FAILURE -> {
                                //forceLogout(this@ComposeTicketActivity)
                            }
                        }
                    }
                }

            })
    }
    override fun onClick(v: View) {
        when (v.id) {
            R.id.btnComposeTicket -> {
                if (etSubject.text.toString().trim().length == 0) {
                    Toast(this, getString(R.string.enter_sub))
                    return
                } else if (strTitleId.trim().length == 0) {
                    Toast(this, getString(R.string.enter_title))
                    return
                } else if (etDescription.text.toString().trim().length == 0) {
                    Toast(this, getString(R.string.enter_desc))
                    return
                }
                composeMessage()

            }
        }
    }
    private fun composeMessage() {

        CustomProgressbar.showProgressBar(this
            , false)
        val map = HashMap<String, String>()
        map["order_id"] = orderId
        map["ticketSenderID"] = SharedPref.getUserId(this)
        map["ticketTitle"] = strTitleId
        map["subject"] = etSubject.text.toString().trim()
        map["ticketBody"] = etDescription.text.toString().trim()


        RetrofitClientSingleton.getInstance().createTicket(map)
            .enqueue(object : retrofit2.Callback<SuccessModel> {
                override fun onFailure(call: Call<SuccessModel>?, t: Throwable?) {
                    CustomProgressbar.hideProgressBar()
                    Toast(this@CreateTicketActivity, getString(R.string.something_went_wrong))
                }

                override fun onResponse(call: Call<SuccessModel>?, response: Response<SuccessModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val successModel = response.body()
                        when (response.body()?.status) {
                            SUCCESS -> {
                                Toast(this@CreateTicketActivity, successModel!!.message!!)
                                val intent = Intent(this@CreateTicketActivity, CustomerSupportActivity::class.java)
                                startActivity(intent)
                                finish()
                            }
                            FAILURE -> {
                                Toast(this@CreateTicketActivity, successModel!!.message!!)
                            }
                            AUTH_FAILURE -> {
                                //forceLogout(this@ComposeTicketActivity)
                            }
                        }
                    }
                }

            })
    }

    override fun onItemSelected(arg0: AdapterView<*>, arg1: View, position: Int, id: Long) {
        if (titleArray != null) {
            if (titleArray.size > 0) {
                strTitle = titleArray[position]
                strTitleId = "" + categoryListArray[position].id!!
            }
        }
    }

    override fun onNothingSelected(arg0: AdapterView<*>) {
        // TODO Auto-generated method stub
    }
}
