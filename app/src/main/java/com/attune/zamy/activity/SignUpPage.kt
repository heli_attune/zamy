package com.attune.zamy.activity

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.os.CountDownTimer
import android.os.Handler
import android.provider.Settings
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.CommonModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.activity_sign_up_page.*
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response


class SignUpPage : AppCompatActivity() {

    private lateinit var screenName: String
    lateinit var deviceId: String
    var isOTP: Boolean = false
    lateinit var uName: String
    lateinit var uEmial: String

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        setContentView(R.layout.activity_sign_up_page)
        initView()
    }

    private fun initView() {


        screenName = intent.getStringExtra(SCREEN_REDIRECTION) ?: "0"
        deviceId = Settings.Secure.getString(this.contentResolver, Settings.Secure.ANDROID_ID)
        btnOtp.text = resources.getString(R.string.request_otp)
        btnOtp.setOnClickListener {
            validation()

        }
        tvSignin.setOnClickListener {
            val LoginIntent = Intent(this@SignUpPage, LoginPage::class.java)
            startActivity(LoginIntent)
            finish()
        }

        tilSignUpMobile.editText?.doOnTextChanged { text, start, count, after ->
            if (text!!.isEmpty()) {
                tilSignUpMobile.error = getString(R.string.please_enter_mobileno)
            } else if (text.length < 10) {
                tilSignUpMobile.error = getString(R.string.enter_valid_mobile)
            } else {
                tilSignUpMobile.error = ""
                tilSignUpMobile.setEndIconDrawable(R.drawable.ic_check_green)
                tilSignUpMobile.boxStrokeColor = ContextCompat.getColor(this, R.color.green)
                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    tilSignUpMobile.hintTextColor = getColorStateList(R.color.green)
                }
            }
        }

        tilSignUpEmail.editText?.doOnTextChanged { text, start, count, after ->

            if (text!!.isEmpty()) {
                tilSignUpEmail.error = getString(R.string.please_enter_email)
            } else
                if (isValidEmail(text.toString())) {
                    tilSignUpEmail.error = ""
                    tilSignUpEmail.setEndIconDrawable(R.drawable.ic_check_green)
                    tilSignUpEmail.boxStrokeColor = ContextCompat.getColor(this, R.color.green)
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                        tilSignUpEmail.hintTextColor = getColorStateList(R.color.green)
                    }
                } else {
                    tilSignUpEmail.error = getString(R.string.please_enter_valid_email)
                }
        }

        tilSignUpName.editText?.doOnTextChanged { text, start, count, after ->

            if (text!!.isEmpty()) {
                tilSignUpName.error = getString(R.string.enter_user_name)
            } else {
                tilSignUpName.error = ""
            }
        }


        btnResend.setOnClickListener {
            if (edtUserName.text.toString().trim().isEmpty()) {
                tilSignUpName.error = getString(R.string.please_enter_username)
            } else if (!isValidEmail(edtEmail.text.toString().trim())) {
                tilSignUpEmail.error = getString(R.string.please_enter_valid_email)
            } else if (edtEmail.text.toString().trim().isEmpty()) {
                tilSignUpEmail.error = getString(R.string.please_enter_email)
            } else if (edtmobileNo.text.toString().trim().isEmpty()) {
                tilSignUpMobile.error = getString(R.string.please_enter_mobileno)
            } else if (edtmobileNo.text!!.length < 10) {
                tilSignUpMobile.error = "Please Enter Valid Mobile"
            } else if (edtPassword.text.toString().trim().isEmpty()) {
                tilSignUpPassword.error = getString(R.string.please_enter_password)
            } else if (edtPassword.text!!.length <= 6) {
                tilSignUpPassword.error = "Password must be at least 6 character"
            } else {
                isOTP = true
                edtOTP.text!!.clear()
                if (isNetworkAvailable(this@SignUpPage)) {
                    requestOTPAPI()
                } else {
                    Toast(this@SignUpPage, getString(R.string.network_error))
                }
            }
        }
        val ha = Handler()
        ha.postDelayed(Runnable {
            //call function
        }, 30000)


        object : CountDownTimer(30000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                mTextField.text = "seconds remaining: " + millisUntilFinished / 1000
            }

            override fun onFinish() {
                mTextField.text = "done!"
            }
        }.start()

    }

    private fun validation() {
        if (edtUserName.text.toString().trim().isEmpty()) {
            Toast(this@SignUpPage, getString(R.string.please_enter_username))
        } else if (!isValidText(edtUserName.text.toString().trim())) {
            Toast(this@SignUpPage, getString(R.string.please_enter_valid_username))
        } else if (!isValidEmail(edtEmail.text.toString().trim())) {
            Toast(this@SignUpPage, getString(R.string.please_enter_valid_email))
        } else if (edtEmail.text.toString().trim().isEmpty()) {
            Toast(this@SignUpPage, getString(R.string.please_enter_email))
        } else if (edtmobileNo.text.toString().trim().isEmpty()) {
            Toast(this@SignUpPage, getString(R.string.please_enter_mobileno))
        } else if (edtmobileNo.text!!.length < 10) {
            Toast(this@SignUpPage, "Please Enter Valid Mobile")
        } else if (edtPassword.text.toString().trim().isEmpty()) {
            Toast(this@SignUpPage, getString(R.string.please_enter_password))
        } else if (edtPassword.text!!.length <= 6) {
            Toast(this@SignUpPage, "Password must be at least 6 character")
        } else {
            if (!isNetworkAvailable(this@SignUpPage)) {
                Toast(this@SignUpPage, getString(R.string.no_internet_connection))
            } else {
                if (!isOTP) {
                    requestOTPAPI()
                } else {
                    if (edtOTP.text.toString().trim().isEmpty()) {
                        Toast(this@SignUpPage, "Please Enter OTP")
                    } else {
                        verifyOTPAPI()
                    }
                }
            }
        }
    }

    private fun requestOTPAPI() {
        CustomProgressbar.showProgressBar(this, false)
        val map = HashMap<String, String>()
        map["deviceID"] = deviceId
        map["username"] = edtUserName.text.toString().trim()
        map["email"] = edtEmail.text.toString().trim()
        map["mobile"] = edtmobileNo.text.toString().trim()

        RetrofitClientSingleton.getInstance().requestOTP(map)
            .enqueue(object : Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Toast(this@SignUpPage, getString(R.string.please_try_again))
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val responsedata = response.body() as CommonModel
                        if (responsedata.status == 1) {
                            Toast(this@SignUpPage, responsedata.message)
                            isOTP = true
                            edtOTP.visibility = View.VISIBLE
                            btnOtp.text = "Sign Up"
                            btnResend.visibility = View.VISIBLE
                        } else {
                            Toast(this@SignUpPage, responsedata.message)
                        }
                    } else {
                        Toast(this@SignUpPage, "" + response.body()!!.message)

                    }
                }

            })
    }

    private fun verifyOTPAPI() {
        CustomProgressbar.showProgressBar(this, false)
        val map = HashMap<String, String>()
        map["deviceID"] = deviceId
        map["username"] = edtUserName.text.toString().trim()
        map["email"] = edtEmail.text.toString().trim()
        map["mobile"] = edtmobileNo.text.toString().trim()
        map["OTP"] = edtOTP.text.toString().trim()
        map["password"] = edtPassword.text.toString().trim()
        map["referral_code"] = edtReferralCode.text.toString().trim()

        RetrofitClientSingleton.getInstance().verifyOTP(map)
            .enqueue(object : Callback<CommonModel> {
                override fun onFailure(call: Call<CommonModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Toast(this@SignUpPage, getString(R.string.please_try_again))
                }

                override fun onResponse(call: Call<CommonModel>, response: Response<CommonModel>) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        val responsedata = response.body() as CommonModel
                        if (responsedata.status == 1) {
                            btnOtp.visibility = View.VISIBLE
                            Toast(this@SignUpPage, responsedata.message)
                            if (screenName != null && screenName != "" && screenName.equals("1")) {
                                val LoginIntent = Intent(this@SignUpPage, MainActivity::class.java)
                                startActivity(LoginIntent)
                                finish()
                            } else {
                                val LoginIntent = Intent(this@SignUpPage, LoginPage::class.java)
                                startActivity(LoginIntent)
                                finish()
                            }
                            val LoginIntent = Intent(this@SignUpPage, LoginPage::class.java)
                            startActivity(LoginIntent)
                            finish()

                        } else {
                            Toast(this@SignUpPage, responsedata.message)

                        }
                    }
                }

            })
    }


}
