package com.attune.zamy.activity

import android.annotation.SuppressLint
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatImageView
import androidx.appcompat.widget.AppCompatTextView
import androidx.core.content.ContextCompat
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.AboutUs
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.Toast
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.back_toolbar.*
import retrofit2.Call
import retrofit2.Response

class AboutUsActivity : AppCompatActivity() {

    lateinit var imgStayHome: AppCompatImageView
    lateinit var imgBecomeDegital: AppCompatImageView
    lateinit var imgBeaBoos: AppCompatImageView

    lateinit var lblStayHome: AppCompatTextView
    lateinit var lblBecomeDegital: AppCompatTextView
    lateinit var lblBeaBoos: AppCompatTextView
    lateinit var tvStayHomeDetails: AppCompatTextView
    lateinit var tvBecomeDetails: AppCompatTextView
    lateinit var tvBeaboosDetails: AppCompatTextView
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_about_us)

        tvTitle.text = "About us"
        imgBakToolbar.setOnClickListener {
            finish()
        }
        getAdout()
        imgStayHome = findViewById(R.id.imgStayHome)
        imgBecomeDegital = findViewById(R.id.imgBecomeDegital)
        imgBeaBoos = findViewById(R.id.imgBeaBoos)


        lblStayHome = findViewById(R.id.lblStayHome)
        lblBecomeDegital = findViewById(R.id.lblBecomeDegital)
        lblBeaBoos = findViewById(R.id.lblBeaBoos)


        tvStayHomeDetails = findViewById(R.id.tvStayHomeDetails)
        tvBecomeDetails = findViewById(R.id.tvBecomeDetails)
        tvBeaboosDetails = findViewById(R.id.tvBeaboosDetails)

    }

    private fun getAdout() {
        CustomProgressbar.showProgressBar(this@AboutUsActivity, false)
        RetrofitClientSingleton
            .getInstance()
            .aboutUs()
            .enqueue(object : retrofit2.Callback<AboutUs> {
                override fun onFailure(call: Call<AboutUs>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Toast(this@AboutUsActivity, getString(R.string.something_went_wrong))
                }

                @SuppressLint("SetTextI18n")
                override fun onResponse(
                    call: Call<AboutUs>,
                    response: Response<AboutUs>
                ) {

                    if (response.isSuccessful) {
                        CustomProgressbar.hideProgressBar()
                        if (response.body()!!.data != null) {
                            response.body()!!.data?.apply {
                                if (img_1 != null && img_1 != "") {
                                    Glide.with(this@AboutUsActivity)
                                        .load(image_url + img_1.toString())
                                        .into(imgStayHome)
                                } else {
                                    Glide.with(this@AboutUsActivity)
                                        .load(
                                            ContextCompat.getDrawable(
                                                this@AboutUsActivity,
                                                R.drawable.safe
                                            )
                                        )
                                        .into(imgStayHome)
                                }
                                if (img_2 != null && img_2 != "") {
                                    Glide.with(this@AboutUsActivity)
                                        .load(image_url + img_2)
                                        .into(imgBecomeDegital)
                                } else {
                                    Glide.with(this@AboutUsActivity)
                                        .load(
                                            ContextCompat.getDrawable(
                                                this@AboutUsActivity,
                                                R.drawable.register
                                            )
                                        )
                                        .into(imgStayHome)
                                }
                                if (img_3 != null && img_3 != "") {
                                    Glide.with(this@AboutUsActivity)
                                        .load(image_url + img_3)
                                        .into(imgBeaBoos)
                                } else {
                                    Glide.with(this@AboutUsActivity)
                                        .load(
                                            ContextCompat.getDrawable(
                                                this@AboutUsActivity,
                                                R.drawable.delivery
                                            )
                                        )
                                        .into(imgStayHome)
                                }

                                lblStayHome.text = "" + title_1
                                lblBecomeDegital.text = "" + title_2
                                lblBeaBoos.text = "" + title_3

                                tvStayHomeDetails.text = "" + desc_1
                                tvBecomeDetails.text = "" + desc_2
                                tvBeaboosDetails.text = "" + desc_3


                            }
                        } else {
                            Toast(this@AboutUsActivity, response.body()!!.message)
                        }
                    }
                }

            })
    }
}