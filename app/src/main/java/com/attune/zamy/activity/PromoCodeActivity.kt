package com.attune.zamy.activity

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.attune.zamy.R
import com.attune.zamy.adapter.PromoCodeAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.listener.PromoCodeListener
import com.attune.zamy.model.CouponListModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.activity_promo_code.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.no_data_layout.*
import retrofit2.Call
import retrofit2.Response


class PromoCodeActivity : AppCompatActivity(), PromoCodeListener {
    override fun onTermsClick(pos: Int, promoDetail: CouponListModel.DataBean) {

    }

    private lateinit var restaurand_Id: String
    private lateinit var pageRedirect: String
    private val TAG = PromoCodeActivity::class.java.simpleName
    var mAdapter: PromoCodeAdapter? = null
    var mPromoArrayList: ArrayList<CouponListModel.DataBean> = ArrayList()
    private var couponKey: String? = null
    private var couponName: String? = null
    var isValue: Boolean = true
    private var restoID = ""
    private var ismyApply = true

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        setContentView(R.layout.activity_promo_code)
        initView()
    }

    private fun initView() {
        val intent = intent
        restaurand_Id = intent.getStringExtra(RESTAURANT_ID_PASS) ?: "0"
        pageRedirect = intent.getStringExtra("ACCOUNT") ?: "0"

        toolbarSet()
        rvPromoCode.layoutManager = LinearLayoutManager(this)
        rvPromoCode.setHasFixedSize(true)

        mAdapter = PromoCodeAdapter(this, this, pageRedirect)
        rvPromoCode.adapter = mAdapter



        if (isNetworkAvailable(this)) {
            getListOfCoupons()
        }
    }

    private fun toolbarSet() {
        txtTitle.text = resources.getString(R.string.promo_code)
        imgback.setOnClickListener {
            onBackPressed()
            finish()
        }
    }

    private fun getListOfCoupons() {
        val map = HashMap<String, String>()
        map["restaurant_id"] = restaurand_Id
        map["user_id"] = SharedPref.getUserId(this@PromoCodeActivity)
        CustomProgressbar.showProgressBar(this, false)
        RetrofitClientSingleton.getInstance().getCoupanList(map)
            .enqueue(object : retrofit2.Callback<CouponListModel> {
                override fun onFailure(call: Call<CouponListModel>?, t: Throwable?) {
                    CustomProgressbar.hideProgressBar()
                    Toast(this@PromoCodeActivity, t!!.message)
                }

                override fun onResponse(
                    call: Call<CouponListModel>?,
                    response: Response<CouponListModel>?
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response?.isSuccessful!!) {
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                mPromoArrayList = response.body()!!.data!!
                                mAdapter!!.setList(response.body()!!.data!!)
                            }
                            FAILURE -> {
                                Toast(this@PromoCodeActivity, response.body()?.message)
                                 showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                            }
                            AUTH_FAILURE -> {

                            }
                        }
                    } else {
                        Toast(this@PromoCodeActivity, response.body()?.message)
                        showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                    }
                }

            })
    }

    /*override fun onClick(v: View?) {
        when (v?.id) {
            R.id.btnApplyPromo -> {
                if (isNetworkAvailable(this)) {
                    if (etEnterPromo.text.trim().isEmpty()) {
                        toast(this, getString(R.string.please_enter_promo))
                    } else {
                        showPromoDialog()
                    }
                } else
                    toast(this, R.string.network_error)
            }
        }
    }*/

    /*private fun showPromoDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_apply_promo)
        dialog.show()
        dialog.tvPromoMsg.text = "Are you sure you want to apply ${etEnterPromo.text} Promo Code?"
        dialog.llNoCancel.setOnClickListener { dialog.dismiss() }

        dialog.llYesApply.setOnClickListener {
            dialog.dismiss()
            applyPromoCode()
        }
    }*/


    /* override fun onTermsClick(pos: Int, promoDetail: CouponListModel.DataBean) {
         val dialog = Dialog(this)
         dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
         dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
         dialog.setContentView(R.layout.dialog_view_more)
         dialog.llViewMoreNutrition.visibility = View.GONE
         dialog.show()

         dialog.tvDescription.text = promoDetail.terms_and_conditions
         dialog.tvtitleDescription.text = promoDetail.coupons_name


         dialog.ivCloseDesc.setOnClickListener { dialog.dismiss() }
     }*/

    override fun onPromoRadioOnClick(pos: Int, promoDetail: CouponListModel.DataBean) {
        couponName = promoDetail.coupon_code
        couponKey = promoDetail.id

        if (couponName != null && !couponName.equals("")) {
            sendBroadcastMsg(couponName, couponKey, restaurand_Id)
            val intent = Intent()
            intent.putExtra("applay", 1)
            intent.putExtra("coupon_name", couponName)
            this@PromoCodeActivity.setResult(RESULT_OK, intent)
            finish()
        }

        //  Toast(this,couponName)


        /*etEnterPromo.setText(promoDetail.title)
        etEnterPromo.setSelection(promoDetail.title!!.length)*/
    }

    private fun sendBroadcastMsg(couponName: String?, couponKey: String?, resto: String?) {
        val intent = Intent(APPLY_COUPAN)
        intent.putExtra(PROMOCODE, couponName)
        intent.putExtra(RESTAURANT_ID_PASS, resto)
        LocalBroadcastManager.getInstance(this).sendBroadcast(intent)
        finish()
    }


}
