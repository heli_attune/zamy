package com.attune.zamy.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.attune.zamy.R
import com.attune.zamy.adapter.AddressAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.interaface.onItemAddressSelected
import com.attune.zamy.model.GetAddressModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.activity_manage_address.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import kotlinx.android.synthetic.main.no_data_layout.*
import retrofit2.Call
import retrofit2.Response

class ManageAddressActivity : AppCompatActivity(), onItemAddressSelected {


    lateinit var addressAdapter: AddressAdapter
    var linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_manage_address)

        customtoolbar.txtTitle.text = "Manage Address"
        customtoolbar.imgback.setOnClickListener {
            finish()
        }


        btnAddAddress.setOnClickListener {
            val intent = Intent(this@ManageAddressActivity, AddAddressActivity::class.java)
            intent.putExtra("add", 0)
            startActivity(intent)
        }


    }


    override fun onResume() {
        super.onResume()
        if (isNetworkAvailable(this)) {
            getAddressList()

        } else {
            Toast(this, "Please Check Internet")
        }
    }

    private fun getAddressList() {

        CustomProgressbar.showProgressBar(this, false)
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(this)
        RetrofitClientSingleton
            .getInstance()
            .getAddress(map)
            .enqueue(object : retrofit2.Callback<GetAddressModel> {
                override fun onFailure(call: Call<GetAddressModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Toast(this@ManageAddressActivity, t.message)
                }

                override fun onResponse(
                    call: Call<GetAddressModel>,
                    response: Response<GetAddressModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        rvAddressList.visibility = View.VISIBLE
                        btnAddAddress.visibility = View.VISIBLE
                        val apiResponse = response.body()!!.data
                        when (response.body()!!.status) {
                            SUCCESS -> {
                                val datumList = apiResponse!!.address_book!!
                                addressAdapter = AddressAdapter(
                                    this@ManageAddressActivity,
                                    datumList, this@ManageAddressActivity
                                )
                                //addressAdapter = activity?.let { AddressAdapter(it,datumList,this) }!!
                                rvAddressList.layoutManager = linearLayoutManager
                                rvAddressList!!.adapter = addressAdapter
                                addressAdapter.notifyDataSetChanged()
                            }
                            FAILURE -> {

                            }
                        }
                    } else {
                        showNoDataLayout(llParentNoData, "Add address")
                    }

                }

            })
    }

    override fun onAddressSelected(
        item: GetAddressModel.DataBean.AddressBookBean,
        isSelected: Boolean,
        isEdit: Boolean
    ) {
        if (isEdit) {
            val intent = Intent(this@ManageAddressActivity, AddAddressActivity::class.java)
            intent.putExtra("AddressModel", item)
            intent.putExtra("edit", 1)
            startActivity(intent)
        } else {

        }
    }
}
