package com.attune.zamy.activity

import android.location.Location
import android.location.LocationManager
import android.os.CountDownTimer
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.model.LatLongModel
import com.google.android.gms.common.api.GoogleApiClient
import com.google.android.gms.common.api.PendingResult
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResult
import com.google.android.gms.maps.GoogleMap
import com.google.android.gms.maps.model.LatLng
import com.google.android.gms.maps.model.Marker
import java.util.*

class TrackOrdersonMapsActivity : AppCompatActivity()/*, OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener, View.OnClickListener*/ {

    private val TAG = TrackOrdersonMapsActivity::class.java.simpleName
    private val MIN_DISTANCE_CHANGE_FOR_UPDATES = 1f // 10 meters
    // The minimum time between updates in milliseconds
    private val MIN_TIME_BW_UPDATES = (1000 * 5).toLong()
    private lateinit var mMap: GoogleMap
    private lateinit var googleApiClient: GoogleApiClient
    lateinit var locationManager: LocationManager
    lateinit var locationRequest: LocationRequest
    lateinit var locationSettingsRequest: LocationSettingsRequest.Builder

    lateinit var pendingResult: PendingResult<LocationSettingsResult>

    // flag for GPS status
    internal var isGPSEnabled = false

    // flag for network status
    internal var isNetworkEnabled = false

    // flag for GPS status
    internal var canGetLocation = false

    internal var location: Location? = null // location
    internal var latitudeCustomer: Double = 0.toDouble() // latitude
    internal var longitudeCustomer: Double = 0.toDouble() // longitude


    internal var latitudeDriver: Double = 0.toDouble() // latitude
    internal var longitudeDriver: Double = 0.toDouble()

    internal lateinit var MarkerPoints: ArrayList<LatLng>
    var latlongModel: ArrayList<LatLongModel> = ArrayList<LatLongModel>()
    private var timer: CountDownTimer? = null
    private var i = -1

    private var deliveryboy_id = ""

    var camZoom: Boolean = false
    private lateinit var deliveryMarker: Marker
    var bearing = 0.0


   /* // private lateinit var destinationMarker: Marker
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_track_on_maps)
        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
        val mapFragment = supportFragmentManager
                .findFragmentById(R.id.map) as SupportMapFragment
        mapFragment.getMapAsync(this)


        setSupportActionBar(toolbar)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)
        supportActionBar?.title = getString(R.string.track_order)


        if (intent.extras != null) {

            //  latitudeCustomer = intent.extras.getDouble("latitude")
            //  longitudeCustomer = intent.extras.getDouble("longitude")

            deliveryboy_id = intent.extras.getString("deliveryboy_id")

            latitudeCustomer = intent.extras.getDouble("latitude")

            longitudeCustomer = intent.extras.getDouble("longitude")



            fabCall.setOnClickListener(this)

        }
        MarkerPoints = ArrayList()
        latlongModel = ArrayList()


        ///////todo temp data/////


//        val obj = LatLongModel()
//        obj.latitude = 23.0332
//        obj.longitude = 72.5094
//        latlongModel.add(obj)
//
//        val obj1 = LatLongModel()
//        obj1.latitude = 23.03223
//        obj1.longitude = 72.50906
//        latlongModel.add(obj1)
//
//
//        val obj2 = LatLongModel()
//        obj2.latitude = 23.02858
//        obj2.longitude = 72.50792
//        latlongModel.add(obj2)
//
//
//        val obj3 = LatLongModel()
//        obj3.latitude = 23.02698
//        obj3.longitude = 72.50743
//        latlongModel.add(obj3)
//
//        val obj4 = LatLongModel()
//        obj4.latitude = 23.02459
//        obj4.longitude = 72.50664
//        latlongModel.add(obj4)
//
//        val obj5 = LatLongModel()
//        obj5.latitude = 23.02172
//        obj5.longitude = 72.50573
//        latlongModel.add(obj5)
//
//
//        val obj6 = LatLongModel()
//        obj6.latitude = 23.01843
//        obj6.longitude = 72.50473
//        latlongModel.add(obj6)
//
//
//        val obj7 = LatLongModel()
//        obj7.latitude = 23.00817
//        obj7.longitude = 72.50149
//        latlongModel.add(obj7)
//
//        val obj8 = LatLongModel()
//        obj8.latitude = 22.99743
//        obj8.longitude = 72.49815
//        latlongModel.add(obj8)
//
//        val obj9 = LatLongModel()
//        obj9.latitude = 22.99676
//        obj9.longitude = 72.49898
//        latlongModel.add(obj9)


    }

    private fun getDriverInfo() {
        val map = HashMap<String, String>()

        map["user_id"] = deliveryboy_id
        RetrofitClientSingleton.getInstance().getDriverInfo(SharedPref.getAuthToken(this), map)
                .enqueue(object : retrofit2.Callback<DriverInfo> {
                    override fun onFailure(call: Call<DriverInfo>?, t: Throwable?) {
                        stopProgress()
                        toast(this@TrackOrdersonMapsActivity, R.string.something_went_wrong)
                    }

                    override fun onResponse(call: Call<DriverInfo>?, response: Response<DriverInfo>?) {
                        stopProgress()
                        if (response!!.isSuccessful) {
                            val userDataResponse = response.body()

                            when (response.body()?.success) {
                                SUCCESS -> {
                                    val userBean = userDataResponse!!.data

                                    if (userBean != null) {


                                        val requestOptions = RequestOptions()
                                        requestOptions.placeholder(R.drawable.ic_user_placeholder)
                                        requestOptions.centerCrop()
                                        requestOptions.dontAnimate()
                                        requestOptions.error(R.drawable.ic_user_placeholder)

                                        Glide.with(this@TrackOrdersonMapsActivity)
                                                .load(userBean.get_avatar_url)
                                                .apply(requestOptions)
                                                .into(ivProfileMap)



                                        tvDriverName.text = userBean.driver_name
                                        tvDriverMobile.text = userBean.mobile
                                        val tempLat = latitudeDriver
                                        val tempLong = longitudeDriver

                                        if (latitudeDriver != 0.0) {


                                            if (tempLat != userBean.latitude!!.toDouble()) {
                                                latitudeDriver = userBean.latitude!!.toDouble()
                                                longitudeDriver = userBean.longitude!!.toDouble()
                                                if (userBean.angle!!.length > 0) {
                                                    bearing = userBean.angle!!.toDouble()

                                                }


                                                val obj = LatLongModel()
                                                obj.latitude = latitudeDriver
                                                obj.longitude = longitudeDriver
                                                latlongModel.add(obj)


                                                i++
                                                MarkerPoints.clear()

                                                setData(latitudeDriver, longitudeDriver)
                                                return
                                            }
                                        } else {
                                            latitudeDriver = userBean.latitude!!.toDouble()
                                            longitudeDriver = userBean.longitude!!.toDouble()
                                            if (userBean.angle!!.length > 0) {
                                                bearing = userBean.angle!!.toDouble()

                                            }

                                            val obj = LatLongModel()
                                            obj.latitude = latitudeDriver
                                            obj.longitude = longitudeDriver
                                            latlongModel.add(obj)


                                            i++
                                            MarkerPoints.clear()
                                            if (tempLat != userBean.latitude!!.toDouble())
                                                setData(latitudeDriver, longitudeDriver)
                                        }


                                    }


                                }

                                FAILURE -> {
                                    toast(this@TrackOrdersonMapsActivity, response.body()!!.message)
                                }
                                AUTH_FAILURE -> {
                                    forceLogout(this@TrackOrdersonMapsActivity)
                                }
                                else -> {
                                    toast(this@TrackOrdersonMapsActivity, R.string.something_went_wrong)
                                }
                            }
                        }
                    }

                })


    }


    override fun onMapReady(googleMap: GoogleMap) {
        mMap = googleMap

        mMap.setOnMapClickListener { point ->
            Log.e("lat", "" + point.latitude)
            Log.e("lon", "" + point.longitude)

        }
        checkLocationPermission()

    }

    private fun startLocationService() {
        if (!isMyServiceRunning(this, FusedLocationAPIService::class.java)) {
            val intent = Intent(this, FusedLocationAPIService::class.java)
            this.startService(intent)
        }
    }

    *//**
     * to check runtime permissions
     *//*
    private fun checkLocationPermission() {
        val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)

        if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_LOCATION)
        } else {
            enableGpsIfNotEnabled()
            if (::mMap.isInitialized) {
                mMap.isMyLocationEnabled = false
            }
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int,
                                            permissions: Array<String>,
                                            grantResults: IntArray) {
        if (requestCode == REQUEST_LOCATION) {
            if (grantResults.size == 1 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                checkLocationPermission()
            } else {
                showDialogPermissionRational(this, "Go to settings and allow location permission")
            }
        }
    }


    private fun enableGpsIfNotEnabled() {
        locationManager = getSystemService(Context.LOCATION_SERVICE) as LocationManager
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
            // toast(this, "enableGpsIfNotEnabled Gps is Enabled")
//            startLocationService()

//            latitudeDriver = latlongModel[i].latitude!!
//            longitudeDriver = latlongModel[i].longitude!!
//
//            setData(latlongModel[i].latitude!!, latlongModel[i].longitude!!)


            getDriverInfo()

            timer = object : CountDownTimer(8000, 8000) {

                override fun onTick(millisUntilFinished: Long) {
                    getDriverInfo()
                }

                override fun onFinish() {
                    try {
                        timer!!.start()
                    } catch (e: Exception) {
                        Log.e("Error", "Error: " + e.toString())
                    }

                }
            }.start()

        } else {
            enableGps()
        }
    }

    private fun enableGps() {
        googleApiClient = GoogleApiClient.Builder(this)
                .addApi(LocationServices.API).addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build()
        googleApiClient.connect()
        initLocationSetting()
    }

    private fun initLocationSetting() {
        locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = (1 * 1000).toLong()
        locationRequest.fastestInterval = (1 * 1000).toLong()

        locationSettingsRequest = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)

        initResults()

    }

    private fun initResults() {
        val locationRequest = LocationRequest.create()
        locationRequest.priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        locationRequest.interval = 1000
        locationRequest.fastestInterval = 1000
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val result = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build())
        result.addOnCompleteListener { task ->
            try {
                val response = task.getResult(ApiException::class.java)
                //request for locations

            } catch (exception: ApiException) {
                when (exception.statusCode) {
                    LocationSettingsStatusCodes.RESOLUTION_REQUIRED ->
                        // Location settings are not satisfied. But could be fixed by showing the
                        // user a dialog.
                    {
                        try {
                            // Cast to a resolvable exception.
                            val resolvable = exception as ResolvableApiException
                            // Show the dialog by calling startResolutionForResult(),
                            // and check the result in onActivityResult().
                            resolvable.startResolutionForResult(this@TrackOrdersonMapsActivity, REQUEST_LOCATION)
                        } catch (e: IntentSender.SendIntentException) {
                            // Ignore the error.
                        } catch (e: ClassCastException) {
                            // Ignore, should be an impossible error.
                        }
                    }

                    LocationSettingsStatusCodes.SETTINGS_CHANGE_UNAVAILABLE -> {
                    }
                }// Location settings are not satisfied. However, we have no way to fix the
                // settings so we won't show the dialog.
            }
        }
    }

    //callback method
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent) {
        val states = LocationSettingsStates.fromIntent(data)
        when (requestCode) {
            REQUEST_LOCATION -> when (resultCode) {
                Activity.RESULT_OK -> {
                    // All required changes were successfully made
//                    startLocationService()
                }
                Activity.RESULT_CANCELED ->
                    finish()
                else -> {
                }
            }
        }
    }

    override fun onConnectionFailed(p0: ConnectionResult) {
    }

    override fun onConnected(p0: Bundle?) {
    }

    override fun onConnectionSuspended(p0: Int) {
    }


    override fun onLocationChanged(location: Location) {
        toast(this, "${location.latitude} ${location.longitude}")
        Log.e(TAG, "onLocationChanged ${location.latitude} ${location.longitude}")

    }

    override fun onStatusChanged(provider: String, status: Int, extras: Bundle) {

    }

    override fun onProviderEnabled(provider: String) {

    }

    override fun onProviderDisabled(provider: String) {

    }

    private fun setData(lat: Double, longi: Double) {


        Log.e("CustomerLat", "" + latitudeCustomer)
        Log.e("longitudeCustomer", "" + longitudeCustomer)
        Log.e("ii", "" + i)


        val delivery = LatLng(lat, longi)
        val customer = LatLng(latitudeCustomer, longitudeCustomer)

        MarkerPoints.add(delivery)
        MarkerPoints.add(customer)





        if (i == 0) {
//            var bearing = 0.0
//            bearing = 198.00601196289062


            val destinationmaker = MarkerOptions()
            destinationmaker.position(customer)
            destinationmaker.icon(BitmapDescriptorFactory.fromResource(R.drawable.marker_black))
            mMap.addMarker(destinationmaker)

            val originmaker = MarkerOptions()
            originmaker.position(delivery)
            originmaker.title(tvDriverName.text.toString())
            originmaker.icon(BitmapDescriptorFactory.fromResource(R.drawable.deliveryboy_map))
            deliveryMarker = mMap.addMarker(originmaker)
            deliveryMarker.rotation = bearing.toFloat()


            val url = getUrl(delivery, customer)
            val FetchUrl = FetchUrl()
            FetchUrl.execute(url)


            val cameraPosition = CameraPosition.Builder()
                    .target(LatLng(lat, longi))
                    //.bearing(bearing.toFloat())
                    .zoom(18f).build()


            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 5000, null)
        }


        //todo animate delivery marker*************************************************

        if (i != 0) {


            val nextPosition = LatLng(lat, longi)
            val currentPosition = LatLng(latlongModel[i - 1].latitude!!, latlongModel[i - 1].longitude!!)

            var bearing = getBearing(currentPosition, nextPosition)

            // bearing -= 90.0

            val zoom = mMap.cameraPosition.zoom
            val cameraPosition = CameraPosition.Builder()
                    .target(LatLng(lat, longi))
                    //           .bearing(bearing.toFloat())
                    .zoom(zoom).build()

            deliveryMarker.position = delivery
            deliveryMarker.rotation = bearing.toFloat()

            mMap.animateCamera(CameraUpdateFactory.newCameraPosition(cameraPosition), 5000, null)

            animatedMarker(currentPosition, nextPosition, deliveryMarker)


        }

        //   distanceTimeTask()


    }

    private fun getUrl(origin: LatLng, dest: LatLng): String {

        // Origin of route
        val str_origin = "origin=" + origin.latitude + "," + origin.longitude

        // Destination of route
        val str_dest = "destination=" + dest.latitude + "," + dest.longitude


        // Sensor enabled
        val sensor = "sensor=false"

        // Building the parameters to the web service
        val parameters = "$str_origin&$str_dest&$sensor"

        // Output format
        val output = "json"

        // Building the url to the web service


        return "https://maps.googleapis.com/maps/api/directions/$output?$parameters"
    }

    @Throws(IOException::class)
    private fun downloadUrl(strUrl: String): String {
        var data = ""
        var iStream: InputStream? = null
        var urlConnection: HttpURLConnection? = null
        try {
            val url = URL(strUrl)

            // Creating an http connection to communicate with url
            urlConnection = url.openConnection() as HttpURLConnection

            // Connecting to url
            urlConnection.connect()

            // Reading data from url
            iStream = urlConnection.inputStream

            val br = BufferedReader(InputStreamReader(iStream!!))

            val sb = StringBuffer()


            var line: String?

            do {

                line = br.readLine()

                if (line == null)

                    break

                sb.append(line)


            } while (true)



            data = sb.toString()
            Log.d("downloadUrl", data)
            br.close()

        } catch (e: Exception) {
            Log.d("Exception", e.toString())
        } finally {
            iStream!!.close()
            urlConnection!!.disconnect()
        }
        return data
    }

    // Fetches data from url passed
    private inner class FetchUrl : AsyncTask<String, Void, String>() {

        override fun doInBackground(vararg url: String): String {

            // For storing data from web service
            var data = ""

            try {
                // Fetching the data from web service
                data = downloadUrl(url[0])



                Log.d("Background Task data", data)
            } catch (e: Exception) {
                Log.d("Background Task", e.toString())
            }

            return data
        }

        override fun onPostExecute(result: String) {
            super.onPostExecute(result)

            val parserTask = ParserTask()

            // Invokes the thread for parsing the JSON data
            parserTask.execute(result)

        }
    }

    *//**
     * A class to parse the Google Places in JSON format
     *//*
    private inner class ParserTask : AsyncTask<String, Int, List<List<HashMap<String, String>>>>() {

        // Parsing the data in non-ui thread
        override fun doInBackground(vararg jsonData: String): List<List<HashMap<String, String>>>? {

            val jObject: JSONObject
            var routes: List<List<HashMap<String, String>>>? = null

            try {
                jObject = JSONObject(jsonData[0])
                Log.d("ParserTask", jsonData[0])
                val parser = DataParser()
                Log.d("ParserTask", parser.toString())

                // Starts parsing data
                routes = parser.parse(jObject)
                Log.d("ParserTask", "Executing routes")
                Log.d("ParserTask", routes!!.toString())

            } catch (e: Exception) {
                Log.d("ParserTask", e.toString())
                e.printStackTrace()
            }

            return routes
        }

        // Executes in UI thread, after the parsing process
        override fun onPostExecute(result: List<List<HashMap<String, String>>>) {
            var points: ArrayList<LatLng>
            var lineOptions: PolylineOptions? = null

            // Traversing through all the routes
            for (i in result.indices) {
                points = ArrayList()
                lineOptions = PolylineOptions()

                // Fetching i-th route
                val path = result[i]

                // Fetching all the points in i-th route
                for (j in path.indices) {
                    val point = path[j]

                    val lat = point["lat"]!!.toDouble()
                    val lng = point["lng"]!!.toDouble()
                    val position = LatLng(lat, lng)

                    points.add(position)
                }

                // Adding all the points in the route to LineOptions
                lineOptions.addAll(points)
                lineOptions.width(12f)
                lineOptions.color(resources.getColor(R.color.black))

                Log.d("onPostExecute", "onPostExecute lineoptions decoded")

            }

            // Drawing polyline in the Google Map for the i-th route
            if (lineOptions != null) {
                mMap.addPolyline(lineOptions)
            } else {
                Log.d("onPostExecute", "without Polylines drawn")
            }
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                onBackPressed()
            }
        }
        return super.onOptionsItemSelected(item)

    }


    //todo to marker animation*********************************************************************

    private fun animatedMarker(startPosition: LatLng, nextPosition: LatLng, mMarker: Marker) {

        val handler = Handler()
        val start = SystemClock.uptimeMillis()
        val interpolator = AccelerateDecelerateInterpolator()
        val durationInMs = 5000f
        val hideMarker = false

        handler.post(object : Runnable {
            var elapsed: Long = 0
            var t: Float = 0.toFloat()
            var v: Float = 0.toFloat()

            override fun run() {
                // Calculate progress using interpolator
                elapsed = SystemClock.uptimeMillis() - start
                t = elapsed / durationInMs
                v = interpolator.getInterpolation(t)

                val currentPosition = LatLng(
                        startPosition.latitude * (1 - t) + nextPosition.latitude * t,
                        startPosition.longitude * (1 - t) + nextPosition.longitude * t)

                mMarker.position = currentPosition

                // Repeat till progress is complete.
                if (t < 1) {
                    // Post again 16ms later.
                    handler.postDelayed(this, 16)
                } else {
                    mMarker.isVisible = !hideMarker
                }
            }
        })

    }

    //todo to get bearing*********************************************************************
    private fun getBearing(begin: LatLng, end: LatLng): Double {
        val lat = Math.abs(begin.latitude - end.latitude)
        val lng = Math.abs(begin.longitude - end.longitude)
        if (begin.latitude < end.latitude && begin.longitude < end.longitude)
            return Math.toDegrees(Math.atan(lng / lat)).toFloat().toDouble()
        else if (begin.latitude >= end.latitude && begin.longitude < end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 90).toFloat().toDouble()
        else if (begin.latitude >= end.latitude && begin.longitude >= end.longitude)
            return (Math.toDegrees(Math.atan(lng / lat)) + 180).toFloat().toDouble()
        else if (begin.latitude < end.latitude && begin.longitude >= end.longitude)
            return (90 - Math.toDegrees(Math.atan(lng / lat)) + 270).toFloat().toDouble()
        return (-1f).toDouble()
    }

    override fun onClick(v: View) {
        when (v.id) {
            R.id.fabCall -> {
                val intent = Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + tvDriverMobile.text.toString()))
                startActivity(intent)
            }
        }
    }

    private fun distanceTimeTask() {
//        showProgressDialog(this, toolbar)

        RetrofitClientSingleton.getInstanceForGoogleApi().getDirection(SharedPref.getAuthToken(this), "$latitudeDriver,$longitudeDriver",
                "$latitudeCustomer,$longitudeCustomer", "driving", "false")
                .enqueue(object : retrofit2.Callback<EstimatedTimeModel> {
                    override fun onFailure(call: Call<EstimatedTimeModel>?, t: Throwable?) {
//                        stopProgress()
                        toast(this@TrackOrdersonMapsActivity, R.string.something_went_wrong)
                    }

                    override fun onResponse(call: Call<EstimatedTimeModel>?, response: Response<EstimatedTimeModel>?) {
//                        stopProgress()
                        if (response!!.isSuccessful) {
                            when (response.body()?.status) {
                                "OK" -> {
                                    val result = response.body()!!.routes[0].legs[0].steps[0].duration!!.text!!
                                    estimatedTime.text = "Your HOP Meal food is $result away..."
                                }
                                else -> {
                                }
                            }
                        }
                    }

                })
    }
//
//    //todo to get estimated time and distance*********************************************************************
//    private inner class DistanceTimeAsynTask : AsyncTask<String, Void, String>() {
//
//        override fun doInBackground(vararg params: String): String {
//            var stringBuilder = StringBuilder()
//            var dist: Double? = 0.0
//            var estimatedtime: String? = ""
//
//
//            try {
//
//                val url = "http://maps.googleapis.com/maps/api/directions/json?origin=" + latitudeDriver + "," + longitudeDriver + "&destination=" + latitudeCustomer + "," + longitudeCustomer + "&mode=driving&sensor=false"
//
//                val httppost = HttpPost(url)
//
//                val client = DefaultHttpClient()
//                val response: HttpResponse
//                stringBuilder = StringBuilder()
//
//
//                response = client.execute(httppost)
//                val entity = response.entity
//                val stream = entity.content
//                var b: Int
//
//
//                do {
//                    b = stream.read()
//                    if (b == -1)
//                        break
//                    stringBuilder.append(b.toChar())
//
//                } while (true)
//
//            }  catch (e: IOException) {
//            }
//
//            var jsonObject = JSONObject()
//            try {
//
//                jsonObject = JSONObject(stringBuilder.toString())
//
//                Log.e(TAG, "jsonObject >> $jsonObject")
//                val array = jsonObject.getJSONArray("routes")
//
//                val routes = array.getJSONObject(0)
//
//                val legs = routes.getJSONArray("legs")
//
//                val steps = legs.getJSONObject(0)
//
//                val distance = steps.getJSONObject("duration")
//                var time = distance.getString("text")
//                Log.i("time", time.toString())
//                Log.i("Distance", distance.toString())
//                dist = java.lang.Double.parseDouble(distance.getString("text").replace("[^\\.0123456789]".toRegex(), ""))
//                estimatedtime = "" + time.toString()
//
//
//            } catch (e: JSONException) {
//                e.printStackTrace()
//            }
//
//            return estimatedtime.toString()
//        }
//
//        override fun onPostExecute(result: String) {
//            estimatedTime.text = "Your HOP Meal food is " + result + " away..."
//            // Toast.makeText(this@MapsActivity, "" + result, Toast.LENGTH_SHORT).show()
//        }
//
//        override fun onPreExecute() {}
//
//    }


    override fun onBackPressed() {
        super.onBackPressed()
        if (timer != null) {
            timer!!.cancel()
        }
    }

    override fun onDestroy() {
        super.onDestroy()
        if (timer != null) {
            timer!!.cancel()
        }
    }
*/
}
