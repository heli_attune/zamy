package com.attune.zamy.activity

import android.content.Intent
import android.graphics.PorterDuff
import android.os.Bundle
import android.widget.ArrayAdapter
import androidx.appcompat.app.AppCompatActivity
import androidx.core.widget.doOnTextChanged
import com.attune.zamy.R
import com.attune.zamy.adapter.StoreAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.CalculateDistanceModel
import com.attune.zamy.model.StoreModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.activity_delivery1.*
import kotlinx.android.synthetic.main.title_toolbar.*
import retrofit2.Call
import retrofit2.Response
import java.util.*
import kotlin.collections.HashMap
import kotlin.collections.set

class DeliveryActivity : AppCompatActivity() {

    private lateinit var strPickupAddress: String
    private lateinit var strDeliveryAddress: String
    private lateinit var store: String
    lateinit var storeList: List<StoreModel.DataBean>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_delivery1)

        titleToolbar.text = "Pickup & Drop"

        btndelivery1NextStep.setOnClickListener {
//            val intent= Intent(applicationContext,Delivery2Activity::class.java)
//            startActivity(intent)
            validation()
//            finish()
        }

        if (isNetworkAvailable(this)) {
            callgetStoreApi()
        }


//        spstore.onItemSelectedListener = object :
//            AdapterView.OnItemSelectedListener {
//            override fun onItemSelected(
//                parent: AdapterView<*>,
//                view: View,
//                pos: Int,
//                id: Long
//            ) {
//                store = storeList[pos].res_name.toString()
//
//            }
//
//            override fun onNothingSelected(parent: AdapterView<*>?) {
//                store = storeList[0].res_name.toString()
//            }
//        }


        //pick up input layout
        tilItemDetails.editText?.doOnTextChanged { text, start, count, after ->
            when (text!!.length) {
                0 -> {
                    tilItemDetails.error = "Please Enter Item Details"
                }
                else -> {
                    tilItemDetails.error = ""
                }

            }
        }


        tilPickupAddress.editText?.doOnTextChanged { text, start, count, after ->
            when (text!!.length) {
                0 -> {
                    tilPickupAddress.error = "Please Enter Pick up address"
                }
                in 8..50 -> {
                    tilPickupAddress.error = ""
                    tilPickupAddress.setEndIconTintMode(PorterDuff.Mode.CLEAR)
                }
                else -> {
                    tilPickupAddress.error = ""
                    tilPickupAddress.setEndIconTintMode(PorterDuff.Mode.CLEAR)
                }

            }
        }

        tilPickUpPinCode.editText?.doOnTextChanged { text, start, count, after ->
            when (text!!.length) {
                0 -> {
                    tilPickUpPinCode.error = "Please Enter Pin Code"
                }
                in 1..5 -> {
                    tilPickUpPinCode.error = "Please Enter Valid Pin Code"
                }
                else -> {
                    tilPickUpPinCode.error = ""
                    tilPickUpPinCode.setEndIconTintMode(PorterDuff.Mode.CLEAR)
                }

            }
        }

        tnlPickupContactNo.editText?.doOnTextChanged { text, start, count, after ->

            when (text!!.length) {
                0 -> {
                    tnlPickupContactNo.error = "Please Enter Mobile"
                }
                in 1..9 -> {
                    tnlPickupContactNo.error = "Please Enter Valid Mobile"
                }
                else -> {
                    tnlPickupContactNo.error = ""
                    tnlPickupContactNo.setEndIconTintMode(PorterDuff.Mode.CLEAR)
                }

            }
        }

        //delivery input layout
        tilDeliveryAddress.editText?.doOnTextChanged { text, start, count, after ->

            when (text!!.length) {
                0 -> {
                    tilDeliveryAddress.error = "Please Enter Delivery Address"
                }
                else -> {
                    tilDeliveryAddress.error = ""
                    tilDeliveryAddress.setEndIconTintMode(PorterDuff.Mode.CLEAR)
                }

            }
        }

        tilDeliveryPinCode.editText?.doOnTextChanged { text, start, count, after ->
            when (text!!.length) {
                0 -> {
                    tilDeliveryPinCode.error = "Please Enter Pin Code"
                }
                in 1..5 -> {
                    tilDeliveryPinCode.error = "Please Enter Valid Pin Code"
                }
                else -> {
                    tilDeliveryPinCode.error = ""
                    tilDeliveryPinCode.setEndIconTintMode(PorterDuff.Mode.CLEAR)
                }

            }
        }

        tilDeliveryContactNo.editText?.doOnTextChanged { text, start, count, after ->

            when (text!!.length) {
                0 -> {
                    tilDeliveryContactNo.error = "Please Enter Mobile"
                }
                in 1..9 -> {
                    tilDeliveryContactNo.error = "Please Enter Valid Mobile"
                }
                else -> {
                    tilDeliveryContactNo.error = ""
                    tilDeliveryContactNo.setEndIconTintMode(PorterDuff.Mode.CLEAR)
                }

            }
        }


    }

    private fun validation() {
        if (edItemdetailsforpickup?.text.toString().trim().isEmpty()) {
            tilItemDetails.error = "Please Enter Pick Up Item"
        } else if (edPickupaddress?.text.toString().trim().isEmpty()) {
            tilPickupAddress.error = getString(R.string.paddress)
        } else if (edPincode.text.toString().trim().isEmpty()) {
            tilPickUpPinCode.error = "Please Enter Pin Code"
        } else if (edPickupcontactno.text.toString().trim().isEmpty()) {
            tnlPickupContactNo.error = "Please Enter Pick Up Contact"
        } else if (edDeliveryaddress?.text.toString().trim().isEmpty()) {
            tilDeliveryAddress.error = getString(R.string.daddress)
        } else if (edDeliveryPincode.text.toString().trim().isEmpty()) {
            tilDeliveryPinCode.error = "Please Enter Delivery Pin Code"
        } else if (edDeliverycontactno.text.toString().trim().isEmpty()) {
            tilDeliveryContactNo.error = "Please Enter Delivery Contact Number"
        } else {
            strPickupAddress = edPickupaddress?.text.toString()
            strDeliveryAddress = edDeliveryaddress?.text.toString()
            if (!isNetworkAvailable(this@DeliveryActivity)) {
                Toast(this@DeliveryActivity, getString(R.string.network_error))
            } else {
                CalculateDistance()
            }
        }
        /* strEmail =  "john"
         strPassword =  "123"
         doLogin()
 */
    }

    private fun callgetStoreApi() {
        CustomProgressbar.showProgressBar(
            this
            , false
        )
        RetrofitClientSingleton.getInstance().getStore()
            .enqueue(object : retrofit2.Callback<StoreModel> {
                override fun onFailure(call: Call<StoreModel>?, t: Throwable?) {
                    CustomProgressbar.hideProgressBar()
                    Toast(this@DeliveryActivity, getString(R.string.something_went_wrong))
                }

                override fun onResponse(call: Call<StoreModel>?, response: Response<StoreModel>?) {
                    CustomProgressbar.hideProgressBar()
                    if (response!!.isSuccessful) {
                        val categoryList = response.body()
                        storeList = response.body()!!.data!!
                        when (response.body()?.status) {
                            SUCCESS -> {
                                if (response.isSuccessful)
                                {
                                    //spinner
                                    val adapter = StoreAdapter(this@DeliveryActivity,
                                        storeList as ArrayList<StoreModel.DataBean>
                                    )
//                                    spstore.adapter = adapter
                                    //custom list adapter
                                    /*val AutoCompleteAdapter =
                                        StoreNameAdapter(this@DeliveryActivity,
                                            storeList as ArrayList<StoreModel.DataBean>
                                        )
                                    actvstorename.setAdapter(AutoCompleteAdapter)*/

                                    //static list adapter
                                   // Get the string array
//                                    val countries: Array<out String> = resources.getStringArray(R.array.countries_array)
                                    val countries: List<String?> =storeList.map { it.res_name }
                                    // Create the adapter and set it to the AutoCompleteTextView
                                    ArrayAdapter<String>(this@DeliveryActivity, android.R.layout.simple_list_item_1, countries).also { adapter ->
                                        actvstorename.setAdapter(adapter)
                                        }

                                } else {
                                    Toast(
                                        this@DeliveryActivity,
                                        getString(R.string.something_went_wrong)
                                    )
                                }

                                /**Set item at position using predefine**/
                                /* if (isEdit) {
                                     spShippingArea.setSelection(
                                         getIndex(
                                             spShippingArea,
                                             mAddressObject.shipping_area.toString(),
                                             shippingAreaList
                                         )
                                     )
                                 }*/

                            }
                            FAILURE -> {
                                Toast(this@DeliveryActivity, categoryList!!.message)
                            }
                            AUTH_FAILURE -> {
                                //forceLogout(this@ComposeTicketActivity)
                            }
                        }
                    }
                }

            })
    }

    private fun CalculateDistance() {
        CustomProgressbar.showProgressBar(this, false)
        val map = HashMap<String, String>()
        map["pickup_address"] = strPickupAddress
        map["delivery_address"] = strDeliveryAddress
        RetrofitClientSingleton.getInstance().CalculateDistanceApi(map)
            .enqueue(object : retrofit2.Callback<CalculateDistanceModel> {
                override fun onFailure(call: Call<CalculateDistanceModel>, t: Throwable) {
                    Toast(this@DeliveryActivity, t.message)
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(
                    call: Call<CalculateDistanceModel>,
                    response: Response<CalculateDistanceModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    val CalculateDistanceResponse = response.body()
                    if (CalculateDistanceResponse != null) {
                        when (CalculateDistanceResponse.status) {
                            SUCCESS -> {
                                val userBean = CalculateDistanceResponse.data
                                userBean?.let {

                                }
                               /* Toast(
                                    this@DeliveryActivity,
                                    CalculateDistanceResponse.message!!*//*
                                )*/
                                val intent =
                                    Intent(applicationContext, Delivery2Activity::class.java)
                                intent.putExtra(
                                    "user_id",
                                    SharedPref.getUserId(this@DeliveryActivity)
                                )
                                intent.putExtra("store", actvstorename.text.toString())
                                intent.putExtra(
                                    "item_details",
                                    edItemdetailsforpickup?.text.toString()
                                )
                                intent.putExtra("pickup_address", edPickupaddress?.text.toString())
                                intent.putExtra("pickup_pincode", edPincode?.text.toString())
                                intent.putExtra(
                                    "pickup_contact_no",
                                    edPickupcontactno?.text.toString()
                                )
                                intent.putExtra(
                                    "delivery_address",
                                    edDeliveryaddress?.text.toString()
                                )
                                intent.putExtra(
                                    "delivery_pincode",
                                    edDeliveryPincode?.text.toString()
                                )
                                intent.putExtra(
                                    "delivery_contact_no",
                                    edDeliverycontactno?.text.toString()
                                )
                                intent.putExtra(
                                    "from_to_distance",
                                    CalculateDistanceResponse.data?.distance!!
                                )
                                intent.putExtra(
                                    "km_amount",
                                    CalculateDistanceResponse.data?.amount!!
                                )
                                intent.putExtra(
                                    "pickup_lattitude",
                                    CalculateDistanceResponse.data?.latitudeFrom!!
                                )
                                intent.putExtra(
                                    "pickup_longitude",
                                    CalculateDistanceResponse.data?.longitudeFrom!!
                                )
                                intent.putExtra(
                                    "delivery_lattitude",
                                    CalculateDistanceResponse.data?.latitudeTo!!
                                )
                                intent.putExtra(
                                    "delivery_longitude",
                                    CalculateDistanceResponse.data?.longitudeTo!!
                                )
                                startActivity(intent)
                            }
                            FAILURE -> {
                                Toast(this@DeliveryActivity, CalculateDistanceResponse.message!!)
                            }
                            else -> {
                                //toast(this@LoginPage, R.string.something_went_wrong)
                            }
                        }
                    }
                }
            })
    }
}