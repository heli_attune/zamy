package com.attune.zamy.activity

import android.app.Dialog
import android.app.ProgressDialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.http.SslError
import android.os.Bundle
import android.util.Log
import android.view.MenuItem
import android.view.View
import android.view.Window
import android.webkit.*
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.R
import com.attune.zamy.utils.Toast
import com.attune.zamy.utils.isNetworkAvailable
import com.attune.zamy.utils.keyOrderId
import kotlinx.android.synthetic.main.activity_payment.*
import kotlinx.android.synthetic.main.dialog_cancel_payment.*
import kotlinx.android.synthetic.main.layout_toolbar.*


class PaymentActivity : AppCompatActivity() {

    var TAG = PaymentActivity::class.java.simpleName
    var orderId: String? = null
    private var progDailog: ProgressDialog? = null
    var paymentUrl = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_payment)
        txtTitle.text="Payment"
        imgback.setOnClickListener {
            onBackPressed()
            finish()
        }


        if (intent.hasExtra(keyOrderId)) {
            orderId = intent.extras!!.getString(keyOrderId)
            paymentUrl = intent.extras!!.getString("payment_url")!!
        }
        setView()
    }

    private fun setView() {

        progDailog = ProgressDialog(this)
        progDailog?.setMessage(getString(R.string.please_wait))
        progDailog?.setCancelable(false)
        wvPayment.webViewClient = MyWebViewClient()
        wvPayment.webChromeClient = MyChromeClient()
        wvPayment.settings.javaScriptEnabled = true

        wvPayment.settings.loadWithOverviewMode = true
        wvPayment.settings.useWideViewPort = true

        wvPayment.settings.builtInZoomControls = false
        wvPayment.settings.setSupportZoom(false)
        wvPayment.settings.javaScriptCanOpenWindowsAutomatically = true
        wvPayment.settings.allowFileAccess = true
        wvPayment.settings.domStorageEnabled = true

        if(isNetworkAvailable(this)) {
            wvPayment.visibility = View.VISIBLE
            wvPayment.loadUrl(paymentUrl)
        } else {
            wvPayment.visibility = View.GONE
        }
    }
  /*  Visa card details
    no. :4263982640269299
    expiry date : 04/2023
    cvv : 738*/
    private inner class MyWebViewClient : WebViewClient() {

        override fun shouldOverrideUrlLoading(view: WebView, request: WebResourceRequest?): Boolean {

            Log.e(TAG, "shouldOverrideUrlLoading : url >> ${view.url}")
            // view.loadUrl(url)
            return super.shouldOverrideUrlLoading(view, request)
        }

        override fun onPageFinished(view: WebView, url: String) {
            super.onPageStarted(view, url, null)
            Log.e(TAG, "onPageFinished : url >> $url")

            val intent = Intent(this@PaymentActivity, OrderSuccessActivity::class.java)
            if (url.contains("success")) {

                Log.e("payment", "Success")

                intent.putExtra("PaymentStatus", "Success")
                intent.putExtra(keyOrderId, orderId)
                this@PaymentActivity.startActivity(intent)

            } else if (url.contains("failure")) {
                Log.e("payment", "Failure")
                intent.putExtra("PaymentStatus", "Failure")
                this@PaymentActivity.startActivity(intent)
            } else if(url.contains("cancelled")) {
                Log.e("payment", "cancelled")
                intent.putExtra("PaymentStatus", "Cancelled")
                this@PaymentActivity.startActivity(intent)
            }

        }

        override fun onReceivedError(view: WebView?, request: WebResourceRequest?, error: WebResourceError?) {
            Log.e(TAG, "onReceivedError : error >> $error")
            super.onReceivedError(view, request, error)

        }


      override fun onReceivedSslError(view: WebView, handler: SslErrorHandler, error: SslError) {
          val builder = AlertDialog.Builder(this@PaymentActivity)
          var message = "SSL Certificate error."
          when (error.primaryError) {
              SslError.SSL_UNTRUSTED -> message = "The certificate authority is not trusted."
              SslError.SSL_EXPIRED -> message = "The certificate has expired."
              SslError.SSL_IDMISMATCH -> message = "The certificate Hostname mismatch."
              SslError.SSL_NOTYETVALID -> message = "The certificate is not yet valid."
          }
          message += " Do you want to continue anyway?"

          builder.setTitle("SSL Certificate Error")
          builder.setMessage(message)
          builder.setPositiveButton("continue"
          ) { dialog, which -> handler.proceed() }
          builder.setNegativeButton("cancel"
          ) { dialog, which -> handler.cancel() }
          val dialog = builder.create()
          dialog.show()
      }

    }

    private fun cancelledPaymentDialog() {
        val dialog = Dialog(this)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.setContentView(R.layout.dialog_cancel_payment)

        dialog.show()

        dialog.llCancelPayment.setOnClickListener {
            dialog.dismiss()
        }

        dialog.llOkPayment.setOnClickListener {
            dialog.dismiss()

            if (!isNetworkAvailable(this@PaymentActivity)) {
                Toast(this@PaymentActivity, getString(R.string.network_error))
            } else {
//                if (paymentMethod == "paypal_express") {
                val intent = Intent(this@PaymentActivity, OrderSuccessActivity::class.java)
                intent.putExtra("PaymentStatus", "Failure")
                intent.putExtra(keyOrderId, orderId)
                startActivity(intent)
//                } else {
//                    cancelledPayment(dialog.llOkPayment)
//                }
            }
        }
    }

    override fun onBackPressed() {
        if (wvPayment.canGoBack()) {
            wvPayment.goBack()
        } else {
            // Let the system handle the back button
            cancelledPaymentDialog()
        }
    }

    override fun onOptionsItemSelected(item: MenuItem?): Boolean {
        when (item?.itemId) {
            android.R.id.home -> {
                cancelledPaymentDialog()
            }
        }
        return super.onOptionsItemSelected(item)
    }

    inner class MyChromeClient : WebChromeClient() {
        override fun onPermissionRequest(request: PermissionRequest) {
            this@PaymentActivity.runOnUiThread {
                if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.LOLLIPOP) {
                    request.grant(request.resources)
                }
            }
        }


        override fun onProgressChanged(view: WebView?, newProgress: Int) {
            super.onProgressChanged(view, newProgress)

            if (newProgress == 100) {
                if (progDailog != null) {
                    progDailog?.dismiss()
                }
            } else {
                if (progDailog != null) {
                    progDailog?.show()
                }
            }
        }

    }

    override fun onStop() {
        if (progDailog != null && progDailog!!.isShowing) {
            progDailog?.dismiss()
        }

        progDailog = null

        super.onStop()
    }
}
