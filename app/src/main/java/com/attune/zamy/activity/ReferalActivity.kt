package com.attune.zamy.activity

import android.content.ClipData
import android.content.ClipboardManager
import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.R
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.model.ReferralInfoModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.activity_referal.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import kotlinx.android.synthetic.main.no_data_layout.*
import retrofit2.Call
import retrofit2.Response

class ReferalActivity : AppCompatActivity(), View.OnClickListener {


    private var strInviteMessage = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_referal)

        customtoolbar.txtTitle.text = "My Referral Point "
        customtoolbar.imgback.setOnClickListener {
            finish()
        }
        initLoading()
    }

    private fun initLoading() {
        getRefrrealCode()

        ivCopyReferral.setOnClickListener {
            copyToClipBoard(this, tvReferralCode.text.toString())
            Toast(this, "Copied to clipboard")
        }
        btnInviteFriends.setOnClickListener {
            shareApp(this, strInviteMessage)
        }


    }

    fun getRefrrealCode() {
        CustomProgressbar.showProgressBar(this, false)
        val map = HashMap<String, String>()
        map["user_id"] = SharedPref.getUserId(this)
        RetrofitClientSingleton
            .getInstance()
            .getReferralCode(map)
            .enqueue(object : retrofit2.Callback<ReferralInfoModel> {
                override fun onFailure(call: Call<ReferralInfoModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                    Log.e("TAG", t.message)
                }

                override fun onResponse(
                    call: Call<ReferralInfoModel>,
                    response: Response<ReferralInfoModel>
                ) {

                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        if (response != null) {
                            when (response.body()!!.status) {
                                SUCCESS -> {
                                    setReferralData(response.body()!!.data!!)

                                }
                                FAILURE -> {

                                }
                                AUTH_FAILURE -> {

                                }
                            }
                        }
                    } else {
                        showNoDataLayout(llParentNoData, getString(R.string.no_data_fnd))
                        llReferral.visibility = View.GONE
                    }

                }

            })
    }


    override fun onClick(v: View) {
    }

    private fun setReferralData(referralBean: ReferralInfoModel.DataBean) {
        llReferral.visibility = View.VISIBLE
        tvReferralCode.text = referralBean.reffaral_code
        tvReferralValidity.text = referralBean.offer_validity
        tvReferralOffer.text = referralBean.offer
        tvReferralPoint.text = referralBean.earning_point

        val requestOptions = RequestOptions()
        requestOptions.placeholder(R.drawable.ic_place_holder)
        requestOptions.centerCrop()
        requestOptions.dontAnimate()
        requestOptions.error(R.drawable.ic_place_holder)

        /*Glide.with(mView!!.context)
                .load(referralBean.main_image)
                .apply(requestOptions)
                .into(mView!!.ivReferral)*/

        Glide.with(this)
            .load(referralBean.main_image)
            .apply(requestOptions)
            .into(ivReferralCover)

        strInviteMessage = referralBean.invite_message!!
    }

    fun copyToClipBoard(context: Context, str: String) {
        var clipboard = context.getSystemService(Context.CLIPBOARD_SERVICE) as ClipboardManager
        val clip = ClipData.newPlainText("Copied text", str)
        clipboard.setPrimaryClip(clip)
    }
}
