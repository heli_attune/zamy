package com.attune.zamy.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.R
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*

class OrderRatingActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_order_rating)

        customtoolbar.txtTitle.text = "Rating"

        customtoolbar.imgback.setOnClickListener {
            finish()
        }
    }
}
