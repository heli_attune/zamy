package com.attune.zamy.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.Fragment
import com.attune.zamy.R
import com.attune.zamy.fragment.CartToolbarFragment
import com.attune.zamy.fragment.RestaurantsDetailsFragment
import com.attune.zamy.fragment.TopGroceryFragment
import com.attune.zamy.fragment.TopRestaurantFragment
import com.attune.zamy.utils.RESTAURANT_ID_PASS


class TopRestaurantsPage : AppCompatActivity() {
    private lateinit var restaurant_id: String
    lateinit var screenName: String
    lateinit var direction:String
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        setContentView(R.layout.activity_top_restaurants_page)
        initView()

    }

    private fun initView() {
        val intent = intent

        restaurant_id = intent.getStringExtra(RESTAURANT_ID_PASS) ?: "0"
        screenName = intent.getStringExtra("PageName") ?: "Top"

        when (screenName) {
            "Top" -> {
                pushFragments(TopRestaurantFragment(), false, false, true, false, "", null)
            }
            "Grocery" -> {
                pushFragments(TopGroceryFragment(), false, false, true, false, "", null)
            }

            "Detail" -> {
                val args = Bundle()
                args.putString(RESTAURANT_ID_PASS, restaurant_id)
                pushFragments(RestaurantsDetailsFragment(), false, false, true, false, "", args)

            }
            "ViewCart" -> {
                val args = Bundle()
                args.putString(RESTAURANT_ID_PASS, restaurant_id)
                pushFragments(CartToolbarFragment(), false, false, true, false, "", args)

            }

        }


    }


    private val currentFragment: Fragment?
        get() {
            val fm = supportFragmentManager
            return fm.findFragmentById(R.id.fragment_restaurant)
        }

    //fragments common methods
    private fun pushFragments(
        fragment: Fragment, isAnimate: Boolean, isAdd: Boolean, isReplace: Boolean,
        isAddToBackStack: Boolean, tag: String, bundle: Bundle?
    ) {

        if (bundle != null) {
            fragment.arguments = bundle
        }


        val fragmentManager = supportFragmentManager
        // Or: FragmentManager fragmentManager = getSupportFragmentManager() fro below 4.0 support
        val fragmentTransaction = fragmentManager.beginTransaction()

        if (currentFragment != null) {
            if (currentFragment!!.tag!!.equals(tag, ignoreCase = true)) {
                return
            }
        }



        when {
            isAdd -> {
                //add fragments
                fragmentTransaction.add(R.id.fragment_restaurant, fragment, tag)
            }
            isReplace -> {
                //replace fragments
                fragmentTransaction.replace(R.id.fragment_restaurant, fragment, tag)
            }
            else -> {
            }
        }

        //determine backstack
        if (isAddToBackStack) {
            fragmentTransaction.addToBackStack(tag)
        }
        if (currentFragment != null) {
            fragmentTransaction.hide(currentFragment!!)
        }

        if (!isFinishing) {
            fragmentTransaction.commitAllowingStateLoss()
        }
    }

    override fun onBackPressed() {
        if (fragmentManager.backStackEntryCount > 0) {
            fragmentManager.popBackStack()
        } else {
            super.onBackPressed()
        }
    }
}
