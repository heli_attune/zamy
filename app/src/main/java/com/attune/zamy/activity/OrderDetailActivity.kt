package com.attune.zamy.activity

import android.annotation.SuppressLint
import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.LinearLayout
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.attune.zamy.R
import com.attune.zamy.adapter.MyOrderDetailListAdapter
import com.attune.zamy.custom.CustomProgressbar
import com.attune.zamy.interaface.onReturnItemClick
import com.attune.zamy.model.CancelOrderModel
import com.attune.zamy.model.MyOrderDetailsModel
import com.attune.zamy.model.SuccessModel
import com.attune.zamy.retrofitClient.RetrofitClientSingleton
import com.attune.zamy.utils.*
import kotlinx.android.synthetic.main.activity_order_detail.*
import kotlinx.android.synthetic.main.layout_toolbar.*
import kotlinx.android.synthetic.main.layout_toolbar.view.*
import retrofit2.Call
import retrofit2.Response


class OrderDetailActivity : AppCompatActivity(), onReturnItemClick {

    lateinit var orderStatus: String
    var orderId = ""
    var deliveryboy_id = ""
    var latitude = ""
    var longitude = ""
    val linearLayoutManager = LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false)
    lateinit var delivery_boy_data: MyOrderDetailsModel.DataBean.OrderBean.OrderTrackingBean
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState ?: Bundle())
        setContentView(R.layout.activity_order_detail)


        intiView()


    }

    private fun intiView() {
        orderId = intent.extras!!["order_id"].toString()
        if (isNetworkAvailable(this)) {
            getMyOrderListApi(orderId)

        } else {
            Toast(this, getString(R.string.no_internet_connection))
        }

        toolBarSetup()
        btnCancel.setOnClickListener {
            if (!isNetworkAvailable(this)) {
                Toast(this, getString(R.string.no_internet_connection))
            } else {
                cancelOrderAPI()
            }
        }
        imgCustomerSupport.visibility = View.VISIBLE
        imgCustomerSupport.setOnClickListener {
            val intent =
                Intent(
                    this@OrderDetailActivity,
                    CreateTicketActivity::class.java
                )
            intent.putExtra("orderId", orderId)
            startActivity(intent)
        }
    }

    private fun toolBarSetup() {
        customtoolbar.txtTitle.text = "Order Details"
        customtoolbar.imgback.setOnClickListener {
            finish()
            imgCustomerSupport.visibility = View.GONE
        }


    }

    /* private fun getMyTrackingOrder() {
         CustomProgressbar.showProgressBar(this@OrderDetailActivity, false)
         val map = HashMap<String, String>()
         map["order_id"] = "44"
         RetrofitClientSingleton
             .getInstance()
             .getMyOrderTracking(map)
             .enqueue(object : retrofit2.Callback<OrderTrackingModel> {
                 override fun onResponse(
                     call: Call<OrderTrackingModel>,
                     response: Response<OrderTrackingModel>
                 ) {
                     val apiResponse = response.body()!!.data!!
                     if (response.isSuccessful) {
                         when (response.body()!!.status) {
                             SUCCESS -> {
                                 if (apiResponse.order != null) {
                                     customtoolbar.imgMap.visibility = View.VISIBLE
                                     customtoolbar.imgMap.setOnClickListener {
                                         val intent =
                                             Intent(
                                                 this@OrderDetailActivity,
                                                 OrderTrackingMapActivity::class.java
                                             )
                                         intent.putExtra("deliveryboy_data", apiResponse)

                                         startActivity(intent)
                                     }
                                 }
                             }
                             FAILURE -> {

                             }
                         }
                     }
                 }

                 override fun onFailure(call: Call<OrderTrackingModel>, t: Throwable) {
                 }

             })

     }*/

    private fun getMyOrderListApi(orderId: String) {

        CustomProgressbar.showProgressBar(this@OrderDetailActivity, false)
        val map = HashMap<String, String>()
        map["order_id"] = orderId
        RetrofitClientSingleton
            .getInstance()
            .getMyOrderDetails(map)
            .enqueue(object : retrofit2.Callback<MyOrderDetailsModel> {
                override fun onFailure(call: Call<MyOrderDetailsModel>, t: Throwable) {

                    CustomProgressbar.hideProgressBar()
                }

                @SuppressLint("SetTextI18n")
                override fun onResponse(
                    call: Call<MyOrderDetailsModel>,
                    response: Response<MyOrderDetailsModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    val apiResponse = response.body()!!.data!!.order
                    if (apiResponse != null) {
                        if (response.isSuccessful) {
                            when (response.body()!!.status) {
                                SUCCESS -> {
                                    val mAdapter = MyOrderDetailListAdapter(
                                        this@OrderDetailActivity,
                                        apiResponse, this@OrderDetailActivity
                                    )
                                    rvDetailOrder.layoutManager = linearLayoutManager
                                    rvDetailOrder.adapter = mAdapter
                                    val lLayout = findViewById<LinearLayout>(R.id.llStatus)
                                    if (apiResponse.id != null) {
                                        tvOrderId.text = "#" + apiResponse.id.toString()

                                    }
                                    if (apiResponse.created_date != null) {
                                        tvOrderDate.text = apiResponse.created_date.toString()
                                    }

                                    tvOrderStatus.text = apiResponse.order_status
                                    orderStatus = apiResponse.order_status.toString()
                                    if (apiResponse.order_status.equals("Waiting for Acceptance")) {
                                        tvOrderStatus.setTextColor(resources.getColor(R.color.white))
                                        lLayout.setBackgroundColor(resources.getColor(R.color.colorPrimary))
                                        btnCancel.visibility = View.VISIBLE
                                    }
                                    if (apiResponse.order_status.equals("completed")) {
                                        lLayout.setBackgroundColor(resources.getColor(R.color.green))
                                        tvOrderStatus.setTextColor(resources.getColor(R.color.white))
                                        imgMap.visibility = View.GONE
                                    }
                                    if (apiResponse.order_status.equals("processing")) {
                                        lLayout.setBackgroundColor(resources.getColor(R.color.orange))
                                        tvOrderStatus.setTextColor(resources.getColor(R.color.white))
                                        btnCancel.visibility = View.VISIBLE
                                        imgMap.visibility = View.GONE
                                    }
                                    if (apiResponse.order_status.equals("pending")) {
                                        lLayout.setBackgroundColor(resources.getColor(R.color.yellow))
                                        tvOrderStatus.setTextColor(resources.getColor(R.color.white))
                                        btnCancel.visibility = View.VISIBLE
                                        imgMap.visibility = View.GONE

                                    }
                                    if (apiResponse.order_status.equals("cancel-request")) {
                                        lLayout.setBackgroundColor(resources.getColor(R.color.red_ea1b25))
                                        tvOrderStatus.setTextColor(resources.getColor(R.color.white))
                                        imgMap.visibility = View.GONE
                                    }
                                    if (apiResponse.order_status.equals("on-hold")) {
                                        lLayout.setBackgroundColor(resources.getColor(R.color.gray))
                                        tvOrderStatus.setTextColor(resources.getColor(R.color.white))
                                        imgMap.visibility = View.GONE
                                    }
                                    if (apiResponse.order_status.equals("cancelled")) {
                                        lLayout.setBackgroundColor(resources.getColor(R.color.red_ea1b25))
                                        tvOrderStatus.setTextColor(resources.getColor(R.color.white))
                                        imgMap.visibility = View.GONE
                                    }
                                    if (apiResponse.order_status.equals("failed")) {
                                        lLayout.setBackgroundColor(resources.getColor(R.color.red_ea1b25))
                                        tvOrderStatus.setTextColor(resources.getColor(R.color.white))
                                        imgMap.visibility = View.GONE
                                    }

                                    if (apiResponse.order_status.equals("On the way")) {
                                        lLayout.setBackgroundColor(resources.getColor(R.color.red_ea1b25))
                                        tvOrderStatus.setTextColor(resources.getColor(R.color.white))
                                        imgMap.visibility = View.VISIBLE
                                        setMapimageVisible()
                                    }

                                    if (apiResponse.customer_name != null) {
                                        tvName.text = apiResponse.customer_name
                                    }
                                    if (apiResponse.order_total != null) {
                                        tvSubTotal.text = "₹ " + apiResponse.sub_total
                                    }
                                    if (apiResponse.delivery_charge != null) {
                                        tvDeliveryCharge.text = "₹ " + apiResponse.delivery_charge
                                    }
                                    if (apiResponse.gst != null) {
                                        tvGst.text = "₹ " + apiResponse.gst
                                    }
                                    if (apiResponse.payment_method != null) {
                                        tvPaymentMethod.text = apiResponse.payment_method
                                    }
                                    if (apiResponse.discount_amount != null) {
                                        tvDiscountAmount.text = "₹ -" + apiResponse.discount_amount
                                    }
                                    if (apiResponse.sub_total != null) {
                                        tvTotal.text = "₹ " + apiResponse.order_total
                                    }
                                    /* if (apiResponse.location != null) {
                                         tvLandmark.text = apiResponse.location
                                     } else {
                                         tvLandmark.visibility = View.GONE
                                     }*/
                                    if (apiResponse.shipping_address != null) {
                                        tvOrderDetailAddress.text = apiResponse.shipping_address
                                    }
                                    if (apiResponse.order_tracking != null) {
                                        delivery_boy_data = apiResponse.order_tracking!!
//                                        setMapimageVisible()
                                    }
                                    orderStatusButtonShowHide()
                                }
                                FAILURE -> {

                                }
                            }
                        }
                    } else {
                        Toast(this@OrderDetailActivity, getString(R.string.something_went_wrong))
                    }

                }

            })
    }

    fun orderStatusButtonShowHide() {
        if (orderStatus.equals("pending") || orderStatus.equals("processing") || orderStatus.equals(
                "Waiting for Acceptance"
            )
        ) {
            btnCancel.visibility = View.VISIBLE
        } else {
            btnCancel.visibility = View.GONE
        }
    }


    private fun setMapimageVisible() {
        imgMap.visibility = View.VISIBLE

        imgMap.setOnClickListener {
            val intent =
                Intent(
                    this@OrderDetailActivity,
                    OrderTrackingMapActivity::class.java
                )
            intent.putExtra("deliveryboy_data", delivery_boy_data)
            intent.putExtra("order_id", orderId)

            if (delivery_boy_data.customer_latitude != null && delivery_boy_data.customer_longitude != null) {
                if (delivery_boy_data.customer_latitude!!.isNotEmpty()) {
                    intent.putExtra("latitude", delivery_boy_data.customer_latitude!!.toDouble())
                } else {
                    intent.putExtra("latitude", 23.08806)
                }
                if (delivery_boy_data.customer_longitude!!.isNotEmpty()) {
                    intent.putExtra("longitude", delivery_boy_data.customer_longitude!!.toDouble())
                } else {
                    intent.putExtra("longitude", 72.58524)
                }
            } else {
                intent.putExtra("latitude", 23.08806)
                intent.putExtra("latitude", 72.58524)
            }

            startActivity(intent)
        }
    }

    private fun cancelOrderAPI() {
        CustomProgressbar.showProgressBar(this, false)
        val map = HashMap<String, String>()
        map["order_id"] = orderId

        CustomProgressbar.showProgressBar(this, false)
        RetrofitClientSingleton
            .getInstance()
            .cancelOrder(map)
            .enqueue(object : retrofit2.Callback<CancelOrderModel> {
                override fun onFailure(call: Call<CancelOrderModel>, t: Throwable) {
                    CustomProgressbar.hideProgressBar()
                }

                override fun onResponse(
                    call: Call<CancelOrderModel>,
                    response: Response<CancelOrderModel>
                ) {
                    CustomProgressbar.hideProgressBar()
                    if (response.isSuccessful) {
                        /*   val homeScreenData = response.body()*/
                        when (response.body()!!.status) {
                            SUCCESS -> {

                                Toast(this@OrderDetailActivity, response.body()!!.message)
                                getMyOrderListApi(orderId)


                            }
                            FAILURE -> {
                                Toast(this@OrderDetailActivity, response.body()!!.message)

                            }
                            AUTH_FAILURE -> {
                            }

                        }
                    }
                    Log.e("Cancel order", response.body().toString())
                }
            })

    }

    fun getReturnOrderStatus(id: String?, foodmenuId: String?) {
        val map = HashMap<String, String>()
        map["order_id"] = id.toString()
        map["order_item_id"] = foodmenuId.toString()
        RetrofitClientSingleton.getInstance().returnRequestStatusApi(map)
            .enqueue(object : retrofit2.Callback<SuccessModel> {
                override fun onFailure(call: Call<SuccessModel>, t: Throwable) {

                }

                override fun onResponse(
                    call: Call<SuccessModel>,
                    response: Response<SuccessModel>
                ) {
                    Log.e("TAG", "onResponse: $response")
                }

            });

    }

    override fun onItemReturn(
        id: String?,
        foodmenuId: String?,
        qty: String?,
        orderItemBean: MyOrderDetailsModel.DataBean.OrderBean.OrderItemBean
    ) {
        val intent = Intent(this@OrderDetailActivity, ReturnOrderActivity::class.java)
        intent.putExtra("orderid", id)
        intent.putExtra("foodmenuid", foodmenuId)
        intent.putExtra("qty", qty)
        intent.putExtra("itembean", orderItemBean)
        startActivity(intent)
    }

}
