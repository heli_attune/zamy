package com.attune.zamy.activity

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import com.attune.zamy.R
import com.attune.zamy.utils.SharedPref
import com.attune.zamy.utils.Toast
import com.attune.zamy.utils.isNetworkAvailable
import com.attune.zamy.utils.keyOrderId
import com.hopmeal.android.utils.cartCount
import kotlinx.android.synthetic.main.activity_order_success.*

class OrderSuccessActivity : AppCompatActivity(), View.OnClickListener {

    val TAG = OrderSuccessActivity::class.java.simpleName
    var order_id: String? = null
    var paymentStatus: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState?: Bundle())
        setContentView(R.layout.activity_order_success)

        setViews()

       // SharedPref.setStringValue(this@OrderSuccessActivity, coupenCode, "")

    }

    private fun setViews() {

        if (intent.hasExtra(keyOrderId)) {
            order_id = intent.extras!!.getString(keyOrderId)
            paymentStatus = intent.extras!!.getString("PaymentStatus")

            if (isNetworkAvailable(this)) {
               // getCartStatus()+-
            } else {
                Toast(this,resources.getString(R.string.network_error))
            }
            if (paymentStatus.equals("Success")) {
                imgLogo.setImageResource(R.drawable.ic_paymentcomplete)
                tvThanksPurchaseMsg.visibility = View.VISIBLE
                txtOrderId.visibility = View.VISIBLE
                tvPaymentNote.visibility = View.VISIBLE

                tvPaymentStatus.text = this@OrderSuccessActivity.getString(R.string.order_received)
                tvThanksPurchaseMsg.text = this@OrderSuccessActivity.getString(R.string.thank_purchase_msg)

                tvPaymentNote.text = this@OrderSuccessActivity.getString(R.string.tynote)

                txtOrderId.text = "Your order #: $order_id"

                //  SharedPref.setIntValue(this, cartId, 0)
                SharedPref.setIntValue(this, cartCount, 0)

//                afterPlaceOrderSuccess()
                if (isNetworkAvailable(this)) {
                  //  getSMSMessage()
                } else {
                    Toast(this,resources.getString(R.string.network_error))
                }

            } else if (paymentStatus.equals("Failure")) {
                imgLogo.setImageResource(R.drawable.ic_error)
                tvPaymentStatus.text = this@OrderSuccessActivity.getString(R.string.failure_payment)
                tvThanksPurchaseMsg.visibility = View.INVISIBLE
                txtOrderId.text = "Your order # is : $order_id"
                txtOrderId.visibility = View.VISIBLE
                tvPaymentNote.visibility = View.INVISIBLE
            } else if(paymentStatus.equals("Cancelled")) {
                imgLogo.setImageResource(R.drawable.ic_error)
                tvPaymentStatus.text = this@OrderSuccessActivity.getString(R.string.failure_payment)
                tvThanksPurchaseMsg.visibility = View.INVISIBLE
                txtOrderId.text = "Your order # is : $order_id"
                txtOrderId.visibility = View.VISIBLE
                tvPaymentNote.visibility = View.INVISIBLE
            }
        } else {
            tvPaymentStatus.text = this@OrderSuccessActivity.getString(R.string.failure_payment)
            tvThanksPurchaseMsg.visibility = View.INVISIBLE
            txtOrderId.text = "Your order # is : $order_id"
            tvPaymentNote.visibility = View.INVISIBLE
        }

        btncontinueShopping.setOnClickListener(this)
    }


    override fun onClick(v: View) {
        when (v.id) {

            R.id.btncontinueShopping -> {

                redirectToHome()

            }

            else -> {
            }
        }
    }

    private fun redirectToHome() {
        val i = Intent(this, MainActivity::class.java)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK)
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        startActivity(i)
    }

    override fun onBackPressed() {
        super.onBackPressed()
        redirectToHome()
    }
/*

    private fun getCartStatus() {
        //  showProgressDialog(this, rlParentOrderSuccess)
        val map = HashMap<String, String>()

        map["user_id"] = SharedPref.getUserId(this)
        map["order_id"] = order_id!!
        if (paymentStatus.equals("Success")) {
            map["payment_status"] = "1"
        } else {
            map["payment_status"] = "0"
        }

        RetrofitClientSingleton.getInstance().getCartStatus(SharedPref.getAuthToken(this), map).enqueue(object : retrofit2.Callback<SuccessModel> {

            override fun onFailure(call: Call<SuccessModel>, t: Throwable) {
                stopProgress()
                toast(this@OrderSuccessActivity, getString(R.string.something_went_wrong))
            }

            override fun onResponse(call: Call<SuccessModel>?, response: Response<SuccessModel>) {
                stopProgress()
                if (response.isSuccessful) {

                    when (response.body()?.success) {

                        SUCCESS -> {
                            Log.d(TAG, response.body()?.message)
                        }

                        FAILURE -> {
                            Log.d(TAG, response.body()?.message)
                        }

                    }
                }
            }
        })

    }

    private fun getSMSMessage() {
        showProgressDialog(this, rlParentOrderSuccess)
        val map = HashMap<String, String>()

        map["user_id"] = SharedPref.getUserId(this)
        map["order_id"] = order_id!!

        RetrofitClientSingleton.getInstance().getSMSMessage(SharedPref.getAuthToken(this), map).enqueue(object : retrofit2.Callback<SuccessModel> {

            override fun onFailure(call: Call<SuccessModel>, t: Throwable) {
                stopProgress()
                toast(this@OrderSuccessActivity, getString(R.string.something_went_wrong))
            }

            override fun onResponse(call: Call<SuccessModel>?, response: Response<SuccessModel>) {
                stopProgress()
                if (response.isSuccessful) {

                    when (response.body()?.success) {

                        SUCCESS -> {
                            Log.d(TAG, response.body()?.message)
                        }

                        FAILURE -> {
                            Log.d(TAG, response.body()?.message)
                        }

                    }
                }
            }
        })

    }

    private fun afterPlaceOrderSuccess() {
        val map = HashMap<String, String>()

        map[keyOrderId] = order_id!!
        map["userID"] = SharedPref.getUserId(this)

        showProgressDialog(this@OrderSuccessActivity, tvPaymentStatus)

        RetrofitClientSingleton.getInstance().afterPlaceOrderSuccess(SharedPref.getAuthToken(this), map).enqueue(object : retrofit2.Callback<SuccessModel> {

            override fun onFailure(call: Call<SuccessModel>, t: Throwable) {
                stopProgress()
                toast(this@OrderSuccessActivity, getString(R.string.something_went_wrong))
            }

            override fun onResponse(call: Call<SuccessModel>?, response: Response<SuccessModel>) {
                stopProgress()
                if (response.isSuccessful) {
                    val paymentOrderStatusResponse = response.body()

                    when (paymentOrderStatusResponse?.success) {

                        SUCCESS -> {

                        }

                        FAILURE -> {
                            toast(this@OrderSuccessActivity, paymentOrderStatusResponse.message)
                        }

                    }
                }
            }
        })
    }
*/

}
