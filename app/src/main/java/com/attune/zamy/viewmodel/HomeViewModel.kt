package com.attune.zamy.viewmodel

import android.app.Application
import android.content.Context
import android.util.Log
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.attune.zamy.model.HomeScreenModel
import com.attune.zamy.utils.SharedPref.getUserId

class HomeViewModel(
    application: Application,
    val mContext: Context
) : AndroidViewModel(application) {
    private var dashBoardsLiveData: MutableLiveData<List<HomeScreenModel>>? =
        null

    val dashBoardsDataLive: LiveData<List<HomeScreenModel>>?
        get() {
            if (dashBoardsLiveData != null) {
                dashBoardsLiveData =
                    MutableLiveData()
                DashBoardData()
            }
            return dashBoardsLiveData
        }

    fun DashBoardData() {
        val userID = getUserId(mContext)
        Log.e(TAG, "loadDashBoardData: $userID")
    }

    companion object {
        private const val TAG = "HomeViewModel"
    }

}