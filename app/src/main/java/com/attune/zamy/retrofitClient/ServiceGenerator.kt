package com.attune.zamy.retrofitClient

import com.attune.zamy.diffutils.CartListModelDiff
import com.attune.zamy.model.*
import okhttp3.RequestBody
import retrofit2.Call
import retrofit2.http.*


interface ServiceGenerator {


    /**GET METHOD**/


    @GET(GET_SHIPPING_AREA)
    fun getShippingArea(): Call<ShippingAreaModel>


    /**POST METHOD**/

    @FormUrlEncoded
    @POST(REGISTER_OTP)
    fun requestOTP(@FieldMap map: HashMap<String, String>): Call<CommonModel>

    @FormUrlEncoded
    @POST(VERIFY_OTP)
    fun verifyOTP(@FieldMap map: HashMap<String, String>): Call<CommonModel>

    @FormUrlEncoded
    @POST(LOGIN)
    fun loginAPI(@FieldMap map: HashMap<String, String>): Call<UserModel>

    @FormUrlEncoded
    @POST(FORGOT_PASSWORD)
    fun forgotPasswrodAPI(@FieldMap map: HashMap<String, String>): Call<ForgotPasswordModel>

    @GET(DEACTIVE_PROFILE)
    fun DeactiveProfilePicture(): Call<SuccessModel>

    @FormUrlEncoded
    @POST(HOME_SCREEN)
    fun getDashboardData(@FieldMap map: HashMap<String, String>): Call<HomeScreenModel>

    @FormUrlEncoded
    @POST(REASTAURANT_MENU)
    fun getReastaurentMenu(@FieldMap map: HashMap<String, String>): Call<RestaurantsMenuModel>


    @FormUrlEncoded
    @POST(TOP_RESTAURANT_LIST)
    fun getTopRestaurantList(@FieldMap map: HashMap<String, String>): Call<TopRestaurantModel>

    @FormUrlEncoded
    @POST(ADD_REMOVE_FAVOURITE)
    fun addRemoveFavouriteRestaurant(@FieldMap map: HashMap<String, String>): Call<CommonModel>

    @FormUrlEncoded
    @POST(ADD_TO_CART)
    fun addtoCart(@FieldMap map: HashMap<String, String>): Call<CommonModel>

    @FormUrlEncoded
    @POST(REMOVE_TO_CART)
    fun removeItemfromCart(@FieldMap map: HashMap<String, String>): Call<CommonModel>

    @FormUrlEncoded
    @POST(CLEAR_CART)
    fun clearCart(@FieldMap map: HashMap<String, String>): Call<CommonModel>


    @FormUrlEncoded
    @POST(CART_LIST)
    fun getCartList(@FieldMap map: HashMap<String, String>): Call<CartListModel>


    @FormUrlEncoded
    @POST(CART_LIST)
    fun getCartDiffList(@FieldMap map: HashMap<String, String>): Call<CartListModelDiff>


    @FormUrlEncoded
    @POST(UPDATE_CART)
    fun updateCart(@FieldMap map: HashMap<String, String>): Call<CommonModel>


    @FormUrlEncoded
    @POST(SEARCH_DISH_RESTAURANT)
    fun getsearchDishRestaurant(@FieldMap map: HashMap<String, String>): Call<SearchDataModel>


    @FormUrlEncoded
    @POST(GET_USER_PROFILE)
    fun getUserProfile(@FieldMap map: HashMap<String, String>): Call<ProfileModel>

    /* @FormUrlEncoded
     @POST(GET_USER_PROFILE)
     fun getUserProfile(@FieldMap map: HashMap<String, String>): Call<ProfileModel>*/

    /*@FormUrlEncoded
    @POST(UPDATE_PROFILE)
    fun UpdateUserProfile(@FieldMap map: HashMap<String, String>): Call<CommonModel>*/


    @Multipart
    @POST(UPDATE_PROFILE)
    fun UpdateUserProfile(@PartMap map: HashMap<String, RequestBody>): Call<ProfileUpdateResponse>


    @FormUrlEncoded
    @POST(ADD_UPDATE_ADDRESS)
    fun addAddres(@FieldMap map: HashMap<String, String>): Call<AddAddressModel>

    @FormUrlEncoded
    @POST(DELETE_ADDRESS)
    fun deleteAddres(@FieldMap map: HashMap<String, String>): Call<CommonModel>

    @FormUrlEncoded
    @POST(GET_ADDRESS)
    fun getAddress(@FieldMap map: HashMap<String, String>): Call<GetAddressModel>

    @FormUrlEncoded
    @POST(GTE_MY_ORDER_LIST)
    fun getMyOrderList(@FieldMap map: HashMap<String, String>): Call<MyOrderListModel>

    @FormUrlEncoded
    @POST(GET_MY_ORDER_DETAILS)
    fun getMyOrderDetails(@FieldMap map: HashMap<String, String>): Call<MyOrderDetailsModel>

    @FormUrlEncoded
    @POST(GET_MY_ORDER_TRACKING)
    fun getMyOrderTracking(@FieldMap map: HashMap<String, String>): Call<OrderTrackingModel>


    @FormUrlEncoded
    @POST(GET_FAVOURITE_REASTUARANT_LIST)
    fun getFavouriteReasturantList(@FieldMap map: HashMap<String, String>): Call<FavouriteReastaurantModel>


    @FormUrlEncoded
    @POST(CHECK_SHOPPING_ADDRESS)
    fun checkAddress(@FieldMap map: HashMap<String, String>): Call<CommonModel>

    @FormUrlEncoded
    @POST(CHECKOUT_PAYMENT)
    fun doCheckoutPayment(@FieldMap map: HashMap<String, String>): Call<CheckoutModel>


    @FormUrlEncoded
    @POST(CONTACT_US)
    fun contactUs(@FieldMap map: HashMap<String, String>): Call<ContactUsModel>

    @GET(ABOUTUS)
    fun aboutUs(): Call<AboutUs>


    @FormUrlEncoded
    @POST(CHECKOUT_PAYMENT)
    fun doCheckoutPaytmPayment(@FieldMap map: HashMap<String, String>): Call<PaytmCheckoutModel>

    @FormUrlEncoded

    @POST(RETURNPRODUCT)
    fun returnRequestApi(@FieldMap map: HashMap<String, String>): Call<SuccessModel>


    @FormUrlEncoded
    @POST(RETURN_STATUS)
    fun returnRequestStatusApi(@FieldMap map: HashMap<String, String>): Call<SuccessModel>

    @FormUrlEncoded
    @POST(GET_COUPAN_LIST)
    fun getCoupanList(@FieldMap map: HashMap<String, String>): Call<CouponListModel>


    @FormUrlEncoded
    @POST(APPLY_COUPAN_CODE)
    fun applyPromoCode(@FieldMap map: HashMap<String, String>): Call<ApplyPromoCode>


    @FormUrlEncoded
    @POST(REMOVE_COUPAN_CODE)
    fun removePromoCode(@FieldMap map: HashMap<String, String>): Call<RemoveCoupanModel>


    @FormUrlEncoded
    @POST(GET_DELIVERY_BOY)
    fun getDeliveryBoy(@FieldMap map: HashMap<String, String>): Call<DeliveryBoyModel>

    @FormUrlEncoded
    @POST(CANCEL_ORDER)
    fun cancelOrder(@FieldMap map: HashMap<String, String>): Call<CancelOrderModel>

    @FormUrlEncoded
    @POST(CREATE_TICKET)
    fun createTicket(@FieldMap map: HashMap<String, String>): Call<SuccessModel>

    @GET(CUSTOMER_CATEGORY_ISSUE_LIST)
    fun getCustomerCategoryIssueList(): Call<CustomerIssueModel>

    @FormUrlEncoded
    @POST(CUSTOMER_SUPPORT_LIST)
    fun getCustomerSUPPORTList(@FieldMap map: HashMap<String, String>): Call<CustomerSupportList>


    @FormUrlEncoded
    @POST(CUSTOMER_SUPPORT_CONVERSATION)
    fun getcustomerConversation(@FieldMap map: HashMap<String, String>): Call<TicketListingConversationModel>


    @FormUrlEncoded
    @POST(GET_REASTURANT_INFO)
    fun getReasturantInfo(@FieldMap map: HashMap<String, String>): Call<ReasturantInfoModel>


    @FormUrlEncoded
    @POST(REPLY_TICKET)
    fun replyTicket(@FieldMap map: HashMap<String, String>): Call<SuccessModelData>

    @FormUrlEncoded
    @POST(UPDATE_FIRE_TOKEN)
    fun updateFirebaseToken(@FieldMap map: HashMap<String, String>): Call<SuccessModel>


    @FormUrlEncoded
    @POST(GET_GALLERY)
    fun getGallery(@FieldMap map: HashMap<String, String>): Call<GalerryResponse>


    @FormUrlEncoded
    @POST(GET_REFERRAL_CODE)
    fun getReferralCode(@FieldMap map: HashMap<String, String>): Call<ReferralInfoModel>

    @GET(CHECKVERSION)
    fun checkVersion(): Call<CheckVersionModel>

    @FormUrlEncoded
    @POST(SOCIALLOGIN)
    fun getSocialLogin(@FieldMap map: HashMap<String, String>): Call<UserModel>

    @FormUrlEncoded
    @POST(UPDATE_MOBILE_WITH_GOOGLE)
    fun updateMobileNumberWithGoogle(@FieldMap map: HashMap<String, String>): Call<CommonModel>


    @FormUrlEncoded
    @POST(REQUEST_MOBILE_OTP)
    fun mobileOTPWithGoogle(@FieldMap map: HashMap<String, String>): Call<CommonModel>

    @GET(GET_STORE)
    fun getStore(): Call<StoreModel>

    @FormUrlEncoded
    @POST(CALCULATE_DISTANCE)
    fun CalculateDistanceApi(@FieldMap map: HashMap<String, String>): Call<CalculateDistanceModel>

    @FormUrlEncoded
    @POST(PICKUP_DROP_SERVICE_ADD)
    fun PickupDropAddApi(@FieldMap map: HashMap<String, String>): Call<DeliveryResponseModel>

}