package com.attune.zamy.retrofitClient


import com.attune.zamy.BuildConfig
import com.google.gson.GsonBuilder
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit


class RetrofitClientSingleton {

        companion object {
            val TAG = RetrofitClientSingleton::class.java.simpleName
            var retrofit: Retrofit? = null
            fun getInstance(): ServiceGenerator {
                if (retrofit == null) {
                    val loggingInterceptor = HttpLoggingInterceptor()
                    loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

                    val okHttpBuilder = OkHttpClient.Builder()

                    okHttpBuilder.addInterceptor(BasicAuthInterceptor())


                    if (BuildConfig.DEBUG) {
                        okHttpBuilder.addInterceptor(loggingInterceptor)
                    }

                    okHttpBuilder.readTimeout(5, TimeUnit.MINUTES)
                    okHttpBuilder.connectTimeout(5, TimeUnit.MINUTES)
                    okHttpBuilder.writeTimeout(5, TimeUnit.MINUTES)

                    val client = okHttpBuilder.build()

                    retrofit = Retrofit.Builder().baseUrl(BASEURL)
                        .addConverterFactory(GsonConverterFactory.create(
                            GsonBuilder()
                                .setLenient()
                                .create()))
                        .client(client)
                        .build()
                }

                return retrofit!!.create(ServiceGenerator::class.java)
            }








        fun getInstanceForGoogleApi(): ServiceGenerator {
            val loggingInterceptor = HttpLoggingInterceptor()
            loggingInterceptor.level = HttpLoggingInterceptor.Level.BODY

            val okHttpBuilder = OkHttpClient.Builder()

            if (BuildConfig.DEBUG) {
                okHttpBuilder.addInterceptor(loggingInterceptor)
//                okHttpBuilder.sslSocketFactory(sslContext.socketFactory,trustManager)
            }

            okHttpBuilder.readTimeout(3, TimeUnit.MINUTES)
            okHttpBuilder.connectTimeout(3, TimeUnit.MINUTES)
            okHttpBuilder.writeTimeout(3, TimeUnit.MINUTES)

            val client = okHttpBuilder.build()


            val retrofit = Retrofit.Builder().baseUrl("http://maps.googleapis.com/maps/api/")
                .addConverterFactory(GsonConverterFactory.create())
                .client(client)
                .build()

            return retrofit.create(ServiceGenerator::class.java)
        }
    }

}