package com.attune.zamy.retrofitClient


/**Live Api Url**/
const val BASEURL = "https://zamy.in/api/"

/**Local Url**/
//const val BASEURL = "http://103.92.122.19/zamy/api/"


//api
const val REGISTER_OTP = BASEURL + "authentication/request_otp"
const val VERIFY_OTP = BASEURL + "authentication/verify_otp"
const val CHECKVERSION = BASEURL + "authentication/get_app_version"
const val LOGIN = BASEURL + "authentication/login"
const val FORGOT_PASSWORD = BASEURL + "authentication/forgot_password"
const val UPDATE_FIRE_TOKEN = BASEURL + "authentication/update_token"
const val GET_REFERRAL_CODE = BASEURL + "authentication/get_refer_code"
const val DEACTIVE_PROFILE = BASEURL + "authentication/deactive_profile"
const val HOME_SCREEN = BASEURL + "api/dasboard_api"
const val TOP_RESTAURANT_LIST = BASEURL + "api/top_restaurant_list"
const val ADD_REMOVE_FAVOURITE = BASEURL + "api/add_remove_favourite_restaurant"
const val REASTAURANT_MENU = BASEURL + "api/restaurant_category_menu"

const val GET_ADDRESS_LIST = BASEURL + "get_address_book"

//http://zamy.in/api/cart_api/get_cart_data
/**Cart**/
const val ADD_TO_CART = BASEURL + "cart_api/add_to_cart"
const val REMOVE_TO_CART = BASEURL + "cart_api/remove_cart_item"
const val CLEAR_CART = BASEURL + "cart_api/clear_cart"
const val UPDATE_CART = BASEURL + "cart_api/update_cart"
const val CART_LIST = BASEURL + "cart_api/get_cart_data"
const val CHECKOUT_PAYMENT = BASEURL + "cart_api/checkout"

const val CHECK_SHOPPING_ADDRESS = BASEURL + "cart_api/check_shipping_address"

/**Search**/
const val SEARCH_DISH_RESTAURANT = BASEURL + "api/search_restaurant_dish"

/**Profile**/
const val GET_USER_PROFILE = BASEURL + "authentication/get_user_profile"

const val UPDATE_PROFILE = BASEURL + "authentication/update_profile"

/**Address**/
const val GET_SHIPPING_AREA = BASEURL + "/api/shipping_area"
const val ADD_UPDATE_ADDRESS = BASEURL + "api/add_update_address_book"
const val GET_ADDRESS = BASEURL + "api/get_address_book"

const val DELETE_ADDRESS = BASEURL + "api/delete_address_book"

/**Order**/
const val GTE_MY_ORDER_LIST = BASEURL + "api/get_customer_order_list"
const val GET_MY_ORDER_DETAILS = BASEURL + "api/order_detail"


const val GET_MY_ORDER_TRACKING = BASEURL + "api/order_tracking"

/**Favourite**/
const val GET_FAVOURITE_REASTUARANT_LIST = BASEURL + "api/get_favourite_restaurant"

const val GET_REASTURANT_INFO = BASEURL + "api/restaurant_info"


/**Contact Us**/
const val CONTACT_US = BASEURL + "api/contact_us"
const val ABOUTUS = BASEURL + "api/about_us"

const val RETURNPRODUCT = BASEURL + "api/return_request"
const val RETURN_STATUS = BASEURL + "api/return_status"


//coupan list
const val GET_COUPAN_LIST = BASEURL + "/cart_api/coupon_list"
const val APPLY_COUPAN_CODE = BASEURL + "/cart_api/apply_coupon"
const val REMOVE_COUPAN_CODE = BASEURL + "/cart_api/remove_coupon"
const val GET_DELIVERY_BOY = BASEURL + "/api/track_delivery_boy"

const val CANCEL_ORDER = BASEURL + "/cart_api/cancel_order"
const val CUSTOMER_CATEGORY_ISSUE_LIST =
    BASEURL + "/Customer_support/customer_support_category_issue"
const val CUSTOMER_SUPPORT_LIST = BASEURL + "Customer_support/customer_support_ticket_detail"
const val CREATE_TICKET = BASEURL + "Customer_support/create_customer_ticket"


const val CUSTOMER_SUPPORT_CONVERSATION = BASEURL + "Customer_support/customer_support_conversation"
const val REPLY_TICKET = BASEURL + "Customer_support/reply_ticket"


const val GET_GALLERY = BASEURL + "api/restaurant_gallery"


const val SOCIALLOGIN = BASEURL + "authentication/social_signup"


const val UPDATE_MOBILE_WITH_GOOGLE = BASEURL + "authentication/update_phone"

const val REQUEST_MOBILE_OTP = BASEURL + "authentication/request_no_otp"

/**Delivery**/
const val GET_STORE = BASEURL + "/api/get_store"
const val CALCULATE_DISTANCE = BASEURL + "/api/calculate_distance"
const val PICKUP_DROP_SERVICE_ADD = BASEURL + "/api/pickup_drop_service_add"






