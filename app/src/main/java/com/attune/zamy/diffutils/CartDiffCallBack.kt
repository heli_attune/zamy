package com.attune.zamy.diffutils

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil
import com.attune.zamy.diffutils.CartListModelDiff.DataBeanDiff.ItemsBeanDiff

class CartDiffCallBack(
    var newList: MutableList<ItemsBeanDiff>?,
    var oldList: MutableList<ItemsBeanDiff>?
) : DiffUtil.Callback() {
    override fun getOldListSize(): Int {
        return if (oldList != null) oldList!!.size else 0
    }

    override fun getNewListSize(): Int {
        return if (newList != null) newList!!.size else 0
    }

    override fun areItemsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Boolean {
        return newList!![newItemPosition] == oldList!![oldItemPosition]
    }

    override fun areContentsTheSame(
        oldItemPosition: Int,
        newItemPosition: Int
    ): Boolean {
        val result = newList!![newItemPosition]
            .compareTo(
                oldList!![oldItemPosition]
            )
        return result == 0
    }

    override fun getChangePayload(oldItemPosition: Int, newItemPosition: Int): Any? {
        val newDataBeanDiff = newList!![newItemPosition]
        val oldDataBeanDiff = newList!![oldItemPosition]
        val di = Bundle()
        if (newDataBeanDiff.qty != oldDataBeanDiff.qty) {
            di.putInt("qty", newDataBeanDiff.qty)
        }
        return if (di.size() == 0) {
            null
        } else di
    }

}