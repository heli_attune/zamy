package com.attune.zamy.diffutils

class CartListModelDiff {

    /**
     * status : 1
     * message : Get cart data successfully.
     * data : {"id":"58","user_id":"4","total":"1350","created_date":"2019-10-22 22:38:49","items":[{"rowid":"840","restaurant_id":"3","food_menu_id":"84","menu_name":"27 Tandoori Butter-","variation_id":"0","variation_name":"","price":"50.00","qty":"27 Tandoori Butter-","subtotal":1350}]}
     */

    var status: Int = 0
    var message: String? = null
    var data: DataBeanDiff? = null

    class DataBeanDiff : Comparable<DataBeanDiff>, Cloneable {
        /**
         * id : 58
         * user_id : 4
         * total : 1350
         * created_date : 2019-10-22 22:38:49
         * items : [{"rowid":"840","restaurant_id":"3","food_menu_id":"84","menu_name":"27 Tandoori Butter-","variation_id":"0","variation_name":"","price":"50.00","qty":"27 Tandoori Butter-","subtotal":1350}]
         */

        var id: String? = null
        var user_id: String? = null
        var cart_count: Int = 0
        var total: String? = null
        var total_amount: Double? = null
        var coupon_amount: String? = null
        var coupon_code: String? = null
        var delivery_charge: Int = 0
        var created_date: String? = null
        var complementery: String? = null
        var user_phone: String? = null
        var min_order_amount: String? = null
        var gst: String? = null
        var items: MutableList<ItemsBeanDiff>? = null

        class ItemsBeanDiff : Comparable<ItemsBeanDiff>, Cloneable {
            /**
             * rowid : 840
             * restaurant_id : 3
             * food_menu_id : 84
             * menu_name : 27 Tandoori Butter-
             * variation_id : 0
             * variation_name :
             * price : 50.00
             * qty : 27 Tandoori Butter-
             * subtotal : 1350
             */

            var rowid: String? = null
            var restaurant_id: String? = null
            var food_menu_id: String? = null
            var menu_name: String? = null
            var variation_id: String? = null
            var variation_name: String? = null
            var price: String? = null
            var qty: Int = 0
            var subtotal: Int = 0
            var pricetotal: Double = 0.0
            var menu_logo: String? = null
            override fun compareTo(other: ItemsBeanDiff): Int {
                val itemsBeanDiff = ItemsBeanDiff()
                if (itemsBeanDiff.qty == this.qty) {
                    return 0
                } else {
                    return 1
                }

            }

        }

        override fun compareTo(other: DataBeanDiff): Int {
            val dataBeanDiff = DataBeanDiff()
            if (dataBeanDiff == this) {
                return 0
            } else {
                return 1
            }
        }
    }
}

