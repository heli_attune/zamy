package com.attune.zamy.diffutils


import com.google.gson.annotations.SerializedName

data class DataClassCart(
    @SerializedName("data")
    val data: Data,
    @SerializedName("message")
    val message: String, // Get cart data successfully.
    @SerializedName("status")
    val status: Int // 1
) {
    data class Data(
        @SerializedName("cart_count")
        val cartCount: Int, // 3
        @SerializedName("complementery")
        val complementery: String,
        @SerializedName("coupon_amount")
        val couponAmount: String, // 0
        @SerializedName("coupon_code")
        val couponCode: String,
        @SerializedName("created_date")
        val createdDate: String, // 2020-09-04 09:42:15
        @SerializedName("delivery_charge")
        val deliveryCharge: String, // 20
        @SerializedName("gst")
        val gst: String, // 0.00
        @SerializedName("id")
        val id: String, // 693
        @SerializedName("items")
        val items: List<Item>,
        @SerializedName("min_order_amount")
        val minOrderAmount: String, // 100
        @SerializedName("sub_total")
        val subTotal: String, // 220.00
        @SerializedName("total")
        val total: String, // 220.00
        @SerializedName("total_amount")
        val totalAmount: String, // 240.00
        @SerializedName("updated_date")
        val updatedDate: String, // 2020-09-04 03:16:47
        @SerializedName("user_id")
        val userId: String, // 156
        @SerializedName("user_phone")
        val userPhone: String // 7001700090
    ) {
        data class Item(
            @SerializedName("food_menu_id")
            val foodMenuId: String, // 2599
            @SerializedName("menu_logo")
            val menuLogo: String, // https://zamy.in/pos.zamy.in/uploads/FoodMenu/Small/7a73d3efe06f1f67b677cd640202bf46.jpg
            @SerializedName("menu_name")
            val menuName: String, // Chi. Popcorn
            @SerializedName("price")
            val price: String, // 80.00
            @SerializedName("qty")
            val qty: Any, // null
            @SerializedName("restaurant_id")
            val restaurantId: String, // 54
            @SerializedName("rowid")
            val rowid: String, // 25990
            @SerializedName("subtotal")
            val subtotal: Int, // 80
            @SerializedName("swiggy_logo")
            val swiggyLogo: String, // https://zamy.in/pos.zamy.in/uploads/FoodMenu/Small/7a73d3efe06f1f67b677cd640202bf46.jpg
            @SerializedName("variation_id")
            val variationId: String, // 0
            @SerializedName("variation_name")
            val variationName: String
        )
    }
}